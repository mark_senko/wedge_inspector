function [Ysim,Yexp] = UpdateSpectra(X, iX, tX, ToolParms, ModParms, Yexp)
% Unified simulate spectrum function, updated for WI v4.0.

[ToolParms,ModParms] = UpdateParms(X,iX,tX,ToolParms,ModParms);

if ToolParms.uT
    if ToolParms.mode ~= 3
        MyM11 = imag(Yexp(:,1));
         Yexp = real(Yexp);
    else
        MyM11 = Yexp(:,1);
        Yexp(:,1) = ones(size(Yexp,1),1);
    end
    [Ysim,~] = IntegrateSpectrum([ToolParms.ThkEMA X(1)], ModParms.NK, ToolParms);
else
    [Ysim,MyM11] = IntegrateSpectrum([ToolParms.ThkEMA X(1)], ModParms.NK, ToolParms);
end

Yexp = ModifySpectrum(ToolParms,Yexp,MyM11);