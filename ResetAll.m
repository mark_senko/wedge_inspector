% Reset All
set(handles.uibuttongroup1,'visible','off');
set(handles.uibuttongroup2,'visible','off');
set(handles.uibuttongroup3,'visible','off');
set(handles.uibuttongroup4,'visible','off');
set(handles.uibuttongroup5,'visible','off');
set(handles.saveSettings,'visible','off');
set(handles.saveBinary,'visible','off');
set(handles.abortbutton,'visible','off');
set(handles.export_spectra,'visible','off');

% populate uitable2 (model settings
Parms = {'Lmin','Lmax','AOI','NA','BW',...
         'W-vis','W-ir','Fit Thickness','ThkEMA','E1 alpha','E1 beta',...
         'Bm1 (AF)','Bm2 (Rad.)','Bm3','Bm4','Bm5',...
         'DO13','DO4'}';
Select = {' ',' ',false,false,false,...
          false,false,true,false,false,false,...
          false,false,false,false,false,...
          false,false}';
Type = {'None','None','None','None','Polynom.',...
        'Polynom.','Polynom.','None','None','None','None',...
        'None','None','None','None','None',...
        'Cauchy','Polynom.'}';
Num = {1,1,1,1,2,...
       5,4,1,1,1,1,...
       1,1,1,1,1,...
       3,1}';
Splt = {[],[],false,false,true,...
        [],[],[],[],false,false,...
        false,false,false,false,false,...
        false,false}';
Val1 = {190,1700,65,0.065,0,...
       [],[],[],0,1.0031,1,...
       0,0,0,0,0,...
       [],[]}';
Val2 = {[],[],[],[],0,...
       [],[],[],[],[],[],...
       [],[],[],[],[],...
       [],[]}';
ValO = {[],[],[],[],[],...
       [],[],[],[],[],[],...
       [],[],[],[],[],...
       [],[]}';
LwL = {[],[],0.01,0.001,0,...
       5,2,[],0,0.001,0.001,...
       0.01,0.01,0.01,0.01,0.01,...
       0.01,0.01}';
UpL = {[],[],0.01,0.001,15,...
       5,2,[],10,2,2,...
       0.01,0.01,0.01,0.01,0.01,...
       0.01,0.01}';
Data = [Select, Parms, Type, Splt, Num, Val1, Val2, repmat(ValO,[1 8]), LwL, UpL];
set(handles.uitable2,'data',Data);
handles.PathMat = {};

% setup abort button
handles.STOP = 0;

% setup ModParms for NK
handles.ModParms = {};
handles.ModParms.NKselect = 1;
handles.ModParms.SiDat = {};
handles.ModParms.SiO2Dat = {};
handles.ModParms.SiF = 'Si_JAW.txt';
handles.ModParms.EMAF = 'INTR_JAW.txt';
handles.ModParms.SiO2xyF = 'SiO2_JAW.txt';
handles.ModParms.SiO2zF = 'SiO2_JAW.txt';
handles.ModParms.NK = [];
handles.ModParms.mthd = 'spline';

% setup empty results slots
handles.results.Ye = [];
handles.results.Ys = [];
handles.results.MSE = [];
handles.results.Nparms = 1;
handles.results.Ls = [];


% Get default NKs
if isempty(handles.PathMat)
    PathMat = [pwd '\Mats'];
    if ~exist(PathMat,'dir')
        uiwait(msgbox({'No MATERIALS folder found.' 'Please select MATERIALS folder.'},'Folder Not Found','error'));
        PathMat = uigetdir(pwd,'Please Select the Materials Folder');
        handles.PathMat = PathMat;
    else
        handles.PathMat = PathMat;
    end
end
mthd = handles.ModParms.mthd;
[pp_Si_n,pp_Si_k]           = GetNKs_v3([PathMat '\' handles.ModParms.SiF],mthd);
[pp_intr_n,pp_intr_k]       = GetNKs_v3([PathMat '\' handles.ModParms.EMAF],mthd);
[pp_sio2_xy_n,pp_sio2_xy_k] = GetNKs_v3([PathMat '\' handles.ModParms.SiO2xyF],mthd);
[pp_sio2_z_n,pp_sio2_z_k]   = GetNKs_v3([PathMat '\' handles.ModParms.SiO2zF],mthd);
handles.ModParms.ppNK = {pp_Si_n,pp_Si_k,pp_intr_n,pp_intr_k,pp_sio2_xy_n,...
                         pp_sio2_xy_k,pp_sio2_z_n,pp_sio2_z_k};

% setup Controls panel
handles.CtrlData = {};
handles.CtrlData.mode = 1;
handles.CtrlData.centeredWcal = 1;
handles.CtrlData.NewLens = 0;
handles.CtrlData.BmProf = 1;
handles.CtrlData.BwSlcs = 3;
handles.CtrlData.NaSlcs = 11;
handles.CtrlData.A3Apod = 0;
handles.CtrlData.m11s = 1;
handles.CtrlData.uT = 0;
handles.CtrlData.MatLS = 0;
handles.CtrlData.RMSEthres = 0.0001;
handles.CtrlData.SerFitIter = 8;
handles.CtrlData.ParFitIter = 3;
handles.CtrlData.Depol = 0;
handles.CtrlData.no_w4 = 0;
handles.CtrlData.only_w0 = 0;
handles.CtrlData.JAWmse = 1;
handles.CtrlData.CompCal = 0;

% populate uitable1 (Si model settings)
if isempty(handles.ModParms.SiDat)
    Parms1 = {'Eps Inf.','Eps Imag.'...
             'E1','Amp1','Gamma1','Beta1',...
             'E2','Amp2','Gamma2','Beta2',...
             'E3','Amp3','Gamma3','Beta3',...
             'E4','Amp4','Gamma4','Beta4',...
             'E5','Amp5','Gamma5','Beta5',...
             '1D E1','1D E2','1D Gamma','1D Amp',...
             'N fit','K fit'}';
    Select1 = {false,false,...
              false,false,false,false,...
              false,false,false,false,...
              false,false,false,false,...
              false,false,false,false,...
              false,false,false,false,...
              false,false,false,false,...
              false,false}';
%     Val1 = {0.000002,-0.000017,...
%            3.350383,1.897579,0.037345,-0.276280,...
%            3.763237,7.048575,0.167779,-0.150490,...
%            4.276734,2.448933,0.046421, 0.067861,...
%            5.211842,0.360344,0.059544,-0.185110,...
%            4.392339,5.509833,0.973708, 0.411299}'; % From Tool #119
%       Val1 = {1.314,0.954,...
%              3.350383,1.897579,0.037345,-0.276280,...
%              3.763237,7.048575,0.167779,-0.150490,...
%              4.276734,2.448933,0.046421, 0.067861,...
%              5.211842,0.360344,0.059544,-0.185110,...
%              4.392339,5.509833,0.973708, 0.411299,...
%              3.556000,4.334000,0.111589, 0.000000,...
%              0,0}'; % From Tool #119
      Val1 = {0.953426,0.097131, ...  % Set to give NSi = 3.877 at 632.8 nm
             3.37394, 1.84943, 0.037312, -0.234323, ...
             3.75058, 5.87886, 0.145069, -0.134146, ...
             4.26748, 2.75095, 0.046885,  0.03755098, ...
             4.65649, 10.5313, 1.519910,  0.318310, ...
             5.23844, 0.37586, 0.060917, -0.147449, ...
             3.56496, 4.46198, 0.111589,  0.000000, ...
             0.0,0.0}'; % From Opsal & Aspnes
    LwL1 = {-10,-10,-10,-10, 0,-2,-10,-10, 0,-2,-10,-10, 0,-2,-10,-10, 0,-2,-10,-10, 0,-2, 0, 0, 0, 0,0.01,0.01}';
    UpL1 = { 10, 10, 10, 10,10, 2, 10, 10,10, 2, 10, 10,10, 2, 10, 10,10, 2, 10, 10,10, 2,10,10,10,20,0.01,0.01}';
    handles.ModParms.SiDat = [Select1, Parms1, Val1, LwL1, UpL1];
end

% populate uitable2 (SiO2 model settings)
if isempty(handles.ModParms.SiO2Dat)
    Parms2 = {'EpsInf','IR Pole','UV Pole',...
             'EMA1 Per.','EMA Void',...
             'Air Cauc. A','Air Cauc. B','Air Cauc. C',...
             'Nfit_SiO2'}';
    Select2 = {false,false,false,...
              false,false,false,false,false,...
              false}';
%     Val2 = {1.49,0.033,77.28,...
%            0.5,0}';
    Val2 = {1.490783,0.009985,75.37635,...      %Set to get 1.460 at 632.8 nm
           0.5,0,1,0,0,...
           0}';
    LwL2 = {0.01,0.01,0.01,0.01,0.01,0.00001,0.00001,0.00001,0.1}';
    UpL2 = {0.01,0.01,0.01,0.01,0.01,0.00001,0.00001,0.00001,0.1}';
    handles.ModParms.SiO2Dat = [Select2, Parms2, Val2, LwL2, UpL2];
end

% setup plots
handles.plots.allMM = false;
handles.plots.offDiags = false;
handles.plots.offDiagsMax = false;
handles.plots.onDiags = false;
handles.plots.depolind = false;
handles.plots.MMthks = false;
handles.plots.m12 = false;
handles.plots.m13 = false;
handles.plots.m14 = false;
handles.plots.m21 = false;
handles.plots.m22 = false;
handles.plots.m23 = false;
handles.plots.m24 = false;
handles.plots.m31 = false;
handles.plots.m32 = false;
handles.plots.m33 = false;
handles.plots.m34 = false;
handles.plots.m41 = false;
handles.plots.m42 = false;
handles.plots.m43 = false;
handles.plots.m44 = false;
handles.plots.m14m41 = false;
handles.plots.m13m31 = false;
handles.plots.m23m32 = false;
handles.plots.m24m42 = false;
handles.plots.AOI = false;
handles.plots.NA = false;
handles.plots.visWvls = false;
handles.plots.IRWvls = false;
handles.plots.allNCS = false;
handles.plots.Nonly = false;
handles.plots.Conly = false;
handles.plots.Sonly = false;
handles.plots.NCSthks = false;
handles.plots.DOs = false;
handles.plots.WD1 = false;
handles.plots.WD2 = false;
handles.plots.int = false;
handles.plots.NCSret = false;
handles.plots.NCSatt = false;
handles.plots.T1ret = false;
handles.plots.T2ret = false;
handles.plots.T1att = false;
handles.plots.T2att = false;
handles.plots.T1reOA = false;
handles.plots.T2reOA = false;
handles.plots.T1imOA = false;
handles.plots.T2imOA = false;
handles.plots.dGam1 = false;
handles.plots.iGam1 = false;
handles.plots.dGam2 = false;
handles.plots.iGam2 = false;
handles.plots.dPsi1 = false;
handles.plots.dPhi1 = false;
handles.plots.dPsi2 = false;
handles.plots.dPhi2 = false;
handles.plots.rp11 = false;
handles.plots.ip11 = false;
handles.plots.rp12 = false;
handles.plots.ip12 = false;
handles.plots.rp21 = false;
handles.plots.ip21 = false;
handles.plots.rp22 = false;
handles.plots.ip22 = false;
handles.plots.cont.N = false;
handles.plots.cont.C = false;
handles.plots.cont.S = false;
handles.plots.cont.NCS = false;
handles.plots.cont.NCSsig = false;
handles.plots.cont.offdiagcombos = false;
handles.plots.cont.depol = false;
handles.plots.cont.mmall = false;
handles.plots.cont.offdiags = false;
handles.plots.cont.ondiags = false;
handles.plots.cont.uT = false;
handles.plots.cont.mmsig = false;
handles.plots.fitparms = false;

% clear waitbars
set(handles.axes1,'Xtick',[],'Ytick',[],'Xlim',[0 1000]);
box(handles.axes1,'on');
set(handles.axes2,'Xtick',[],'Ytick',[],'Xlim',[0 1000]);
box(handles.axes2,'on');
upwait_v2('c',handles.axes1,' ','c',handles.axes2,' ');

% setup grapher function
if isfield(handles,'figh')
    if isvalid(handles.figh)
        close(handles.figh);
    end
end
handles.figh = gobjects(1);
delete(handles.figh);
handles.axh = gobjects(0);

% Make sure uitable 1 is empty
set(handles.uitable1,'data',{});

