% Test Lens Matrix depolarization
% AGB 2021

% Declare files
pth = 'F:\Recent Work\Neptune\Mark Lee SEACU problem 20210308\PostSEACU_OnTool\CE Files (PostSEACU)\';
fil1 = 'SEPreCorr.dat';
fil2 = 'SEPostCorr.dat';

% Load files
[L, AOI, Y, SigY, PathF, Files] = openSEspectraALL(pth,[{fil1},{fil2}],[],[]);

% Matrix multiplication
NumL = length(L);
ML1 = permute(Y(:,1:16,1),[3 2 1]);
ML1 = reshape(ML1,[4 4 NumL]);
ML1 = permute(ML1,[2 1 3]);
ML1 = complex(ML1);
ML2 = permute(Y(:,1:16,2),[3 2 1]);
ML2 = reshape(ML2,[4 4 NumL]);
ML2 = permute(ML2,[2 1 3]);
ML2 = complex(ML2);
MLT = MM3D_v2(ML2,ML1);
MLT = permute(MLT,[2 1 3]);
MLT = reshape(MLT,[1 16 NumL]);
MLT = permute(MLT,[3 2 1]);

% Graph results
clr = [0 0.2 1];
figh = gobjects(1);
delete(figh);
[figh,axh] = WIgrapher(figh,[],L,Y(:,1:16,1),0,clr);
clr = [1 0.2 0];
[figh,axh] = WIgrapher(figh,axh,L,Y(:,1:16,2),0,clr);
clr = [0 0 0];
[figh,axh] = WIgrapher(figh,axh,L,MLT,0,clr);

