function [Y,M11] = IntegrateSpectrum(To, NK, ToolParms)
% simulate Mueller matrix for TF pad
% same as Sim_TF_MM but the BW convolution is done in MM space rather than Xln space.
% To =  layer thicknesses in the form [T_subs T_layer1 T_layer2 ... 0]
% NK = complex index of refractions in the form: [N_subs N_layer1 N_layer2 ... 1]
% ToolParms.NumNASlices  for NA integration
% ToolParms.dAOIs        matrix of dAOI for the NA integration. Its length = NumNASlices
% ToolParms.W       matrix of weights for NA integration + shaper     
% ToolParms.Lo      final wvl vector with normal wvl spacing
% ToolParms.Lh      wvl vector with small wvl spacing to be used for BW convolution

NumWvls = length(ToolParms.Lh);
persistent Last

%% simulate Mk_NA = integrate Mk(AOI)
% Attempt new integration with polarization rotation

% first check to see if we can skip this calculation
if ~isempty(ToolParms.LimitedUpdate) && (sum(To~=Last.To)==0) && all(Last.LU==ToolParms.LimitedUpdate)
    LUgo = (sum(ToolParms.LimitedUpdate == [2,3,4,8,9,10,16,17])>=1);
else
    LUgo = true;
    Last.To = To;
    Last.LU = ToolParms.LimitedUpdate;
end

% if we can't skip, calculate
if (ToolParms.BPtype ~= 6) && (LUgo || (~isfield(Last,'Mlk_b_h')))
    if ToolParms.NumNASlices > 1
        Mlk_NA = 0;
        for nS = 1:ToolParms.NumNASlices
            MyAOI = ToolParms.AOIp.s + ToolParms.dAOIs(nS,:);
            if ~isreal(MyAOI) || ~isreal(To) || ~isreal(ToolParms.Lh)
                keyboard;
            end
            NK = real(NK) - 1i.*abs(imag(NK));
            Mlk = wendgine2(MyAOI,real(NK),imag(NK),ToolParms.Lh',To);
            Mlk_NA = Mlk_NA + Mlk.*permute(ToolParms.W(nS,:),[1 3 2]);
        end
        Mlk_b_h = Mlk_NA;
    else  % Skip NA integration if NA = 0
        MyAOI = ToolParms.AOIp.s;
        NK = real(NK) - 1i.*abs(imag(NK));
        Mlk_b_h = wendgine2(MyAOI,real(NK),imag(NK),ToolParms.Lh',To);
    end
    Last.Mlk_b_h = Mlk_b_h;
    Last.Skip = 0;
elseif (LUgo || isempty(Last.Mlk_b_h))
    % Do Fred's Fourier model
    NK_FM = NK(2:4,:);
    dz = CalcWvlDep(To(2),ToolParms.BMp,3);
    Nx = 2048;
    Mlk_b_h = Fourier_SE_v12e(NK_FM,To,ToolParms.AOIp.s,ToolParms.Lh.',...
                            ToolParms.ALPHAp.s,ToolParms.BETAp.s,...
                            ToolParms.NAp.s,ToolParms.BMp.s(1,:),dz,Nx);
    Last.Mlk_b_h = Mlk_b_h;
    Last.Skip = 0;
else
    Mlk_b_h = Last.Mlk_b_h;
    Last.Skip = 1;
%     disp(['Skipped spectra generation. LimitedUpdate=' num2str(ToolParms.LimitedUpdate)]);
end

%% Add Lens Effects (after NA before BW)

% check to see if we can skip Lens matrix calculation
if ~isempty(ToolParms.LimitedUpdate)
    LUgo = (sum(ToolParms.LimitedUpdate == [4,12,13,14,15])>=1);
else
    LUgo = true;
end 

% check to see if we can skip the whole Lens calculation
if (Last.Skip==0) || LUgo || ~isfield(Last,'LMM1')
    MMtmp = complex(Mlk_b_h);
    if ToolParms.mode ~= 1
        if (ToolParms.nlens == 1) && (LUgo || isempty(Last.LMM1))
            [LMM1,LMM2] = LensMM_J(ToolParms.Lh,ToolParms.LNSp.s,false);
            Last.LMM1 = LMM1;
            Last.LMM2 = LMM2;
        elseif (LUgo || isempty(Last.LMM1))
            [LMM1,LMM2] = JAWlensMM(ToolParms.DO,ToolParms.WD,ToolParms.Lh);
            Last.LMM1 = LMM1;
            Last.LMM2 = LMM2;
        else
            LMM1 = Last.LMM1;
            LMM2 = Last.LMM2;
%             disp(['Skipped Lens Calculation. LimitedUpdate=' num2str(ToolParms.LimitedUpdate)]);
            
        end
        LMM1 = complex(LMM1);
        LMM2 = complex(LMM2);
        MMtmp = MM3D_v2(MMtmp,LMM1);
        MMtmp = MM3D_v2(LMM2,MMtmp);
    end
    MMtmp = permute(MMtmp, [2 1 3]);
    MMtmp = reshape(MMtmp, [1 16 NumWvls]);
    Mlk_Out_h = permute(MMtmp, [3 2 1]);
    Last.Mlk_Out_h = Mlk_Out_h;
else
    Mlk_Out_h = Last.Mlk_Out_h;
    Last.Skip = Last.Skip +1;
% %     disp('Skipped Lens Calculation.');
end
    

%% BW convolution

% check to see if we can skip BW calculation
if ~isempty(ToolParms.LimitedUpdate)
    LUgo = (ToolParms.LimitedUpdate == 4);
else
    LUgo = true;
end

% if no BW change and previous 2 checks passed, skip this
if LUgo || (Last.Skip ~= 2) || ~isfield(Last,'Mlk_Out')
    nLo = length(ToolParms.Lo);
    iLov = (ToolParms.Lo<=1000);
    iLor = (ToolParms.Lo>1000);
    iLv = ToolParms.iLh_VIS;
    iLr = ToolParms.iLh_IR;
    Mlk_Out = zeros(nLo,16);
    if (sum(iLv) > 0) && (sum(iLr) > 0)
        for nK = 1:16
            Mlk_Out(iLov,nK) = ToolParms.iMv*Mlk_Out_h(1:ToolParms.Ldiv,nK);
            Mlk_Out(iLor,nK) = ToolParms.iMr*Mlk_Out_h((ToolParms.Ldiv+1):end,nK);
        end
    elseif sum(iLr) > 0
        for nK = 1:16
            Mlk_Out(iLor,nK) = ToolParms.iMr*Mlk_Out_h((ToolParms.Ldiv+1):end,nK);
        end
    elseif sum(iLv) > 0
        for nK = 1:16
            Mlk_Out(iLov,nK) = ToolParms.iMv*Mlk_Out_h(1:ToolParms.Ldiv,nK);
        end
    end
    Last.Mlk_Out = Mlk_Out;
else
    Mlk_Out = Last.Mlk_Out;
    Last.Skip = Last.Skip +1;
%     disp('Skipped Bandwidth Calculation.');
end
% Mlk_Out = Mlk_Out_h;


%% Output either NCS or MM depending on ToolParms

M11 = Mlk_Out(:,6);

interference = 0;
if interference == 1
    % interference setup
    amp = 0.02;
    srcmin = 910; srcmax = 980;
    L = ToolParms.L;
    iLsrc = (L<=srcmax) & (L>=srcmin);
    Lx = linspace(-3,3,sum(iLsrc)).';
    GLx = amp.*exp(-Lx.^2./2)./sum(iLsrc);
    GLx = repmat(GLx,[1 16]);
%     recmid = 1200;
%     iLrec = logical(L-L);
%     [~,idx] = min(abs(L-recmid));
%     iLvec = floor(idx-sum(iLsrc)/2+1):1:floor(idx+sum(iLsrc)/2);
%     iLrec(iLvec) = true;
    recmin = 620; recmax = 810;
    iLrec = (L<=recmax) & (L>=recmin);
    Lx = linspace(-3,3,sum(iLrec)).';
    MMint = sum(Mlk_Out(iLsrc,:).*GLx,1);
    MMint = repmat(MMint,[sum(iLrec) 1]);
    GLxr = exp(-Lx.^2./2);
    GLxr = repmat(GLxr,[1 16]);
    Mlk_Out(iLrec,:) = Mlk_Out(iLrec,:) + MMint.*GLxr;
end

Mlk_Out = Mlk_Out./repmat(M11,[1 16]);

if ToolParms.mode ~= 3
    N = -(Mlk_Out(:,2)+Mlk_Out(:,5))./2;
    C = (Mlk_Out(:,11)+Mlk_Out(:,16))./2;
    S = (Mlk_Out(:,12)-Mlk_Out(:,15))./2;
    p = sqrt(N.^2+C.^2+S.^2);
    Y = [N C S]./[p p p];
    if ToolParms.m11s
        Y = Y.*[M11 M11 M11];
    end
else
    Y = Mlk_Out;
    if ToolParms.Depol==1
% NCS Depolarization
   DI = (Y(:,2) + Y(:,5)).^2 + (Y(:,11) + Y(:,16)).^2 + (Y(:,12) - Y(:,15)).^2;
   DI = sqrt(DI)./(sqrt(4)*Y(:,1));
        Y(:,2) = Y(:,2)./DI;
        Y(:,5) = Y(:,5)./DI;
        Y(:,11) = Y(:,11)./DI;
        Y(:,16) = Y(:,16)./DI;
        Y(:,12) = Y(:,12)./DI;
        Y(:,15) = Y(:,15)./DI;
% Gil Depolarization
%         D1 = sqrt(sum(Y.^2,2)-Y(:,1).^2)./(sqrt(3)*Y(:,1));
%         DI = repmat(D1,[1 15]);
%         Y(:,2:16) = Y(:,2:16)./DI;
%         Y(:,6) = Y(:,6).*D1;
% Before Jon's NCS depolaraization
%        DI = sqrt(sum(Y.^2,2)-Y(:,1).^2)./(sqrt(3)*Y(:,1));
%        DI = repmat(DI,[1 15]);
%        Y(:,2:16) = Y(:,2:16)./DI;

    end
    if ToolParms.m11s
        Y = Y.*repmat(M11,[1 16]);
    end
end

if ToolParms.mode == 4
    if ToolParms.m11s
        Mlk_Out = Mlk_Out.*repmat(M11,[1 16]);
    end
    Y = [Mlk_Out, Y];
end

