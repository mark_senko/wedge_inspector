function [LMM1,LMM2] = LensMM(Lh,LNSp)
% Generate the 2 matrices LMM1 and LMM2, which are effectively rotated retarders 
% with complex optical activity and absorption.

% Setup input parameters
NumL = length(Lh);
% Lh = permute(Lh,[3 2 1]);
Ret  = permute(LNSp(1,:),[1 3 2])./2;
Abs  = permute(LNSp(2,:),[1 3 2])./2;
Ret1 = permute(LNSp(3,:),[1 3 2])./2;
Abs1 = permute(LNSp(4,:),[1 3 2])./2;
ReAct1 = permute(LNSp(5,:),[1 3 2])./2;
ImAct1 = permute(LNSp(6,:),[1 3 2])./2;
Ret2 = permute(LNSp(7,:),[1 3 2])./2;
Abs2 = permute(LNSp(8,:),[1 3 2])./2;
ReAct2 = permute(LNSp(9,:),[1 3 2])./2;
ImAct2 = permute(LNSp(10,:),[1 3 2])./2;

J = zeros(2,2,NumL);
J(1,1,:) = exp(-1i.*Ret - Abs);
J(2,2,:) = exp( 1i.*Ret + Abs);
J(1,2,:) = 1i.*(Ret1 + ImAct1) + Abs1 + ReAct1 ;
J(2,1,:) = 1i.*(Ret1 - ImAct1) + Abs1 - ReAct1 ;
LMM1 = Jm2Mm(J);

J = zeros(2,2,NumL);
J(1,1,:) = exp(-1i.*Ret - Abs);
J(2,2,:) = exp( 1i.*Ret + Abs);
J(1,2,:) = 1i.*(Ret2 + ImAct2) + Abs2 + ReAct2 ;
J(2,1,:) = 1i.*(Ret2 - ImAct2) + Abs2 - ReAct2 ;
LMM2 = Jm2Mm(J);

% Check to make sure all structures are complex
LMM1 = complex(LMM1);
LMM2 = complex(LMM2);
end

