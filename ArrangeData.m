% Arrange the data after loading - segmented from original file

% center wavecals
Lc = 0.6;
Wcal.nVis(4) = Wcal.nVis(4) + 4*Lc*Wcal.nVis(5);
Wcal.nVis(3) = Wcal.nVis(3) + 3*Lc*Wcal.nVis(4) - 6*Lc^2*Wcal.nVis(5);
Wcal.nVis(2) = Wcal.nVis(2) + 2*Lc*Wcal.nVis(3) - 3*Lc^2*Wcal.nVis(4) + 4*Lc^3*Wcal.nVis(5);
Wcal.nVis(1) = Wcal.nVis(1) + Lc*Wcal.nVis(2) - Lc^2*Wcal.nVis(3) + Lc^3*Wcal.nVis(4) - Lc^4*Wcal.nVis(5);
Wcal.Lc_Vis = Lc*1000;

Lc = 1.35;
Wcal.nIR(3) = Wcal.nIR(3) + 3*Lc*Wcal.nIR(4);
Wcal.nIR(2) = Wcal.nIR(2) + 2*Lc*Wcal.nIR(3) - 3*Lc^2*Wcal.nIR(4);
Wcal.nIR(1) = Wcal.nIR(1) + Lc*Wcal.nIR(2) - Lc^2*Wcal.nIR(3) + Lc^3*Wcal.nIR(4);
Wcal.Lc_IR = Lc*1000;

% update handles
handles.L = L;
handles.Y = Y;
handles.SigY = SigY;
handles.PathF = PathF;
handles.Files = Files;
handles.CEcal.Wcal = Wcal;
handles.M11 = M11;

% update GUI
NumF = length(Files);
Data =  cell(NumF,3);
Data(:,1) = {true};
Data(:,2) = Files;
Data = Data(:,1:3);
ColNames = {' ','File','Thk'};
set(handles.uitable1,'ColumnName',ColNames);
set(handles.uitable1,'data',Data);
set(handles.saveSettings,'visible','on');
set(handles.saveBinary,'visible','on');
set(handles.uibuttongroup1,'visible','on');
set(handles.uibuttongroup2,'visible','on');
set(handles.uibuttongroup3,'visible','on');
set(handles.uibuttongroup4,'visible','on');
set(handles.uibuttongroup5,'visible','on');
set(handles.export_spectra,'visible','on');
if ~handles.M11
    set(handles.plotI0fromuT,'enable','off');
end

% update uitable2
Data = get(handles.uitable2,'data');
Data(3,6) = num2cell(AOIcal);
Data(6,6:10) = num2cell(Wcal.nVis);
Data(7,6:9) = num2cell(Wcal.nIR);
if handles.CtrlData.mode == 1
    Data(17,6:8) = num2cell(DO(1:3));
    Data(18,6:8) = {0, 0, DO(4)};
elseif handles.CtrlData.NewLens == 0
    Data(17,6:8) = num2cell(DO(1:3));
    Data(18,6:8) = {0, 0, DO(4)};
    Data(19,6:8) = num2cell(WD(1,:));
    Data(20,6:8) = num2cell(WD(2,:));
end
set(handles.uitable2,'data',Data);