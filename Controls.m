function varargout = Controls(varargin)
% CONTROLS MATLAB code for Controls.fig
%      CONTROLS, by itself, creates a new CONTROLS or raises the existing
%      singleton*.
%
%      H = CONTROLS returns the handle to a new CONTROLS or the handle to
%      the existing singleton*.
%
%      CONTROLS('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in CONTROLS.M with the given input arguments.
%
%      CONTROLS('Property','Value',...) creates a new CONTROLS or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before Controls_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to Controls_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help Controls

% Last Modified by GUIDE v2.5 27-Nov-2019 11:41:20

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @Controls_OpeningFcn, ...
                   'gui_OutputFcn',  @Controls_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before Controls is made visible.
function Controls_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to Controls (see VARARGIN)

% Choose default command line output for Controls
handles.output = hObject;

% Setup Controls structure
handles.CtrlData = varargin{1};
handles.M11 = varargin{2};

% Data Options
set(handles.datamode,'Value',handles.CtrlData.mode);
set(handles.denorm,'Value',handles.CtrlData.m11s);
if (handles.CtrlData.m11s ~= 1) || (~handles.M11)
    set(handles.uT,'Enable','off');
else
    set(handles.uT,'Enable','on');
    set(handles.uT,'Value',handles.CtrlData.uT);
end
set(handles.MMdepol,'Value',handles.CtrlData.Depol);

% Model Options
set(handles.centeredWcal,'Value',handles.CtrlData.centeredWcal);
set(handles.beamprofile,'Value',handles.CtrlData.BmProf);
set(handles.BWslices,'String',num2str(handles.CtrlData.BwSlcs));
set(handles.NAslices,'String',num2str(handles.CtrlData.NaSlcs));
set(handles.apodizer,'Value',handles.CtrlData.A3Apod);
set(handles.lensmodel,'Value',handles.CtrlData.NewLens);
set(handles.no_w4,'Value',handles.CtrlData.no_w4);
set(handles.wvl_shift_only,'Value',handles.CtrlData.only_w0);
if handles.CtrlData.no_w4
    handles.only_w0.Enable = 'off';
elseif handles.CtrlData.only_w0
    handles.no_w4.Enable = 'off';
end
set(handles.comp_fit,'Value',handles.CtrlData.CompCal);
if handles.CtrlData.mode == 1
    set(handles.lensmodel,'Enable','off');
    set(handles.MMdepol,'Enable','off');
    set(handles.comp_fit,'Enable','off');
end

% Fitting options
set(handles.matlabLS,'Value',handles.CtrlData.MatLS);
set(handles.rmsethreshold,'String',num2str(handles.CtrlData.RMSEthres));
set(handles.Sfit_iters,'String',num2str(handles.CtrlData.SerFitIter));
set(handles.Pfit_iters,'String',num2str(handles.CtrlData.ParFitIter));
set(handles.JAWmse,'Value',handles.CtrlData.JAWmse);

% Update handles structure
datamode_Callback(hObject, eventdata, handles);
guidata(hObject, handles);

% UIWAIT makes Controls wait for user response (see UIRESUME)
uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = Controls_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
% varargout{1} = handles.output;
varargout{1} = handles.CtrlData;
guidata(hObject,handles);
figure1_CloseRequestFcn(hObject, eventdata, handles);


% --- Executes on button press in accept.
function accept_Callback(hObject, eventdata, handles)
% hObject    handle to accept (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
CtrlData = handles.CtrlData;

% Data Options
CtrlData.mode = get(handles.datamode,'Value');
CtrlData.m11s = get(handles.denorm,'Value');
CtrlData.uT = get(handles.uT,'Value');
CtrlData.Depol = get(handles.MMdepol,'Value');

% Fitting Options
CtrlData.MatLS = get(handles.matlabLS,'Value');
tmp = get(handles.rmsethreshold,'String');
CtrlData.RMSEthres = str2double(tmp);
tmp = get(handles.Sfit_iters,'String');
CtrlData.SerFitIter = str2double(tmp);
tmp = get(handles.Pfit_iters,'String');
CtrlData.ParFitIter = str2double(tmp);
CtrlData.JAWmse = get(handles.JAWmse,'Value');

% Model Options
CtrlData.centeredWcal = get(handles.centeredWcal,'Value');
CtrlData.BmProf = get(handles.beamprofile,'Value');
tmp = get(handles.BWslices,'String');
CtrlData.BwSlcs = str2double(tmp);
tmp = get(handles.NAslices,'String');
CtrlData.NaSlcs = str2double(tmp);
CtrlData.A3Apod = get(handles.apodizer,'Value');
CtrlData.NewLens = get(handles.lensmodel,'Value');
CtrlData.no_w4 = get(handles.no_w4,'Value');
CtrlData.only_w0 = get(handles.wvl_shift_only,'Value');
CtrlData.CompCal = get(handles.comp_fit,'Value');

% Output variables
% handles.WvlDepVars = get(handles.uitable1,'Data');
handles.CtrlData = CtrlData;

guidata(hObject, handles);
close(handles.figure1);


% --- Executes on selection change in beamprofile.
function beamprofile_Callback(hObject, eventdata, handles)
% hObject    handle to beamprofile (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns beamprofile contents as cell array
%        contents{get(hObject,'Value')} returns selected item from beamprofile


% --- Executes during object creation, after setting all properties.
function beamprofile_CreateFcn(hObject, eventdata, handles)
% hObject    handle to beamprofile (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function BWslices_Callback(hObject, eventdata, handles)
% hObject    handle to BWslices (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of BWslices as text
%        str2double(get(hObject,'String')) returns contents of BWslices as a double


% --- Executes during object creation, after setting all properties.
function BWslices_CreateFcn(hObject, eventdata, handles)
% hObject    handle to BWslices (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function NAslices_Callback(hObject, eventdata, handles)
% hObject    handle to NAslices (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of NAslices as text
%        str2double(get(hObject,'String')) returns contents of NAslices as a double


% --- Executes during object creation, after setting all properties.
function NAslices_CreateFcn(hObject, eventdata, handles)
% hObject    handle to NAslices (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in apodizer.
function apodizer_Callback(hObject, eventdata, handles)
% hObject    handle to apodizer (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of apodizer



function rmsethreshold_Callback(hObject, eventdata, handles)
% hObject    handle to rmsethreshold (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of rmsethreshold as text
%        str2double(get(hObject,'String')) returns contents of rmsethreshold as a double


% --- Executes during object creation, after setting all properties.
function rmsethreshold_CreateFcn(hObject, eventdata, handles)
% hObject    handle to rmsethreshold (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in denorm.
function denorm_Callback(hObject, eventdata, handles)
% hObject    handle to denorm (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of denorm
if get(handles.denorm,'Value') == 1
    set(handles.uT,'Enable','on');
else
    set(handles.uT,'Enable','off');
end


% --- Executes on button press in matlabLS.
function matlabLS_Callback(hObject, eventdata, handles)
% hObject    handle to matlabLS (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of matlabLS



function Sfit_iters_Callback(hObject, eventdata, handles)
% hObject    handle to Sfit_iters (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of Sfit_iters as text
%        str2double(get(hObject,'String')) returns contents of Sfit_iters as a double


% --- Executes during object creation, after setting all properties.
function Sfit_iters_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Sfit_iters (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function Pfit_iters_Callback(hObject, eventdata, handles)
% hObject    handle to Pfit_iters (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of Pfit_iters as text
%        str2double(get(hObject,'String')) returns contents of Pfit_iters as a double

% --- Executes during object creation, after setting all properties.
function Pfit_iters_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Pfit_iters (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function figure1_CloseRequestFcn(hObject, eventdata, handles)
% hObject    handle to figure1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: delete(hObject) closes the figure
% delete(hObject);
if isequal(get(hObject, 'waitstatus'), 'waiting')
    % The GUI is still in UIWAIT, call UIRESUME
    uiresume(hObject);
else
    % The GUI is no longer waiting, just close it
    delete(hObject);
end

% --- Executes on selection change in datamode.
function datamode_Callback(hObject, eventdata, handles)
% hObject    handle to datamode (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns datamode contents as cell array
%        contents{get(hObject,'Value')} returns selected item from datamode
if get(handles.datamode,'Value') ~= 1
    set(handles.lensmodel,'Enable','on');
    set(handles.comp_fit,'Enable','on');
    if get(handles.datamode,'Value') == 2
        set(handles.MMdepol,'Value',false);
        set(handles.MMdepol,'Enable','off');
    else
        set(handles.MMdepol,'Enable','on');
    end
else
    set(handles.lensmodel,'Value',false);
    set(handles.MMdepol,'Value',false);
    set(handles.comp_fit,'Value',false);
    set(handles.lensmodel,'Enable','off');
    set(handles.MMdepol,'Enable','off');
    set(handles.comp_fit,'Enable','off');
end

% --- Executes during object creation, after setting all properties.
function datamode_CreateFcn(hObject, eventdata, handles)
% hObject    handle to datamode (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% --- Executes on button press in MMdepol.
function MMdepol_Callback(hObject, eventdata, handles)
% hObject    handle to MMdepol (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of MMdepol


% --- Executes on button press in centeredWcal.
function centeredWcal_Callback(hObject, eventdata, handles)
% hObject    handle to centeredWcal (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of centeredWcal
warndlg('Changing the wavelength calibration centering resets the Wcal parameters to the original CompleteEASE calibrated values.','Warning');


% --- Executes on button press in lensmodel.
function lensmodel_Callback(hObject, eventdata, handles)
% hObject    handle to lensmodel (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of lensmodel
warndlg('Changing the lens model resets the lens, DO, and WD parameters.','Warning');


% --- Executes on button press in uT.
function uT_Callback(hObject, eventdata, handles)
% hObject    handle to uT (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of uT


% --- Executes on button press in no_w4.
function no_w4_Callback(hObject, eventdata, handles)
% hObject    handle to no_w4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of no_w4
if get(hObject,'Value') == 1
    handles.wvl_shift_only.Enable = 'off';
end
if get(hObject,'Value') == 0
    handles.wvl_shift_only.Enable = 'on';
end


% --- Executes on selection change in JAWmse.
function JAWmse_Callback(hObject, eventdata, handles)
% hObject    handle to JAWmse (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns JAWmse contents as cell array
%        contents{get(hObject,'Value')} returns selected item from JAWmse


% --- Executes during object creation, after setting all properties.
function JAWmse_CreateFcn(hObject, eventdata, handles)
% hObject    handle to JAWmse (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in comp_fit.
function comp_fit_Callback(hObject, eventdata, handles)
% hObject    handle to comp_fit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of comp_fit


% --- Executes on button press in wvl_shift_only.
function wvl_shift_only_Callback(hObject, eventdata, handles)
% hObject    handle to wvl_shift_only (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of wvl_shift_only
if get(hObject,'Value') == 1
    handles.no_w4.Enable = 'off';
else
    handles.no_w4.Enable = 'on';
end
