% Export spectra with lens correction removed from MM data into new files

% Uses ADAP to simplify the saving process / preserve compatibility
% Needs to reload the file to preserve comments / header
% AGB 2019

% Reset BW slices
BWslcs = handles.CtrlData.BwSlcs;
handles.CtrlData.BwSlcs = 0;

% Ask for location to dump new files
save_dir = uigetdir([getenv('USERPROFILE') '\Desktop\'],'Select a folder for converted .dat files:');

% Run SimSetup to prepare for Lens MM calculation
tabdata = get(handles.uitable2,'data');
tabdata{1,6} = min(handles.L);
tabdata{2,6} = max(handles.L);
set(handles.uitable2,'data',tabdata);
guidata(hObject, handles);
SimSetup;

% Initialize GUI status bars
upwait_v2(0,handles.axes1,'Inverting Lens MMs...',0,handles.axes2,['Correcting file 1 of ' num2str(NumF)]);

% Invert Lens MMs
if ToolParms.nlens == 1
    [LM1,LM2] = LensMM_J(handles.L,LNSp.s);
else
    [LM1,LM2] = JAWlensMM(ToolParms.DO,ToolParms.WD,handles.L);
end
% [LM1,LM2] = LensMM_J(handles.L,LNSp.s);
LM1i = LM1-LM1;
LM2i = LM1i;
% aye = eye(4);
% opts.POSDEF = true;
% opts.SYM = true;
for l = 1:NumL
%     LM1i(:,:,l) = linsolve(LM1(:,:,l),aye,opts);
    LM1i(:,:,l) = inv(LM1(:,:,l));
    LM2i(:,:,l) = inv(LM2(:,:,l));
    upwait_v2(l/NumL,handles.axes1,'Inverting Lens MMs...',0,handles.axes2,['Correcting file 1 of ' num2str(NumF)]);
end
LM1i = complex(LM1i);
LM2i = complex(LM2i);

% For each file, load ADAP, change data, save new file
Fs = handles.Files(iSelect);
MyYs = handles.Y(:,1:16,iSelect);
MySigs = handles.SigY(:,1:16,iSelect);
MMforms = {'M12','M13','M14','M21','M22','M23','M24','M31','M32','M33','M34','M41','M42','M43','M44'};
d = adapdata;
for f = 1:NumF
    % remove lens effects from MM
    MyY = MyYs(:,:,f);
    MyMM = permute(MyY, [3 2 1]);
    MyMM = reshape(MyMM, [4 4 NumL]);
    MyMM = permute(MyMM, [2 1 3]);
    MyMM = complex(MyMM);
    MyMM = MM3D_v2(MyMM,LM1i);
    MyMM = MM3D_v2(LM2i,MyMM);
    MyMM = permute(MyMM, [2 1 3]);
    MyMM = reshape(MyMM, [1 16 NumL]);
    MyMM = permute(MyMM, [3 2 1]);
    
    % load file, change data, and save
    MyF = Fs{f};
    d = loaddata(d,[handles.PathF MyF]);
    for m = 1:15
        data = get(d,MMforms{m});
        data(:,3) = MyMM(:,m+1);
        d = set(d,MMforms{m},data);
    end
    
    % save new file
    save(d,[save_dir '\' MyF(1:end-4) '_LENS_ADJ.dat']);
    
    % update GUI
    upwait_v2(0.1,handles.axes1,'Removing lens effects...',f/NumF,handles.axes2,['Correcting file ' num2str(f) ' of ' num2str(NumF)]);
end

% restore BW slices
handles.CtrlData.BwSlcs = BWslcs;

% update GUI
upwait_v2('c',handles.axes1,' ','c',handles.axes2,' ');
