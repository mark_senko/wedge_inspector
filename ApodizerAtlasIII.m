function T = ApodizerAtlasIII(NumSlices)
% transmittance of Atlas-III apodizer

% NumSlices = 31;
DiamApertureLens = 3.64;
X = linspace(-DiamApertureLens/2,DiamApertureLens/2,NumSlices);
T = exp(-4*(abs(X/2.7)).^3);

% plot(X,T,'o-'); grid on

