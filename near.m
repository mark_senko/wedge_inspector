function kk=near(f,x)
  [mf,nf]=size(f);
  [mx,nx]=size(x);
  f=f(:);
  for jj=1:nx,
    for ii=1:mx
      d=(f-x(ii,jj)).^2;
      [m,kk(ii,jj)]=min(d);
    end
  end

  