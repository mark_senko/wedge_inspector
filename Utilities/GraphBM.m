% Graph Beam profile parameters from calibration file

% ---Load Settings.
if ~exist('DefaultPathDataFiles.mat','file')
    PathF= [cd '\'];
else
    load DefaultPathDataFiles PathF
    if ~exist('PathF','var')
        PathF = [cd '\'];
    end
    if PathF == 0
        PathF = [cd '\'];
    end
end
[FileName,PathF,FI] = uigetfile('*.xlsx','Read from Excel',PathF);
if FI == 0 ; return; end
if isempty(PathF)
    PathF = [cd '\'];
end
MyFile = [PathF FileName];

% Pull out Table 2
[~,~,newData] = xlsread(MyFile);
[s1,~]=size(newData);
fin = 0;
for i=1:s1 % find end of table 2
    if strcmp(newData{i,1},'Eps Inf')
        break;
    else
        fin = fin +1;
    end
end
Data = newData(2:fin,1:16);
for i=1:(fin-1)
    for j=3:16
        if isnan(Data{i,j})
            Data{i,j} = [];
        end
    end
end

% Graph BM @ 300nm, 600nm, 1200nm
wvls = linspace(190,1700,1511);
numBM = 0;
for i=5:14
    if isempty(Data{12,i})
        break;
    else
        numBM = numBM + 1;
    end
end
spnx = linspace(1240/190,1240/1700,numBM);
spnx = 1240./spnx;
bm1 = spline(spnx,cell2mat(Data(12,5:(5+numBM))),wvls);
bm2 = spline(spnx,cell2mat(Data(13,5:(5+numBM))),wvls);
bm3 = spline(spnx,cell2mat(Data(14,5:(5+numBM))),wvls);
bm4 = spline(spnx,cell2mat(Data(15,5:(5+numBM))),wvls);
bm5 = spline(spnx,cell2mat(Data(16,5:(5+numBM))),wvls);

figure('windowstyle','docked');
plot(wvls,bm1,wvls,bm2,wvls,bm3,wvls,bm4,wvls,bm5);
grid on
box on
title('BM Parameters VS Wavelength');
ylabel('Intensity [arb.]');
xlabel('Wavelength [nm]');
legend('bm1','bm2','bm3','bm4','bm5');

figure('windowstyle','docked');
NA = Data{4,5};
intd = linspace(-asind(NA),asind(NA),11);
intx = linspace(-1,1,11);
iwvl = (wvls == 300);
thr = [bm1(iwvl),bm2(iwvl),bm3(iwvl),bm4(iwvl),bm5(iwvl)];
thr = interp1([-1,-0.5,0,0.5,1],thr,intx);
iwvl = (wvls == 600);
six = [bm1(iwvl),bm2(iwvl),bm3(iwvl),bm4(iwvl),bm5(iwvl)];
six = interp1([-1,-0.5,0,0.5,1],six,intx);
iwvl = (wvls == 1200);
twv = [bm1(iwvl),bm2(iwvl),bm3(iwvl),bm4(iwvl),bm5(iwvl)];
twv = interp1([-1,-0.5,0,0.5,1],twv,intx);
hold on
plot(intd,thr,'b');
plot(intd,six,'g');
plot(intd,twv,'r');
hold off
ylim([0 1]);
grid on
box on
title('Beam Profiles at Specific Wavelengths')
ylabel('Intensity [arb.]');
xlabel('\Theta - \Theta_0 [deg]');
legend('@300nm','@600nm','@1200nm');






