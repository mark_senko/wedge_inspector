% Collect calibration data, pre WI fitting

% Get all Tool directories
F_multi = uigetdir();
allfiles = dir(F_multi);
dirFlags = [allfiles.isdir];
T_folders = allfiles(dirFlags);
T_folders = {T_folders.name};
T_folders = T_folders(3:end);
T_folders = natsort(T_folders);
nTools = length(T_folders);

% Setup collection cell array
parms = {'Tool # \ Parameters','JAW Det. SN','JAW Rec. SN',...
         'AOI','W0','W1','W2','W3','W4','W0_2','W1_2','W2_2','W3_2',...
         'DO1','DO2','DO3','DO4',...
         'WNRET_1','WNRET_2','WDSP1_1','WDSP1_2','WDSP2_1','WDSP2_2'};
all_data = cell(nTools+1,length(parms));
all_data(1,:) = parms;

% Collect data
h = waitbar(0,'Collecting Data...');
for i_T = 1:nTools
    % Get AOI, DOs, and WnDsp
    f = [F_multi '\' T_folders{i_T} '\SE\CompleteEASEhard.cnf']
    [AOI,DO,WnDsp] = Read_CEhard(f);
    
    % Get Wcal
    f = [F_multi '\' T_folders{i_T} '\SE\rc2-cal.cnf']
    Wcal = Read_WcalSE(f);
    W_v = Wcal.C_VIS + Wcal.Cc_VIS;
    W_i = Wcal.C_IR  + Wcal.Cc_IR;
    
    % Get receiver and detector SNs
    f = [F_multi '\' T_folders{i_T} '\SE\hardware.cnf']
    [DetSN,RecSN] = read_hardware_cnf(f);
    
    % organize data
    data = {T_folders{i_T},DetSN,RecSN,AOI,...
            W_v(1),W_v(2),W_v(3),W_v(4),W_v(5),W_i(1),W_i(2),W_i(3),W_i(4)...
            DO(1),DO(2),DO(3),DO(4),...
            WnDsp(1,1),WnDsp(2,1),WnDsp(1,2),WnDsp(1,3),WnDsp(2,2),WnDsp(2,3)};
            
    all_data(i_T+1,:) = data;
    waitbar(i_T/nTools,h);
end
close(h);

% save data
[f,p] = uiputfile('*.xlsx');
xlswrite([p f],all_data,1);
disp('Done.');