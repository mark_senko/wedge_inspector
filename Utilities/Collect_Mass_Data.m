% Collect calibration data

% Get all Tool directories
F_multi = uigetdir();
allfiles = dir(F_multi);
dirFlags = [allfiles.isdir];
T_folders = allfiles(dirFlags);
T_folders = {T_folders.name};
T_folders = T_folders(3:end);
T_folders = natsort(T_folders);
nTools = length(T_folders);

% Setup collection cell array
parms = {'Tool # \ Parameters','JAW Det. SN','JAW Rec. SN','RMSE',...
         'Vis AOI','IR AOI','IR AOI slope','NA','Vis BW',...
         'IR BW','W0','W1','W2','W3','W4','W0_2','W1_2','W2_2','W3_2',...
         'DO1','DO2','DO3','DO4','LVF Peak'};
all_data = cell(nTools+1,length(parms));
all_data(1,:) = parms;

% Collect data
h = waitbar(0,'Collecting Data...');
for i_T = 1:nTools
    f = [F_multi '\' T_folders{i_T} '\SE\WI_PA_Results\calibration.xlsx']
    cal = xlsread(f,1,'E4:I19');
    f = [F_multi '\' T_folders{i_T} '\SE\WI_PA_Results\Results.xlsx'];
    readps = xlsread(f,1,'B:B');
    for i_r = 2:length(readps)
        if ~isnan(readps(i_r))
            rmse = readps(i_r)
            LVFp = readps(i_r+1)
            break;
        end
    end
    [DetSN,RecSN] = read_hardware_cnf([F_multi '\' T_folders{i_T} '\SE\hardware.cnf']);
    data = {T_folders{i_T},DetSN,RecSN,rmse,cal(1,1),cal(1,3),cal(1,4),cal(2,1),...
            cal(3,1),cal(3,2),cal(4,1),cal(4,2),cal(4,3),cal(4,4),cal(4,5),...
            cal(5,1),cal(5,2),cal(5,3),cal(5,4),cal(15,1),cal(15,2),cal(15,3),...
            cal(16,3),LVFp};
    all_data(i_T+1,:) = data;
    waitbar(i_T/nTools,h);
end
close(h);

% save data
[f,p] = uiputfile('*.xlsx');
xlswrite([p f],all_data,1);
disp('Done.');