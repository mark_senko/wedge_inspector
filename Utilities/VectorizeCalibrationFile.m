% Code to load and vectorize a WI calibration file

% Setup while loop
KeepGoing = true;
ToolCount = 1;
excel = actxserver('excel.application');

% main while loop, will keep adding tools until done
while KeepGoing
    % Get calibration file
    [fil,pth] = uigetfile('*.xlsx');

    % Find end of table
    workbook = excel.Workbooks.Open([pth fil], [], true, [],'OntoEngrOnly');
%     rowEnd = sheet.Range('A1').End('xlDown').Row;
%     colEnd = sheet.Range('A1').End('xlToRight').Column;
    sheet = excel.ActiveWorkbook.Sheets;
    sheet = sheet.get('Item',1);
    sheet.Activate;
    newTable = excel.ActiveSheet.UsedRange.Value;
    workbook.Close;
    [s1,~]=size(newTable);
    fin = 0;
    for i=1:s1 % find end of table 2
        if strcmp(newTable{i,1},'Eps Inf.')
            break;
        else
            fin = fin +1;
        end
    end
    
    % Setup Vector Table
    if ToolCount == 1
        ToolData = newTable(2:fin,5:14);
        iTD = false(size(ToolData));
        for i=1:fin-1
            for j=1:10
                iTD(i,j) = ~isnan(ToolData{i,j});
            end
        end
        ToolData = cell(sum(sum(iTD)),4);
        ToolData = [{'Parameter'},{'#'},{'Wvl Type'},{'Detector Split'};ToolData];
        cnt = 2;
        for i=1:fin-1
            NumP = sum(iTD(i,:));
            parm = repmat(newTable(i+1,1),NumP,1);
            num  = num2cell(linspace(1,NumP,NumP).');
            wtyp = repmat(newTable(i+1,2),NumP,1);
            splt = repmat(newTable(i+1,3),NumP,1);
            ToolData(cnt:cnt+NumP-1,1:4) = [parm,num,wtyp,splt];
            cnt = cnt + NumP;
        end
    end
    
    % Ask for Tool name
    ToolName = inputdlg('Please enter the tool name:');
    
    % Read calibration table to vector table
    newblock = cell(sum(sum(iTD)),1);
    RowCount = 2;
    for i=1:fin-1
        for j=1:10
            if iTD(i,j)
                newblock(RowCount) = newTable(i+1,j+4);
                RowCount = RowCount + 1;
            end
        end
    end
    newblock(1) = ToolName;
    ToolData = [ToolData, newblock];

    % Ask to continue
    moretools = questdlg('Finished with this tool. Would you like to add another tool?',...
                         'Continue?','Yes','No','No');
    if strcmp(moretools,'No')
        KeepGoing = false;
    end
    
    % Iterate ToolCount
    ToolCount = ToolCount + 1;
end
% Quit Excel
excel.Quit;

% Prompt for Excel filename
[fil,pth] = uiputfile('WI_vectorized_cals.xlsx');

% Save Excel file
xlswrite([pth fil],ToolData);