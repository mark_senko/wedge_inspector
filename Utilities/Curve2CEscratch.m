function [] = Curve2CEscratch(A)
% Create CE test scratchpad

path = 'C:\Users\aboosalis\Desktop\';
file = 'CE_scratch_from_Matlab.scratch';
fid = fopen([path file],'wt');

fprintf(fid,'\n');
fprintf(fid,'start_Titles\n');
fprintf(fid,'\t''Spectroscopic Intensity Data''\t\n');
fprintf(fid,'\t''Wavelength (nm)''\t\n');
fprintf(fid,'\t''Intensity''\t\n');
fprintf(fid,'\t''''\t\n');
fprintf(fid,'\t18\t\n');
fprintf(fid,'\t18\t\n');
fprintf(fid,'\t\n');
fprintf(fid,'end_Titles\n');

fprintf(fid,'\n');
fprintf(fid,'start_Settings\n');
fprintf(fid,'\tT\t\n');
fprintf(fid,'\t\n');
fprintf(fid,'end_Settings\n');

fprintf(fid,'\n');
fprintf(fid,'start_Data\n');
fprintf(fid,'\t1\t\n');
fprintf(fid,'\tstart_Curve 0\n');
fprintf(fid,'\t\t\n');
fprintf(fid,'\t\tstart_Curve Parameters\n');
fprintf(fid,'\t\t\tT\t\n');
fprintf(fid,'\t\t\tF\t\n');
fprintf(fid,'\t\t\t-65536\t\n');
fprintf(fid,'\t\t\t0\t\n');
fprintf(fid,'\t\t\t''Intensity from Matlab''\t\n');
fprintf(fid,'\t\t\t\n');
fprintf(fid,'\t\tend_Curve Parameters\n');
fprintf(fid,'\t\t%d\t\n',size(A,1));

for i=1:size(A,1)
    fprintf(fid,'\t\t%f\t%f\t\n',A(i,1),A(i,2));
end

fprintf(fid,'\t\t\n');
fprintf(fid,'\tend_Curve 0\n');
fprintf(fid,'\t\n');
fprintf(fid,'end_Data\n');

fprintf(fid,'\n');
fprintf(fid,'start_Trends\n');
fprintf(fid,'\t0\t\n');
fprintf(fid,'\t\n');
fprintf(fid,'end_Trends\n');

fclose(fid);
end

