function [XData,YData] = GetPlotData()
% Gets data from .fig files and saves it as an Excel file

[Files,Path]=uigetfile('.fig','Choose .fig files to load data from...','Multiselect','on');
h=waitbar(0,'Getting Data...');
if ~iscell(Files)
    Files = {Files};
end
[~,nFiles]=size(Files);
XData=cell(nFiles);
YData=cell(nFiles);
for i=1:nFiles
    load([Path Files{i}],'-mat');
    hAxes = get(gca);
    hProperties = hAxes.Children;
    XData{i} = get(hProperties, 'XData');
    YData{i} = get(hProperties, 'YData');
    close(gcf);
    waitbar(i/nFiles,h);
end
close(h);
[PutName,PutPath]=uiputfile('.xlsx','Save As...');
for i=1:nFiles
    xlswrite([PutPath PutName],XData{i}',Files{i},'A');
    xlswrite([PutPath PutName],YData{i}',Files{i},'B');
end

end

