function [di1, di2] = LensInPlaneRetardance(Lo, DO, Split)
% calculate lenses in-plane retardances in degrees
% Split is the fraction of the total retardance assigned to T1

% debugg %%%%%%%%
% DO = DO.*[1 0.1 0.001 0.1];
%%%%%%%%%%%%

B1 = DO*Split;
B2 = DO*(1-Split);

l2 = ((Lo./500).^2);
q = 500./Lo;
q2 = q.*q;
q4 = q2.*q2;

% new formulas
di1 = q.*(B1(1) + B1(2)*q2 + B1(3)*q4 + B1(4)*l2);
di2 = q.*(B2(1) + B2(2)*q2 + B2(3)*q4 + B2(4)*l2);
