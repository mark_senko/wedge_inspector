function s = loaddata(s, fileName)
% S = LOADDATA(S, FILENAME)
% Load data to object S from FILENAME

% AllTypes = {'uR', 'sR', 'pR', 'SE', 'NICX'};
% Alias    = {'SR', 'TM', 'TE', 'SE', 'NICX'};

% Read in the whole data file as lines of strings
try
    data = textread(fileName, '%s', 'delimiter', '\n');
catch
    error('Error reading data file %s!', fileName);
end
nLines = length(data); % number of lines read
% Remove empty lines at the end
while isempty(data{nLines})
    nLines = nLines - 1;
end
data = data(1:nLines);
% Clear the class
s = clear(s);
s.filename = fileName;

% Check data file
isXmlFormat = false;
xmlSignature = '<?xml';
line = data{1};
if strncmpi(line, xmlSignature, length(xmlSignature))
    isXmlFormat = true;
end
if isXmlFormat
    dllFileName = 'DiffractCommon.dll';
    libAlias = 'NDCMN';
    if ~libisloaded(libAlias)
        [notfound, warnings] = loadlibrary(dllFileName, @mHeader, 'alias', libAlias);
        %[notfound,warnings]=loadlibrary(dllFileName, 'NDCommon.h', 'alias', libAlias);
    end
    
    key = 'NanoDiffract_2183FA10-00B2-40F2-A890-272F2EF3C07C';
    
    rawData = xml2struct(fileName);
    rawData = rawData.NanoDiffractData;
    %dataVersion = rawData.Attributes.Version;
    
    s.wvlUnit = rawData.DataSet.Units.Text;
    s.seInfo = rawData.DataSet.DataHeader.Text;
    if ~isempty(s.seInfo)
        if ~strcmpi(s.seInfo(1:4), 'VASE') && s.seInfo(1) ~= ';'
            s.seInfo = [';', s.seInfo];
        end
    end
    s.analysisHeader = rawData.DataSet.AnalysisHeader;
    s.comment = AnalysisHeaderStruct2Str(rawData.DataSet.AnalysisHeader);
    
    allData = rawData.DataSet.Data;
    
    nDataTypes = length(allData);
    s.dataType = cell(1, nDataTypes);
    idxValidTye = 0;
    for iData = 1:nDataTypes
        if nDataTypes == 1
            d = rawData.DataSet.Data.Text;
            dataType = rawData.DataSet.Data.Attributes.DataType;
        else
            d = rawData.DataSet.Data{iData}.Text;
            dataType = rawData.DataSet.Data{iData}.Attributes.DataType;
        end
        
        % Convert to adapdata syntax
        if dataType == 'E'
            dataType = 'SE';
        elseif dataType(1) == 'm' && dataType(2) ~= 'R'
            dataType = ['M', dataType(3:end)];
        end
        if strcmpi(dataType, 'ane') ||strcmpi(dataType, 'asp') ||...
                strcmpi(dataType, 'aps')
            % Ignore them
        else
            idxValidTye = idxValidTye + 1;
            s.dataType{idxValidTye} = dataType;
            
            
            % Matlab struct definition
            spectrum = struct(...
                'dataType', 0, ...
                'dAOI', 0, ...
                'numPoints', 0, ...
                'pWvls', [], ...
                'pValues1', [], ...
                'pValues2', [], ...
                'pStdDev1', [], ...
                'pStdDev2', [], ...
                'pSpectralWeight1', [], ...
                'pSpectralWeight2', []);
            if nDataTypes == 1
                dataSize = str2double(rawData.DataSet.Data.Attributes.RawSize);
            else
                dataSize = str2double(rawData.DataSet.Data{iData}.Attributes.RawSize);
            end
            
            [errorID, x1, x2, spectrum] = calllib(libAlias, 'NDData_DecryptSpectrumData', d, length(d), dataSize, key, spectrum);
            [ m, dataTypeID, aoi, nWvls ] = SpectrumStruct2Array( spectrum );
            % Is it SE or NICX?
            if strcmpi(dataType, 'SE') && aoi < 20
                s.NICX = m; % This is NICX
                s.dataType{idxValidTye} = 'NICX';
            else % Regular SE and all others
                s.(dataType) = m;
            end
        end
    end
    s.dataType = s.dataType(1:idxValidTye);
    
else % Plain text format
    
    dataLines = []; % for storing starting data row numbers
    % Check the first 2000 lines to find the the first line of data
    firstDataLine = 0;
    for iLine = 1:min(2000, nLines)
        line = data{iLine};
        if isempty(line)
            continue; % ignore empty lines
        elseif strncmp(line, 'nm', 2)
            s.wvlUnit = 'nm';
        elseif strncmp(line, 'VASE', 4) || strncmp(line, ';EllipsometerType', length(';EllipsometerType'))
            s.seInfo = line;
        else
            type = checkdatatype(line);
            if ~isempty(type)
                s.dataType{1} = type;
                firstDataLine = iLine;
                break;
            else
                if isempty(s.comment)
                    s.comment = line;
                else
                    s.comment = sprintf('%s\n%s', s.comment, line);
                end
            end
        end
    end
    try
        s.analysisHeader = Str2AnalysisHeaderStruct(s.comment);
    catch
    end
    
    if firstDataLine == 0
        error('Unrecognized data format!')
    end
    nDataLines = nLines-firstDataLine+1;
    
    % Check last line
    line = data{nLines};
    type = checkdatatype(line);
    if strcmp(s.dataType{1}, type) && nLines < 2048 % In case we have SE + NICX
        % Single data type: simply
        dataLines = [firstDataLine,nLines+1]; % nLines+1 is used for later convenience
    else % two or more data types exist
        % Build a string of first characters from each data line
        % A-Z is  65-90, a-z is 97 to 122
        h = '';
        for idx = 1:nDataLines
            iLine = idx+firstDataLine-1;
            h(idx) = data{iLine}(1); % the first character in each data line
            if h(idx) == 'E' % 69, SE data start with E or just digits
                % Check AOI
                dd = sscanf(data{iLine}, 'E %e%e')';
                if dd(2) < 20
                    h(idx) = 'X'; % 88 (Assigned signature to NICX)
                end
            elseif (h(idx)<='9' && h(idx) >='0')
                h(idx) = 'E'; % 69,
                dd = sscanf(data{iLine}, '%e%e')';
                if dd(2) < 20
                    h(idx) = 'X'; % Give it a signature to NICX
                end
            elseif h(idx) == 'm'
                % Use the MM index (11, 12, 13, 14, 21, 22, ..., 44)
                % They will not interfere with any letters
                h(idx) = str2double(data{iLine}(3:4)); 
            elseif h(idx) == 's' % 115
                h(idx) = 's';
            elseif h(idx) == 'p' % 112
                h(idx) = 'p';
            elseif h(idx) == 'u' % 117
                h(idx) = 'u';
            elseif h(idx) == 'a' % ane, asp, aps
                if data{iLine}(2) == 'n' % ane
                    h(idx) = 'n';
                elseif data{iLine}(2) == 's' % asp
                    h(idx) = 't';
                elseif data{iLine}(2) == 'p' % aps
                    h(idx) = 'q';
                end
            else % Unknown types to ignore
                h(idx) = 'Z'; % 122
            end
        end
        % Remove unknown types
        mskN = find(h == 'Z');
        h(mskN) = [];
        data(mskN+firstDataLine-1) = [];
        nLines = length(data);
        %nDataLines = length(h);
        % difference of h shows where data type changes
        dfh = [0, diff(h)];
        idxBeginNewType = find(dfh); % find indices of nonzero elements
        ii = 2; % The first data type has been detected before
        for iLine = idxBeginNewType
            line = data{iLine+firstDataLine-1};
            dt = checkdatatype(line);
            if ~isempty(dt)
                s.dataType{ii} = checkdatatype(line);
                ii = ii+1;
            end
        end
        % Contruct indices of first line of new data types
        % nLines+1 is added at the end for later programming convenience
        dataLines = [firstDataLine,idxBeginNewType+firstDataLine-1, nLines+1];
    end
    
    % Read in data
    nTypes = length(s.dataType);
    for iType = 1:nTypes % Work on each data type
        type = s.dataType{iType}; % data type
        iStart = dataLines(iType); % start line number
        iEnd = dataLines(iType+1); % end line number
        N = iEnd-iStart;% total number of lines
        switch type
            case {'sR', 'pR', 'uR', 'uT'}
                s.(type) = ones(N, 5); % preallocate memory
                for k = 1:N
                    iLine = k+iStart-1;
                    line = data{iLine};
                    % uT is also consider uR, thus line(1:2) instead of
                    % simply type in sscanf(line, [line(1:2), ...
                    x = sscanf(line, [line(1:2), '%e%e%e%e%e'])';
                    s.(type)(k,1:length(x)) = x; 
                end
                
            case {'SE', 'NICX'} % 'NICX' means NICX data
                s.(type) = ones(N, 8); % preallocate memory
                line = data{iStart};
                s.hasLeadingE = (line(1) == 'E');
                for k = 1:N
                    iLine = k+iStart-1;
                    line = data{iLine};
                    if s.hasLeadingE
                        x = sscanf(line, 'E %e%e%e%e%e%e%e%e')';
                    else
                        x = sscanf(line, '%e%e%e%e%e%e%e%e')';
                    end
                    s.(type)(k,1:length(x)) = x; 
                end
            case {'AnE', 'Asp', 'Aps'}
                s.(type) = ones(N, 8); % preallocate memory
                line = data{iStart};
                for k = 1:N
                    iLine = k+iStart-1;
                    line = data{iLine};
                    x = sscanf(line, '%e%e%e%e%e%e%e%e')';
                    s.(type)(k,1:length(x)) = x; 
                end                
            case {'mR'}
                s.(type) = ones(N, 8); % preallocate memory
                line = data{iStart};
                for k = 1:N
                    iLine = k+iStart-1;
                    line = data{iLine};
                    x = sscanf(line, ['mR', '%e%e%e%e%e%e%e%e'])';
                    s.(type)(k,1:length(x)) = x; 
                end 
            otherwise
                if type(1) == 'M'
                    s.(type) = ones(N, 8);
                    for k = 1:N
                        iLine = k+iStart-1;
                        line = data{iLine};
                        x = sscanf(line, [['mm', type(2:3)], '%e%e%e%e%e%e%e%e'])';
                        s.(type)(k,1:length(x)) = x; 
                    end
                end              
        end
    end
    
end
% Add NCR data if it is NICX
if hasdatatype(s, 'sR') && hasdatatype(s, 'pR') && hasdatatype(s, 'NICX')
    s = addncr(s);
end
% Add NCS data if it is SE
if hasdatatype(s, 'SE') || hasdatatype(s, 'E')
    s = addncs(s);
end