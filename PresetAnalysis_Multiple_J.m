% Preset Analysis for Jon's lens model
% Adjustment made to handle multiple fits in series
tic;

% reset to MM mode
ResetAll;
handles.CtrlData.mode = 3;
handles.CtrlData.NewLens = 1;
tabdata = get(handles.uitable2,'data');
adds = [{false,'NCS Ret.', 'Polynom.', true, 10, 0},...
        repmat({0},[1 9]),{0.001,0.001};...
        {false,'NCS Att.', 'Polynom.', true, 10, 0},...
        repmat({0},[1 9]),{0.001,0.001};...
        {false,'T1 Ret.', 'Polynom.', true, 10, 0},...
        repmat({0},[1 9]),{0.001,0.001};...
        {false,'T1 Att.', 'Polynom.', true, 10, 0},...
        repmat({0},[1 9]),{0.001,0.001};...
        {false,'T1 Real Act.', 'Polynom.', true, 10, 0},...
        repmat({0},[1 9]),{0.001,0.001};...
        {false,'T1 Imag Act.', 'Polynom.', true, 10, 0},...
        repmat({0},[1 9]),{0.001,0.001};...
        {false,'T2 Ret.', 'Polynom.', true, 10, 0},...
        repmat({0},[1 9]),{0.001,0.001};...
        {false,'T2 Att.', 'Polynom.', true, 10, 0},...
        repmat({0},[1 9]),{0.001,0.001};...
        {false,'T2 Real Act.', 'Polynom.', true, 10, 0},...
        repmat({0},[1 9]),{0.001,0.001};...
        {false,'T2 Imag Act.', 'Polynom.', true, 10, 0},...
        repmat({0},[1 9]),{0.001,0.001}];
tabdata = [tabdata(1:16,:); adds];
set(handles.uitable2,'data',tabdata);
guidata(hObject,handles);

% Get all Tool directories
F_multi = uigetdir();
if ~F_multi
    return;
end
allfiles = dir(F_multi);
dirFlags = [allfiles.isdir];
T_folders = allfiles(dirFlags);
T_folders = {T_folders.name};
T_folders = T_folders(3:end);
T_folders = natsort(T_folders);
if ismember('SE',T_folders)
    nTools = 1;
else
    nTools = length(T_folders);
end

for i_T = 1:nTools % main loop
    
    % Pick a tool folder and load
    if nTools ~= 1
        PathF = [F_multi '\' T_folders{i_T} '\SE\']
    else
        PathF = [F_multi '\SE\']
    end
    Files = dir(fullfile(PathF,'*.dat'));
    Files = {Files.name};
    Files = natsort(Files);
    LoadData_DAT;
    ArrangeData;
    guidata(hObject, handles);

    % Estimate thicknesses and fit for them
    PopulateThks;
    SerialFit;
    if op.iterations == -1; break; end

    % Run through all three phases of the JLC
    JLC_p1a; pause(1);
    JLC_p1b; pause(1);
    JLC_p1c; pause(1);
    JLC_p2a; pause(1);
    JLC_p2b; pause(1);
%     JLC_p2c; pause(1);
    JLC_p3a; pause(1);
    JLC_p3b; pause(1);
    JLC_p3a; pause(1);

    % create figures
    close(handles.figh);
    GUI_WedgeDataInspector('plotFitParams_Callback',handles.plotFitParams,[],handles);
    GUI_WedgeDataInspector('plotwvldeps_Callback',handles.plotwvldeps,[],handles);
    pause(1);

    % spectrum analysis
    ResidualAnalysis_J; % <- mostly just to save graphs
    
    % Reset for next run
    % reset to MM mode
    ResetAll;
    handles.CtrlData.mode = 3;
    handles.CtrlData.NewLens = 1;
    tabdata = get(handles.uitable2,'data');
    adds = [{false,'NCS Ret.', 'Polynom.', true, 10, 0},...
            repmat({0},[1 9]),{0.001,0.001};...
            {false,'NCS Att.', 'Polynom.', true, 10, 0},...
            repmat({0},[1 9]),{0.001,0.001};...
            {false,'T1 Ret.', 'Polynom.', true, 10, 0},...
            repmat({0},[1 9]),{0.001,0.001};...
            {false,'T1 Att.', 'Polynom.', true, 10, 0},...
            repmat({0},[1 9]),{0.001,0.001};...
            {false,'T1 Real Act.', 'Polynom.', true, 10, 0},...
            repmat({0},[1 9]),{0.001,0.001};...
            {false,'T1 Imag Act.', 'Polynom.', true, 10, 0},...
            repmat({0},[1 9]),{0.001,0.001};...
            {false,'T2 Ret.', 'Polynom.', true, 10, 0},...
            repmat({0},[1 9]),{0.001,0.001};...
            {false,'T2 Att.', 'Polynom.', true, 10, 0},...
            repmat({0},[1 9]),{0.001,0.001};...
            {false,'T2 Real Act.', 'Polynom.', true, 10, 0},...
            repmat({0},[1 9]),{0.001,0.001};...
            {false,'T2 Imag Act.', 'Polynom.', true, 10, 0},...
            repmat({0},[1 9]),{0.001,0.001}];
    tabdata = [tabdata(1:16,:); adds];
    set(handles.uitable2,'data',tabdata);
    guidata(hObject,handles);
    
    % Close all the figures
    if nTools ~= 1
        set(GUI_WedgeDataInspector, 'HandleVisibility', 'off');
        close all;
        set(GUI_WedgeDataInspector, 'HandleVisibility', 'on');
        close(1);
        clearvars -except handles i_T nTools F_multi T_folders hObject
    end
end

toc