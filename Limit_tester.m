% Limit tester
% limits are designed to give the limit at the limit, and give 90% of the
% limit at infinity.

val = (-100:1:100)./20; % values from -5 to 5
orig = val; % copy original values
UL = 2.*ones(size(val)); % upper limit at 2
LL = -2.*ones(size(val)); % lower limit at -2

iP = (val > UL); % find values above the limit
val(iP) = LL(iP)+0.9.*(UL(iP)-LL(iP))+0.1.*(UL(iP)-LL(iP)).*exp((UL(iP)-val(iP))./(UL(iP)-LL(iP)));
iP = (val < LL); % find values below the limit
val(iP) = UL(iP)-0.9.*(UL(iP)-LL(iP))-0.1.*(UL(iP)-LL(iP)).*exp((val(iP)-LL(iP))./(UL(iP)-LL(iP)));

plot(orig,val); % plot adjusted values as a function of the original values
ylabel('Output Values');
xlabel('Input Values');