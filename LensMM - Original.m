function [LMM1,LMM2] = LensMM(Lh,LNSp)
% Generate the 2 matrices LMM1 and LMM2, which are rotated retarders 
% with optical activity and absorption.

% Setup input parameters
NumL = length(Lh);
% Lh = permute(Lh,[3 2 1]);
Ret1  = permute(LNSp(1,:),[1 3 2]);
Abs1  = permute(LNSp(2,:),[1 3 2]);
Thta1 = permute(LNSp(3,:),[1 3 2]);
Act1  = permute(LNSp(4,:),[1 3 2]);
Ret2  = permute(LNSp(5,:),[1 3 2]);
Abs2  = permute(LNSp(6,:),[1 3 2]);
Thta2 = permute(LNSp(7,:),[1 3 2]);
Act2  = permute(LNSp(8,:),[1 3 2]);

% Define Rotation matrices
Rot1 = zeros(4,4,NumL);
Rot1(1,1,:) = 1;
Rot1(4,4,:) = 1;
Rot2 = Rot1;
Rot1(2,2,:) = cosd(2.*Thta1);
Rot1(3,3,:) = cosd(2.*Thta1);
Rot1(2,3,:) = sind(2.*Thta1);
Rot1(3,2,:) = -sind(2.*Thta1);
Rot2(2,2,:) = cosd(2.*Thta2);
Rot2(3,3,:) = cosd(2.*Thta2);
Rot2(2,3,:) = sind(2.*Thta2);
Rot2(3,2,:) = -sind(2.*Thta2);
Rot1i = permute(Rot1,[2 1 3]);
Rot2i = permute(Rot2,[2 1 3]);

% Optically Rotate + Retard Simultaneously, Lens 1
RotJ = [cosd(Act1), -sind(Act1);...
        sind(Act1),  cosd(Act1)];
RotJ = complex(RotJ);
J = zeros(2,2,NumL);
J(1,1,:) = exp(-1i.*Ret1 - Abs1);
J(2,2,:) = exp( 1i.*Ret1 + Abs1);
J = complex(J);
J = MM3D_v2(RotJ,J);
LMM1 = Jm2Mm(J);

% Optically Rotate + Retard Simultaneously, Lens 2
RotJ = [cosd(Act2), -sind(Act2);...
        sind(Act2),  cosd(Act2)];
RotJ = complex(RotJ);
J = zeros(2,2,NumL);
J(1,1,:) = exp(-1i.*Ret2 - Abs2);
J(2,2,:) = exp( 1i.*Ret2 + Abs2);
J = complex(J);
J = MM3D_v2(RotJ,J);
LMM2 = Jm2Mm(J);

% Check to make sure all structures are complex
LMM1 = complex(LMM1);
LMM2 = complex(LMM2);
Rot1 = complex(Rot1);
Rot2 = complex(Rot2);
Rot1i = complex(Rot1i);
Rot2i = complex(Rot2i);

% Rotate to fast axis
LMM1 = MM3D_v2(LMM1,Rot1i);
LMM1 = MM3D_v2(Rot1,LMM1);
LMM2 = MM3D_v2(LMM2,Rot2i);
LMM2 = MM3D_v2(Rot2,LMM2);

end

