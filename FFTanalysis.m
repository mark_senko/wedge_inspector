function varargout = FFTanalysis(varargin)
% FFTANALYSIS MATLAB code for FFTanalysis.fig
%      FFTANALYSIS, by itself, creates a new FFTANALYSIS or raises the existing
%      singleton*.
%
%      H = FFTANALYSIS returns the handle to a new FFTANALYSIS or the handle to
%      the existing singleton*.
%
%      FFTANALYSIS('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in FFTANALYSIS.M with the given input arguments.
%
%      FFTANALYSIS('Property','Value',...) creates a new FFTANALYSIS or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before FFTanalysis_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to FFTanalysis_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help FFTanalysis

% Last Modified by GUIDE v2.5 20-Apr-2021 14:32:27

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @FFTanalysis_OpeningFcn, ...
                   'gui_OutputFcn',  @FFTanalysis_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before FFTanalysis is made visible.
function FFTanalysis_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to FFTanalysis (see VARARGIN)

% Choose default command line output for FFTanalysis
handles.output = hObject;

% grab data from input
handles.tab1data = varargin{1};
handles.Ye = varargin{2};
handles.Ys = varargin{3};
handles.uT = varargin{4};
handles.SigY = varargin{5};
handles.Ls = varargin{6};
PathMat = varargin{7};

% Get default NKs
if isempty(PathMat)
    PathMat = [pwd '\Mats'];
    if ~exist(PathMat,'dir')
        uiwait(msgbox({'No MATERIALS folder found.' 'Please select MATERIALS folder.'},'Folder Not Found','error'));
        PathMat = uigetdir(pwd,'Please Select the Materials Folder');
    end
end
mthd = 'linear';
[pp_sio2_n,~] = GetNKs_v3([PathMat '\SiO2_JAW.txt'],mthd);
handles.pp_sio2_n = pp_sio2_n;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes FFTanalysis wait for user response (see UIRESUME)
uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = FFTanalysis_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = 'OK';


% --- Executes on selection change in dataselect.
function dataselect_Callback(hObject, eventdata, handles)
% hObject    handle to dataselect (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns dataselect contents as cell array
%        contents{get(hObject,'Value')} returns selected item from dataselect


% --- Executes during object creation, after setting all properties.
function dataselect_CreateFcn(hObject, eventdata, handles)
% hObject    handle to dataselect (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in method.
function method_Callback(hObject, eventdata, handles)
% hObject    handle to method (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns method contents as cell array
%        contents{get(hObject,'Value')} returns selected item from method


% --- Executes during object creation, after setting all properties.
function method_CreateFcn(hObject, eventdata, handles)
% hObject    handle to method (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in yaxisunit.
function yaxisunit_Callback(hObject, eventdata, handles)
% hObject    handle to yaxisunit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns yaxisunit contents as cell array
%        contents{get(hObject,'Value')} returns selected item from yaxisunit


% --- Executes during object creation, after setting all properties.
function yaxisunit_CreateFcn(hObject, eventdata, handles)
% hObject    handle to yaxisunit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in uTnorm.
function uTnorm_Callback(hObject, eventdata, handles)
% hObject    handle to uTnorm (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of uTnorm


% --- Executes on button press in signorm.
function signorm_Callback(hObject, eventdata, handles)
% hObject    handle to signorm (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of signorm


% --- Executes on button press in logscale.
function logscale_Callback(hObject, eventdata, handles)
% hObject    handle to logscale (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of logscale


% --- Executes on button press in overwvl.
function overwvl_Callback(hObject, eventdata, handles)
% hObject    handle to overwvl (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of overwvl
if get(hObject,'Value')
    handles.overwvlmin.Enable = 'on';
    handles.overwvlmax.Enable = 'on';
else
    handles.overwvlmin.Enable = 'off';
    handles.overwvlmax.Enable = 'off';
end
guidata(hObject, handles);


function overwvlmin_Callback(hObject, eventdata, handles)
% hObject    handle to overwvlmin (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of overwvlmin as text
%        str2double(get(hObject,'String')) returns contents of overwvlmin as a double


% --- Executes during object creation, after setting all properties.
function overwvlmin_CreateFcn(hObject, eventdata, handles)
% hObject    handle to overwvlmin (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function overwvlmax_Callback(hObject, eventdata, handles)
% hObject    handle to overwvlmax (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of overwvlmax as text
%        str2double(get(hObject,'String')) returns contents of overwvlmax as a double


% --- Executes during object creation, after setting all properties.
function overwvlmax_CreateFcn(hObject, eventdata, handles)
% hObject    handle to overwvlmax (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in plotnow.
function plotnow_Callback(hObject, eventdata, handles)
% hObject    handle to plotnow (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

%% Setup
% Gather data into local vars
Ye = handles.Ye;
Ys = handles.Ys;
uT = handles.uT;
SigY = handles.SigY(:,1:16,:);
Ls = handles.Ls;
pp_sio2_n = handles.pp_sio2_n;
tab1data = handles.tab1data;

% parse table 1 data
iSelect = cell2mat(tab1data(:,1));
if sum(iSelect)==0
    msgbox('Select some files first.')
    return
end
thks = tab1data(iSelect,3);
NumF = sum(iSelect);

% check if some Thk are missing
for nF = 1:NumF
    if isempty(thks{nF})
        msgbox('Thk inputs are missing.')
        return
    end
end
thks = cell2mat(thks);

% override wavelength?
NumL = size(Ye,1);
if handles.overwvl.Value
    lmax = str2double(handles.overwvlmax.String);
    lmin = str2double(handles.overwvlmin.String);
    iL = (Ls <= lmax) & (Ls >= lmin);
    Ls = Ls(iL);
end

% Estimate I0 in case we need it
maxdata = importdata('M11_max.xlsx');
m11max = transpose(spline(maxdata(:,1),maxdata(:,2),Ls));
Io = max(uT,[],1)./m11max;

% setup frequency vectors
dT = thks-circshift(thks,1);
dT = mean(dT(2:end));
Ft = 1/abs(dT);
numts = round(abs((max(thks)-min(thks))/dT));
Ts = linspace(max(thks),min(thks),numts);
Fts = Ft.*(0:1:(floor(numts/2))-1)./numts;
Fts = Fts.';
Lest = EstimateLfromFs(pp_sio2_n,Fts,0.01);

%% Organize Data
% switch and normalize data
switch handles.dataselect.Value
    case 1
        Ne = permute(Ye(:,1,iSelect),[3 1 2]);
        Ns = permute(Ys(:,1,iSelect),[3 1 2]);
        Se = permute(SigY(:,1,iSelect),[3 1 2]);
        Z = Ne-Ns;
        if handles.signorm.Value; Z=Z./Se; end
        info = '|F(M11 err.)| - ';
    case 2
%         Ne = permute(Ye(:,1:3,iSelect),[3 1 2]);
%         Ns = permute(Ys(:,1:3,iSelect),[3 1 2]);
%         Se = permute(SigY(:,1:3,iSelect),[3 1 2]);
%         if handles.signorm.Value
%             MSEmode = 2;
%         else
%             MSEmode = 1;
%         end
%         Z = MSEcalc_v2(MSEmode,2,Ne,Ns,Se,1);
%         info = '|F(NCS RMSE)| - ';

        Ne = permute(Ye(:,2,iSelect),[3 1 2]);
        Ns = permute(Ys(:,2,iSelect),[3 1 2]);
        Se = permute(SigY(:,2,iSelect),[3 1 2]);
        Z = Ne-Ns;
        if handles.signorm.Value; Z=Z./Se; end
        info = '|F(M12 err.)| - ';
    case 3
        Ne = permute(Ye(:,6,iSelect),[3 1 2]);
        Ns = permute(Ys(:,6,iSelect),[3 1 2]);
        Se = permute(SigY(:,6,iSelect),[3 1 2]);
        Z = Ne-Ns;
        if handles.signorm.Value; Z=Z./Se; end
        info = '|F(M22 err.)| - ';
    case 4
        Ne = permute(Ye(:,11,iSelect),[3 1 2]);
        Ns = permute(Ys(:,11,iSelect),[3 1 2]);
        Se = permute(SigY(:,11,iSelect),[3 1 2]);
        Z = Ne-Ns;
        if handles.signorm.Value; Z=Z./Se; end
        info = '|F(M33 err.)| - ';
    case 5
        Ne = permute(Ye(:,12,iSelect),[3 1 2]);
        Ns = permute(Ys(:,12,iSelect),[3 1 2]);
        Se = permute(SigY(:,12,iSelect),[3 1 2]);
        Z = Ne-Ns;
        if handles.signorm.Value; Z=Z./Se; end
        info = '|F(34 err.)| - ';
    case 6
        if handles.uTnorm.Value
            Zn = repmat(Io,[NumF 1]);
            Z = uT(iSelect,:)./Zn;
        else
            Z = uT(iSelect,:);
        end
        info = '|F(uT)| - ';
end

% Interpolate if required
if handles.method.Value == 1
    Zi = zeros(numts,NumL);
    for iL = 1:NumL
        Zi(:,iL) = interp1(thks,Z(:,iL),Ts);
    end
    Z = Zi;
    thks = Ts.';
    NumF = numts;
end

%% Apodization if required
tadj = thks.*(NumF/max(thks));
xt2 = tadj-(max(tadj)-min(tadj))/2;
xt2 = xt2./max(xt2);
switch handles.apodmeth.Value
    case 2
        Ht2 = cos(pi.*xt2./2).^2;
        Ht2 = repmat(Ht2,[1 NumL]);
        Z = Z.*Ht2;
        info = [info 'Hann apod. - '];
    case 3
        Gt2 = exp(-(xt2.*3.5).^2./2)./sqrt(2*pi);
        Gt2 = repmat(Gt2,[1 NumL]);
        Z = Z.*Gt2;
        info = [info 'Gauss apod. - '];
    case 4
        a0 = 25/46;
        Ht2 = a0 - (1-a0).*cos(2*pi.*tadj./NumF);
        Ht2 = repmat(Ht2,[1 NumL]);
        Z = Z.*Ht2;
        info = [info 'Hamming apod. - '];
    case 5
        a0 = 7938/18608; a1 = 9240/18608; a2 = 1430/18608;
        Ht2 = a0 - a1.*cos(2.*pi.*tadj./NumF) + a2.*cos(4.*pi.*tadj./NumF);
        Ht2 = repmat(Ht2,[1 NumL]);
        Z = Z.*Ht2;
        info = [info 'Blackman apod. - '];
end

%% Do transform & plot
figure('windowstyle','docked');
switch handles.method.Value
    case 1
        % normal fft
        z = abs(fft(Z,[],1)./numts);
        z = z(1:floor(numts/2),:);
        info = [info 'interpolated fft'];
    case 2
        % nufft
        z = abs(nufft(Z,tadj)./NumF);
        z = z(1:floor(NumF/2),:);
        % recalculate Fts and Lest
        Fts = Ft.*(0:1:(floor(NumF/2)-1))./NumF;
        Fts = Fts.';
        Lest = EstimateLfromFs(pp_sio2_n,Fts,0.01);
        info = [info 'nonuniform fft'];
end

% log scale if checkbox
if handles.logscale.Value
    z = log(z);
    info = [info ' - log scale'];
end

% plot
switch handles.yaxisunit.Value
    case 1
%         imagesc([min(Ls) max(Ls)],[min(Fts) max(Fts)],z);
        [WVLSN,FTS] = meshgrid(Ls,Fts);
        h = pcolor(WVLSN,FTS,z);
        h.EdgeAlpha=0;
        ylabel('Frequency [cycles/nm]');
    case 2
        [WVLSN,WVLSE] = meshgrid(Ls,Lest);
        h = pcolor(WVLSN,WVLSE,z);
        h.EdgeAlpha=0;
        ylabel('Estimated Wavelength [nm]');
end
title(info);
xlabel('Wavelength [nm]');
colormap('bone');
disp('Done.');

% --- Executes on selection change in apodmeth.
function apodmeth_Callback(hObject, eventdata, handles)
% hObject    handle to apodmeth (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns apodmeth contents as cell array
%        contents{get(hObject,'Value')} returns selected item from apodmeth


% --- Executes during object creation, after setting all properties.
function apodmeth_CreateFcn(hObject, eventdata, handles)
% hObject    handle to apodmeth (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
