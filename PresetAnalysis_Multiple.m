% Preset Analysis for MFG
% Adjustment made to handle multiple fits in series
tic;

% Get all Tool directories
F_multi = uigetdir();
if ~F_multi
    return;
end
allfiles = dir(F_multi);
dirFlags = [allfiles.isdir];
T_folders = allfiles(dirFlags);
T_folders = {T_folders.name};
T_folders = T_folders(3:end);
T_folders = natsort(T_folders);
if ismember('SE',T_folders)
    nTools = 1;
else
    nTools = length(T_folders);
end

for i_T = 1:nTools % main loop
    
    % Pick a tool folder and load
    if nTools ~= 1
        PathF = [F_multi '\' T_folders{i_T} '\SE\']
    else
        PathF = [F_multi '\SE\']
    end
    Files = dir(fullfile(PathF,'*.dat'));
    Files = {Files.name};
    Files = natsort(Files);
    LoadData_DAT_v2;
    ArrangeData;
    guidata(hObject, handles);

    % Estimate thicknesses and fit for them
    PopulateThks;
    SerialFit;
    if op.iterations == -1; break; end

    % Change up parameters
    handles.CtrlData.m11s = true;
    tabdata = get(handles.uitable2,'data');
    F = {' ',' ',true,true,true,true,false,true,false,false,false,false,false,...
         false,false,false,true,true}';
    tabdata(:,1) = F; 
    tabdata(3,3:9) = {'None',true,4,tabdata{3,6},0,tabdata{3,6},0};
    tabdata(3,16:17) = {0.01,0.01};
    tabdata(4,3:7) = {'None',false,1,0.065,[]};
    tabdata(4,16:17) = {0.001,0.001};
    set(handles.uitable2,'data',tabdata);
    guidata(hObject, handles);

    % Subselect data
    SubSelect(handles.uitable1,'2');
    hGuiFig = findobj('Tag','subselectfig','Type','figure');
    if ~isempty(hGuiFig)
        % get the handles
        subhands = guidata(hGuiFig);
        % call the pushbutton1 callback using the name of the GUI
        SubSelect('pushbutton1_Callback',subhands.pushbutton1,[],subhands);
    end

    % Parallel Fit Vis
    ParallelFit;
    if op.iterations == -1; break; end

    % Copy results over
    GUI_WedgeDataInspector('CopySim_Callback',handles.CopySim,[],handles);

    % Change up parameters
    tabdata = get(handles.uitable2,'data');
    tabdata(1:2,6) = {1010;1650};
    tabdata(3,3) = {'Polynom.'};
    tabdata(3,7) = {0};
    F = {' ',' ',true,false,true,false,true,false,false,false,false,false,false,...
         false,false,false,false,false}';
    tabdata(:,1) = F;
    set(handles.uitable2,'data',tabdata);
    guidata(hObject, handles);

    % Parallel Fit IR
    ParallelFit;
    if op.iterations == -1; break; end

    % Copy results over
    GUI_WedgeDataInspector('CopySim_Callback',handles.CopySim,[],handles);

    % Reselect all files
    data = get(handles.uitable1,'data');
    data(:,1) = repmat({true},[size(data,1) 1]);
    set(handles.uitable1,'data',data);
    guidata(hObject, handles);

    % Change up parameters
    tabdata = get(handles.uitable2,'data');
    tabdata(1:2,6) = {210;1650};
    F = {' ',' ',false,false,false,false,false,true,false,false,false,false,false,...
         false,false,false,false,false}';
    tabdata(:,1) = F;
    set(handles.uitable2,'data',tabdata);
    guidata(hObject, handles);

    % serial fit thickness for all experiments
    SerialFit;
    if op.iterations == -1; break; end

    % create figures
    close(handles.figh);
    GUI_WedgeDataInspector('plotFitParams_Callback',handles.plotFitParams,[],handles);
    GUI_WedgeDataInspector('plotwvldeps_Callback',handles.plotwvldeps,[],handles);
    pause(1);

    % spectrum analysis
    ResidualAnalysis;
    
    % Reset for next run
    ResetAll;
    
    % Close all the figures
    if nTools ~= 1
        set(GUI_WedgeDataInspector, 'HandleVisibility', 'off');
        close all;
        set(GUI_WedgeDataInspector, 'HandleVisibility', 'on');
        close(1);
        clearvars -except handles i_T nTools F_multi T_folders hObject
    end
end

toc