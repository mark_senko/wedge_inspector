% Jon's Preset Analysis - Wavlength by Wavelength fitting

% Setup Wavelength vectors
lmin = 195;
lmax = 230;
lminv = lmin:6:(lmax - 8);
lmaxv = (lmin + 8):6:lmax;

% Setup fitparms
fitparms = cell(39,length(lminv)+1);

% Check Options (can comment out if not needed)
% for iC = 1:6
%     switch iC
%         case 1 % Check that we're only fitting W0
%             A = handles.CtrlData.only_w0; B = true;
%         case 2 % Check that N&K is using Critical Point model
%             A = handles.ModParms.NKselect; B = 2;
%         case 3 % Check that centered Wcal is on
%             A = handles.CtrlData.centeredWcal; B = true;
%         case 4 % Check that the new lens model is on
%             A = handles.CtrlData.NewLens; B = true;
%         case 5 % Check that the compensator corrections are on
%             A = handles.CtrlData.CompCal; B = true;
%         case 6 % Check that Mueller Matrix mode is on
%             A = handles.CtrlData.mode; B = 3;
%     end
%     if A ~= B
%         msgbox(['Common Option #' num2str(iC) ' Failed'],'Error','error');
%         return;
%     end
% end

% Change N&K Parameters (new)
nNK = 0;
nameNK=[];
Nadj_Si = true;
Kadj_Si = true;
Nadj_SiO2 = true;

if Nadj_Si
    handles.ModParms.SiDat{27,1} = true; % fit Silicon N
    nNK = nNK + 1;
    nameNK = [nameNK;{'N adj.'}];
end

if Kadj_Si
    handles.ModParms.SiDat{28,1} = true; % fit Silicon K
    nNK = nNK + 1;
    nameNK = [nameNK;{'K adj.'}];
end

if Nadj_SiO2
    handles.ModParms.SiO2Dat{9,1} = true; % fit SiO2 N
    nNK = nNK + 1;
    nameNK = [nameNK;{'N SiO2 adj.'}];
    
end

% Wavelength Loop
for iL = 1:length(lminv)

    % Change Model Table Parameters
    % Current version includes new lens model, comp. corrections, and n&k
    tabdata = get(handles.uitable2,'data');
    F = {' ',' ',false,false,false,...
         false,false,false,false,...
         false,false,...
         false,false,false,false,false,...
         false,false,...
         false,false,false,false,false,false,false,false,...
         false,false,false,false,false,false,false,false}';

    tabdata(:,1) = F(1:length(tabdata));
    tabdata{1,6} = lminv(iL);
    tabdata{2,6} = lmaxv(iL);
    set(handles.uitable2,'data',tabdata);
    guidata(hObject, handles);
    
    %setup fitparms (edited to include N&K adjustments)
    if iL == 1
        fitparms(1:length(tabdata)+nNK+3,1) = [tabdata(1:5,2); {'BW - IR'}; tabdata(6:end,2);...
                         nameNK;{'RMSE'};{'NCS RMSE'}];
    end
    
    %do parallel fit
    ParallelFit;
    if op.iterations == -1; return; end

    % Copy results over
    GUI_WedgeDataInspector('CopySim_Callback',handles.CopySim,[],handles);

% % Comment out next section if doing wedge-cal
% Beginning of lens-cal
    % Change up parameters
%     tabdata = get(handles.uitable2,'data');
%     F = {' ',' ',false,false,false,...
%          false,false,false,false,...
%          false,false,...
%          false,false,false,false,false,...
%          true,true,...
%          true,true,true,true,true,true,true,true,...
%          true,true,true,true,true,true,true,true}';
%     tabdata(:,1) = F; 
%     tabdata{1,6} = lminv(iL);
%     tabdata{2,6} = lmaxv(iL);
%     set(handles.uitable2,'data',tabdata);
%     guidata(hObject, handles);
% 
%     %setup fitparms
%     if iL == 1
%         fitparms(:,1) = [tabdata(1:5,2); {'BW - IR'}; tabdata(6:end,2);{'RMSE'};{'NCS RMSE'}];
%     end
%    
%     % Do parallel fit
%     ParallelFit;
%     if op.iterations == -1; return; end
% 
%     % Copy results over
%     GUI_WedgeDataInspector('CopySim_Callback',handles.CopySim,[],handles);
% End of lens-cal

    % Calculate Residuals
Yexp = handles.results.Ye;
Ysim = handles.results.Ys;
Ls = handles.results.Ls;
%     Yexp = handles.Ye;
%     Ysim = handles.Ys;
%     Ls = handles.Ls;
    NumL = length(Ls);
    NumF = size(Yexp,3);

    RMSE = MSEcalc(1,Yexp-Ysim,[],0);

    % requires handles.CtrlData.mode == 3
    if handles.CtrlData.mode ~= 3
        error('This analysis requires CtrlData.mode = 3 (MM)');
        return
    end

    Nexp=[(Ye(:,2,:)+Ye(:,5,:))./2, (Ye(:,11,:)+Ye(:,16,:))./2, (Ye(:,12,:)-Ye(:,15,:))./2];
    Nsim=[(Ys(:,2,:)+Ys(:,5,:))./2, (Ys(:,11,:)+Ys(:,16,:))./2, (Ys(:,12,:)-Ys(:,15,:))./2];
    RMSE_NCS = (sum(sum(sum((Nexp-Nsim).^2,1),2),3)./(NumL*3*NumF)).^0.5;
%   RMSE_NCS = MSEcalc(1,Nexp-Nsim,[],0);
    
%   Save parameters
    tabdata = get(handles.uitable2,'data');
%     fitparms(:,1 + iL) = [tabdata(:,6);{RMSE};{RMSE_NCS}];
    NKadj = [];
    if Nadj_Si
        NKadj = [NKadj; ModParms.SiDat(27,3)];
    end
    if Kadj_Si
        NKadj = [NKadj; ModParms.SiDat(28,3)];
    end
    if Nadj_SiO2
        NKadj = [NKadj;ModParms.SiO2Dat(9,3)];
    end
    fitparms(1:length(tabdata)+nNK+3,1 + iL) = [tabdata(1:5,6); tabdata(5,7); tabdata(6:end,6);...
                          NKadj;{RMSE};{RMSE_NCS}]; %(edited for N&K)
end

%Save to Excel
Foldr = handles.PathF;
xlswrite([Foldr 'Results.xlsx'],fitparms);
