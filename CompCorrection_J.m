function [Ye] = CompCorrection_J(Ye,Comp,iW)
% Modify data to reflect change in compensator calibration.

% Version 1: uses 6 fit parameters and 4 compensator calibration values
% from CompleteEASE. Parameters are as follows:
%   1) dGam1: delta in retardance of source compensator
%   2) dPsi1: delta in azimuth of source compensator fast axis
%   3) dPhi1: delta in azimuth of source polarizer
%   4) dGam2: delta in retardance of receiver compensator
%   5) dPsi2: delta in azimuth of receiver compensator fast axis
%   6) dPhi2: delta in azimuth of receiver polarizer

% Define input parameters
if isfield(Comp,'GamSn')
    y1 = Comp.GamSn(iW);
    y2 = Comp.GamRn(iW);
    A  = Comp.An(iW);
    P  = Comp.Pn(iW);
else
    y1 = Comp.GamS(iW);
    y2 = Comp.GamR(iW);
    A  = Comp.A(iW);
    P  = Comp.P(iW);
end
dy1   = Comp.CCp.s(1,iW).';
dPsi1 = Comp.CCp.s(2,iW).';
dPhi1 = Comp.CCp.s(3,iW).';
dy2   = Comp.CCp.s(4,iW).';
dPsi2 = Comp.CCp.s(5,iW).';
dPhi2 = Comp.CCp.s(6,iW).';

% Define new variables
a1  = cosd(y1./2).^2.*cosd(2.*P);
b1  = cosd(y1./2).^2.*sind(2.*P);
a2  = cosd(y2./2).^2.*cosd(2.*A);
b2  = cosd(y2./2).^2.*sind(2.*A);
da1 = -dy1.*sind(y1)./(1+cosd(y1));
dc1 = dy1.*sind(y1)./(1-cosd(y1));
dt1 = dy1./tand(y1);
da2 = -dy2.*sind(y2)./(1+cosd(y2));
dc2 = dy2.*sind(y2)./(1-cosd(y2));
dt2 = dy2./tand(y2);

% Assign MM
% M11 = Ye(:,1);
% M12 = Ye(:,2);
% M13 = Ye(:,3);
% M14 = Ye(:,4);
% M21 = Ye(:,5);
% M22 = Ye(:,6);
% M23 = Ye(:,7);
% M24 = Ye(:,8);
% M31 = Ye(:,9);
% M32 = Ye(:,10);
% M33 = Ye(:,11);
% M34 = Ye(:,12);
% M41 = Ye(:,13);
% M42 = Ye(:,14);
% M43 = Ye(:,15);
% M44 = Ye(:,16);

% Perform calculations
Ye(:,16) = Ye(:,16).*(1-(dt1+dt2));             %M44

Ye(:,8)  = Ye(:,8) + Ye(:,12).*(dPsi2+dPhi2);   %M24
Ye(:,12) = Ye(:,12).*(1 -(dt1+dc2));            %M34
Ye(:,14) = Ye(:,14) + Ye(:,15).*(dPhi1+dPsi1);  %M42
Ye(:,15) = Ye(:,15).*(1-(dt2+dc1));             %M43

Ye(:,4)  = Ye(:,4)-(b2.*Ye(:,12)).*(da2-dc2)-2*(a2.*Ye(:,12)).*dPhi2; %M14
Ye(:,13) = Ye(:,13)-(b1.*Ye(:,15)).*(da1-dc1)-2*(a1.*Ye(:,15)).*dPhi1; %M41

dM22     = -Ye(:,6).*(dc1+dc2); 
Ye(:,6)  = Ye(:,6) + dM22;                      %M22
dM33     = -Ye(:,11).*(dc1+dc2);
Ye(:,11) = Ye(:,11) + dM33;                     %M33
Ye(:,7)  = Ye(:,7)-Ye(:,6).*(dPhi1+dPsi1)+Ye(:,11).*(dPhi2+dPsi2);          %M23
Ye(:,10) = Ye(:,10)-Ye(:,6).*(dPhi2+dPsi2)+Ye(:,11).*(dPhi1+dPsi1);         %M32

dM12     = -Ye(:,2).*dc1-(a2.*Ye(:,6)).*(da2-dc2)+2*(b2.*Ye(:,6)).*dPhi2;
Ye(:,2)  = Ye(:,2) + dM12;                      %M12
dM21     = -Ye(:,5).*dc2-(a1.*Ye(:,6)).*(da1-dc1)+2*(b1.*Ye(:,6)).*dPhi1;
Ye(:,5)  = Ye(:,5) + dM21;                  	%M21
Ye(:,3)  = Ye(:,3)-(b2.*Ye(:,11)).*(da2-dc2)-Ye(:,2).*(dPhi1+dPsi1)-2*(a2.*Ye(:,11)).*dPhi2; %M13
Ye(:,9)  = Ye(:,9)-(b1.*Ye(:,11)).*(da1-dc1)-Ye(:,5).*(dPhi2+dPsi2)-2*(a1.*Ye(:,11)).*dPhi1; %M31

Ye(:,1)  = Ye(:,1)-(a1.*Ye(:,2).*da1) - (a2.*Ye(:,5).*da2) + ...
           b1.*Ye(:,2).*dPhi1 + b2.*Ye(:,5).*dPhi2 - ...
           (a1.*a2.*Ye(:,6) + b1.*b2.*Ye(:,11)).*(da1+da2) + ...
           2*(b1.*a2.*Ye(:,6)-a1.*b2.*Ye(:,11)).*dPhi1 + ...
           2*(b2.*a1.*Ye(:,6)-a2.*b1.*Ye(:,11)).*dPhi2 - ...
           (a1.*dM12 + a2.*dM21 + a1.*a2.*dM22 + b1.*b2.*dM33);  %M11

end

