% Load Data Script - Segmented from Original
% V2 - Modified to read more of the rc2_cal.cnf file

[L, ~, Y, SigY, PathF, Files] = openSEspectraALL(PathF,Files(:),handles.axes1,handles.axes2);

% read cal files
file_rc2_cal = [PathF 'rc2-cal.cnf'];
RC2Cal = ReadRC2Cal(file_rc2_cal);
Wcal.C_VIS = RC2Cal.WvlCal.VIS;
Wcal.Cc_VIS = RC2Cal.WvlCal.cVIS;
Wcal.C_IR = RC2Cal.WvlCal.NIR;
Wcal.Cc_IR = RC2Cal.WvlCal.cNIR;
Wcal.nVis = Wcal.C_VIS + Wcal.Cc_VIS;
Wcal.nIR = Wcal.C_IR + Wcal.Cc_IR;
% if Wcal.nVis(end) == 0; Wcal.nVis(end) = 60; end % correct broken files???
[~,~,handles.CEcal.A,handles.CEcal.P,handles.CEcal.GamR,handles.CEcal.GamS,~,~,~] = ExtractCal2_AGB2021(RC2Cal,L);
File_CEhard = [PathF 'CompleteEASEhard.cnf'];
[AOIcal,DO,WD]=Read_CEhard_v2(File_CEhard);
handles.CEcal.AOIcal = AOIcal;
handles.CEcal.DO = DO;
handles.CEcal.WD = WD;

% Determine Io, generate M11
upwait_v2(0.1,handles.axes1,'Determining I0...',0.1,handles.axes2,'Determining I0...');
Lz(1,1,:) = L;
if mean(Y(:,end)) ~= 0
    uT = squeeze(Y(:,end,:));
    upwait_v2(0.2,handles.axes1,'Determining I0...',0.2,handles.axes2,'Determining I0...');
    maxdata = importdata('M11_max.xlsx');
    upwait_v2(0.8,handles.axes1,'Determining I0...',0.8,handles.axes2,'Determining I0...');
    m11max = spline(maxdata(:,1),maxdata(:,2),L);
    Io = max(uT,[],2)./m11max;
    M11 = uT./repmat(Io,[1 size(uT,2)]);
else
    M11 = [];
end
handles.M11 = M11;

% Remove JAW lens behavior
upwait_v2(0,handles.axes1,'Removing JAW Lens Correction',0,handles.axes2,'Removing JAW Lens Correction');
Lz(1,1,:) = L;
NumL = length(L);
Deli(1,1,:) = 250./Lz.*(DO(1)+DO(2).*(500/Lz).^2+DO(3).*(500/Lz).^4+DO(4).*(Lz./500).^2);
Delo1(1,1,:) = (-500)./Lz.*(WD(1,1)+WD(1,2).*(500./Lz).^2+WD(1,3).*(500./Lz).^4);
Delo2(1,1,:) = 500./Lz.*(WD(2,1)+WD(2,2).*(500./Lz).^2+WD(2,3).*(500./Lz).^4);
ons = ones(1,1,NumL);
zos = zeros(1,1,NumL);
Mo1t = [ons,zos,zos,zos;zos,cosd(Delo1),zos,sind(Delo1);zos,zos,ons,zos;zos,-sind(Delo1),zos,cosd(Delo1)];
Mo2t = [ons,zos,zos,zos;zos,cosd(Delo2),zos,sind(Delo2);zos,zos,ons,zos;zos,-sind(Delo2),zos,cosd(Delo2)];
Mi1t = [ons,zos,zos,zos;zos,ons,zos,zos;zos,zos,cosd(Deli),sind(Deli);zos,zos,-sind(Deli),cosd(Deli)];

NumF = length(Files);
Mo1t = complex(Mo1t);
Mo2t = complex(Mo2t);
Mi1t = complex(Mi1t);
Mi2t = Mi1t;
for nF=1:NumF
    MMtmp = Y(:,1:16,nF);
    MMtmp = permute(MMtmp,[3 2 1]);
    MMtmp = reshape(MMtmp,[4 4 NumL]);
    MMtmp = permute(MMtmp,[2 1 3]);
    MMtmp = complex(MMtmp);
    MMtmp = MM3D_v2(MMtmp,Mi1t);
    MMtmp = MM3D_v2(MMtmp,Mo1t);
    MMtmp = MM3D_v2(Mi2t,MMtmp);
    MMtmp = MM3D_v2(Mo2t,MMtmp);
    MMtmp = permute(MMtmp,[2 1 3]);
    MMtmp = reshape(MMtmp,[1 16 NumL]);
    MMtmp = permute(MMtmp,[3 2 1]);
    Y(:,1:16,nF) = MMtmp;
    upwait_v2(nF/NumF,handles.axes1,'Removing JAW Lens Correction',nF/NumF,handles.axes2,'Removing JAW Lens Correction');
end