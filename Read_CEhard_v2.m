function [AOI,DO,WnDsp] = Read_CEhard_v2(File_CEhard)
% Reads'CompleteEASEhard.cnf' and grabs AOI, delta offset, window
% dispersion and window retardance parameters
% AGB 2020

% Open the file
fid  =  fopen(File_CEhard);

% Setup variables
WnDsp = zeros(2,3);
DO = zeros(1,4);

% While we have reached the end of the file, keep looking for data
while ~feof(fid) 
    % get a line of text
    txt = fgetl(fid);
    
    % see if it contains an =
    if contains(txt,'=')
        % if so, split
        txt2 = strsplit(txt,'=');
        % remove white space to prevent errors
        txt2{1} = txt2{1}(~isspace(txt2{1}));
        % switch on the result and fill variables
        switch txt2{1}
            case 'NominalAngle'
                AOI = str2double(txt2{2});
            case 'WinRet1'
                WnDsp(1,1) = str2double(txt2{2});
            case 'WinRet2'
                WnDsp(2,1) = str2double(txt2{2});
            case 'DeltaOffset1'
                DO(1) = str2double(txt2{2});
            case 'DeltaOffset2'
                DO(2) = str2double(txt2{2});
            case 'DeltaOffset3'
                DO(3) = str2double(txt2{2});
            case 'DeltaOffset4'
                DO(4) = str2double(txt2{2});
            case 'WinDisp1'
                WnDsp(1,2) = str2double(txt2{2});
            case 'WinDisp2'
                WnDsp(1,3) = str2double(txt2{2});
            case 'WinDisp1-2'
                WnDsp(2,2) = str2double(txt2{2});
            case 'WinDisp2-2'
                WnDsp(2,3) = str2double(txt2{2});
            otherwise
                continue;
        end
    end
end

% Close the file
fclose(fid);
