% Populate Thicknesses

% Experimental revision based on diff
% This version uses FFT to estimate thickness
% This version interpolates prior to the FFT

% Plot estimates?
plt = [1 1 1 1];

% Grab data
Data = get(handles.uitable1,'data');
NumF = size(Data,1);
L = handles.L;
% iL = (L <= 1700 & L >= 190);
% L = L(iL);
eV = 1239./L;
% NumE = length(eV);
% deV = (max(eV)-min(eV))/NumE;
% deV = min(abs(eV-circshift(eV,1)));
deV = 0.003;
eV2 = linspace(min(eV),max(eV),(max(eV)-min(eV))/deV);
NumE = length(eV2);
FEs = (1/deV).*(0:1:(floor(NumE/2))-1)./NumE;
% eV2 = linspace(min(eV),max(eV),NumE);
% eV2 = eV*NumE/max(eV);
S = handles.Y(:,12,:);
% S = handles.M11(iL,:);
S = permute(S,[1 3 2]);
S = S(:,:,1);
AOI = handles.CEcal.AOIcal;

% Get SiO2 N&K
if isempty(handles.PathMat)
    PathMat = [pwd '\Mats'];
    if ~exist(PathMat,'dir')
        uiwait(msgbox({'No MATERIALS folder found.' 'Please select MATERIALS folder.'},'Folder Not Found','error'));
        PathMat = uigetdir(pwd,'Please Select the Materials Folder');
        handles.PathMat = PathMat;
    else
        handles.PathMat = PathMat;
    end
end
NK_sio2 = GetNKs([handles.PathMat '\SiO2_jaw.txt'],L);
% NK_sio2 = ones(size(NK_sio2)).*1.5;
mn_sio2 = mean(NK_sio2);
AOIt = asind(sind(AOI)./NK_sio2);
% AOIt = mean(AOIt);
NK_si = GetNKs([handles.PathMat '\Si_jaw.txt'],L);
mn_si = mean(real(NK_si))+1i*mean(imag(NK_si));
AOIt3 = asind(sind(AOI)./NK_si);
NK_intr = GetNKs([handles.PathMat '\intr_jaw.txt'],L);
mn_intr = mean(NK_si./2+NK_sio2./2);
AOIt2 = asind(sind(AOI)./NK_intr);
% AOIt2 = mean(AOIt2);
r01s = (cosd(AOI)-NK_sio2.*cosd(AOIt))./(cosd(AOI)+NK_sio2.*cosd(AOIt));
r12s = (NK_sio2.*cosd(AOIt)-NK_si.*cosd(AOIt2))./(NK_sio2.*cosd(AOIt)+NK_si.*cosd(AOIt2));
% exp2 = exp(-2i*2*pi*mn_sio2*cosd(AOIt)*0);
% bigR = (r01s/exp2+r12s)/(1+r01s*r12s*exp2);
bigR = (1-r01s.^2).*r12s;
bigR = mean(bigR);
bigRa = angle(bigR)-pi;
disp(bigRa);
AOIt2 = mean(AOIt2);
AOIt3 = mean(AOIt3);
AOIt = mean(AOIt);

% For each file, estimate thickness
thks = zeros(NumF,1);
phas = thks;

for i = 1:1:NumF
    % interpolate first
    myS = interp1(eV,S(:,i),eV2,'spline');
%     myS = diff(myS); eV2=eV2(1:end-1); NumE=length(eV2);
%     FEs = (1/deV).*(0:1:(floor(NumE/2))-1)./NumE;
    
    if plt(1)
        figure('windowstyle','docked');
        plot(eV,S(:,i));
        hold on
        plot(eV2,myS);
%         plot(eV(1:end-1),diff(S(:,i)));
        xlabel('Frequency [eV]');
        ylabel('S');
        grid on;
    end
    
    % Take the non-uniform Fourier transform 
    T = fft(myS);
    T = T(1:floor(NumE/2));
    Tabs = abs(T./NumE);
    Treal = real(T./NumE);
    Timag = imag(T./NumE);
    
    % plot the nufft
    if plt(2)
        figure('windowstyle','docked');
        grid on; hold on;
        plot(FEs,Tabs);
        xlabel('Frequency [cycles/eV]');
        ylabel('|F(S)|');
    end
    
    % Find the highest point
    iF = FEs>0.5;
    FEsf = FEs(iF);
    Tabsf = Tabs(iF);
    [Tmax,ind] = max(Tabsf);
    Fmax = FEsf(ind);
    Tp = Tabsf(ind-1:ind+1);
    Fp = FEsf(ind-1:ind+1);
    
    % Fit parabola to data points
    P0 = [-0.01 Fmax Tmax];
%     dfnc2 = @(P) sum(((P(1).*(Fp-P(2)).^2+P(3)) -Tp).^2);
%     P1 = fminsearch(dfnc2,P0);

    % Use spline to find maximum
    P1 = P0;
    Fq = linspace(min(Fp),max(Fp),99);
    Tq = interp1(Fp,Tp,Fq,'spline');
    [P1(1),P1(3)] = max(Tq);
    P1(2) = Fq(P1(3));

    % Force Fmax as answer
%     P1(2) = Fmax;
    
    % Estimate thickness (Pedro)
%     P = 1/(1239*P1(2)); % Period in 1/nm
%     C = 2*mn_sio2/cosd(AOIt)-2*tand(AOIt)*sind(AOI);
%     thks(i) = 1/(C*P);    

    % Estimate thickness (Alex with phase correction)
    TmxI = interp1(FEs,Timag,P1(2),'spline');
    TmxR = interp1(FEs,Treal,P1(2),'spline');
    phas(i) = abs(atan2(TmxI,TmxR)); % phase angle of the peak
%     E1 = eV(end)+phas(i)/(2*pi)/P1(2); % Energy of first crossing
%     thks(i) = 620/(mn_sio2*cosd(AOIt))*(E1+E1^2*P1(2));
%     thks(i) = 620/(mn_sio2*cosd(AOIt)/(P1(2)));
    thks(i) = (620*P1(2)-0)/(mn_sio2*cosd(AOIt));
    
    % plot fit results
    if plt(3)
%         Fp2 = linspace(min(Fp),max(Fp),21);
%         dfnc3 = @(P) P(1).*(Fp2-P(2)).^2+P(3);
%         plot(Fp2,dfnc3(P0),'-r');
%         plot(Fp2,dfnc3(P1),'-k');
        plot(Fq,Tq,'-r');
        plot(P1(2),P1(1),'ok');
        legend('|F(S)|','Initial Guess',['Fit - ' num2str(thks(i))]);
        keyboard;
    end    
    
    % disp thickness
    fprintf('Estimated thickness for point %d is %f.\n',i,thks(i));
end
Data(:,3) = num2cell(thks);
set(handles.uitable1,'data',Data);

if plt(4)
    figure('windowstyle','docked');
    plot(thks);
%     hold on
%     plot(phas);
    xlabel('Point No.');ylabel('Thickness Estimate SiO2 [nm]');
    grid on;
    keyboard;
end