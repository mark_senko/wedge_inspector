function [u,t]=FSifft(U,df)
% use FSifft(...)*sqrt(n) for energy conservation
  n=size(U);
  u=fftshift(ifft(ifftshift(U,1),n(1),1),1);
  if nargout>1,  
    if nargin<2, df=1; end
    [m,n]=size(u); %ASSUME m is the length
    F=m*df;
    dt=1/F;
    %t=[0:m-1]'*dt;
    t=[-ceil((m-1)/2):floor((m-1)/2)]'*dt;
  end
  
    