function varargout = NKedit(varargin)
% NKEDIT MATLAB code for NKedit.fig
%      NKEDIT, by itself, creates a new NKEDIT or raises the existing
%      singleton*.
%
%      H = NKEDIT returns the handle to a new NKEDIT or the handle to
%      the existing singleton*.
%
%      NKEDIT('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in NKEDIT.M with the given input arguments.
%
%      NKEDIT('Property','Value',...) creates a new NKEDIT or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before NKedit_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to NKedit_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help NKedit

% Last Modified by GUIDE v2.5 22-Dec-2020 15:12:19

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @NKedit_OpeningFcn, ...
                   'gui_OutputFcn',  @NKedit_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before NKedit is made visible.
function NKedit_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to NKedit (see VARARGIN)

% Choose default command line output for NKedit
handles.output = hObject;
handles.ModParms = varargin{1};
% populate uitable1 (Si model settings)
if isempty(handles.ModParms.SiDat)
    Parms1 = {'Eps Inf.','Eps Imag.'...
             'E1','Amp1','Gamma1','Beta1',...
             'E2','Amp2','Gamma2','Beta2',...
             'E3','Amp3','Gamma3','Beta3',...
             'E4','Amp4','Gamma4','Beta4',...
             'E5','Amp5','Gamma5','Beta5',...
             '1D E1','1D E2','1D Gamma','1D Amp',...
             'N fit','K fit'}';
    Select1 = {false,false,...
              false,false,false,false,...
              false,false,false,false,...
              false,false,false,false,...
              false,false,false,false,...
              false,false,false,false,...
              false,false,false,false,...
              false,false}';
      Val1 = {1.314,0.954,...
             3.350383,1.897579,0.037345,-0.276280,...
             3.763237,7.048575,0.167779,-0.150490,...
             4.276734,2.448933,0.046421, 0.067861,...
             5.211842,0.360344,0.059544,-0.185110,...
             4.392339,5.509833,0.973708, 0.411299,...
             3.556000,0.000000,0.111589, 4.334000,...
             0,0}'; % From Tool #119
    LwL1 = {-10,-10,-10,-10, 0,-2,-10,-10, 0,-2,-10,-10, 0,-2,-10,-10, 0,-2,-10,-10, 0,-2, 0, 0, 0, 0,-0.1,-0.1}';
    UpL1 = { 10, 10, 10, 10,10, 2, 10, 10,10, 2, 10, 10,10, 2, 10, 10,10, 2, 10, 10,10, 2,10,10,10,20,0.1,0.1}';
    handles.ModParms.SiDat = [Select1, Parms1, Val1, LwL1, UpL1];
else
    SiDat = handles.ModParms.SiDat;
end
set(handles.uitable1,'data',SiDat);

% populate uitable2 (SiO2 model settings)
if isempty(handles.ModParms.SiO2Dat)
    Parms = {'CaucA','CaucB','CaucC',...
             'EMA1 Per.','EMA Void',...
             'Air Cauc. A','Air Cauc. B','Air Cauc. C',...
             'Nfit_SiO2'}';
    Select = {false,false,false,...
              false,false,false,false,false,...
              false}';
    Val = {1.451,3174.1,0,...
           0.5,0,1,0,0,...
           0}';
    SiO2Dat = [Select, Parms, Val];
else
    SiO2Dat = handles.ModParms.SiO2Dat;
end
set(handles.uitable2,'data',SiO2Dat);

% populate .txt files
if ~isempty(handles.ModParms.SiF)
    set(handles.Sifile,'String',handles.ModParms.SiF);
end
if ~isempty(handles.ModParms.EMAF)
    set(handles.EMAfile,'String',handles.ModParms.EMAF);
end
if ~isempty(handles.ModParms.SiO2xyF)
    set(handles.SiO2xyfile,'String',handles.ModParms.SiO2xyF);
end
if ~isempty(handles.ModParms.SiO2zF)
    set(handles.SiO2zfile,'String',handles.ModParms.SiO2zF);
end
if ~isempty(handles.ModParms.mthd)
    switch handles.ModParms.mthd
        case 'linear'
            handles.interpmethod.Value = 1;
        case 'pchip'
            handles.interpmethod.Value = 2;
        case 'spline'
            handles.interpmethod.Value = 3;
    end
end

% fix nk select
if ~isempty(handles.ModParms.NKselect)
    set(handles.nkselect,'Value',handles.ModParms.NKselect);
    switch handles.ModParms.NKselect
        case 1
            set(handles.status1,'BackgroundColor',[0,1,0]);
            set(handles.status2,'BackgroundColor',[0.64,0.08,0.18]);
        case 2
            set(handles.status2,'BackgroundColor',[0,1,0]);
            set(handles.status1,'BackgroundColor',[0.64,0.08,0.18]);
        otherwise
            disp('NOPE');
    end
end

handles.PathMat = {};

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes NKedit wait for user response (see UIRESUME)
uiwait(handles.figure1);

% --- Outputs from this function are returned to the command line.
function varargout = NKedit_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
% varargout{1} = handles.output;
varargout{1} = handles.ModParms;
guidata(hObject,handles);
figure1_CloseRequestFcn(hObject, eventdata, handles);

% --- Executes on selection change in nkselect.
function nkselect_Callback(hObject, eventdata, handles)
% hObject    handle to nkselect (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns nkselect contents as cell array
%        contents{get(hObject,'Value')} returns selected item from nkselect
NKsel = get(hObject,'Value');
switch NKsel
    case 1
        set(handles.status1,'BackgroundColor',[0,1,0]);
        set(handles.status2,'BackgroundColor',[0.64,0.08,0.18]);
    case 2
        set(handles.status2,'BackgroundColor',[0,1,0]);
        set(handles.status1,'BackgroundColor',[0.64,0.08,0.18]);
    otherwise
        disp('NOPE');
end
handles.ModParms.NKselect = NKsel;
guidata(hObject, handles);

% --- Executes during object creation, after setting all properties.
function nkselect_CreateFcn(hObject, eventdata, handles)
% hObject    handle to nkselect (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in plotdisp.
function plotdisp_Callback(hObject, eventdata, handles)
% hObject    handle to plotdisp (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
NKsel = get(handles.nkselect,'Value');
Lo = handles.ModParms.Lo;
if NKsel == 1
    if isempty(handles.PathMat)
        if ~exist('PathMaterials.mat','file')
            uiwait(msgbox({'No MATERIALS folder found.' 'Please select MATERIALS folder.'},'Error','error'));
            PathMat = uigetdir([cd '\'],'Please Select the Materials Folder');
            save PathMaterials PathMat
            handles.PathMat = PathMat;
        else
            load PathMaterials PathMat
            handles.PathMat = PathMat;
        end
    else
        PathMat = handles.PathMat;
    end
    SiF = get(handles.Sifile,'String');
    SiO2XYF = get(handles.SiO2xyfile,'String');
    SiO2ZF = get(handles.SiO2zfile,'String');   
    EMAF = get(handles.EMAfile,'String');
    SiNK = GetNKs([PathMat '\' SiF],Lo);
    SiO2xyNK = GetNKs([PathMat '\' SiO2XYF],Lo);
    SiO2zNK = GetNKs([PathMat '\' SiO2ZF],Lo);
    EMANK = GetNKs([PathMat '\' EMAF],Lo);

    Noffset = handles.uitable2.Data{9,3}; 
    SiO2xyNK = SiO2xyNK + Noffset;
    SiO2zNK = SiO2zNK + Noffset;
    Noffset=handles.uitable1.Data{27,3};
    Koffset = handles.uitable1.Data{28,3};
    SiNK= SiNK + Noffset - 1i*Koffset;
    SiNK = real(SiNK) + 1i*min(imag(SiNK),0);
else
    SiDat = get(handles.uitable1,'data');
    SiO2Dat = get(handles.uitable2,'data');
    [SiNK,EMANK,SiO2xyNK] = Si_SiO2_Aspnes_v2(Lo,SiDat,SiO2Dat);
    Noffset = handles.uitable2.Data{9,3}; 
    SiO2xyNK = SiO2xyNK + Noffset;
    SiO2zNK = SiO2xyNK;
end

figure
subplot(2,1,1)
plot(Lo,real(SiNK),Lo,real(SiO2xyNK),Lo,real(SiO2zNK),Lo,real(EMANK));
xlim([Lo(1) Lo(end)]);
ylim([0 inf]);
ylabel('N');
xlabel('Wavelength (nm)');
legend('Si','SiO2 (XY)','SiO2 (Z)','EMA');
title('Model Dispersion');
grid on
box on
subplot(2,1,2)
plot(Lo,-imag(SiNK),Lo,-imag(SiO2xyNK),Lo,-imag(SiO2zNK),Lo,-imag(EMANK));
xlim([Lo(1) Lo(end)]);
ylim([0 inf]);
ylabel('K');
xlabel('Wavelength (nm)');
legend('Si','SiO2 (XY)','SiO2 (Z)','EMA');
grid on
box on


% --- Executes on button press in submitnk.
function submitnk_Callback(hObject, eventdata, handles)
% hObject    handle to submitnk (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
ModParms = handles.ModParms;
ModParms.SiDat = get(handles.uitable1,'data');
ModParms.SiO2Dat = get(handles.uitable2,'data');
ModParms.SiF = get(handles.Sifile,'String');
ModParms.SiO2xyF = get(handles.SiO2xyfile,'String');
ModParms.SiO2zF = get(handles.SiO2zfile,'String');
ModParms.EMAF = get(handles.EMAfile,'String');
temp = handles.interpmethod.String;
ModParms.mthd = temp{handles.interpmethod.Value};

if ModParms.NKselect == 1
    if isempty(handles.PathMat)
        if ~exist('PathMaterials.mat','file')
            uiwait(msgbox({'No MATERIALS folder found.' 'Please select MATERIALS folder.'},'Folder Not Found','error'));
            PathMat = uigetdir(handles.PathF,'Please Select the Materials Folder');
            save PathMaterials PathMat
            handles.PathMat = PathMat;
        else
            load PathMaterials PathMat
            handles.PathMat = PathMat;
        end
    else
        PathMat = handles.PathMat;
    end
    % PathMat = 'C:\Mats\'
    [pp_Si_n,pp_Si_k] =           GetNKs_v3([PathMat '\' ModParms.SiF],ModParms.mthd);
    [pp_intr_n,pp_intr_k] =       GetNKs_v3([PathMat '\' ModParms.EMAF],ModParms.mthd);
    [pp_sio2_xy_n,pp_sio2_xy_k] = GetNKs_v3([PathMat '\' ModParms.SiO2xyF],ModParms.mthd);
    [pp_sio2_z_n,pp_sio2_z_k] =   GetNKs_v3([PathMat '\' ModParms.SiO2zF],ModParms.mthd);
    ModParms.ppNK = {pp_Si_n,pp_Si_k,pp_intr_n,pp_intr_k,pp_sio2_xy_n,...
                     pp_sio2_xy_k,pp_sio2_z_n,pp_sio2_z_k};
end

handles.ModParms = ModParms;
guidata(hObject, handles);
close(handles.figure1);


function SiO2xyfile_Callback(hObject, eventdata, handles)
% hObject    handle to SiO2xyfile (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of SiO2xyfile as text
%        str2double(get(hObject,'String')) returns contents of SiO2xyfile as a double


% --- Executes during object creation, after setting all properties.
function SiO2xyfile_CreateFcn(hObject, eventdata, handles)
% hObject    handle to SiO2xyfile (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function EMAfile_Callback(hObject, eventdata, handles)
% hObject    handle to EMAfile (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of EMAfile as text
%        str2double(get(hObject,'String')) returns contents of EMAfile as a double


% --- Executes during object creation, after setting all properties.
function EMAfile_CreateFcn(hObject, eventdata, handles)
% hObject    handle to EMAfile (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function Sifile_Callback(hObject, eventdata, handles)
% hObject    handle to Sifile (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of Sifile as text
%        str2double(get(hObject,'String')) returns contents of Sifile as a double


% --- Executes during object creation, after setting all properties.
function Sifile_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Sifile (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in ResetMatPath.
function ResetMatPath_Callback(hObject, eventdata, handles)
% hObject    handle to ResetMatPath (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

PathMat = uigetdir([cd '\'],'Please Select the Materials Folder');
if PathMat ~= 0
    save PathMaterials PathMat
    handles.PathMat = PathMat;
end



% --- Executes on button press in SiSelect.
function SiSelect_Callback(hObject, eventdata, handles)
% hObject    handle to SiSelect (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
MyVal = get(handles.SiSelect,'val');
Data = get(handles.uitable1,'data');
Data(:,1) = {logical(MyVal)};
set(handles.uitable1,'data',Data);


% --- Executes on button press in SiO2Select.
function SiO2Select_Callback(hObject, eventdata, handles)
% hObject    handle to SiO2Select (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
MyVal = get(handles.SiO2Select,'val');
Data = get(handles.uitable2,'data');
Data(:,1) = {logical(MyVal)};
set(handles.uitable2,'data',Data);

function figure1_CloseRequestFcn(hObject, eventdata, handles)
% hObject    handle to figure1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: delete(hObject) closes the figure
% delete(hObject);

if isequal(get(hObject, 'waitstatus'), 'waiting')
    % The GUI is still in UIWAIT, call UIRESUME
    uiresume(hObject);
else
    % The GUI is no longer waiting, just close it
    delete(hObject);
end



function SiO2zfile_Callback(hObject, eventdata, handles)
% hObject    handle to SiO2zfile (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of SiO2zfile as text
%        str2double(get(hObject,'String')) returns contents of SiO2zfile as a double


% --- Executes during object creation, after setting all properties.
function SiO2zfile_CreateFcn(hObject, eventdata, handles)
% hObject    handle to SiO2zfile (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in interpmethod.
function interpmethod_Callback(hObject, eventdata, handles)
% hObject    handle to interpmethod (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns interpmethod contents as cell array
%        contents{get(hObject,'Value')} returns selected item from interpmethod


% --- Executes during object creation, after setting all properties.
function interpmethod_CreateFcn(hObject, eventdata, handles)
% hObject    handle to interpmethod (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
