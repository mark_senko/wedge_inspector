function [Ye] = CompCorrection(Ye,Comp,iW)
% Modify data to reflect change in compensator calibration.

% Version 1: uses 6 fit parameters and 4 compensator calibration values
% from CompleteEASE. Parameters are as follows:
%   1) dGam1: delta in retardance of source compensator
%   2) iGam1: delta in attenuation of source compensator
%   3) dPsi1: delta in azimuth of source compensator fast axis
%   4) dPhi1: delta in azimuth of source polarizer
%   5) dGam2: delta in retardance of receiver compensator
%   6) iGam2: delta in attenuation of receiver compensator
%   7) dPsi2: delta in azimuth of receiver compensator fast axis
%   8) dPhi2: delta in azimuth of receiver polarizer

% Define input parameters
if isfield(Comp,'GamSn')
    y1 = Comp.GamSn(iW);
    y2 = Comp.GamRn(iW);
    A  = Comp.An(iW);
    P  = Comp.Pn(iW);
else
    y1 = Comp.GamS(iW);
    y2 = Comp.GamR(iW);
    A  = Comp.A(iW);
    P  = Comp.P(iW);
end
    PC = P + 90;            % P + C
    AW = A - 90;            % A + W
    
dy1   = Comp.CCp.s(1,iW).';
iy1   = Comp.CCp.s(2,iW).'; % new parameter!
dPsi1 = Comp.CCp.s(3,iW).';
dPhi1 = Comp.CCp.s(4,iW).';
dy2   = Comp.CCp.s(5,iW).';
iy2   = Comp.CCp.s(6,iW).'; % new parameter!
dPsi2 = Comp.CCp.s(7,iW).';
dPhi2 = Comp.CCp.s(8,iW).';
rp11  = Comp.CCp.s(9,iW).'; 
ip11  = Comp.CCp.s(10,iW).'; 
rp12  = Comp.CCp.s(11,iW).'; 
ip12  = Comp.CCp.s(12,iW).'; 
rp21  = Comp.CCp.s(13,iW).'; 
ip21  = Comp.CCp.s(14,iW).'; 
rp22  = Comp.CCp.s(15,iW).'; 
ip22  = Comp.CCp.s(16,iW).'; 

% Define new variables
sg1 = sind(y1);
cg1 = cosd(y1);
tg1 = tand(y1);
shg1 = sind(y1./2);
chg1 = cosd(y1./2);
thg1 = tand(y1./2);
sg2 = sind(y2);
cg2 = cosd(y2);
tg2 = tand(y2);
shg2 = sind(y2./2);
chg2 = cosd(y2./2);
thg2 = tand(y2./2);

sp1 = sind(2.*P);
cp1 = cosd(2.*P);
sp2 = sind(2.*A);
cp2 = cosd(2.*A);

a1  = chg1.^2.*cp1;
b1  = chg1.^2.*sp1;
a2  = chg2.^2.*cp2;
b2  = chg2.^2.*sp2;

da1 = -dy1.*thg1;
dc1 = dy1./thg1;
dt1 = dy1./tg1;
di1 = iy1./sg1;
da2 = -dy2.*thg2;
dc2 = dy2./thg2;
dt2 = dy2./tg2;
di2 = iy2./sg2;

rp1 = sqrt(rp11.^2 + ip11.^2);
rp2 = sqrt(rp12.^2 + ip12.^2);

% Assign MM
% M11 = Ye(:,1);
% M12 = Ye(:,2);
% M13 = Ye(:,3);
% M14 = Ye(:,4);
% M21 = Ye(:,5);
% M22 = Ye(:,6);
% M23 = Ye(:,7);
% M24 = Ye(:,8);
% M31 = Ye(:,9);
% M32 = Ye(:,10);
% M33 = Ye(:,11);
% M34 = Ye(:,12);
% M41 = Ye(:,13);
% M42 = Ye(:,14);
% M43 = Ye(:,15);
% M44 = Ye(:,16);

% Perform calculations
dM44 = - Ye(:,16).*(dt1+dt2) ...                                      %dM44
       - di1.*cp1.*Ye(:,15) + di2.*cp2.*Ye(:,12) ...
       + 0 ...
       + sp1.*ip21.*Ye(:,15) + sp2.*ip22.*Ye(:,12);

dM24 = - di1.*sp1.*Ye(:,6) ...                                        %dM24
       + (2.*dPsi2).*Ye(:,12)...
       - ip21.*(cp1.*Ye(:,6) - Ye(:,5));
       
dM34 = -Ye(:,12).*(dt1+dc2) - di1.*cp1.*Ye(:,11) ...                  %dM34
       + 0 ...
       - ip21.*sp1.*Ye(:,11);

dM43 = -Ye(:,15).*(dt2+dc1) + di2.*cp2.*Ye(:,11) ...                  %dM43
       + 0 ...
       + ip22.*sp2.*Ye(:,11);

dM42 = + di2.*sp2.*Ye(:,6) ...                                        %dM42
       + (2.*dPsi1).*Ye(:,15)...
       + ip22.*(cp2.*Ye(:,6) - ip22.*Ye(:,2));

dM14 = + (dc2-da2).*b2.*Ye(:,12) + di1.*sp1.*Ye(:,2)  ...             %dM14
       - 2.*(dPhi2).*a2.*Ye(:,12)...
       + ip21.*(rp12.*cp1.*Ye(:,6) + ip12.*sp1.*Ye(:,11) + Ye(:,1));

dM41 = + (dc1-da1).*b1.*Ye(:,15) - di2.*sp2.*Ye(:,5)  ...             %dM41
       - 2.*(dPhi1).*a1.*Ye(:,15) ...
       - ip22.*(rp11.*cp2.*Ye(:,6) + ip11.*sp2.*Ye(:,11) + Ye(:,1));

dM22 = - Ye(:,6).*(dc1+dc2);                                          %dM22   

dM33 = - Ye(:,11).*(dc1+dc2);                                         %dM33
   
dM23 = - (2.*dPsi1).*Ye(:,6) + (2.*dPsi2).*Ye(:,11);      %dM23

dM32 = - (2.*dPsi2).*Ye(:,6) + (2.*dPsi1).*Ye(:,11);      %dM32

dM12 = - Ye(:,2).*dc1 + (dc2-da2).*a2.*Ye(:,6) ...                    %dM12
       + 2.*(dPhi2).*b2.*Ye(:,6)...
       + 2.*rp12.*(sp2 - ip12).*Ye(6);

dM13 = + (dc2-da2).*b2.*Ye(:,11) ...                                  %dM13
       - (2.*dPsi1).*Ye(:,2) - 2.*(dPhi2).*a2.*Ye(:,11)...
       - 2.*rp22.*(cp2 - rp12).*Ye(:,11) - 2.*ip22.*rp2.*Ye(:,15);

dM31 = + (dc1-da1).*b1.*Ye(:,11) ...                                  %dM31
       - (2.*dPsi2).*Ye(:,5) - 2.*(dPhi1).*a1.*Ye(:,11)...
       + 2.*rp21.*(cp1 - rp11).*Ye(:,11) - 2.*ip21.*rp1.*Ye(:,12);

dM21 = -Ye(:,5).*dc2 + (dc1-da1).*a1.*Ye(:,6) ...                     %dM21
       + 2.*(dPhi1).*b1.*Ye(:,6)...
       - 2.*rp21.*(sp1 - ip12).*Ye(:,6);

% dM11 = -(a1.*Ye(:,2).*da1) - (a2.*Ye(:,5).*da2) ...
%        + b1.*Ye(:,2).*dPhi1 + b2.*Ye(:,5).*dPhi2 ...
%         -(a1.*a2.*Ye(:,6) + b1.*b2.*Ye(:,11)).*(da1+da2) ...
%         + 2*(b1.*a2.*Ye(:,6) - a1.*b2.*Ye(:,11)).*dPhi1 ...
%         + 2*(b2.*a1.*Ye(:,6) - a2.*b1.*Ye(:,11)).*dPhi2 ...
%         -(a1.*dM12 + a2.*dM21 + a1.*a2.*dM22 + b1.*b2.*dM33);       %dM11

dM11n = dM22./Ye(:,6);                                               %dM22n
  N11 = 1 - dM11n;

%   M11 = Ye(:,1);
% dM11n = dM11./Ye(:,1);                                             %dM11n
%   N11 = 1 - dM11n;

% disp(['Value dM11 = ' num2str(dM11(1))])
% disp(['Value dM11n = ' num2str(dM11n(1))])
% disp(['Value 1 - dM11n = ' num2str(N11(1))])
% disp(['Value dM12 = ' num2str(dM12(1))])
% disp(['Value dM21 = ' num2str(dM21(1))])
% disp(['Value dM13 = ' num2str(dM13(1))])
% disp(['Value dM31 = ' num2str(dM31(1))])
% disp(['Value dM14 = ' num2str(dM14(1))])
% disp(['Value dM41 = ' num2str(dM41(1))])

Ye(:,16) = Ye(:,16).*(1 - dM11n) + dM44;             %M44 
Ye(:,6)  = Ye(:,6).*(1 - dM11n) + dM22;              %M22
Ye(:,8)  = Ye(:,8).*(1 - dM11n) + dM24;              %M24
Ye(:,12) = Ye(:,12).*(1 - dM11n) + dM34;             %M34
Ye(:,14) = Ye(:,14).*(1 - dM11n) + dM42;             %M42
Ye(:,15) = Ye(:,15).*(1 - dM11n) + dM43;             %M43

Ye(:,4)  = Ye(:,4).*(1 - dM11n) + dM14;              %M14
Ye(:,13) = Ye(:,13).*(1 - dM11n) + dM41;             %M41

Ye(:,6)  = Ye(:,6).*(1 - dM11n) + dM22;              %M22
Ye(:,11) = Ye(:,11).*(1 - dM11n) + dM33;             %M33
Ye(:,7)  = Ye(:,7).*(1 - dM11n) + dM23;              %M23
Ye(:,10) = Ye(:,10).*(1 - dM11n) + dM32;             %M32

Ye(:,2)  = Ye(:,2).*(1 - dM11n) + dM12;              %M12
Ye(:,5)  = Ye(:,5).*(1 - dM11n) + dM21;              %M21
Ye(:,3)  = Ye(:,3).*(1 - dM11n) + dM13;              %M13
Ye(:,9)  = Ye(:,9).*(1 - dM11n) + dM31;              %M31

% Ye(:,1)  = Ye(:,1)                                  %M11


end

