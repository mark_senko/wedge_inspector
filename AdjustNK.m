function [NK] = AdjustNK(NK,alphas,betas)
% Uses alpha and beta to adjust NK for wedge inspector v4.0

 if sum(betas ~= 1)>0
    NK_Si = NK(1,:);
    E1_Si = real(NK_Si).^2 + imag(NK_Si).^2;
    E1_Si = betas.*(E1_Si - 1) + 1;
    E2_Si = 2.*betas.*real(NK_Si).*imag(NK_Si);
    NK(2,:) = sqrt((sqrt(E1_Si.^2+E2_Si.^2) + E1_Si)./2)...
        - 1i.*sqrt((sqrt(E1_Si.^2+E2_Si.^2) - E1_Si)./2);
 end
 if sum(alphas ~= 1)>0
    NK_sio2_xy = NK(4,:);
    E1_SiO2 = real(NK_sio2_xy).^2 + imag(NK_sio2_xy).^2;
    E1_SiO2 = alphas.*(E1_SiO2 - 1) + 1;
    E2_SiO2 = 2.*alphas.*real(NK_sio2_xy).*imag(NK_sio2_xy);
    NK(5,:) = sqrt((sqrt(E1_SiO2.^2+E2_SiO2.^2) + E1_SiO2)./2)...
        - 1i.*sqrt((sqrt(E1_SiO2.^2+E2_SiO2.^2) - E1_SiO2)./2);
 end

end

