function Ye = ModifySpectrum(ToolParms, Ye, M11)
% Modify experimental spectrum according to new WcaL and M11 status.
% Updated function from Pedro's original for WI v3.0.
% Updated to add compensator corrections for WI v4.1a.
% Updated to fix wavelength shift induced endpoint error for WI v4.3a

% Modify the wavelength calibration if necessary
W_old = ToolParms.Wcal_o;
W_new = ToolParms.Wcal_n;
if sum(W_new~=W_old)>0
    Ye = RC2WaveCalCorrection_v2(ToolParms.L, Ye, W_old, W_new, ToolParms.Lc);
    Ye = Ye(ToolParms.iW,:); % Adust to window after wave cal correction
    if ToolParms.uT
        M11 = RC2WaveCalCorrection_v2(ToolParms.L, M11, W_old, W_new, ToolParms.Lc);
        M11 = M11(ToolParms.iW); % Adjust to window after wave cal correction
    end
    if ToolParms.Comp.enable
        ToolParms.Comp.An = RC2WaveCalCorrection_v2(ToolParms.L, ToolParms.Comp.A, W_old, W_new, ToolParms.Lc);
        ToolParms.Comp.Pn = RC2WaveCalCorrection_v2(ToolParms.L, ToolParms.Comp.P, W_old, W_new, ToolParms.Lc);
        ToolParms.Comp.GamRn = RC2WaveCalCorrection_v2(ToolParms.L, ToolParms.Comp.GamR, W_old, W_new, ToolParms.Lc);
        ToolParms.Comp.GamSn = RC2WaveCalCorrection_v2(ToolParms.L, ToolParms.Comp.GamS, W_old, W_new, ToolParms.Lc);
    else
        if isfield(ToolParms.Comp,'An')
            ToolParms.Comp = rmfield(ToolParms.Comp,'An');
            ToolParms.Comp = rmfield(ToolParms.Comp,'Pn');
            ToolParms.Comp = rmfield(ToolParms.Comp,'GamRn');
            ToolParms.Comp = rmfield(ToolParms.Comp,'GamSn');
        end
    end
else
    Ye = Ye(ToolParms.iW,:);
    if ToolParms.uT
        M11 = M11(ToolParms.iW); % Adjust to window
    end
end

% Renormalize Data to M22 if Data Mode is 3 (Jon)
if ToolParms.mode == 3
    Y6 = Ye(:,6);
    Y6 = repmat(Y6,[1,16]);
    Ye = Ye./Y6;
end

% Denormalize everything!
[~,s2] = size(Ye);
MMn = repmat(M11,[1 s2]);
Ye = Ye.*MMn;

% apply compensator corrections
if ToolParms.Comp.enable
    Ye = CompCorrection(Ye,ToolParms.Comp,ToolParms.iW);
end

% apply new DO
if ToolParms.mode == 1
    DO_new = ToolParms.DO;
    DO_old = ToolParms.DO_CE;
    if sum(DO_new ~= DO_old)>0
        [Di_old, ~] = LensInPlaneRetardance(ToolParms.Lo, DO_old, 1);
        [Di_new, ~] = LensInPlaneRetardance(ToolParms.Lo, DO_new, 1);    
        Di = Di_new - Di_old;
%         Di = Di_old - Di_new;
        CDi = cosd(Di);
        SDi = sind(Di);      
        C_new = Ye(:,2).*CDi - Ye(:,3).*SDi;
        S_new = Ye(:,3).*CDi + Ye(:,2).*SDi;
        Ye(:,2) = C_new;
        Ye(:,3) = S_new;
    end
end

% Decide wether to return to normalized MM or not
if ~ToolParms.m11s
    [~,s2] = size(Ye);
    MMn = repmat(M11,[1 s2]);
    Ye = Ye./MMn;
end

% remove depolarization
if ToolParms.Depol==1
% NCS Depolarization
   DI = (Ye(:,2) + Ye(:,5)).^2 + (Ye(:,11) + Ye(:,16)).^2 + (Ye(:,12) - Ye(:,15)).^2;
   DI = sqrt(DI)./(sqrt(4)*Ye(:,6));
    Ye(:,2) = Ye(:,2)./DI;
    Ye(:,5) = Ye(:,5)./DI;
    Ye(:,11) = Ye(:,11)./DI;
    Ye(:,16) = Ye(:,16)./DI;
    Ye(:,12) = Ye(:,12)./DI;
    Ye(:,15) = Ye(:,15)./DI;
% Gil Depolarization
%     D1 = sqrt(sum(Ye.^2,2)-Ye(:,6).^2)./(sqrt(3)*Ye(:,6));
%     DI = repmat(D1,[1 15]);
%     Ye(:,2:16) = Ye(:,2:16)./DI;
% Before Jon's compensator merge
%    DI = sqrt(sum(Ye.^2,2)-Ye(:,1).^2)./(sqrt(3)*Ye(:,1));
%    DI = repmat(DI,[1 15]);
%    Ye(:,2:16) = Ye(:,2:16)./DI;
end
