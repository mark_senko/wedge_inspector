function [MM] = Jm2Mm(JM)
% Jones Matrix to Mueller Matrix converter
% relies on MM3D_v2.c for matrix multiplication
% expects 2x2xL and 4x4xL matrices
% written by AGB

[s1,s2,s3] = size(JM);

if (s1~=2) || (s2~=2)
    disp('Error: JM wrong size.');
    return
end

J2 = conj(JM);
% J2 = JM;
% JM = conj(JM);
MM = zeros(4,4,s3);
MM(1:2,1:2,:) = JM(1,1,:).*J2; % Kronecker product
MM(1:2,3:4,:) = JM(1,2,:).*J2;
MM(3:4,1:2,:) = JM(2,1,:).*J2;
MM(3:4,3:4,:) = JM(2,2,:).*J2;
QQ = [1 0 0 1;1 0 0 -1; 0 1 1 0; 0 1i -1i 0];
QQinv = [0.5 0.5 0 0 ; 0 0 0.5 -0.5i; 0 0 0.5 0.5i; 0.5 -0.5 0 0];
if isreal(MM)
    MM = complex(MM);
end
if s3~=1
    MM = MM3D_v2(MM,QQinv);
    MM = MM3D_v2(QQ,MM);
else
    MM = QQ*MM*QQinv;
end

end

