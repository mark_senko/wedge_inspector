% Generate M11 maximum from 2um wedge to determine Io

h = waitbar(0,'Generating M11 maximum...');
L = linspace(190,1800,1611);
AOI = (L-L+65);
nk_si = GetNKs('C:\Users\aboosalis\Documents\Mats\Si_JAW.txt',L);
nk_sio2 = GetNKs('C:\Users\aboosalis\Documents\Mats\sio2_noc.txt',L);

figure('windowstyle','docked');
plot(L,real(nk_si),L,-imag(nk_si));
figure('windowstyle','docked');
plot(L,real(nk_sio2),L,-imag(nk_sio2));

NK = [nk_si; nk_si; nk_sio2; nk_sio2; nk_sio2];
M11s = (AOI-AOI).';
for i=1:10000
    M=wendgine(AOI,real(NK),imag(NK),L,[0 i/5]); 
    M11s=max([squeeze(M(1,1,:)) M11s],[],2); 
    waitbar(i/10000,h);
end

waitbar(0.5,h,'Writing M11_max.xlsx...');
xlswrite('M11_max.xlsx',[L.' M11s],1);
close(h);

figure('windowstyle','docked');
plot(L,M11s);
box on
grid on
xlabel('Wavelength [nm]');
ylabel('M11 maximum');

