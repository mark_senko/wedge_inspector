function [Lest] = EstimateLfromFs(Nfunc,Fts,stopdel)
% Used specifically to find the estimated wavelengths from a set of
% frequencies in 1/nm, recursively. Nfunc is the ModParms.ppnk
% interpolation function for SiO2 from WI. Fts is a vector of frequencies.
% AGB 2021

sio2n = 1.5*ones(length(Fts),1);
angt = asind(sind(65)./sio2n);
Lest = (sio2n - sind(angt).*cosd(25)).*2./(Fts.*cosd(angt));
maxdel = stopdel+1;
while maxdel >= stopdel
    sio2n = Nfunc(Lest);
    angt = asind(sind(65)./sio2n);
    Lest_old = Lest;
    Lest = (sio2n - sind(angt).*cosd(25)).*2./(Fts.*cosd(angt));
    maxdel = max(abs(Lest_old-Lest));
%         disp(maxdel);
end

end

