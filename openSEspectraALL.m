function [L, AOI, Y, SigY, PathF, Files] = openSEspectraALL(varargin)
% Open SE spectra from .dat files.
% Opens multiple files.
% Opens encrypted files.
% Can specify data type.
% Outputs MM, NCS, Psi/Delta
% Expected arguments: Path, Files(cell).

PathF = varargin{1};
Files = varargin{2};
NumF = length(Files);
axh1 = varargin{3};
axh2 = varargin{4};

[L, AOI, ~, ~] = openSEsingle(PathF,Files{1});
Y = zeros(length(L),22,NumF);
SigY = Y;
for nF=1:NumF
    disp(Files{nF});
%     [~, ~, Y(:,nF), Sigma(:,nF), ~, ~, ~,~] = OpenSpectrum3(PathF,Files{nF});
    [~, ~, Y(:,:,nF), SigY(:,:,nF)] = openSEsingle(PathF,Files{nF});
    upwait_v2(nF/NumF,axh1,'Reading Files',nF/NumF,axh2,'Reading Files');
end

upwait_v2('c',axh1,' ','c',axh2,' ');
end

function [L, AOI, Y, SigY] = openSEsingle(PathF,File)
    d=adapdata();
    d=loaddata(d,[PathF File]);
    
    Data = get(d,'MM12');
    L = Data(:,1);
    AOI = Data(:,2);
    
    MM = zeros(length(L),16);
    SigMM = MM;
    MM(:,2) = Data(:,3);
    SigMM(:,2) = Data(:,5);
    Data = get(d,'MM13');
    MM(:,3) = Data(:,3);
    SigMM(:,3) = Data(:,5);
    Data = get(d,'MM14');
    MM(:,4) = Data(:,3);
    SigMM(:,4) = Data(:,5);
    Data = get(d,'MM21');
    MM(:,5) = Data(:,3);
    SigMM(:,5) = Data(:,5);
    Data = get(d,'MM22');
    MM(:,6) = Data(:,3);
    SigMM(:,6) = Data(:,5);
    Data = get(d,'MM23');
    MM(:,7) = Data(:,3);
    SigMM(:,7) = Data(:,5);
    Data = get(d,'MM24');
    MM(:,8) = Data(:,3);
    SigMM(:,8) = Data(:,5);
    Data = get(d,'MM31');
    MM(:,9) = Data(:,3);
    SigMM(:,9) = Data(:,5);
    Data = get(d,'MM32');
    MM(:,10) = Data(:,3);
    SigMM(:,10) = Data(:,5);
    Data = get(d,'MM33');
    MM(:,11) = Data(:,3);
    SigMM(:,11) = Data(:,5);
    Data = get(d,'MM34');
    MM(:,12) = Data(:,3);
    SigMM(:,12) = Data(:,5);
    Data = get(d,'MM41');
    MM(:,13) = Data(:,3);
    SigMM(:,13) = Data(:,5);
    Data = get(d,'MM42');
    MM(:,14) = Data(:,3);
    SigMM(:,14) = Data(:,5);
    Data = get(d,'MM43');
    MM(:,15) = Data(:,3);
    SigMM(:,15) = Data(:,5);
    Data = get(d,'MM44');
    MM(:,16) = Data(:,3);
    SigMM(:,16) = Data(:,5);
    
    MM(:,1) = ones(length(L),1);
    SigMM(:,1) = 0.01.*MM(:,1);
%     Data = get(d,'mR');
%     if isempty(Data)
%         MM(:,1) = ones(length(L),1);
%     else
%         MM(:,1) = Data(:,3);
%     end

    try
        Data = get(d,'SE');
        Psi = Data(:,3);
        Delta = Data(:,4);
        SigmaPsi = Data(:,5);
        SigmaDelta = Data(:,6);

        [N,C,S, SigN, SigC, SigS] = Deg2NCS(Psi,Delta,SigmaPsi,SigmaDelta,0);
    catch
        N = (-MM(:,2)-MM(:,5))./2;
        C = (MM(:,11)+MM(:,16))./2;
        S = (MM(:,12)-MM(:,15))./2;
        p = sqrt(N.^2 + C.^2 + S.^2);
        N = N./p; C = C./p; S = S./p;
        SigN = SigMM(:,2);
        SigC = SigMM(:,11);
        SigS = SigMM(:,12);
        Psi = N-N;
        Delta = Psi;
        SigmaPsi = Psi;
        SigmaDelta = Psi;
        disp('Warning: No Psi/Delta data found. Substituting MM data.');
    end

%     Data = get(d,'NCS');
%     N = Data(:,3);
%     C = Data(:,4);
%     S = Data(:,5);
%     SigmaN = Data(:,6);
%     SigmaC = Data(:,7);
%     SigmaS = Data(:,8);

    try
        uT = get(d,'uT');
        uT = uT(:,3);
    catch
        disp('No uT data found!');
        uT = N-N;
    end
    SiguT = (uT-uT);

    Y = [MM N C S Psi Delta uT];
    SigY = [SigMM SigN SigC SigS SigmaPsi SigmaDelta SiguT];
end
