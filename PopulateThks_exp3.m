% Populate Thicknesses

% Experimental revision based on diff
% This version uses non-uniform FFT to estimate thickness

% Plot estimates?
plt = [1 1 1 1];

% Grab data
Data = get(handles.uitable1,'data');
NumF = size(Data,1);
L = handles.L;
iL = (L <= 1700 & L >= 190);
% L = L(iL);
eV = 1239./L;
NumE = length(eV);
deV = (max(eV)-min(eV))/NumE;
FEs = (1/deV).*(0:1:(floor(NumE/2))-1)./NumE;
% eV2 = linspace(min(eV),max(eV),NumE);
eV2 = eV*NumE/max(eV);
S = handles.Y(:,12,:); % get m34 instead of S explicitly
S = permute(S,[1 3 2]);
S = S(:,:,1);
AOI = handles.CEcal.AOIcal;

% Get SiO2 N&K
if isempty(handles.PathMat)
    PathMat = [pwd '\Mats'];
    if ~exist(PathMat,'dir')
        uiwait(msgbox({'No MATERIALS folder found.' 'Please select MATERIALS folder.'},'Folder Not Found','error'));
        PathMat = uigetdir(pwd,'Please Select the Materials Folder');
        handles.PathMat = PathMat;
    else
        handles.PathMat = PathMat;
    end
end
NK_sio2 = GetNKs([handles.PathMat '\SiO2_jaw.txt'],L);
% NK_sio2 = ones(size(NK_sio2)).*1.5;
AOIt = asind(sind(AOI)./NK_sio2);
AOIt = mean(AOIt);
mn_sio2 = mean(NK_sio2);

% For each file, estimate thickness
thks = zeros(NumF,1);
phas = thks;

for i = 1:1:NumF
    if plt(1)
        figure('windowstyle','docked');
        plot(eV,S(:,i));
%         plot(eV(1:end-1),diff(S(:,i)));
        xlabel('Frequency [eV]');
        ylabel('S');
        grid on;
    end
    
    % Take the non-uniform Fourier transform 
    T = nufft(S(:,i),eV2);
    T = T(1:floor(NumE/2),:);
    Tabs = abs(T./NumE);
    Treal = real(T./NumE);
    Timag = imag(T./NumE);
    
    % plot the nufft
    if plt(2)
        figure('windowstyle','docked');
        plot(FEs,Tabs);
        grid on; hold on;
%         thres = max(Tabs)/10;
%         ithres = Tabs<thres;
%         T2 = T;
%         T2(ithres) = 0;
%         Tphas = atan2(imag(T2),real(T2));
%         plot(FEs,Tphas);
        xlabel('Frequency [cycles/eV]');
        ylabel('|F(S)|');
    end
    
    % Find the highest point
    [Tmax,ind] = max(Tabs);
    Fmax = FEs(ind);
    Tp = Tabs(ind-1:ind+1).';
    Fp = FEs(ind-1:ind+1);
    
    % Fit parabola to data points
    P0 = [-1 Fmax Tmax];
%     dfnc2 = @(P) sum(((P(1).*(Fp-P(2)).^2+P(3)) -Tp).^2);
%     P1 = fminsearch(dfnc2,P0);
%     P1(2) = Fmax; %force no fitting condition

    % Use spline to find maximum
    P1 = P0;
    Fq = linspace(min(Fp),max(Fp),99);
    Tq = interp1(Fp,Tp,Fq,'spline');
    [P1(1),P1(3)] = max(Tq);
    P1(2) = Fq(P1(3));
    
    % Estimate thickness (Pedro)
%     P = 1/(1239*P1(2)); % Period in 1/nm
%     C = 2*mn_sio2/cosd(AOIt)-2*tand(AOIt)*sind(AOI);
%     thks(i) = 1/(C*P);    

    % Estimate thickness (Alex with phase correction)
    TmxI = interp1(FEs,Timag,P1(2),'spline');
    TmxR = interp1(FEs,Treal,P1(2),'spline');
    phas(i) = atan2(TmxI,TmxR); % phase angle of the peak
%     E1 = eV(end)+abs(phas)/(2*pi)/P1(2); % Energy of first crossing
%     thks(i) = 620/(mn_sio2*cosd(AOIt))*(E1+E1^2*P1(2));
%     thks(i) = 620/(mn_sio2*cosd(AOIt)/(P1(2)-abs(phas(i)/(2*pi))));
    thks(i) = 620*P1(2)/(mn_sio2*cosd(AOIt));
    
    % plot fit results
    if plt(3)
%         Fp2 = linspace(min(Fp),max(Fp),21);
%         dfnc3 = @(P) P(1).*(Fp2-P(2)).^2+P(3);
%         plot(Fp2,dfnc3(P0),'-r');
%         plot(Fp2,dfnc3(P1),'-k');
        plot(Fq,Tq,'-r');
        plot(P1(2),P1(1),'ok');
        legend('|F(S)|','Initial Guess',['Fit - ' num2str(thks(i))]);
        keyboard;
    end    
    
    % disp thickness
    fprintf('Estimated thickness for point %d is %f.\n',i,thks(i));
end
Data(:,3) = num2cell(thks);
set(handles.uitable1,'data',Data);

if plt(4)
    figure('windowstyle','docked');
    plot(thks);
    xlabel('Point No.');ylabel('Thickness Estimate SiO2 [nm]');
    grid on;
    keyboard;
end