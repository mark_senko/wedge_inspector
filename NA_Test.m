% Test and plot NA fitting

% Pedro's way
NumNA = 51;
dx = 2/NumNA;
x = (1:NumNA)*dx;
x = x-mean(x);
W = sqrt(1-x.^2);
W = W./max(W);
plot(x,W);
grid on; box on;
ylim([0 1]);
pbaspect([2 1 1]);

% Trapezoidal quadrature
x2 = linspace(-1,1,NumNA+1);
C = sqrt(1-x2.^2);
for i=2:NumNA+1
    W(i-1) = min(C(i),C(i-1))*dx + dx*0.5*(max(C(i),C(i-1))-min(C(i),C(i-1)));
end
W = W./max(W);
hold on
plot(x,W);

% Circle quadrature
x2 = linspace(-1,1,NumNA+1);
dx = 2/(NumNA+1);
xs = x2(1:NumNA);
xb = x2(2:end);
Cs = sqrt(1-xs.^2);
Cb = sqrt(1-xb.^2);

W = min(Cs,Cb).*dx;
W = W + abs(Cb-Cs).*(dx/2);
lcs = sqrt(abs(Cb-Cs).^2 + dx^2);
W = W + asin(lcs./2) - lcs./4.*sqrt(4-lcs.^2);

W = W./max(W);
plot(x,W);


% finalize graph
hold off
legend('Midpoint','Trapezoid','Exact');
xlabel('Normalized Angular Spread');
ylabel('Normalized Weight');