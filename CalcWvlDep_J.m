function [Pl] = CalcWvlDep(L,P,rw)
[s1,NumP]=size(P.val);
if s1 > 1
    P.val = P.val(rw,:);
    P.typ = P.typ{rw};
    P.spt = P.spt(rw);
end

NumL = length(L);
Pl = zeros(1,NumL);
L = L';
switch P.typ
    case 'None'
        if P.spt==0
            Pl = P.val(1).*ones(1,NumL);
        else
            iL = (L <= 1000);
            Pl(iL) = P.val(1);
            Pl(~iL) = P.val(NumP/2+1);
        end
    case 'Cauchy'
        if P.spt==0
            for i=1:NumP
                Pl = (P.val(i)*1000^(2*(i-1))).*L.^(-2*(i-1)) + Pl;
            end
        else
            for i=1:NumP/2
                iL = (L <= 1000);
                Pl(iL) = (P.val(i)*1000^(2*(i-1))).*L(iL).^(-2*(i-1)) + Pl(iL);
                i2 = i+NumP/2;
                Pl(~iL) = (P.val(i2)*1000^(2*(i-1))).*L(~iL).^(-2*(i-1)) + Pl(~iL);
            end
        end
    case 'Polynom.'
        if P.spt==0
            for i=1:NumP
                Pl = (P.val(i)*1000^(i-1)).*(1./L-1/1000).^(i-1) + Pl;
            end
        else
            for i=1:NumP/2
                iL = (L <= 1000);
                Pl(iL) = (P.val(i)*1000^(i-1)).*((1./L(iL)-1/600)).^(i-1) + Pl(iL);
                i2 = i+NumP/2;
                Pl(~iL) = (P.val(i2)*1000^(i-1)).*((1./L(~iL)-1/1350)).^(i-1) + Pl(~iL);
            end
        end
    case 'Spline'
        if P.spt==0
            Ls = linspace(1240/190,1240/1700,NumP);
            Pl = spline(Ls,P.val,1240./L);
        else
            iL = (L <= 1000);
            Ls = linspace(1240/190,1240/1000,NumP/2);
            Pl(iL) = spline(Ls,P.val(1:NumP/2),1240./L(iL));
            Ls = linspace(1240/1000,1240/1700,NumP/2);
            Pl(~iL) = spline(Ls,P.val(NumP/2+1:end),1240./L(~iL));
        end
end

% iP = (Pl > P.UL) & (P.UL ~= P.LL);
% Pl(iP) = P.LL+0.9.*(P.UL-P.LL)+0.1.*(P.UL-P.LL).*exp((P.UL-Pl(iP))./(P.UL-P.LL));
% iP = (Pl < P.LL) & (P.UL ~= P.LL);
% Pl(iP) = P.UL-0.9.*(P.UL-P.LL)-0.1.*(P.UL-P.LL).*exp((Pl(iP)-P.LL)./(P.UL-P.LL));

