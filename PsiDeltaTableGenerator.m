% Script to generate HeNe Psi Delta data and save it.

% For quick look up of Angstrom Advanced data.
% Outputs an Excel file.
% AGB 2021

% Requirements:
% -> wendgine2.mex64
% -> GetNKs.m
% -> NCStoPD.m
% -> Si_JAW.txt
% -> SiO2_JAW.txt

% Define parameters
L = 633; % HeNe wavelength
AOI = 65; % Presumed AOI
minT = 0; % minimum thickness of table
maxT = 2000; % maximum thickness of table
numT = 2001; % length of table
NK_air = 1.0003;
file_name = 'Psi_Delta_Table.xlsx';

% Grab NKs



