% Fit Setup for Serial and Parallel Fits

%% Setup the variable vectors X, iX, tX
X = [0; AOIp.val'; NAp.val'; BW_VIS; BW_IR; ToolParms.Wcal_n'; ThkEMA;...
     ALPHAp.val'; BETAp.val'; reshape(permute(BMp.val,[2 1]),[],1);...
     reshape(permute(LNSp.val,[2 1]),[],1)];

iXw4 = handles.CtrlData.no_w4;
if Lmin > 1000
    iX = [tabdata{8,1};... %Fit Thickness
          zeros(length(AOIp.val)*tabdata{3,4}/2,1);...
          repmat(tabdata{3,1},[length(AOIp.val)/(2^tabdata{3,4}) 1]);...
          zeros(length(NAp.val)*tabdata{4,4}/(2),1);...
          repmat(tabdata{4,1},[length(NAp.val)/(2^tabdata{4,4}) 1]);...
          0; tabdata{5,1};... %bandwidth
          0;0;0;0;0;... %wcal vis
          repmat(tabdata{7,1},[4 1]);... %wcal ir
          tabdata{9,1};... % EMA thickness
          zeros(length(ALPHAp.val)*tabdata{10,4}/(2),1);...
          repmat(tabdata{10,1},[length(ALPHAp.val)/(2^tabdata{10,4}) 1]);...
          zeros(length(BETAp.val)*tabdata{11,4}/(2),1);...
          repmat(tabdata{11,1},[length(BETAp.val)/(2^tabdata{11,4}) 1]);...
          zeros(length(BMp.val)/2*tabdata{12,4},1);...
          repmat(tabdata{12,1},[size(BMp.val,2)/(2^tabdata{12,4}) 1]);...
          zeros(length(BMp.val)*tabdata{13,4}/2,1);...
          repmat(tabdata{13,1},[size(BMp.val,2)/(2^tabdata{13,4}) 1]);...
          zeros(length(BMp.val)*tabdata{14,4}/2,1);...
          repmat(tabdata{14,1},[size(BMp.val,2)/2^tabdata{14,4} 1]);...
          zeros(length(BMp.val)*tabdata{15,4}/2,1);...
          repmat(tabdata{15,1},[size(BMp.val,2)/2^tabdata{15,4} 1]);...
          zeros(length(BMp.val)*tabdata{16,4}/2,1);...
          repmat(tabdata{16,1},[size(BMp.val,2)/2^tabdata{16,4} 1])];
elseif Lmax <= 1000      
    iX = [tabdata{8,1};... %Fit Thickness
          repmat(tabdata{3,1},[length(AOIp.val)/2^tabdata{3,4} 1]);...
          zeros(length(AOIp.val)*tabdata{3,4}/2,1);...
          repmat(tabdata{4,1},[length(NAp.val)/2^tabdata{4,4} 1]);...
          zeros(length(NAp.val)*tabdata{4,4}/2,1);...
          tabdata{5,1}; 0;... %bandwidth
          repmat(tabdata{6,1},[4 1]); (tabdata{6,1} & ~iXw4);... %wcal vis
          0;0;0;0;... %wcal ir
          tabdata{9,1};... % EMA thickness
          repmat(tabdata{10,1},[length(ALPHAp.val)/2^tabdata{10,4} 1]);...
          zeros(length(ALPHAp.val)*tabdata{10,4}/2,1);...
          repmat(tabdata{11,1},[length(BETAp.val)/2^tabdata{11,4} 1]);...
          zeros(length(BETAp.val)*tabdata{11,4}/2,1);...
          repmat(tabdata{12,1},[size(BMp.val,2)/2^tabdata{12,4} 1]);...
          zeros(length(BMp.val)*tabdata{12,4}/2,1);...
          repmat(tabdata{13,1},[size(BMp.val,2)/2^tabdata{13,4} 1]);...
          zeros(length(BMp.val)*tabdata{13,4}/2,1);...
          repmat(tabdata{14,1},[size(BMp.val,2)/2^tabdata{14,4} 1]);...
          zeros(length(BMp.val)*tabdata{14,4}/2,1);...
          repmat(tabdata{15,1},[size(BMp.val,2)/2^tabdata{15,4} 1]);...
          zeros(length(BMp.val)*tabdata{15,4}/2,1);...
          repmat(tabdata{16,1},[size(BMp.val,2)/2^tabdata{16,4} 1])];
          zeros(length(BMp.val)*tabdata{16,4}/2,1);...
else
    iX = [tabdata{8,1};... %Fit Thickness
          repmat(tabdata{3,1},[length(AOIp.val) 1]);...
          repmat(tabdata{4,1},[length(NAp.val) 1]);...
          tabdata{5,1}; tabdata{5,1};... %bandwidth
          repmat(tabdata{6,1},[4 1]); (tabdata{6,1} & ~iXw4);... %wcal vis
          repmat(tabdata{7,1},[4 1]);... %wcal ir
          tabdata{9,1};... % EMA thickness
          repmat(tabdata{10,1},[length(ALPHAp.val) 1]);...
          repmat(tabdata{11,1},[length(BETAp.val) 1]);...
          repmat(tabdata{12,1},[size(BMp.val,2) 1]);...
          repmat(tabdata{13,1},[size(BMp.val,2) 1]);...
          repmat(tabdata{14,1},[size(BMp.val,2) 1]);...
          repmat(tabdata{15,1},[size(BMp.val,2) 1]);...
          repmat(tabdata{16,1},[size(BMp.val,2) 1])];
end
  
LL = [1;... %Fit Thickness
      repmat(tabdata{3,16},[length(AOIp.val) 1]);...
      repmat(tabdata{4,16},[length(NAp.val) 1]);...
      tabdata{5,16}; tabdata{5,16};... %bandwidth
      repmat(tabdata{6,16},[5 1]);... %wcal vis
      repmat(tabdata{7,16},[4 1]);... %wcal ir
      tabdata{9,16};... % EMA thickness
      repmat(tabdata{10,16},[length(ALPHAp.val) 1]);...
      repmat(tabdata{11,16},[length(BETAp.val) 1]);...
      repmat(tabdata{12,16},[numel(BMp.val) 1])];
  
UL = [1;... %Fit Thickness
      repmat(tabdata{3,17},[length(AOIp.val) 1]);...
      repmat(tabdata{4,17},[length(NAp.val) 1]);...
      tabdata{5,17}; tabdata{5,17};... %bandwidth
      repmat(tabdata{6,17},[5 1]);... %wcal vis
      repmat(tabdata{7,17},[4 1]);... %wcal ir
      tabdata{9,17};... % EMA thickness
      repmat(tabdata{10,17},[length(ALPHAp.val) 1]);...
      repmat(tabdata{11,17},[length(BETAp.val) 1]);...
      repmat(tabdata{12,17},[numel(BMp.val) 1])];

tX = [1;... %Fit Thickness
      repmat(2,[length(AOIp.val) 1]);...
      repmat(3,[length(NAp.val) 1]);...
      repmat(4,[2 1]);... %bandwidth
      repmat(5,[5 1]);... %wcal vis
      repmat(6,[4 1]);... %wcal ir
      7;... % EMA thickness
      repmat(8,[length(ALPHAp.val) 1]);...
      repmat(9,[length(BETAp.val) 1]);...
      repmat(10,[numel(BMp.val) 1])];
% Modify based on lens behavior  
if handles.CtrlData.NewLens == 0
    if handles.CtrlData.mode == 1
        iX = [iX; repmat(tabdata{17,1},[tabdata{17,5} 1]);...
              false; false; tabdata{18,1}];
        tX = [tX; repmat(11,[tabdata{17,5} 1]);...
              12; 12; 12];
        LL = [LL; repmat(tabdata{17,16},[3 1]); repmat(tabdata{18,16},[3 1])];
        UL = [UL; repmat(tabdata{17,17},[3 1]); repmat(tabdata{18,17},[3 1])];
    else
        iX = [iX; repmat(tabdata{17,1},[tabdata{17,5} 1]);...
              false; false; tabdata{18,1};...
              repmat(tabdata{19,1},[tabdata{19,5} 1]);...
              repmat(tabdata{20,1},[tabdata{20,5} 1])];
        tX = [tX; repmat(11,[tabdata{17,5} 1]);...
              12; 12; 12;...
              repmat(13,[tabdata{19,5} 1]);...
              repmat(13,[tabdata{20,5} 1])];
        LL = [LL; repmat(tabdata{17,16},[3 1]); repmat(tabdata{18,16},[3 1]);...
             repmat(tabdata{19,16},[3 1]); repmat(tabdata{20,16},[3 1])];
        UL = [UL; repmat(tabdata{17,17},[3 1]); repmat(tabdata{18,17},[3 1]);...
             repmat(tabdata{19,17},[3 1]); repmat(tabdata{20,17},[3 1])];
    end
else
    for i=1:10
        if Lmin > 1000
            iX = [iX; zeros(tabdata{16+i,5}*tabdata{16+i,4}/2,1); repmat(tabdata{16+i,1},[tabdata{16+i,5}/2^tabdata{16+i,4} 1])];
        elseif Lmax <= 1000
            iX = [iX; repmat(tabdata{16+i,1},[tabdata{16+i,5}/2^tabdata{16+i,4} 1]); zeros(tabdata{16+i,5}*tabdata{16+i,4}/2,1);];
        else
            iX = [iX; repmat(tabdata{16+i,1},[tabdata{16+i,5} 1])];
        end
        tX = [tX; repmat(14,[tabdata{16+i,5} 1])];
        LL = [LL; repmat(tabdata{16+i,16},[tabdata{16+i,5} 1])];
        UL = [UL; repmat(tabdata{16+i,17},[tabdata{16+i,5} 1])];
    end 
end      
% add parameters for compensator model
if handles.CtrlData.CompCal == 1
    if handles.CtrlData.NewLens == 1; ind=26; else; ind=20; end
    for i=1:8
        if Lmin > 1000
            iX = [iX; zeros(tabdata{ind+i,5}*tabdata{ind+i,4}/2,1); repmat(tabdata{ind+i,1},[tabdata{ind+i,5}/2^tabdata{ind+i,4} 1])];
        elseif Lmax <= 1000
            iX = [iX; repmat(tabdata{ind+i,1},[tabdata{ind+i,5}/2^tabdata{ind+i,4} 1]); zeros(tabdata{ind+i,5}*tabdata{ind+i,4}/2,1);];
        else
            iX = [iX; repmat(tabdata{ind+i,1},[tabdata{ind+i,5} 1])];
        end
        tX = [tX; repmat(15,[tabdata{ind+i,5} 1])];
        LL = [LL; repmat(tabdata{ind+i,16},[tabdata{ind+i,5} 1])];
        UL = [UL; repmat(tabdata{ind+i,17},[tabdata{ind+i,5} 1])];
    end
end

% Add N&K parameters
if ModParms.NKselect == 2
    iX = [iX; cell2mat(ModParms.SiDat(:,1)); cell2mat(ModParms.SiO2Dat(:,1))];
    X = [X; cell2mat(ModParms.SiDat(:,3)); cell2mat(ModParms.SiO2Dat(:,3))];
    NumMP = length(cell2mat(ModParms.SiDat(:,3)));
    tX = [tX; repmat(15,[NumMP 1])];
    NumMP = length(cell2mat(ModParms.SiO2Dat(:,3)));
    tX = [tX; repmat(16,[NumMP 1])];
    LL = [LL; cell2mat(ModParms.SiDat(:,4)); cell2mat(ModParms.SiO2Dat(:,4))];
    UL = [UL; cell2mat(ModParms.SiDat(:,5)); cell2mat(ModParms.SiO2Dat(:,5))];
end

% Check if there are floating parameters
if sum(iX) == 0
    msgbox('No floating parameters!');
    return
end

% disp([X iX tX LL UL]);
iX = logical(iX);

%% establish parameter names
pNames = cell(length(tX),1);
cnt2 = 1;
pNames(1) = {'Thk'};
for i=2:length(tX)
    if tX(i-1)~=tX(i)
        cnt = 1;
    else
        cnt = cnt+1;
    end
    [nBM1,nBM2] = size(BMp.val);
    [nLN1,nLN2] = size(LNSp.val);
    [nCC1,nCC2] = size(CCp.val);
    switch tX(i)
        case 2             
            pNames(i) = {['AOI_' num2str(cnt)]};
        case 3
            pNames(i) = {['NA_' num2str(cnt)]};
        case 4
            pNames(i) = {['BW_' num2str(cnt)]};
        case 5
            pNames(i) = {['W-vis_' num2str(cnt)]};
        case 6
            pNames(i) = {['W-ir_' num2str(cnt)]};
        case 7
            pNames(i) = {['ThkEMA_' num2str(cnt)]};
        case 8
            pNames(i) = {['E1 alpha_' num2str(cnt)]};
        case 9
            pNames(i) = {['E1 beta_' num2str(cnt)]};
        case 10
            switch cnt2
                case 1
                    pNames(i) = {['Bm1 (AF)_' num2str(cnt)]};
                case 2
                    pNames(i) = {['Bm2 (Rad.)_' num2str(cnt)]};
                case 3
                    pNames(i) = {['Bm' num2str(cnt2) '_' num2str(cnt)]};
                case 4
                    pNames(i) = {['Bm' num2str(cnt2) '_' num2str(cnt)]};
                case 5
                    pNames(i) = {['Bm' num2str(cnt2) '_' num2str(cnt)]};
            end
            if (cnt2==nBM1)&&(cnt==nBM2); cnt2=1;
            elseif cnt == nBM2; cnt=0; cnt2=cnt2+1; end
        case 11
            pNames(i) = {['DO13_' num2str(cnt)]};
        case 12
            pNames(i) = {['DO4_' num2str(cnt)]};
        case 13
            if cnt < 4
                pNames(i) = {['WD1_' num2str(cnt)]};
            else
                pNames(i) = {['WD2_' num2str(cnt-3)]};
            end
        case 14
            switch cnt2
                case 1
                    pNames(i) = {['NCS Ret._' num2str(cnt)]};
                case 2
                    pNames(i) = {['NCS Att._' num2str(cnt)]};
                case 3
                    pNames(i) = {['T1 Ret._' num2str(cnt)]};
                case 4
                    pNames(i) = {['T1 Att._' num2str(cnt)]};
                case 5
                    pNames(i) = {['T1 Real Act._' num2str(cnt)]};
                case 6
                    pNames(i) = {['T1 Imag Act._' num2str(cnt)]};
                case 7
                    pNames(i) = {['T2 Ret._' num2str(cnt)]};
                case 8
                    pNames(i) = {['T2 Att._' num2str(cnt)]};
                case 9
                    pNames(i) = {['T2 Real Act._' num2str(cnt)]};
                case 10
                    pNames(i) = {['T2 Imag Act._' num2str(cnt)]};
            end
            if cnt == nLN2; cnt=0; cnt2=cnt2+1; end
        case 15
            switch cnt2
                case 1
                    pNames(i) = {['dGam1_' num2str(cnt)]};
                case 2
                    pNames(i) = {['iGam1_' num2str(cnt)]};
                case 3
                    pNames(i) = {['dPsi1_' num2str(cnt)]};
                case 4
                    pNames(i) = {['dPhi1_' num2str(cnt)]};
                case 5
                    pNames(i) = {['dGam2_' num2str(cnt)]};
                case 6
                    pNames(i) = {['iGam2_' num2str(cnt)]};
                case 7
                    pNames(i) = {['dPsi2_' num2str(cnt)]};
                case 8
                    pNames(i) = {['dPhi2_' num2str(cnt)]};
            end
            if cnt == nCC2; cnt=0; cnt2=cnt2+1; end
    end
end