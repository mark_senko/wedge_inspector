function [G] = flat_gauss(A,L,L0,s,f)
% Flat peaked gaussian function to fit small LVF RMSE spikes

% This version subtracts 2 Gaussian peaks
% G = (A/(1-1/f)).*(exp(-(L-L0).^2./(2*s^2))-(1/f).*exp(-(L-L0).^2./(2/f*s^2)));

% This version uses the error function
G = 0.5/erf(f/2)*A.*(erf((L-L0+f)./s) - erf((L-L0-f)./s));


end

