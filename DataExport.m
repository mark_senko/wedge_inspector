function varargout = DataExport(varargin)
% DATAEXPORT MATLAB code for DataExport.fig
%      DATAEXPORT, by itself, creates a new DATAEXPORT or raises the existing
%      singleton*.
%
%      H = DATAEXPORT returns the handle to a new DATAEXPORT or the handle to
%      the existing singleton*.
%
%      DATAEXPORT('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in DATAEXPORT.M with the given input arguments.
%
%      DATAEXPORT('Property','Value',...) creates a new DATAEXPORT or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before DataExport_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to DataExport_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help DataExport

% Last Modified by GUIDE v2.5 25-Jan-2021 20:03:21

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @DataExport_OpeningFcn, ...
                   'gui_OutputFcn',  @DataExport_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before DataExport is made visible.
function DataExport_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to DataExport (see VARARGIN)

% Choose default command line output for DataExport
handles.output = hObject;

% ok now start adding to it?
handles.settings.exportgo = false;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes DataExport wait for user response (see UIRESUME)
uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = DataExport_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.settings;
guidata(hObject,handles);
figure1_CloseRequestFcn(hObject, eventdata, handles);


% --- Executes on selection change in data_type_select.
function data_type_select_Callback(hObject, eventdata, handles)
% hObject    handle to data_type_select (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns data_type_select contents as cell array
%        contents{get(hObject,'Value')} returns selected item from data_type_select
handles.settings.datatype = get(hObject,'Value');
guidata(hObject, handles);


% --- Executes during object creation, after setting all properties.
function data_type_select_CreateFcn(hObject, eventdata, handles)
% hObject    handle to data_type_select (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in AOI_select.
function AOI_select_Callback(hObject, eventdata, handles)
% hObject    handle to AOI_select (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns AOI_select contents as cell array
%        contents{get(hObject,'Value')} returns selected item from AOI_select
handles.settings.AOItype = get(hObject,'Value');
if get(hObject,'Value')==3
    handles.export_AOI.Enable = 'on';
    handles.text7.Enable = 'on';
else
    handles.export_AOI.Enable = 'off';
    handles.text7.Enable = 'off';
end
guidata(hObject, handles);


% --- Executes during object creation, after setting all properties.
function AOI_select_CreateFcn(hObject, eventdata, handles)
% hObject    handle to AOI_select (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in export_go.
function export_go_Callback(hObject, eventdata, handles)
% hObject    handle to export_go (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
handles.settings.path = handles.export_folder.String;
fileseed = handles.file_seed.String;
if contains(fileseed,'date')
    d = datetime;
    formout = 'yyyy-mm-ddTHHMM';
    dstr = datestr(d,formout);
    fileseed = replace(fileseed,'date',dstr);
end
handles.settings.fileseed = fileseed;
handles.settings.AOItype = handles.AOI_select.Value;
handles.settings.overrideAOI = str2double(handles.export_AOI.String);
handles.settings.datatype = handles.data_type_select.Value;
handles.settings.exportgo = true;
guidata(hObject, handles);
close(handles.figure1);


function export_folder_Callback(hObject, eventdata, handles)
% hObject    handle to export_folder (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of export_folder as text
%        str2double(get(hObject,'String')) returns contents of export_folder as a double


% --- Executes during object creation, after setting all properties.
function export_folder_CreateFcn(hObject, eventdata, handles)
% hObject    handle to export_folder (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in get_folder.
function get_folder_Callback(hObject, eventdata, handles)
% hObject    handle to get_folder (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
fldr = uigetdir([getenv('USERPROFILE') '\Desktop\'],'Select Data Export Folder:');
if ~fldr; return; end
set(handles.export_folder,'String',fldr);
guidata(hObject, handles);


function file_seed_Callback(hObject, eventdata, handles)
% hObject    handle to file_seed (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of file_seed as text
%        str2double(get(hObject,'String')) returns contents of file_seed as a double


% --- Executes during object creation, after setting all properties.
function file_seed_CreateFcn(hObject, eventdata, handles)
% hObject    handle to file_seed (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes when user attempts to close figure1.
function figure1_CloseRequestFcn(hObject, eventdata, handles)
% hObject    handle to figure1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: delete(hObject) closes the figure
% delete(hObject);
if isequal(get(hObject, 'waitstatus'), 'waiting')
    % The GUI is still in UIWAIT, call UIRESUME
    uiresume(hObject);
else
    % The GUI is no longer waiting, just close it
    delete(hObject);
end



function export_AOI_Callback(hObject, eventdata, handles)
% hObject    handle to export_AOI (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of export_AOI as text
%        str2double(get(hObject,'String')) returns contents of export_AOI as a double


% --- Executes during object creation, after setting all properties.
function export_AOI_CreateFcn(hObject, eventdata, handles)
% hObject    handle to export_AOI (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
