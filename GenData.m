% Generate Data
% AGB 2021

%% ask for new parameters?
output = genquestions();
if output.overwave
    handles.L = linspace(output.overwavemin,output.overwavemax,output.overwavenum).';
end
if output.overthk
    numt = output.overthknum;
    thks = linspace(output.overthkmin,output.overthkmax,numt).';
    data = cell(numt,3);
    data(:,1) = num2cell(true(numt,1));
    data(:,3) = num2cell(thks); 
    handles.uitable1.ColumnName = {' ','File','Thk'};
    handles.uitable1.Data = data;
    handles.Y = zeros(length(handles.L),22,numt);
end
tab2data = handles.uitable2.Data;
tab2data{1,6} = min(handles.L);
tab2data{2,6} = max(handles.L);
handles.uitable2.Data = tab2data;
guidata(hObject, handles);

% Setup Simulation
SimSetup;

%% Perform the simulation for each exp file loaded
figh = handles.figh;
axh = handles.axh;
set(handles.abortbutton,'visible','on');
Y = zeros(length(L),22,NumF);
SigY = Y; SigY(:,1:19,:) = 0.001;
for nF = 1:NumF
    % Simulate
    Thk(nF);
    ToolParms.m11s = 1;
    ToolParms.mode = 4;
%     ToolParms.WD = handles.CEcal.WD;
    [Ysim,~] = IntegrateSpectrum([ThkEMA Thk(nF)], NK, ToolParms);
    
    % add white noise
    if output.noise
        Ysim(:,1:19) = Ysim(:,1:19) + (output.whitenoise).*randn(size(Ysim(:,1:19)));
        m11zc = Ysim(:,1);
        iz = (m11zc < 0);
        m11zc(iz) = (-1).*m11zc(iz);
    end
    
    % move m11 to uT and normalize
    Ysim(:,22) = Ysim(:,1).*50;
    Ysim(:,1:19) = Ysim(:,1:19)./repmat(Ysim(:,1),[1 19]);
    
    % Create Psi/Delta from NCS
    [Ysim(:,20:21),SigY(:,20:21)] = NCStoPD(Ysim(:,17:19),SigY(:,17:19));
    
    % Reconfigure to appropriate L
%     Ysim = transpose(spline(Ls,Ysim.',L));
    
    % Save to Y internal
    Y(:,:,nF) = Ysim;
    
    % Graph via WIgrapher
    [figh,axh] = WIgrapher(figh,axh,L,Ysim(:,1:16),1,[1 0 0]);
    
    % Update wait bar
    upwait_v2(1,handles.axes1,['Generating File ' num2str(nF)],...
              nF/NumF,handles.axes2,[num2str(nF-1) ' of ' num2str(NumF) ' files completed']);
    
    % Stop if necessary
    handles = guidata(hObject);
    if handles.STOP == 1
        handles.STOP = 0;
        set(handles.abortbutton,'visible','off');
        guidata(hObject, handles);
        return;
    end
end

%% Wrap up

% Update wait bar
upwait_v2('c',handles.axes1,' ',...
          'c',handles.axes2,' ');
set(handles.abortbutton,'visible','off');

% update handles
handles.Y(:,:,iSelect) = Y;
handles.SigY(:,:,iSelect) = SigY;
handles.axh = axh;
handles.figh = figh;
% handles.iSelect = iSelect;

% update uitable1
Data = get(handles.uitable1,'data');
NewData = cell(sum(iSelect),3);
for i=1:sum(iSelect)
    NewData(i,2) = {['Gen_Data_' num2str(i)]};
end
NewData(:,1) = Data(iSelect,1);
NewData(:,3) = Data(iSelect,3);
ColNames = {' ','File','Thk'};
set(handles.uitable1,'ColumnName',ColNames);
Data=Data(:,1:3);
Data(iSelect,:) = NewData;
set(handles.uitable1,'data',Data);