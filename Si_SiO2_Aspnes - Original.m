function [SiNK,EMANK,SiO2NK] = Si_SiO2_Aspnes(Lh,SiDat,SiO2Dat)
% Returns N and K values based on Aspnes and Ospal's critical point model
% for the SiO2 on EMA on Si structure. The EMA is calculated as a Maxwell
% inclusion.

SiParms = cell2mat(SiDat(:,3));
SiO2Parms = cell2mat(SiO2Dat(:,3));

% CP model for Si
SiNK = (Lh - Lh);
EVs = 1240./Lh;
% DO MANUALLY, for loop is too slow
% SiNK = SiNK + SiParms(3+j*4)*exp(1i*pi*SiParms(5+j*4))./(1-(EVs./SiParms(2+j*4) + 1i*SiParms(4+j*4)).^2);
SiNK = SiNK + SiParms(4)*exp(1i*pi*SiParms(6))./(1-(EVs./SiParms(3) + 1i*SiParms(5)).^2)...
     + SiParms(8)*exp(1i*pi*SiParms(10))./(1-(EVs./SiParms(7) + 1i*SiParms(9)).^2)...
     + SiParms(12)*exp(1i*pi*SiParms(14))./(1-(EVs./SiParms(11) + 1i*SiParms(13)).^2)...
     + SiParms(16)*exp(1i*pi*SiParms(18))./(1-(EVs./SiParms(15) + 1i*SiParms(17)).^2)...
     + SiParms(20)*exp(1i*pi*SiParms(22))./(1-(EVs./SiParms(19) + 1i*SiParms(21)).^2);
SiNK = (SiNK + SiParms(1) + SiParms(2)*1i).^0.5;
SiNK = SiNK + 0.18271 + 0.33103845i;

% Pole Model for SiO2
% SiO2NK = SiO2Parms(1) + SiO2Parms(2)./Lh.^2 + SiO2Parms(3)./Lh.^4; 
SiO2NK = SiO2Parms(1) + SiO2Parms(2)./(-EVs.^2) + SiO2Parms(3)./(11^2-EVs.^2);
SiO2NK = SiO2NK.*(2*SiO2Parms(5).*(1-SiO2NK)+1+2.*SiO2NK)./(2.*SiO2NK+1+SiO2Parms(5).*(SiO2NK-1));
SiO2NK = SiO2NK.^(0.5);

% Maxwell EMA
SiEps = SiNK.^2;
SiO2Eps = SiO2NK.^2;
EMAEps = SiEps.*(2*SiO2Parms(4).*(SiO2Eps-SiEps)+SiO2Eps+2.*SiEps)./(2.*SiEps+SiO2Eps+SiO2Parms(4).*(SiEps-SiO2Eps));
EMANK = ((abs(EMAEps)+real(EMAEps))./2).^0.5 + 1i*((abs(EMAEps)-real(EMAEps))./2).^0.5;

% Pedro uses reverse notation
SiNK = conj(SiNK);
EMANK = conj(EMANK);
SiO2NK = conj(SiO2NK);
end

