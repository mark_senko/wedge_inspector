function [lvf_c,lvf_a] = findlvf(Ls,rmse_l)
% find lvf center frequency
% use new dual erf method & start at 300 nm. 3/19/2019 AGB

iL = (Ls > 300) & (Ls < 510);
L = Ls(iL);
rmse_l = rmse_l(iL);

%% Round 1, estimate peak

dfnc = @(P) (P(3)+P(2).*exp(-L./(P(1)*100))-rmse_l);
   
guess = [0.5,0.01,0.001]';
LL = [0,1e-4,1e-8]';
UL = [0,1,1]';
   
[P,~] = LbMqBASICv5_new(dfnc,guess,LL,UL,1e-8,1e-8,0.1,10,[]);

rmse_r = rmse_l-(P(3)+P(2).*exp(-L/(P(1)*100)));
[pmax,ic] = max(rmse_r);
lvf_c = L(ic);
if pmax < std(rmse_r)*3 % test to see if peak is too small to fit
    lvf_a = false;
    return;
else
    lvf_a = true;
end    

%% Round 2, fit for peak

% dfnc = @(P) (P(6)+P(5).*exp(-L/(P(4)*100)) + P(1).*exp(-(L-P(2)*1000).^2./(P(3)*1000))-rmse_l);
% dfnc = @(P) (P(6)+P(5).*exp(-L/(P(4)*100)) + (P(1)*P(3)^2)./((L-P(2)*1000).^2+P(3)^2)-rmse_l);
dfnc = @(P) (P(8)+P(7).*exp(-L/(P(6)*100)) + 0.5/erf(P(3)/2)*P(1).*(erf((L-P(2)+P(3))./P(4)) - erf((L-P(2)-P(3))./P(5))));
dfnc2 = @(P) sum((P(8)+P(7).*exp(-L/(P(6)*100)) + 0.5/erf(P(3)/2)*P(1).*(erf((L-P(2)+P(3))./P(4)) - erf((L-P(2)-P(3))./P(5))) -rmse_l).^2);   

guess = [0.01 ,lvf_c,15  ,5,8,P']';
LL =    [1e-5 ,5    ,1  ,2,2,0,1e-4,1e-8]';
UL =    [1    ,5    ,1  ,2,2,0,1   ,1   ]';

% plot guess
% figure('windowstyle','docked');
% plot(L,rmse_l,L,dfnc(guess));

% P = guess;
% [P,~] = LbMqBASICv5_new(dfnc,guess,LL,UL,1e-8,1e-8,0.2,100,[]);
P = fminsearch(dfnc2,guess);

lvf_c = P(2);

%% display results
figure('windowstyle','docked','Name','LVF Position Fit');
% ft = P(6)+P(5).*exp(-L/(P(4)*100)) + P(1).*exp(-(L-P(2)*1000).^2./(P(3)*1000));
% ft = P(6)+P(5).*exp(-L/(P(4)*100)) + (P(1)*P(3)^2)./((L-P(2)*1000).^2+P(3)^2);
ft = dfnc(P);
plot(L,rmse_l,L,ft);
xlabel('Wavelength [nm]');
ylabel('RMSE');
legend('RMSE','LVF Fit');
% disp(P);

end

