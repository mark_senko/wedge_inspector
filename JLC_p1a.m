% Jon's Lens calibration phase 1a

% turn on m11 denormalization
handles.CtrlData.m11s = true;
% get table 2 and store in tabdata
tabdata = get(handles.uitable2,'data');
% set new wavelengths 420 to 990
tabdata(1:2,6) = {420;990};
% set new wave-cal numbers for vis
tabdata(6,8:10) = {130,120,60};
% set fitting parameters (Thk, AOI, NA, W-vis)
F = {' ',' ',... % Lmin & Lmax
    true,...     % AOI
    true,...     % NA
    false,...    % BW
    true,...     % W-vis
    false,...    % W-ir
    true,...     % Thickness
    false,...    % EMA
    false,...    % alpha (SiO2 aniso)
    false,...    % beta (Si aniso)
    false,...    % Bm1
    false,...    % Bm2
    false,...    % Bm3
    false,...    % Bm4
    false,...    % Bm5
    false,...    % NCS Ret.
    false,...    % NCS Att.
    false,...    % T1 Ret.
    false,...    % T1 Att.
    false,...    % T1 Real O.A.
    false,...    % T1 Imag O.A.
    false,...    % T2 Ret.
    false,...    % T2 Att.
    false,...    % T2 Real O.A.
    false};      % T2 Imag O.A.
tabdata(:,1) = F; 

% update tabdata
set(handles.uitable2,'data',tabdata);
guidata(hObject, handles);

% Subselect data
SubSelect(handles.uitable1,'4');
hGuiFig = findobj('Tag','subselectfig','Type','figure');
if ~isempty(hGuiFig)
    % get the handles
    subhands = guidata(hGuiFig);
    % call the pushbutton1 callback using the name of the GUI
    SubSelect('pushbutton1_Callback',subhands.pushbutton1,[],subhands);
end

% Parallel Fit
ParallelFit;
if op.iterations == -1; return; end

% Copy results over
GUI_WedgeDataInspector('CopySim_Callback',handles.CopySim,[],handles);