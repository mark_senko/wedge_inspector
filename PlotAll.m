% Plot All

% Plots everything from the W.I. plot menu starting in version 4.3a+.

% Version 2
% -> Uses SimSetup.m to reduce redundant editing when a change is needed
% -> SimSpectra, SerialFit, and ParallelFit now store results in
%    handles.results
% -> Compatible with WVASE32 style standard deviation RMSE calcualtion
% -> Uses new MSEcalc_v2.m file for rapid RMSE calculation

% AGB 2021

%% SimSetup 
% (gathers most variables for plotting, reduces redundant editing)
SimSetup;

%% Gather data for plotting
% Need to override certain variables from SimSetup.

% get files selected
tabdata1 = get(handles.uitable1,'data');
iSelect = cell2mat(tabdata1(:,1));
if sum(iSelect)==0
    msgbox('Select some files first.')
    return
end
Thk = tabdata1(iSelect,3);
NumF = sum(iSelect);

% check if some Thk are missing
for nF = 1:NumF
    if isempty(Thk{nF})
        msgbox('Thk inputs are missing.')
        return
    end
end
Thk = cell2mat(Thk);

% grab data
err = cell2mat(tabdata1(:,end));
Y = handles.Y;
L = handles.L;
Ye = handles.results.Ye;
Ys = handles.results.Ys;
Ls = handles.results.Ls;
Ye2 = permute(Ye,[2 1 3]);
Ys2 = permute(Ys,[2 1 3]);

% Sigma setup
JAWmse = handles.CtrlData.JAWmse;
if JAWmse==2
    if handles.CtrlData.mode == 3
        SigY = handles.SigY(iW,1:16,iSelect);
    elseif handles.CtrlData.mode == 2
        SigN = sqrt(handles.SigY(iW,2,iSelect).^2 + handles.SigY(iW,5,iSelect).^2)./2;
        SigC = sqrt(handles.SigY(iW,11,iSelect).^2 + handles.SigY(iW,16,iSelect).^2)./2;
        SigS = sqrt(handles.SigY(iW,15,iSelect).^2 + handles.SigY(iW,12,iSelect).^2)./2;
        SigY = [SigN SigC SigS];
    else
        SigY = handles.SigY(iW,17:19,iSelect);
    end
else
    SigY = Ye-Ye+0.001;
end
SigY2 = permute(SigY,[2 1 3]);


MSE = handles.results.MSE;
Nparms = handles.results.Nparms;
MSEmode = handles.CtrlData.JAWmse;
NumL = length(Ls);
NumF = size(Ye,3);
[WVLS,THKS] = meshgrid(Ls,Thk);
tabdata2 = get(handles.uitable2,'data');
% Lmin = tabdata2{1,6};
% Lmax = tabdata2{2,6};
% iL = (L>=Lmin) & (L<=Lmax);

% Special selection vectors
iOffDBs = logical([0 0 1 1 0 0 1 1 1 1 0 0 1 1 0 0]);
iOnDBs  = ~iOffDBs;

%% Go through each potential plot, and if enabled create plot

% MM RMSE plotting
if handles.plots.allMM
    figure('windowstyle','docked');
    rmse = MSEcalc_v2(MSEmode,0,Ye,Ys,SigY,Nparms);
    dYs = MSEcalc_v2(MSEmode,1,Ye,Ys,SigY,Nparms);
    plot(Ls,dYs);
    title(['RMSE by Wvl for all Thks: ' num2str(rmse)]);
    if MSEmode == 1 % Don't plot spec line if not in right mode
        hold on
        mmL =   [195;   203;   209;    240;   360;    361;   405;   406;    458;   1000;  1650];
        mmUCL = [0.009; 0.009; 0.0051; 0.003; 0.0019; 0.003; 0.003; 0.0015; 0.001; 0.001; 0.0013];
        mmUCL = interp1(mmL,mmUCL,Ls);
        plot(Ls,mmUCL);
        legend('MM RMSE','Atlas V Spec. Line');
    else
        legend('MM RMSE');
    end
    grid on; box on;
    xlabel('Wavelength [nm]');
end    
if handles.plots.offDiags
    figure('windowstyle','docked');
    rmse  = MSEcalc_v2(MSEmode,0,Ye(:,iOffDBs,:),Ys(:,iOffDBs,:),SigY(:,iOffDBs,:),Nparms);
    rmses = MSEcalc_v2(MSEmode,1,Ye(:,iOffDBs,:),Ys(:,iOffDBs,:),SigY(:,iOffDBs,:),Nparms);
    plot(Ls,rmses);
    title(['MM Off-diagonal Block: ' num2str(rmse)]);
    grid on; box on;
    xlabel('Wavelength [nm]');
end
if handles.plots.offDiagsMax
    figure('windowstyle','docked');
    % m13 + m31
    Ne=(Ye(:,3,:)+Ye(:,9,:));
    Ns=(Ys(:,3,:)+Ys(:,9,:));
    Se=sqrt(SigY(:,3,:).^2+SigY(:,9,:).^2);
    dNs = MSEcalc_v2(MSEmode,1,Ne,Ns,Se,Nparms);
    rmse_odc = dNs;
    % m23 + m32
    Ne=(Ye(:,7,:)+Ye(:,10,:));
    Ns=(Ys(:,7,:)+Ys(:,10,:));
    Se=sqrt(SigY(:,7,:).^2+SigY(:,10,:).^2);
    dCs = MSEcalc_v2(MSEmode,1,Ne,Ns,Se,Nparms);
    rmse_odc = max([rmse_odc dCs],[],2);
    % m14 - m41
    Ne=(Ye(:,4,:)-Ye(:,13,:));
    Ns=(Ys(:,4,:)-Ys(:,13,:));
    Se=sqrt(SigY(:,4,:).^2+SigY(:,13,:).^2);
    dSs = MSEcalc_v2(MSEmode,1,Ne,Ns,Se,Nparms);
    rmse_odc = max([rmse_odc dSs],[],2);
    % m24 - m42
    Ne=(Ye(:,8,:)-Ye(:,14,:));
    Ns=(Ys(:,8,:)-Ys(:,14,:));
    Se=sqrt(SigY(:,8,:).^2+SigY(:,14,:).^2);
    dSs = MSEcalc_v2(MSEmode,1,Ne,Ns,Se,Nparms);
    rmse_odc = max([rmse_odc dSs],[],2);
    plot(Ls,rmse_odc);
    title('MM Off-diagonal Block Combos, Maximum');
    grid on; box on;
    xlabel('Wavelength [nm]');
    if MSEmode == 1
        hold on
        odL = [195;400;400.1;1700];
        odUCL = [0.004;0.004;0.0008;0.0008];
        odUCL = interp1(odL,odUCL,Ls);
        plot(Ls,odUCL);
        legend('Off-diag MM Combo Max RMSE','Atlas V Spec. Line');
    else
        legend('Off-diag MM Combo Max RMSE');
    end
end
if handles.plots.onDiags
    figure('windowstyle','docked');
    rmse = MSEcalc_v2(MSEmode,0,Ye(:,iOnDBs,:),Ys(:,iOnDBs,:),SigY(:,iOnDBs,:),Nparms);
%     dYs  = ((sum(sum((Ye(:,iOnDBs,:)-Ys(:,iOnDBs,:)).^2,2),3))./(NumF*8)).^0.5;
    dYs  = MSEcalc_v2(MSEmode,1,Ye(:,iOnDBs,:),Ys(:,iOnDBs,:),SigY(:,iOnDBs,:),Nparms);
    plot(Ls,dYs);
    title(['MM On-diagonal Block: ' num2str(rmse)]);
    grid on; box on;
    xlabel('Wavelength [nm]');
end
if handles.plots.depolind
    DI = sqrt(sum(Ye.^2,2)-Ye(:,1,:).^2)./(sqrt(3)*Ye(:,1,:));
    DI = mean(DI,3);
    figure('windowstyle','docked');
    plot(Ls,DI);
    grid on; box on;
    xlabel('Wavelength [nm]');
    ylabel('Average Polarization Index');
end
if handles.plots.MMthks
    figure('windowstyle','docked');
%     rmse = (sum((err.^2)*16*NumL)/(16*NumL*NumF))^0.5;
%     rmse = MSEcalc_v2(MSEmode,1,Ye2,Ys2,SigY2,Nparms);
    plot(Thk,err);
    title('MM RMSE vs SiO2 Thickness');
    grid on; box on;
    xlabel('SiO2 Thickness [nm]');
end
if handles.plots.m12
    figure('windowstyle','docked');
    rmse = MSEcalc_v2(MSEmode,0,Ye(:,2,:),Ys(:,2,:),SigY(:,2,:),Nparms);
    dYs  = MSEcalc_v2(MSEmode,1,Ye(:,2,:),Ys(:,2,:),SigY(:,2,:),Nparms);
    plot(Ls,dYs);
    title(['m12 RMSE: ' num2str(rmse)]);
    grid on; box on;
    xlabel('Wavelength [nm]');
end
if handles.plots.m13
    figure('windowstyle','docked');
    rmse = MSEcalc_v2(MSEmode,0,Ye(:,3,:),Ys(:,3,:),SigY(:,3,:),Nparms);
    dYs  = MSEcalc_v2(MSEmode,1,Ye(:,3,:),Ys(:,3,:),SigY(:,3,:),Nparms);
    plot(Ls,dYs);
    title(['m13 RMSE: ' num2str(rmse)]);
    grid on; box on;
    xlabel('Wavelength [nm]');
end
if handles.plots.m14
    figure('windowstyle','docked');
    rmse = MSEcalc_v2(MSEmode,0,Ye(:,4,:),Ys(:,4,:),SigY(:,4,:),Nparms);
    dYs  = MSEcalc_v2(MSEmode,1,Ye(:,4,:),Ys(:,4,:),SigY(:,4,:),Nparms);
    plot(Ls,dYs);
    title(['m14 RMSE: ' num2str(rmse)]);
    grid on; box on;
    xlabel('Wavelength [nm]');
end
if handles.plots.m21
    figure('windowstyle','docked');
    rmse = MSEcalc_v2(MSEmode,0,Ye(:,5,:),Ys(:,5,:),SigY(:,5,:),Nparms);
    dYs  = MSEcalc_v2(MSEmode,1,Ye(:,5,:),Ys(:,5,:),SigY(:,5,:),Nparms);
    plot(Ls,dYs);
    title(['m21 RMSE: ' num2str(rmse)]);
    grid on; box on;
    xlabel('Wavelength [nm]');
end
if handles.plots.m22
    figure('windowstyle','docked');
    rmse = MSEcalc_v2(MSEmode,0,Ye(:,6,:),Ys(:,6,:),SigY(:,6,:),Nparms);
    dYs  = MSEcalc_v2(MSEmode,1,Ye(:,6,:),Ys(:,6,:),SigY(:,6,:),Nparms);
    plot(Ls,dYs);
    title(['m22 RMSE: ' num2str(rmse)]);
    grid on; box on;
    xlabel('Wavelength [nm]');
end
if handles.plots.m23
    figure('windowstyle','docked');
    rmse = MSEcalc_v2(MSEmode,0,Ye(:,7,:),Ys(:,7,:),SigY(:,7,:),Nparms);
    dYs  = MSEcalc_v2(MSEmode,1,Ye(:,7,:),Ys(:,7,:),SigY(:,7,:),Nparms);
    plot(Ls,dYs);
    title(['m23 RMSE: ' num2str(rmse)]);
    grid on; box on;
    xlabel('Wavelength [nm]');
end
if handles.plots.m24
    figure('windowstyle','docked');
    rmse = MSEcalc_v2(MSEmode,0,Ye(:,8,:),Ys(:,8,:),SigY(:,8,:),Nparms);
    dYs  = MSEcalc_v2(MSEmode,1,Ye(:,8,:),Ys(:,8,:),SigY(:,8,:),Nparms);
    plot(Ls,dYs);
    title(['m24 RMSE: ' num2str(rmse)]);
    grid on; box on;
    xlabel('Wavelength [nm]');
end
if handles.plots.m31
    figure('windowstyle','docked');
    rmse = MSEcalc_v2(MSEmode,0,Ye(:,9,:),Ys(:,9,:),SigY(:,9,:),Nparms);
    dYs  = MSEcalc_v2(MSEmode,1,Ye(:,9,:),Ys(:,9,:),SigY(:,9,:),Nparms);
    plot(Ls,dYs);
    title(['m31 RMSE: ' num2str(rmse)]);
    grid on; box on;
    xlabel('Wavelength [nm]');
end
if handles.plots.m32
    figure('windowstyle','docked');
    rmse = MSEcalc_v2(MSEmode,0,Ye(:,10,:),Ys(:,10,:),SigY(:,10,:),Nparms);
    dYs  = MSEcalc_v2(MSEmode,1,Ye(:,10,:),Ys(:,10,:),SigY(:,10,:),Nparms);
    plot(Ls,dYs);
    title(['m32 RMSE: ' num2str(rmse)]);
    grid on; box on;
    xlabel('Wavelength [nm]');
end
if handles.plots.m33
    figure('windowstyle','docked');
    rmse = MSEcalc_v2(MSEmode,0,Ye(:,11,:),Ys(:,11,:),SigY(:,11,:),Nparms);
    dYs  = MSEcalc_v2(MSEmode,1,Ye(:,11,:),Ys(:,11,:),SigY(:,11,:),Nparms);
    plot(Ls,dYs);
    title(['m33 RMSE: ' num2str(rmse)]);
    grid on; box on;
    xlabel('Wavelength [nm]');
end
if handles.plots.m34
    figure('windowstyle','docked');
    rmse = MSEcalc_v2(MSEmode,0,Ye(:,12,:),Ys(:,12,:),SigY(:,12,:),Nparms);
    dYs  = MSEcalc_v2(MSEmode,1,Ye(:,12,:),Ys(:,12,:),SigY(:,12,:),Nparms);
    plot(Ls,dYs);
    title(['m34 RMSE: ' num2str(rmse)]);
    grid on; box on;
    xlabel('Wavelength [nm]');
end    
if handles.plots.m41
    figure('windowstyle','docked');
    rmse = MSEcalc_v2(MSEmode,0,Ye(:,13,:),Ys(:,13,:),SigY(:,13,:),Nparms);
    dYs  = MSEcalc_v2(MSEmode,1,Ye(:,13,:),Ys(:,13,:),SigY(:,13,:),Nparms);
    plot(Ls,dYs);
    title(['m41 RMSE: ' num2str(rmse)]);
    grid on; box on;
    xlabel('Wavelength [nm]');
end
if handles.plots.m42
    figure('windowstyle','docked');
    rmse = MSEcalc_v2(MSEmode,0,Ye(:,14,:),Ys(:,14,:),SigY(:,14,:),Nparms);
    dYs  = MSEcalc_v2(MSEmode,1,Ye(:,14,:),Ys(:,14,:),SigY(:,14,:),Nparms);
    plot(Ls,dYs);
    title(['m42 RMSE: ' num2str(rmse)]);
    grid on; box on;
    xlabel('Wavelength [nm]');
end
if handles.plots.m43
    figure('windowstyle','docked');
    rmse = MSEcalc_v2(MSEmode,0,Ye(:,15,:),Ys(:,15,:),SigY(:,15,:),Nparms);
    dYs  = MSEcalc_v2(MSEmode,1,Ye(:,15,:),Ys(:,15,:),SigY(:,15,:),Nparms);
    plot(Ls,dYs);
    title(['m43 RMSE: ' num2str(rmse)]);
    grid on; box on;
    xlabel('Wavelength [nm]');
end
if handles.plots.m44
    figure('windowstyle','docked');
    rmse = MSEcalc_v2(MSEmode,0,Ye(:,16,:),Ys(:,16,:),SigY(:,16,:),Nparms);
    dYs  = MSEcalc_v2(MSEmode,1,Ye(:,16,:),Ys(:,16,:),SigY(:,16,:),Nparms);
    plot(Ls,dYs);
    title(['m44 RMSE: ' num2str(rmse)]);
    grid on; box on;
    xlabel('Wavelength [nm]');
end
if handles.plots.m14m41
    figure('windowstyle','docked');
    Ne=(Ye(:,4,:)-Ye(:,13,:));
    Ns=(Ys(:,4,:)-Ys(:,13,:));
%     dYs = (sum((Ne-Ns).^2,3)./(NumF*2)).^0.5;
    Se=sqrt(SigY(:,4,:).^2+SigY(:,13,:).^2);
    dYs = MSEcalc_v2(MSEmode,1,Ne,Ns,Se,Nparms);
    plot(Ls,dYs);
    title('m14 - m41');
    grid on; box on;
    xlabel('Wavelength [nm]');
end
if handles.plots.m13m31
    figure('windowstyle','docked');
    Ne=(Ye(:,3,:)+Ye(:,9,:));
    Ns=(Ys(:,3,:)+Ys(:,9,:));
%     dYs = (sum((Ne-Ns).^2,3)./(NumF*2)).^0.5;
    Se=sqrt(SigY(:,3,:).^2+SigY(:,9,:).^2);
    dYs = MSEcalc_v2(MSEmode,1,Ne,Ns,Se,Nparms);
    plot(Ls,dYs);
    title('m13 + m31');
    grid on; box on;
    xlabel('Wavelength [nm]');
end
if handles.plots.m23m32
    figure('windowstyle','docked');
    Ne=(Ye(:,7,:)+Ye(:,10,:));
    Ns=(Ys(:,7,:)+Ys(:,10,:));
%     dYs = (sum((Ne-Ns).^2,3)./(NumF*2)).^0.5;
    Se=sqrt(SigY(:,7,:).^2+SigY(:,10,:).^2);
    dYs = MSEcalc_v2(MSEmode,1,Ne,Ns,Se,Nparms);
    plot(Ls,dYs);
    title('m23 + m32');
    grid on; box on;
    xlabel('Wavelength [nm]');
end
if handles.plots.m24m42
    figure('windowstyle','docked');
    Ne=(Ye(:,8,:)-Ye(:,14,:));
    Ns=(Ys(:,8,:)-Ys(:,14,:));
%     dYs = (sum((Ne-Ns).^2,3)./(NumF*2)).^0.5;
    Se=sqrt(SigY(:,8,:).^2+SigY(:,14,:).^2);
    dYs = MSEcalc_v2(MSEmode,1,Ne,Ns,Se,Nparms);
    plot(Ls,dYs);
    title('m24 - m42');
    grid on; box on;
    xlabel('Wavelength [nm]');
end

%% System parameter plotting
if handles.plots.AOI
    figure('windowstyle','docked');
    plot(Ls,AOIp.s);
    title('AOI');
    grid on; box on;
    xlabel('Wavelength [nm]');
end    
if handles.plots.NA
    figure('windowstyle','docked');
    plot(Ls,NAp.s);
    title('NA');
    grid on; box on;
    xlabel('Wavelength [nm]');
end

%% Channel to wavelength plotting
if handles.plots.visWvls
    Lc = handles.CEcal.Wcal.Lc_Vis;
    iUV = Ls <= 1000;
    L_UV = (Ls(iUV)-Lc)./1000;
    Cin = cell2mat(tabdata2(6,6:10));
    Px = (Cin(1) + L_UV.*(Cin(2) + L_UV.*(Cin(3) + L_UV.*(Cin(4) + L_UV.*Cin(5)))))';
    figure('Windowstyle','docked');
    plot(Ls(iUV),Px);
    ylabel('JAW Visible Channels (Pixels)');
    xlabel('Wavelength [nm]');
    box on; grid on;
end
if handles.plots.IRWvls
    Lc = handles.CEcal.Wcal.Lc_IR;
    iIR = Ls > 1000;
    L_IR = (Ls(iIR)-Lc)/1000;
    Cin = cell2mat(tabdata2(7,6:9));
    Px = (Cin(1) + L_IR.*(Cin(2) + L_IR.*(Cin(3) + L_IR.*(Cin(4)))))';
    figure('Windowstyle','docked');
    plot(Ls(iIR),Px);
    ylabel('JAW IR Channels (Pixels)');
    xlabel('Wavelength [nm]');
    box on; grid on;
end

%% NCS RMSE plotting
if handles.plots.allNCS
    if handles.CtrlData.mode ~= 3
        rmse = MSEcalc_v2(MSEmode,0,Ye,Ys,SigY,Nparms);
        dYs = MSEcalc_v2(MSEmode,1,Ye,Ys,SigY,Nparms);
        figure('windowstyle','docked');
        plot(Ls,dYs);
        grid on; box on;
        xlabel('Wavelength [nm]');
        title(['NCS RMSE by Wvl: ' num2str(rmse)]);
    else
        Ne=[(Ye(:,2,:)+Ye(:,5,:))./2, (Ye(:,11,:)+Ye(:,16,:))./2, (Ye(:,12,:)-Ye(:,15,:))./2];
        Ns=[(Ys(:,2,:)+Ys(:,5,:))./2, (Ys(:,11,:)+Ys(:,16,:))./2, (Ys(:,12,:)-Ys(:,15,:))./2];
        Se=sqrt([SigY(:,2,:).^2+SigY(:,5,:).^2 SigY(:,11,:).^2+SigY(:,16,:).^2 SigY(:,12,:).^2+SigY(:,15,:).^2]);
        rmse = MSEcalc_v2(MSEmode,0,Ne,Ns,Se,Nparms);
        dYs = MSEcalc_v2(MSEmode,1,Ne,Ns,Se,Nparms);
        figure('windowstyle','docked');
        plot(Ls,dYs);
        grid on; box on;
        xlabel('Wavelength [nm]');
        title(['NCS RMSE by Wvl: ' num2str(rmse)]);
    end
end
if handles.plots.Nonly
    if handles.CtrlData.mode ~= 3
%         dYs = ((sum((Ye(:,1,:)-Ys(:,1,:)).^2,3))./NumF).^0.5;
        dYs = MSEcalc_v2(MSEmode,1,Ye(:,1,:),Ys(:,1,:),SigY(:,1,:),Nparms);
        figure('windowstyle','docked');
        plot(Ls,dYs);
        grid on; box on;
        xlabel('Wavelength [nm]');
        title('N RMSE by Wvl');
    else
        Ne=(Ye(:,2,:)+Ye(:,5,:))./2;
        Ns=(Ys(:,2,:)+Ys(:,5,:))./2;
        Se=sqrt(SigY(:,2,:).^2+SigY(:,5,:).^2);
        dYs = MSEcalc_v2(MSEmode,1,Ne,Ns,Se,Nparms);
        figure('windowstyle','docked');
        plot(Ls,dYs);
        grid on; box on;
        xlabel('Wavelength [nm]');
        title('N RMSE by Wvl');
    end
end
if handles.plots.Conly
    if handles.CtrlData.mode ~= 3
        dYs = MSEcalc_v2(MSEmode,1,Ye(:,2,:),Ys(:,2,:),SigY(:,2,:),Nparms);
        figure('windowstyle','docked');
        plot(Ls,dYs);
        grid on; box on;
        xlabel('Wavelength [nm]');
        title('C RMSE by Wvl');
    else
        Ne=(Ye(:,11,:)+Ye(:,16,:))./2;
        Ns=(Ys(:,11,:)+Ys(:,16,:))./2;
        Se=sqrt(SigY(:,11,:).^2+SigY(:,16,:).^2);
        dYs = MSEcalc_v2(MSEmode,1,Ne,Ns,Se,Nparms);
        figure('windowstyle','docked');
        plot(Ls,dYs);
        grid on; box on;
        xlabel('Wavelength [nm]');
        title('C RMSE by Wvl');
    end
end
if handles.plots.Sonly
    if handles.CtrlData.mode ~= 3
        dYs = MSEcalc_v2(MSEmode,1,Ye(:,3,:),Ys(:,3,:),SigY(:,3,:),Nparms);
        figure('windowstyle','docked');
        plot(Ls,dYs);
        grid on; box on;
        xlabel('Wavelength [nm]');
        title('S RMSE by Wvl');
    else
        Ne=(Ye(:,12,:)-Ye(:,15,:))./2;
        Ns=(Ys(:,12,:)-Ys(:,15,:))./2;
        Se=sqrt(SigY(:,12,:).^2+SigY(:,15,:).^2);
        dYs = MSEcalc_v2(MSEmode,1,Ne,Ns,Se,Nparms);
        figure('windowstyle','docked');
        plot(Ls,dYs);
        grid on; box on;
        xlabel('Wavelength [nm]');
        title('S RMSE by Wvl');
    end
end
if handles.plots.NCSthks
    if handles.CtrlData.mode ~= 3
        rmse = MSEcalc_v2(MSEmode,0,Ye,Ys,SigY,Nparms);
        figure('windowstyle','docked');
        plot(Thk,MSE(iSelect));
        grid on; box on;
        xlabel('SiO2 Thickness [nm]');
        title(['NCS RMSE by Thk: ' num2str(rmse)]);
    else
        Ne=[(Ye(:,2,:)+Ye(:,5,:))./2, (Ye(:,11,:)+Ye(:,16,:))./2, (Ye(:,12,:)-Ye(:,15,:))./2];
        Ns=[(Ys(:,2,:)+Ys(:,5,:))./2, (Ys(:,11,:)+Ys(:,16,:))./2, (Ys(:,12,:)-Ys(:,15,:))./2];
        Se=sqrt([SigY(:,2,:).^2+SigY(:,5,:).^2 SigY(:,11,:).^2+SigY(:,16,:).^2 SigY(:,12,:).^2+SigY(:,15,:).^2]);
        Ne=permute(Ne,[3 1 2]);
        Ns=permute(Ns,[3 1 2]);
        Se=permute(Se,[3 1 2]);
        dYs  = MSEcalc_v2(MSEmode,1,Ne,Ns,Se,Nparms);
        rmse = MSEcalc_v2(MSEmode,0,Ne,Ns,Se,Nparms);
        figure('windowstyle','docked');
        plot(Thk,dYs);
        grid on; box on;
        xlabel('SiO2 Thickness [nm]');
        title(['NCS RMSE by Thk: ' num2str(rmse)]);
    end
end

%% Lens parameter plotting
% Grab all potential lens parameters
if  (handles.CtrlData.mode == 1)
    ind = 18;
    lensp = cell2mat(tabdata2(17:ind,6:(5+tabdata2{17,5})));
elseif (handles.CtrlData.NewLens == 0)
    ind = 20;
    lensp = cell2mat(tabdata2(17:ind,6:(5+tabdata2{17,5})));
else
    ind = 26;
    LNSp.val = cell2mat(tabdata2(17:ind,6:(5+tabdata2{17,5})));
    LNSp.typ = tabdata2(17:ind,3);
    LNSp.spt = cell2mat(tabdata2(17:ind,4));
    LNSp.LL = cell2mat(tabdata2(17:ind,16));
    LNSp.UL = cell2mat(tabdata2(17:ind,17));
    if (handles.CtrlData.mode ~= 1) && (handles.CtrlData.NewLens == 1)
        LNSp.s = zeros(10,NumL);
        for ii=1:10
            LNSp.s(ii,:) = CalcWvlDep(Ls,LNSp,ii,0);
        end
    end
end

%Update compensator parameters
if (handles.CtrlData.mode ~= 1) && (handles.CtrlData.CompCal == 1)
    CCp.s = zeros(16,NumL);
    for ii=1:16
        CCp.s(ii,:) = CalcWvlDep(Ls,CCp,ii,0);
    end
end

if handles.plots.DOs
    DO = [lensp(1,1:3) lensp(2,3)];
    DOvL = 500./Ls.*(DO(1)+DO(2).*(500./Ls).^2+DO(3).*(500./Ls).^4+DO(4).*(Ls./500).^2);
    figure('windowstyle','docked');
    plot(Ls,DOvL);
    grid on; box on;
    xlabel('Wavelength [nm]');
    title('Delta Offsets');
end
if handles.plots.WD1
    WD = [lensp(3,1:3); lensp(4,1:3)];
    Delo1 = (-500)./Ls.*(WD(1,1)+WD(1,2).*(500./Ls).^2+WD(1,3).*(500./Ls).^4);
    figure('windowstyle','docked');
    plot(Ls,Delo1);
    grid on; box on;
    xlabel('Wavelength [nm]');
    title('Window Dispersion 1');
end
if handles.plots.WD2
    WD = [lensp(3,1:3); lensp(4,1:3)];
    Delo2 = (500)./Ls.*(WD(2,1)+WD(2,2).*(500./Ls).^2+WD(2,3).*(500./Ls).^4);
    figure('windowstyle','docked');
    plot(Ls,Delo2);
    grid on; box on;
    xlabel('Wavelength [nm]');
    title('Window Dispersion 2');
end
if handles.plots.NCSret
    figure('windowstyle','docked');
    plot(Ls,LNSp.s(1,:));
    grid on; box on;
    xlabel('Wavelength [nm]');
    title('NCS Retardance');
end
if handles.plots.NCSatt
    figure('windowstyle','docked');
    plot(Ls,LNSp.s(2,:));
    grid on; box on;
    xlabel('Wavelength [nm]');
    title('NCS Attenuation');
end
if handles.plots.T1ret
    figure('windowstyle','docked');
    plot(Ls,LNSp.s(3,:));
    grid on; box on;
    xlabel('Wavelength [nm]');
    title('T1 Retardance');
end
if handles.plots.T2ret
    figure('windowstyle','docked');
    plot(Ls,LNSp.s(7,:));
    grid on; box on;
    xlabel('Wavelength [nm]');
    title('T2 Retardance');
end
if handles.plots.T1att
    figure('windowstyle','docked');
    plot(Ls,LNSp.s(4,:));
    grid on; box on;
    xlabel('Wavelength [nm]');
    title('T1 Attenuation');
end
if handles.plots.T2att
    figure('windowstyle','docked');
    plot(Ls,LNSp.s(8,:));
    grid on; box on;
    xlabel('Wavelength [nm]');
    title('T2 Attenuation');
end
if handles.plots.T1reOA
    figure('windowstyle','docked');
    plot(Ls,LNSp.s(5,:));
    grid on; box on;
    xlabel('Wavelength [nm]');
    title('T1 Real Optical Activity');
end
if handles.plots.T2reOA
    figure('windowstyle','docked');
    plot(Ls,LNSp.s(9,:));
    grid on; box on;
    xlabel('Wavelength [nm]');
    title('T2 Real Optical Activity');
end
if handles.plots.T1imOA
    figure('windowstyle','docked');
    plot(Ls,LNSp.s(6,:));
    grid on; box on;
    xlabel('Wavelength [nm]');
    title('T1 Imag. Optical Activity');
end
if handles.plots.T2imOA
    figure('windowstyle','docked');
    plot(Ls,LNSp.s(10,:));
    grid on; box on;
    xlabel('Wavelength [nm]');
    title('T2 Imag. Optical Activity');
end
if handles.plots.dGam1
    figure('windowstyle','docked');
    plot(Ls,CCp.s(1,:));
    grid on; box on;
    xlabel('Wavelength [nm]');
    ylabel('dGam1');
    title('Compensator Correction');
end
if handles.plots.iGam1
    figure('windowstyle','docked');
    plot(Ls,CCp.s(2,:));
    grid on; box on;
    xlabel('Wavelength [nm]');
    ylabel('iGam1');
    title('Compensator Correction');
end
if handles.plots.dPsi1
    figure('windowstyle','docked');
    plot(Ls,CCp.s(3,:));
    grid on; box on;
    xlabel('Wavelength [nm]');
    ylabel('iPsi1');
    title('Compensator Correction');
end
if handles.plots.dPhi1
    figure('windowstyle','docked');
    plot(Ls,CCp.s(4,:));
    grid on; box on;
    xlabel('Wavelength [nm]');
    ylabel('dPhi1');
    title('Compensator Correction');
end
if handles.plots.dGam2
    figure('windowstyle','docked');
    plot(Ls,CCp.s(5,:));
    grid on; box on;
    xlabel('Wavelength [nm]');
    ylabel('dGam2');
    title('Compensator Correction');
end
if handles.plots.iGam2
    figure('windowstyle','docked');
    plot(Ls,CCp.s(6,:));
    grid on; box on;
    xlabel('Wavelength [nm]');
    ylabel('iGam2');
    title('Compensator Correction');
end
if handles.plots.dPsi2
    figure('windowstyle','docked');
    plot(Ls,CCp.s(7,:));
    grid on; box on;
    xlabel('Wavelength [nm]');
    ylabel('dPsi2');
    title('Compensator Correction');
end
if handles.plots.dPhi2
    figure('windowstyle','docked');
    plot(Ls,CCp.s(8,:));
    grid on; box on;
    xlabel('Wavelength [nm]');
    ylabel('dPhi2');
    title('Compensator Correction');
end
if handles.plots.rp11
    figure('windowstyle','docked');
    plot(Ls,CCp.s(9,:));
    grid on; box on;
    xlabel('Wavelength [nm]');
    ylabel('rp11');
    title('Compensator Correction');
end
if handles.plots.ip11
    figure('windowstyle','docked');
    plot(Ls,CCp.s(10,:));
    grid on; box on;
    xlabel('Wavelength [nm]');
    ylabel('ip11');
    title('Compensator Correction');
end
if handles.plots.rp12
    figure('windowstyle','docked');
    plot(Ls,CCp.s(11,:));
    grid on; box on;
    xlabel('Wavelength [nm]');
    ylabel('rp12');
    title('Compensator Correction');
end
if handles.plots.ip12
    figure('windowstyle','docked');
    plot(Ls,CCp.s(12,:));
    grid on; box on;
    xlabel('Wavelength [nm]');
    ylabel('ip12');
    title('Compensator Correction');
end
if handles.plots.rp21
    figure('windowstyle','docked');
    plot(Ls,CCp.s(13,:));
    grid on; box on;
    xlabel('Wavelength [nm]');
    ylabel('rp21');
    title('Compensator Correction');
end
if handles.plots.ip21
    figure('windowstyle','docked');
    plot(Ls,CCp.s(14,:));
    grid on; box on;
    xlabel('Wavelength [nm]');
    ylabel('ip21');
    title('Compensator Correction');
end
if handles.plots.rp22
    figure('windowstyle','docked');
    plot(Ls,CCp.s(15,:));
    grid on; box on;
    xlabel('Wavelength [nm]');
    ylabel('rp22');
    title('Compensator Correction');
end
if handles.plots.ip22
    figure('windowstyle','docked');
    plot(Ls,CCp.s(16,:));
    grid on; box on;
    xlabel('Wavelength [nm]');
    ylabel('ip22');
    title('Compensator Correction');
end
%% Plot pseudo-intensity from uT
if handles.plots.int
    if handles.M11
        uT = squeeze(Y(:,end,:));
        maxdata = importdata('M11_max.xlsx');
        m11max = spline(maxdata(:,1),maxdata(:,2),L);
        Io = max(uT,[],2)./m11max;
        figure('Name','Io from MAX','windowstyle','docked');
        plot(L,Io);
        grid on
        box on
        xlabel('Wavelength [nm]');
        ylabel('Estimated Lamp Intensity [arb.]');
    else
        disp('Data does not contain uT.');
    end
end

%% Contour plotting
if handles.plots.cont.N
    if handles.CtrlData.mode ~= 3
%         Z = squeeze(((Ye(:,1,:)-Ys(:,1,:)).^2).^0.5).';
        Z = squeeze(MSEcalc_v2(MSEmode,3,Ye(:,1,iSelect),Ys(:,1,iSelect),SigY(:,1,iSelect),Nparms)).';
        figure('windowstyle','docked');
%         contourf(WVLS,THKS,Z);
        h = pcolor(WVLS,THKS,Z);
        h.EdgeAlpha = 0;
        h.FaceColor = 'interp';
%         colormap('bone');
        xlabel('Wavelength [nm]');
        ylabel('SiO2 Thickness [nm]');
        title('N - RMSE');
        colorbar;
    else
        Ne=(Ye(:,2,:)+Ye(:,5,:))./2;
        Ns=(Ys(:,2,:)+Ys(:,5,:))./2;
        %         Z = squeeze(((Ne-Ns).^2).^0.5).';
        Se=sqrt(SigY(:,2,:).^2+SigY(:,5,:).^2);
        Z = squeeze(MSEcalc_v2(MSEmode,3,Ne,Ns,Se,Nparms)).';
        figure('windowstyle','docked');
        contourf(WVLS,THKS,Z);
        xlabel('Wavelength [nm]');
        ylabel('SiO2 Thickness [nm]');
        title('N - RMSE');
        colorbar;
    end
end   
if handles.plots.cont.C
    if handles.CtrlData.mode ~= 3
        Z = squeeze(MSEcalc_v2(MSEmode,3,Ye(:,2,:),Ys(:,2,:),SigY(:,2,:),Nparms)).';
        figure('windowstyle','docked');
        contourf(WVLS,THKS,Z);
        xlabel('Wavelength [nm]');
        ylabel('SiO2 Thickness [nm]');
        title('C - RMSE');
        colorbar;
    else
        Ne=(Ye(:,11,:)+Ye(:,16,:))./2;
        Ns=(Ys(:,11,:)+Ys(:,16,:))./2;
        Se=sqrt(SigY(:,11,:).^2+SigY(:,16,:).^2);
        Z = squeeze(MSEcalc_v2(MSEmode,3,Ne,Ns,Se,Nparms)).';
        figure('windowstyle','docked');
        contourf(WVLS,THKS,Z);
        xlabel('Wavelength [nm]');
        ylabel('SiO2 Thickness [nm]');
        title('C - RMSE');
        colorbar;
    end
end
if handles.plots.cont.S
    if handles.CtrlData.mode ~= 3
        Z = squeeze(MSEcalc_v2(MSEmode,3,Ye(:,3,:),Ys(:,3,:),SigY(:,3,:),Nparms)).';
        figure('windowstyle','docked');
        contourf(WVLS,THKS,Z);
        xlabel('Wavelength [nm]');
        ylabel('SiO2 Thickness [nm]');
        title('S - RMSE');
        colorbar;
    else
        Ne=(Ye(:,12,:)-Ye(:,15,:))./2;
        Ns=(Ys(:,12,:)-Ys(:,15,:))./2;
        Se=sqrt(SigY(:,12,:).^2+SigY(:,15,:).^2);
        Z = squeeze(MSEcalc_v2(MSEmode,3,Ne,Ns,Se,Nparms)).';
        figure('windowstyle','docked');
        contourf(WVLS,THKS,Z);
        xlabel('Wavelength [nm]');
        ylabel('SiO2 Thickness [nm]');
        title('S - RMSE');
        colorbar;
    end
end
if handles.plots.cont.NCS
    if handles.CtrlData.mode ~= 3
        Ne = permute(Ye(:,1:3,iSelect),[3 1 2]);
        Ns = permute(Ys(:,1:3,iSelect),[3 1 2]);
        Se = permute(SigY(:,1:3,iSelect),[3 1 2]);
        Z = MSEcalc_v2(MSEmode,2,Ne,Ns,Se,Nparms);
        figure('windowstyle','docked');
%         contourf(WVLS,THKS,Z);
        h = pcolor(WVLS,THKS,Z);
        h.EdgeAlpha = 0;
        h.FaceColor = 'interp';
%         colormap('bone');
        xlabel('Wavelength [nm]');
        ylabel('SiO2 Thickness [nm]');
        title('NCS RMSE');
        colorbar;
    else
        Ne=[(Ye(:,2,:)+Ye(:,5,:))./2, (Ye(:,11,:)+Ye(:,16,:))./2, (Ye(:,12,:)-Ye(:,15,:))./2];
        Ns=[(Ys(:,2,:)+Ys(:,5,:))./2, (Ys(:,11,:)+Ys(:,16,:))./2, (Ys(:,12,:)-Ys(:,15,:))./2];
        Se=sqrt([SigY(:,2,:).^2+SigY(:,5,:).^2 SigY(:,11,:).^2+SigY(:,16,:).^2 SigY(:,12,:).^2+SigY(:,15,:).^2]);
        Ne = permute(Ne,[3 1 2]);
        Ns = permute(Ns,[3 1 2]);
        Se = permute(Se,[3 1 2]);
        Z = MSEcalc_v2(MSEmode,2,Ne,Ns,Se,Nparms);
        rmse = MSEcalc_v2(MSEmode,0,Ne,Ns,Se,Nparms);
        figure('windowstyle','docked');
        contourf(WVLS,THKS,Z);
        xlabel('Wavelength [nm]');
        ylabel('SiO2 Thickness [nm]');
        title(['NCS RMSE; Total RMSE = ' num2str(rmse)]);
        colorbar;
    end
end
if handles.plots.cont.NCSsig
    if handles.CtrlData.mode ~= 3
%     if true
        Se = handles.SigY(:,17:19,:);
        Se = squeeze(mean(Se,2)).';
        figure('windowstyle','docked');
        h = pcolor(WVLS,THKS,Se);
        h.EdgeAlpha = 0;
        h.FaceColor = 'interp';
        xlabel('Wavelength [nm]');
        ylabel('SiO2 Thickness [nm]');
        title('Average NCS Sigma');
        colorbar;
    else
        SigN = handles.SigY(:,2,:).^2  + handles.SigY(:,5,:).^2;
        SigC = handles.SigY(:,11,:).^2 + handles.SigY(:,16,:).^2;
        SigS = handles.SigY(:,15,:).^2 + handles.SigY(:,12,:).^2;
        Z = squeeze(sqrt(SigN+SigC+SigS)).';
        figure('windowstyle','docked');
        h = pcolor(WVLS,THKS,SigY);
        h.EdgeAlpha = 0;
        h.FaceColor = 'interp';
        xlabel('Wavelength [nm]');
        ylabel('SiO2 Thickness [nm]');
        title('Average NCS Sigma');
        colorbar;
    end
end
if handles.plots.cont.offdiagcombos
    Ne=(Ye(:,3,:)+Ye(:,9,:))./2;
    Ns=(Ys(:,3,:)+Ys(:,9,:))./2;
    Se=sqrt(SigY(:,3,:).^2+SigY(:,9,:).^2);
    Z = squeeze(MSEcalc_v2(MSEmode,3,Ne,Ns,Se,Nparms)).';
%     Z = squeeze(((Ne-Ns).^2).^0.5).';
    figure('windowstyle','docked');
    h = pcolor(WVLS,THKS,Z);
    h.EdgeAlpha = 0;
    h.FaceColor = 'interp';
%     contourf(WVLS,THKS,Z);
    xlabel('Wavelength [nm]');
    ylabel('SiO2 Thickness [nm]');
    title('M13 + M31');
    colorbar;

    Ne=(Ye(:,4,:)-Ye(:,13,:))./2;
    Ns=(Ys(:,4,:)-Ys(:,13,:))./2;
    Se=sqrt(SigY(:,4,:).^2+SigY(:,13,:).^2);
    Z = squeeze(MSEcalc_v2(MSEmode,3,Ne,Ns,Se,Nparms)).';
    figure('windowstyle','docked');
    h = pcolor(WVLS,THKS,Z);
    h.EdgeAlpha = 0;
    h.FaceColor = 'interp';
    xlabel('Wavelength [nm]');
    ylabel('SiO2 Thickness [nm]');
    title('M14 - M41');
    colorbar;

    Ne=(Ye(:,8,:)-Ye(:,14,:))./2;
    Ns=(Ys(:,8,:)-Ys(:,14,:))./2;
    Se=sqrt(SigY(:,8,:).^2+SigY(:,14,:).^2);
    Z = squeeze(MSEcalc_v2(MSEmode,3,Ne,Ns,Se,Nparms)).';
    figure('windowstyle','docked');
    h = pcolor(WVLS,THKS,Z);
    h.EdgeAlpha = 0;
    h.FaceColor = 'interp';
    xlabel('Wavelength [nm]');
    ylabel('SiO2 Thickness [nm]');
    title('M24 - M42');
    colorbar;

    Ne=(Ye(:,7,:)+Ye(:,10,:))./2;
    Ns=(Ys(:,7,:)+Ys(:,10,:))./2;
    Se=sqrt(SigY(:,7,:).^2+SigY(:,10,:).^2);
    Z = squeeze(MSEcalc_v2(MSEmode,3,Ne,Ns,Se,Nparms)).';
    figure('windowstyle','docked');
    h = pcolor(WVLS,THKS,Z);
    h.EdgeAlpha = 0;
    h.FaceColor = 'interp';
    xlabel('Wavelength [nm]');
    ylabel('SiO2 Thickness [nm]');
    title('M23 + M32');
    colorbar;
end
if handles.plots.cont.mmall
    Ne = permute(Ye,[3 1 2]);
    Ns = permute(Ys,[3 1 2]);
    Se = permute(SigY,[3 1 2]);
    Z = MSEcalc_v2(MSEmode,2,Ne,Ns,Se,Nparms);
    rmse = MSEcalc_v2(MSEmode,0,Ne,Ns,Se,Nparms);
    figure('windowstyle','docked');
    h = pcolor(WVLS,THKS,Z);
    h.EdgeAlpha = 0;
    h.FaceColor = 'interp';
%     contourf(WVLS,THKS,Z);
    xlabel('Wavelength [nm]');
    ylabel('SiO2 Thickness [nm]');
    title(['MM RMSE; Total = ' num2str(rmse)]);
    colorbar;
end
if handles.plots.cont.mmsig
    Se = handles.SigY(iW,1:16,iSelect);
    Se = permute(Se,[3 1 2]);
    Se = sqrt(sum(Se.^2,3));
    figure('windowstyle','docked');
    h = pcolor(WVLS,THKS,Se);
    h.EdgeAlpha = 0;
    h.FaceColor = 'interp';
%     contourf(WVLS,THKS,Z);
    xlabel('Wavelength [nm]');
    ylabel('SiO2 Thickness [nm]');
    title('Mueller |\sigma|');
    colorbar;
end
if handles.plots.cont.offdiags
    Ne = permute(Ye(:,iOffDBs,:),[3 1 2]);
    Ns = permute(Ys(:,iOffDBs,:),[3 1 2]);
    Se = permute(SigY(:,iOffDBs,:),[3 1 2]);
    Z = MSEcalc_v2(MSEmode,2,Ne,Ns,Se,Nparms);
    rmse = MSEcalc_v2(MSEmode,0,Ne,Ns,Se,Nparms);
    figure('windowstyle','docked');
    h = pcolor(WVLS,THKS,Z);
    h.EdgeAlpha = 0;
    h.FaceColor = 'interp';
%     contourf(WVLS,THKS,Z);
    xlabel('Wavelength [nm]');
    ylabel('SiO2 Thickness [nm]');
    title(['Off Diag. RMSE; Total = ' num2str(rmse)]);
    colorbar;
end
if handles.plots.cont.ondiags
    Ne = permute(Ye(:,iOnDBs,:),[3 1 2]);
    Ns = permute(Ys(:,iOnDBs,:),[3 1 2]);
    Se = permute(SigY(:,iOnDBs,:),[3 1 2]);
    Z = MSEcalc_v2(MSEmode,2,Ne,Ns,Se,Nparms);
    rmse = MSEcalc_v2(MSEmode,0,Ne,Ns,Se,Nparms);
    figure('windowstyle','docked');
    h = pcolor(WVLS,THKS,Z);
    h.EdgeAlpha = 0;
    h.FaceColor = 'interp';
%     contourf(WVLS,THKS,Z);
    xlabel('Wavelength [nm]');
    ylabel('SiO2 Thickness [nm]');
    title(['On Diag. RMSE; Total = ' num2str(rmse)]);
    colorbar;
end
if handles.plots.cont.depol
    DI = sqrt(sum(Ye.^2,2)-Ye(:,1,:).^2)./(sqrt(3)*Ye(:,1,:));
    DI = permute(DI,[3 1 2]);
    figure('windowstyle','docked');
    h = pcolor(WVLS,THKS,DI);
    h.EdgeAlpha = 0;
    h.FaceColor = 'interp';
    colormap('bone');
    xlabel('Wavelength [nm]');
    ylabel('SiO2 Thickness [nm]');
    title('Polarization Index');
    colorbar;
end
if handles.plots.cont.uT % Pseudointensity uT
    figure('windowstyle','docked');
    uT = squeeze(Y(iW,end,iSelect)).';
    h = pcolor(WVLS,THKS,uT);
    h.EdgeAlpha = 0;
    colormap('bone');
    xlabel('Wavelength [nm]');
    ylabel('SiO2 Thickness [nm]');
    title('uT');
    colorbar;
end

%% Fit parameter plotting
if handles.plots.fitparms
    % First check to see if we've done a fit
    if ~isfield(handles,'FloatedParmNames')
        msgbox('You must perform a fit first!','ERROR','error');    
        return;
    end
    FloatedParmNames = handles.FloatedParmNames;
    FloatedParmVal = handles.FloatedParmVal;
    NumP = size(FloatedParmNames,1);
    if (FloatedParmVal(1,2) ~= FloatedParmVal(end,2)) && (NumP >= 3)
        for nP = 2:NumP-1
            figure('windowstyle','docked','Name',FloatedParmNames{nP})
            plot(FloatedParmVal(:,nP),'.-');
            title(FloatedParmNames{nP});
            grid on
        end
    elseif (FloatedParmVal(1,2) ~= FloatedParmVal(end,2))
        figure('windowstyle','docked','Name',FloatedParmNames{1})
        plot(FloatedParmVal(:,1),'.-');
        title(FloatedParmNames{1});
        grid on
    end
end

%% Optional new graphs??
newgraphs = 0;
if newgraphs == 1
    disp('New graphs here...');
    
    figure('windowstyle','docked');
    simthks = linspace(2,1980,130);
    plot(Thk-simthks.');
    xlabel('Wedge Point No.');
    ylabel('\DeltaSiO2 Thk. (fit-sim) [nm]');
    
%     % FFT test
%     mse = tabdata1(iSelect,end);
%     mse = cell2mat(mse);
%     dT = Thk-circshift(Thk,1);
%     dT = mean(dT(2:end));
%     Ft = 1/abs(dT);
%     numts = round(abs((max(Thk)-min(Thk))/dT));
%     Ts = linspace(max(Thk),min(Thk),numts);
%     Fts = Ft.*(0:1:ceil(numts/2))./numts;
%     Fts = Fts.';
%     Lest = EstimateLfromFs(ModParms.ppNK{5},Fts,0.01);
%     msei=interp1(Thk,mse,Ts);
%     mse = fft(msei);
% %     mse = nufft(mse,Thk);
% %     mse = fftshift(mse);
%     mse = abs(mse);
%     mse = mse(1:ceil(numts/2)+1);
%     figure('windowstyle','docked');
%     plot(Lest,log(mse));
%     grid on
%     xlabel('Estimated Wavelength [nm]');
%     ylabel('|F(RMSE)|');
% %     xlbs = EstimateLfromFs(ModParms.ppNK{5},xticks.',0.01);
% %     xlbs = num2cell(xlbs);
% %     for i=1:length(xlbs); xlbs{i}=num2str(xlbs{i}); end
% %     xticklabels(xlbs);
%     
%     Ne = permute(Ye(:,1,iSelect),[3 1 2]);
%     Ns = permute(Ys(:,1,iSelect),[3 1 2]);
%     Se = permute(SigY(:,1,iSelect),[3 1 2]);
% %     Z = MSEcalc_v2(MSEmode,2,Ne,Ns,Se,Nparms);
%     Z = (Ne-Ns);
%     Zi = zeros(numts,NumL);
%     xt = linspace(-3.5,3.5,numts).';
%     Gt = exp(-xt.^2./2)./sqrt(2*pi);
%     for l=1:NumL
%         Zi(:,l) = interp1(Thk,Z(:,l),Ts.');
% %         Zi(:,l) = Zi(:,l).*Gt;
%     end
%     z = fft(Zi,[],1);
%     z = abs(z./numts);
%     z = z(1:floor(numts/2)+1,:);
% %     [WVLSN,WVLSE] = meshgrid(Ls,Lest);
% %     levs = linspace(5,-2,20);
% %     [WVLSN,FTS] = meshgrid(Ls,Fts);
% %     z = (nufft(Z,Thk));
%     figure('windowstyle','docked');
% %     contourf(WVLSN,WVLSE,log(z),levs,'LineStyle','none');
% %     contourf(WVLSN,FTS,log(z),'Linestyle','none');
% %     pcolor(WVLSN,WVLSE,log(z));
%     imagesc([min(Ls) max(Ls)],[min(Fts) max(Fts)],log(z));
%     colormap('bone');
% %     ylbs = EstimateLfromFs(ModParms.ppNK{5},yticks.',0.01);
% %     ylbs = num2cell(ylbs);
% %     for i=1:length(ylbs); ylbs{i}=num2str(ylbs{i}); end
% %     yticklabels(ylbs);
%     xlabel('Wavelength [nm]');
% %     ylabel('Estimated Wavelength [nm]');
%     ylabel('Frequency [cycles/nm]');
% %     ylabel('Frequency [1/nm]');
%     title('log(|F(N RMSE)|) - interpolated');
%     colorbar;
%     
%     tadj = Thk.*(NumF/max(Thk));
%     xt2 = tadj-(max(tadj)-min(tadj))/2;
%     xt2 = xt2./max(xt2);
%     Ht2 = cos(pi.*xt2./2).^2;
%     Ht2 = repmat(Ht2,[1 NumL]);
%     Gt2 = exp(-(xt2.*3.5).^2./2)./sqrt(2*pi);
%     Gt2 = repmat(Gt2,[1 NumL]);
%     z = nufft(Z.*Ht2,tadj);
%     z = abs(z./numts);
%     z = z(1:ceil(numts/2)+1,:);
%     [WVLSN,WVLSE] = meshgrid(Ls,Lest);
% %     levs = linspace(5,-2,20);
% %     [WVLSN,FTS] = meshgrid(Ls,Fts);
% %     z = (nufft(Z,Thk));
%     figure('windowstyle','docked');
% %     contourf(WVLSN,WVLSE,log(z),levs,'LineStyle','none');
% %     contourf(WVLSN,FTS,log(z),'Linestyle','none');
%     h=pcolor(WVLSN,WVLSE,log(z));
%     h.EdgeAlpha=0;
% %     imagesc([min(Ls) max(Ls)],[min(Fts) max(Fts)],log(z));
%     colormap('bone');
% %     ylbs = EstimateLfromFs(ModParms.ppNK{5},yticks.',0.01);
% %     ylbs = num2cell(ylbs);
% %     for i=1:length(ylbs); ylbs{i}=num2str(ylbs{i}); end
% %     yticklabels(ylbs);
%     xlabel('Wavelength [nm]');
% %     ylabel('Estimated Wavelength [nm]');
%     ylabel('Frequency [cycles/nm]');
% %     ylabel('Frequency [1/nm]');
%     title('log(|F(N RMSE)|) - nufft');
%     colorbar;
%     
%     
%     
%     % Do the same thing with uT?!?
%     Z = permute(Y(ToolParms.iW,end,iSelect),[3 1 2]); % use uT instead
% %     Zn = repmat(sum(Z,1),[length(Ts) 1]);
%     Zn = repmat(Io(ToolParms.iW).',[sum(iSelect) 1]);
%     Z = Z./Zn;
%     Z = sqrt(Z);
%     Z = Z.*Ht2;
% %     figure('windowstyle','docked');
% %     plot(Ls,Zn(1,:)); grid on; xlabel('Wavelength [nm]'); ylabel('Z normalization');
% %     Zi = zeros(length(Ts),NumL);
% %     for l=1:NumL
% %         Zi(:,l) = interp1(Thk,Z(:,l),Ts.');
% %     end
% %     z = fft(Zi,[],1);
%     z = nufft(Z,tadj);
%     z = abs(z./NumF);
%     z = z(1:round(NumF/2),:);
%     [WVLSN,WVLSE] = meshgrid(Ls,Lest);
% %     levs = linspace(2,-10,20);
% %     [WVLSN,FTS] = meshgrid(Ls,Fts);
% %     z = (nufft(Z,Thk));
%     figure('windowstyle','docked');
% %     contourf(WVLSN,WVLSE,log(z),levs,'LineStyle','none');
% %     imagesc([min(Ls) max(Ls)],[min(Fts) max(Fts)],log(z));
% %     contourf(WVLSN,FTS,log(z),levs,'LineStyle','none');
%     h=pcolor(WVLSN,WVLSE,log(z));
%     h.EdgeAlpha = 0;
% %     h.FaceColor = 'interp';
%     colormap('bone');
% %     ylbs = EstimateLfromFs(ModParms.ppNK{5},yticks.',0.01);
% %     ylbs = num2cell(ylbs);
% %     for i=1:length(ylbs); ylbs{i}=num2str(ylbs{i}); end
% %     yticklabels(ylbs);
%     xlabel('Wavelength [nm]');
%     ylabel('Estimated Wavelength [nm]');
% %     ylabel('Frequency [cycles/nm]');
%     title('log(F|uT|)');
%     colorbar;
    
    
%     % 228 Spike vs thickness
%     figure('windowstyle','docked');
%     iL = (L>=226) & (L<=230);
%     m34e = permute(Ye(iL,12,:),[3 1 2]);
%     m34s = permute(Ys(iL,12,:),[3 1 2]);
%     m34d = permute(SigY(iL,12,:),[3 1 2]);
%     m34e = MSEcalc_v2(MSEmode,1,m34e,m34s,m34d,Nparms);
%     m34e = m34e./max(m34e,[],'all');
%     plot(Thk,m34e);
%     grid on; box on;
%     ylabel('m34 RMSE 226-230 nm');
%     xlabel('SiO2 Thickness [nm]');
%     title('228 Spike');
%     M34e = abs(fft(m34e));
%     M34eh = M34e(1:NumF/2+1);
%     Fs = Thk(end)/NumF;
%     f  = Fs*(0:(NumF/2))/NumF;
%     figure('windowstyle','docked');
%     plot(M34e);
%     box on; grid on;
%     xlabel('Spacial Freq. [nm]');
%     ylabel('|FFT(228-rmse)|');

%     % Quick Depolarization index based on m22
%     figure('windowstyle','docked');
%     m22 = squeeze(Ye(:,6,:)).';
%     m22 = max(m22,[],'all')-m22;
%     contourf(WVLS,THKS,m22);
%     xlabel('Wavelength [nm]');
%     ylabel('SiO2 Thickness [nm]');
%     title('Quick Depol (m22)');
%     colorbar;
    
    % Normalized m11
%     figure('windowstyle','docked');
%     m11 = squeeze(Ye(:,1,:)).';
%     m11 = m11./max(m11,[],'all');
%     contourf(WVLS,THKS,m11);
%     xlabel('Wavelength [nm]');
%     ylabel('SiO2 Thickness [nm]');
%     title('m11');
%     colorbar;
    
    % m13 RMSE
%     figure('windowstyle','docked');
%     m13e = permute(Ye(:,3,:),[3 1 2]);
%     m13s = permute(Ys(:,3,:),[3 1 2]);
%     m13d = permute(SigY(:,3,:),[3 1 2]);
%     m13e = MSEcalc_v2(MSEmode,2,m13e,m13s,m13d,Nparms);
%     m13e = m13e./max(m13e,[],'all');
%     contourf(WVLS,THKS,m13e);
%     xlabel('Wavelength [nm]');
%     ylabel('SiO2 Thickness [nm]');
%     title('m13 RMSE');
%     colorbar;

    
%     uT / m11 / m11r vs SiO2 thickness
%     figure('windowstyle','docked');
%     m11 = sum(Ys(:,1,:),1);
%     m11 = permute(m11,[3 1 2])./max(m11,[],'all');
%     m11r = repmat(Io(iW),1,NumF).*permute(Ys(:,1,:),[1 3 2]);
%     m11r = sum(m11r,1);
%     m11r = m11r./max(m11r,[],'all');
%     uT = sum(Y(iW,22,:),1);
%     uT = permute(uT,[3 1 2])./max(uT,[],'all');
%     plot(Thk,m11);
%     hold on
%     plot(Thk,uT);
%     plot(Thk,m11r);
%     box on; grid on;
%     xlabel('SiO2 Thickness [nm]');
%     ylabel('uT + m11');
%     legend('m11','uT','m11r');


end