function varargout = AnalysisFinished(varargin)
% ANALYSISFINISHED MATLAB code for AnalysisFinished.fig
%      ANALYSISFINISHED, by itself, creates a new ANALYSISFINISHED or raises the existing
%      singleton*.
%
%      H = ANALYSISFINISHED returns the handle to a new ANALYSISFINISHED or the handle to
%      the existing singleton*.
%
%      ANALYSISFINISHED('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in ANALYSISFINISHED.M with the given input arguments.
%
%      ANALYSISFINISHED('Property','Value',...) creates a new ANALYSISFINISHED or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before AnalysisFinished_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to AnalysisFinished_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help AnalysisFinished

% Last Modified by GUIDE v2.5 26-Sep-2018 14:23:19

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @AnalysisFinished_OpeningFcn, ...
                   'gui_OutputFcn',  @AnalysisFinished_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before AnalysisFinished is made visible.
function AnalysisFinished_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to AnalysisFinished (see VARARGIN)

% Choose default command line output for AnalysisFinished
handles.output = hObject;

% Update results
if varargin{1} == true
    img = imread('checkmark.jpg');
else
    img = imread('Red_X.png');
end
axes(handles.axes1);
imshow(img);
results = varargin{2};
results = results(3:end,:);
set(handles.uitable1,'data',results);
set(handles.resultstxt,'string',varargin{3});

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes AnalysisFinished wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = AnalysisFinished_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;



function edit1_Callback(hObject, eventdata, handles)
% hObject    handle to edit1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit1 as text
%        str2double(get(hObject,'String')) returns contents of edit1 as a double


% --- Executes during object creation, after setting all properties.
function edit1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
