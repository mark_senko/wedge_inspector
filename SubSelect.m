function varargout = SubSelect(varargin)
% SUBSELECT MATLAB code for SubSelect.fig
%      SUBSELECT, by itself, creates a new SUBSELECT or raises the existing
%      singleton*.
%
%      H = SUBSELECT returns the handle to a new SUBSELECT or the handle to
%      the existing singleton*.
%
%      SUBSELECT('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in SUBSELECT.M with the given input arguments.
%
%      SUBSELECT('Property','Value',...) creates a new SUBSELECT or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before SubSelect_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to SubSelect_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help SubSelect

% Last Modified by GUIDE v2.5 17-Aug-2018 11:53:15

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @SubSelect_OpeningFcn, ...
                   'gui_OutputFcn',  @SubSelect_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before SubSelect is made visible.
function SubSelect_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to SubSelect (see VARARGIN)

% Choose default command line output for SubSelect
handles.output = hObject;

% Get number of files
handles.t1hand = varargin{1};
handles.tData = get(handles.t1hand,'Data');
[s1,~] = size(handles.tData);
handles.NumF = s1;
set(handles.statustext,'String',['Files #1 to #' num2str(s1) ' selectable.']);
set(handles.selectend,'String',num2str(s1));

% See if called from other program
if length(varargin)>1
    set(handles.skips,'String',varargin{2});
end

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes SubSelect wait for user response (see UIRESUME)
% uiwait(handles.subselectfig);


% --- Outputs from this function are returned to the command line.
function varargout = SubSelect_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;



function selectstart_Callback(hObject, eventdata, handles)
% hObject    handle to selectstart (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of selectstart as text
%        str2double(get(hObject,'String')) returns contents of selectstart as a double


% --- Executes during object creation, after setting all properties.
function selectstart_CreateFcn(hObject, eventdata, handles)
% hObject    handle to selectstart (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function selectend_Callback(hObject, eventdata, handles)
% hObject    handle to selectend (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of selectend as text
%        str2double(get(hObject,'String')) returns contents of selectend as a double


% --- Executes during object creation, after setting all properties.
function selectend_CreateFcn(hObject, eventdata, handles)
% hObject    handle to selectend (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in goornogo.
function goornogo_Callback(hObject, eventdata, handles)
% hObject    handle to goornogo (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of goornogo



function skips_Callback(hObject, eventdata, handles)
% hObject    handle to skips (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of skips as text
%        str2double(get(hObject,'String')) returns contents of skips as a double


% --- Executes during object creation, after setting all properties.
function skips_CreateFcn(hObject, eventdata, handles)
% hObject    handle to skips (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in pushbutton1.
function pushbutton1_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get settings
skips = get(handles.skips,'String');
skips = str2double(skips);
selend = get(handles.selectend,'String');
selend = str2double(selend);
selstart = get(handles.selectstart,'String');
selstart = str2double(selstart);
gonogo = get(handles.goornogo,'Value');

% construct logical arrays
So = cell2mat(handles.tData(:,1));
Sn = (ones(selend-selstart+1,1) == 0);
Sn(1:(skips+1):end) = true;
if gonogo == 0
    Sn = ~Sn;
end
So(selstart:selend) = Sn;

% save to table
handles.tData(:,1) = num2cell(So);
set(handles.t1hand,'Data',handles.tData);

guidata(hObject, handles);
close(handles.subselectfig);
