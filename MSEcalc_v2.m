function [MSE] = MSEcalc_v2(mode,ndimsout,Ye,Ys,SigY,Nparms)
% Calculate MSE (JAW) or RMSE

% Version 2
% -> Add support for singular MSE calculation (ndimsout = 0)
% -> Add support for "plot all" contour maps (ndimsout = 2)
% -> Support automatic squeezing and reshaping of data
% -> Doesn't require dY to be computed beforehand

% AGB 2021

%% Check sizes and throw error if necessary
sYe = size(Ye);
sYs = size(Ys);
sSigY = size(SigY);

if (sYe ~= sYs) | (sYe ~= sSigY)
    disp('Error in MSEcalc_v2: input data not the same size!');
    return;
end

%% Compute MSE
ndims = length(sYe);

% divide by Sigmas if necessary
if mode ~= 1
    dY2 = ((Ye-Ys)./SigY).^2;
else
    dY2 = (Ye-Ys).^2;
end

% loop summations
norm = 1;
for i=1:(ndims-ndimsout)
    dY2 = sum(dY2,ndims+1-i);
    norm = norm * sYe(ndims+1-i);
end

% normalize
if mode ~= 1
    if norm==Nparms
        MSE = sqrt(dY2);
    else
        MSE = sqrt(dY2./(norm-Nparms));
    end
else
    MSE = sqrt(dY2./norm);
end


