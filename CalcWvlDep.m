%inputs:
% L    wavelengths to evaluate
% P    parameter structure, or array of parameter structures
% rw   parameter index into array of parameter structures
% iL   logical selection array for wavelenths when parameters are split
%      iL[1] = visible selection, iL[2] = ir selection. 
%      Each may over lap 1000 due to badwidth spreading.
%outputs:
% Pl   row vector containing values of P evaluated at L

function [Pl] = CalcWvlDep(L,P,rw,iL)
[s1,NumP]=size(P.val);
if s1 > 1
    P.val = P.val(rw,:);
    P.typ = P.typ{rw};
    P.spt = P.spt(rw);
end

%make sure the wavelength selections are set up properly for split
%parameters
if ~isequal(size(iL),[length(L),2])
    iL = [L <= 1000,L > 1000];
end

NumL = length(L);
Pl = zeros(1,NumL);
L = L';
iLv = iL(:,1)';
iLr = iL(:,2)';

switch P.typ
    case 'None'
        if P.spt==0
            Pl = P.val(1).*ones(1,NumL);
        else
            Pl(iLv) = P.val(1);
            Pl(iLr) = P.val(NumP/2+1);
        end
    case 'Cauchy'
        if P.spt==0
            for i=1:NumP
                Pl = (P.val(i)*1000^(2*(i-1))).*L.^(-2*(i-1)) + Pl;
            end
        else
            for i=1:NumP/2
                Pl(iLv) = (P.val(i)*1000^(2*(i-1))).*L(iLv).^(-2*(i-1)) + Pl(iLv);
                i2 = i+NumP/2;
                Pl(iLr) = (P.val(i2)*1000^(2*(i-1))).*L(iLr).^(-2*(i-1)) + Pl(iLr);
            end
        end
    case 'Polynom.'
        if P.spt==0
            for i=1:NumP
                Pl = (P.val(i)*1000^(i-1)).*(1./L-1/1000).^(i-1) + Pl;
            end
        else
            for i=1:NumP/2
                Pl(iLv) = (P.val(i)*1000^(i-1)).*((1./L(iLv)-1/600)).^(i-1) + Pl(iLv);
                i2 = i+NumP/2;
                Pl(iLr) = (P.val(i2)*1000^(i-1)).*((1./L(iLr)-1/1350)).^(i-1) + Pl(iLr);
            end
        end
    case 'Spline'
        if P.spt==0
            Ls = linspace(1240/190,1240/1700,NumP);
            Pl = spline(Ls,P.val,1240./L);
        else
            Ls = linspace(1240/190,1240/1000,NumP/2);
            Pl(iLv) = spline(Ls,P.val(1:NumP/2),1240./L(iLv));
            Ls = linspace(1240/1000,1240/1700,NumP/2);
            Pl(iLr) = spline(Ls,P.val(NumP/2+1:end),1240./L(iLr));
        end
end

% iP = (Pl > P.UL) & (P.UL ~= P.LL);
% Pl(iP) = P.LL+0.9.*(P.UL-P.LL)+0.1.*(P.UL-P.LL).*exp((P.UL-Pl(iP))./(P.UL-P.LL));
% iP = (Pl < P.LL) & (P.UL ~= P.LL);
% Pl(iP) = P.UL-0.9.*(P.UL-P.LL)-0.1.*(P.UL-P.LL).*exp((Pl(iP)-P.LL)./(P.UL-P.LL));

