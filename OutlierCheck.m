function [ansr] = OutlierCheck(thks,rmses)
% Checks for outliers in thickness vs RMSE graph

% do a vector length check
NumT = length(thks);
if NumT ~= length(rmses)
    disp('Thickness and RMSE vectors must match. Exiting.');
    return
end

% setup
NumD = length(rmses)-1;
ansr.isgood = true;
ansr.index = false(NumT,1);
stds = zeros(NumD-1,1);
avgs = stds;
rmses_orig = rmses;
recursive_check = true;
ords = {'st','nd','rd','th'};
count = 0;

% Recursive while loop to detect multiple outliers
while recursive_check == true
    % take out any previously found outliers
    if count ~= 0
        for i=1:NumT
            if ansr.index(i) && i==1
                rmses(i) = rmses_orig(2);
            elseif ansr.index(i) && i==NumT
                rmses(i) = rmses_orig(NumT-1);
            elseif ansr.index(i)
                rmses(i) = (rmses_orig(i-1)+rmses_orig(i+1))/2;
            end
        end
    end
    
    % differentiate rmses
    d_rmse = abs(diff(rmses));
    a_rmse = mean(rmses);
    
    % Calculate standard deviation of all points, leaving the most current 2
    % points out. This should find the std without the outlier point. It
    % assumes there is only one outlier.
    stds(1) = std(d_rmse(3:NumD));
    avgs(1) = mean(d_rmse(3:NumD));
    stds(end) = std(d_rmse(1:NumD-2));
    avgs(end) = mean(d_rmse(1:NumD-2));
    for i=2:NumD-2
        stds(i) = std([d_rmse(1:i-1); d_rmse(i+2:NumD)]);
        avgs(i) = mean([d_rmse(1:i-1); d_rmse(i+2:NumD)]);
    end
    ansr.minstd = min(stds)*6 + min(avgs);

    % Run through the points and determine if the differentiation of the RMSE
    % is greater than 6X the minimum std.
    if (d_rmse(1) > ansr.minstd) && (rmses(1) > a_rmse)
        ansr.index(1) = true;
    elseif (d_rmse(end) > ansr.minstd) && (rmses(end) > a_rmse)
        ansr.index(end) = true;
    end
    for i=1:NumD-2
        if (d_rmse(i) > ansr.minstd) && (d_rmse(i+1) > ansr.minstd) && (rmses(i+1) > a_rmse)
            ansr.index(i+1) = true;
        end
    end
    
    % check and set flags
    if sum(ansr.index) == count
        recursive_check = false;
        
        % plot if there are outliers
        if count ~= 0
            figure('windowstyle','docked')
            thks2 = (thks(2:NumT)-thks(1:NumT-1))./2 + thks(1:NumT-1);
            plot(thks2,d_rmse);
            grid on;
            xlabel('Thickness [nm]');
            title('No more outliers detected');
            hold on;
            thks3 = (thks2(2:NumD)-thks2(1:NumD-1))./2 + thks2(1:NumD-1);
            plot(thks3,stds);
            plot(thks3,avgs);
            plot(thks3,stds-stds+ansr.minstd);
            legend('$|\frac{d}{d\mathrm{T}}$RMSE$|$','Omitted Standard Dev.','Omitted Average','$6\times$ Minimum StD + Minimum Avg.','Interpreter','latex');
            drawnow;
        end
    else
        if ansr.isgood == true
            ansr.isgood = false;
        end
        count = sum(ansr.index);
        
        % plot if there are outliers
        figure('windowstyle','docked')
        thks2 = (thks(2:NumT)-thks(1:NumT-1))./2 + thks(1:NumT-1);
        plot(thks2,d_rmse);
        grid on;
        xlabel('Thickness [nm]');
        if count<5; out_ord = ords{count}; else; out_ord = 'th'; end
        title([num2str(count) out_ord ' Outlier Detected']);
        hold on;
        thks3 = (thks2(2:NumD)-thks2(1:NumD-1))./2 + thks2(1:NumD-1);
        plot(thks3,stds);
        plot(thks3,avgs);
        plot(thks3,stds-stds+ansr.minstd);
        legend('$|\frac{d}{d\mathrm{T}}$RMSE$|$','Omitted Standard Dev.','Omitted Average','$6\times$ Minimum StD + Minimum Avg.','Interpreter','latex');
        drawnow;
    end
end

% plot if there were outliers
if count~=0
    figure('windowstyle','docked')
    plot(thks,rmses_orig);
    hold on; grid on;
    plot(thks(ansr.index),rmses_orig(ansr.index),'or');
    xlabel('Thickness [nm]');
    legend('RMSE','Detected Outlier(s)');
end