% Script to setup the simulation, for Simulate, Serial Fit,
% and Parallel Fit.

% Updated to fix wavelength shift induced endpoint error for WI v4.3a


%% Get Started
% define abort
global ABORT

% get data to sim
Data = get(handles.uitable1,'data');
iSelect = cell2mat(Data(:,1));
if sum(iSelect)==0
    msgbox('Select some files first.')
    return
end
Thk = Data(iSelect,3);
NumF = sum(iSelect);

% check if some Thk are missing
for nF = 1:NumF
    if isempty(Thk{nF})
        msgbox('Thk inputs are missing.')
        return
    end
end
Thk = cell2mat(Thk);

% get spectra
L = handles.L;
if handles.CtrlData.mode == 3
    Y = handles.Y(:,1:16,iSelect);
    if handles.CtrlData.uT
        Y(:,1,:) = handles.M11(:,iSelect);
    end
elseif handles.CtrlData.mode == 2
    N = -(handles.Y(:,2,iSelect) + handles.Y(:,5,iSelect))./2;
    C = (handles.Y(:,11,iSelect) + handles.Y(:,16,iSelect))./2;
    S = (handles.Y(:,12,iSelect) - handles.Y(:,15,iSelect))./2;
    p = sqrt(N.^2 + C.^2 + S.^2);
    N = N./p; C = C./p; S = S./p;
    Y = [N C S];
    if handles.CtrlData.uT
        Y(:,1,:) = Y(:,1,:) + 1i.*permute(handles.M11(:,iSelect),[1 3 2]);
    end
else
    Y = handles.Y(:,17:19,iSelect);
    if handles.CtrlData.uT
        Y(:,1,:) = Y(:,1,:) + 1i.*permute(handles.M11(:,iSelect),[1 3 2]);
    end
end
ModParms = handles.ModParms;
NK=ModParms.NK;

% Get control settings
NumNASlices = handles.CtrlData.NaSlcs;
BW_nslc = handles.CtrlData.BwSlcs;
FlgUseShaper = handles.CtrlData.A3Apod;

%% Wvl vectors & BW setup
tabdata = get(handles.uitable2,'data');
Lmin = tabdata{1,6};
Lmax = tabdata{2,6};

BWp.val = cell2mat(tabdata(5,6:(5+tabdata{5,5})));
BWp.typ = tabdata{5,3};
BWp.spt = tabdata{5,4};
BWp.LL = tabdata{5,16};
BWp.UL = tabdata{5,17};

iW = (L>=Lmin)&(L<=Lmax);
iWo = (L>=Lmin)&(L<=Lmax);
Lo = L(iWo);
if mod(BW_nslc,1) ~= 0
    BW_nslc = ceil(BW_nslc);
end
if mod(BW_nslc,2) == 0
    BW_nslc = BW_nslc + 1;
end

iWv = (L>=Lmin)&(L<=1000)&(L<=Lmax);
% Lo(Lo<=1000)==L(iWv)
iWr = (L>1000)&(L>=Lmin)&(L<=Lmax);

BWp.s = CalcWvlDep(Lo,BWp,1,0)/2.355;% Convert from FWHM to sigma

% Fractional spreads across 3 sigma, excluding ends
spr = linspace(-3,3,BW_nslc+2)';
spr = spr(2:end-1);

% Weights applied to the fractional spacing
Wg = linspace(-3,3,BW_nslc+2)'; % calculate coefficients
Wg = Wg(2:end-1);
Wg = exp(-(Wg.^2)./2);
Wg = Wg/sum(Wg(:));

BW_VIS = false;
BW_IR = false;

if BWp.spt == 0
    BW_VIS = sum(BWp.val) ~= 0;
else 
    bwlen = length(BWp.val);
    BW_VIS = sum(BWp.val(1:bwlen/2)) ~= 0;
    BW_IR = sum(BWp.val(bwlen/2 + 1:bwlen)) ~= 0;
end

if BW_VIS
    Lh_v = [repmat(L(iWv),BW_nslc,1)];
    Lh_v = sort(Lh_v);
    Ldiv = length(Lh_v);
    iMv = zeros(1,sum(iWv)*BW_nslc);
    iMv(1:BW_nslc)=Wg;
    iMv = repmat(iMv,sum(iWv),1);
    Lspr_v = [];
    for ii = 1:sum(iWv)
        Lspr_v = [Lspr_v;repmat(BWp.s(ii),BW_nslc,1).*spr];
        iMv(ii,:)=circshift(iMv(ii,:),BW_nslc*(ii-1));
    end
else
    Lh_v = L(iWv);
    Ldiv = length(Lh_v);
    Lspr_v = zeros(Ldiv,1);
    iMv = eye(sum(iWv));
end

if BW_IR
    Lh_r = [repmat(L(iWr),BW_nslc,1)];
    Lh_r = sort(Lh_r);
    Ldiv2 = length(Lh_r);
    iMr = zeros(1,sum(iWr)*BW_nslc);
    iMr(1:BW_nslc)=Wg;
    iMr = repmat(iMr,sum(iWr),1);

    Lspr_r=[];
    for ii = 1:sum(iWr)
        Lspr_r = [Lspr_r;repmat(BWp.s(ii+sum(iWv)),BW_nslc,1).*spr];
        iMr(ii,:)=circshift(iMr(ii,:),BW_nslc*(ii-1));
    end
else
    Lh_r = L(iWr);
    Ldiv2 = length(Lh_r);
    Lspr_r = zeros(Ldiv2,1);
    iMr = eye(sum(iWr));
end

Lh = [Lh_v;Lh_r];
Lspr = [Lspr_v;Lspr_r];
Lh = Lh+Lspr;

NumL = length(Lh);
iLh_VIS = logical([ones(Ldiv,1); zeros(Ldiv2,1)]);
iLh_IR  = logical([zeros(Ldiv,1); ones(Ldiv2,1)]);
iLh = [iLh_VIS,iLh_IR];

%% init Ye, Ys, Ls, Ts, Se
% Ye = Y; % wvl correction?
Ye = Y(iW,:,:); % original line
Ys = Ye-Ye;
Ls = L(iW);

%% Calculate Wvl Dependence
AOIp.val = cell2mat(tabdata(3,6:(5+tabdata{3,5})));
AOIp.typ = tabdata{3,3};
AOIp.spt = tabdata{3,4};
AOIp.LL = tabdata{3,16};
AOIp.UL = tabdata{3,17};
NAp.val = cell2mat(tabdata(4,6:(5+tabdata{4,5})));
NAp.typ = tabdata{4,3};
NAp.spt = tabdata{4,4};
NAp.LL = tabdata{4,16};
NAp.UL = tabdata{4,17};
FlgFitThk = tabdata{8,1};
ThkEMA = tabdata{9,6};
ALPHAp.val = cell2mat(tabdata(10,6:(5+tabdata{10,5})));
ALPHAp.typ = tabdata{10,3};
ALPHAp.spt = tabdata{10,4};
ALPHAp.LL = tabdata{10,16};
ALPHAp.UL = tabdata{10,17};
BETAp.val = cell2mat(tabdata(11,6:(5+tabdata{11,5})));
BETAp.typ = tabdata{11,3};
BETAp.spt = tabdata{11,4};
BETAp.LL = tabdata{11,16};
BETAp.UL = tabdata{11,17};
BMp.val = cell2mat(tabdata(12:16,6:(5+tabdata{12,5})));
BMp.typ = tabdata(12:16,3);
BMp.spt = cell2mat(tabdata(12:16,4));
BMp.LL = tabdata{12,16};
BMp.UL = tabdata{12,17};
if  (handles.CtrlData.mode == 1)
    ind = 18;
elseif (handles.CtrlData.NewLens == 0)
    ind = 20;
else
    ind = 26;
end
LNSp.val = cell2mat(tabdata(17:ind,6:(5+tabdata{17,5})));
LNSp.typ = tabdata(17:ind,3);
LNSp.spt = cell2mat(tabdata(17:ind,4));
LNSp.LL = cell2mat(tabdata(17:ind,16));
LNSp.UL = cell2mat(tabdata(17:ind,17));
if handles.CtrlData.CompCal == 1
    CCp.val = cell2mat(tabdata(ind+1:end,6:(5+tabdata{ind+1,5})));
    CCp.typ = tabdata(ind+1:end,3);
    CCp.spt = cell2mat(tabdata(ind+1:end,4));
    CCp.LL = cell2mat(tabdata(ind+1:end,16));
    CCp.UL = cell2mat(tabdata(ind+1:end,17));
end
    

AOIp.s = CalcWvlDep(Lh,AOIp,1,iLh);
NAp.s  = CalcWvlDep(Lh,NAp,1,iLh);
span = linspace(-1,1,NumNASlices)'; %setup NA slices
dAOIs = span*asind(NAp.s);
ALPHAp.s = CalcWvlDep(Lh,ALPHAp,1,iLh);
BETAp.s = CalcWvlDep(Lh,BETAp,1,iLh);
BMp.s = zeros(5,NumL);
BMp.s(1,:) = CalcWvlDep(Lh,BMp,1,iLh);
BMp.s(2,:) = CalcWvlDep(Lh,BMp,2,iLh);
BMp.s(3,:) = CalcWvlDep(Lh,BMp,3,iLh);
if handles.CtrlData.BmProf == 6 % adjustment for thk dependent dz!
    ToolParms.pThk = Thk;
%     disp('SimSetup.m, dz adjustment made!');
end
BMp.s(4,:) = CalcWvlDep(Lh,BMp,4,iLh);
BMp.s(5,:) = CalcWvlDep(Lh,BMp,5,iLh);
if (handles.CtrlData.mode ~= 1) && (handles.CtrlData.NewLens == 1)
    LNSp.s = zeros(10,NumL);
    for ii=1:10
        LNSp.s(ii,:) = CalcWvlDep(Lh,LNSp,ii,iLh);
    end
end
if (handles.CtrlData.mode ~= 1) && (handles.CtrlData.CompCal == 1)
    CCp.s = zeros(16,length(L));
    for ii=1:16
        CCp.s(ii,:) = CalcWvlDep(L,CCp,ii,0);
    end
end

%% N&Ks
if ModParms.NKselect == 1
    NK_Si = ModParms.ppNK{1}(Lh)-1i.*ModParms.ppNK{2}(Lh);
    Noffset = ModParms.SiDat{27,3};
    Koffset = ModParms.SiDat{28,3};
    NK_Si = NK_Si + Noffset - 1i*Koffset;
    NK_Si = real(NK_Si) + 1i*min(imag(NK_Si),0);
    NK_intr = ModParms.ppNK{3}(Lh)-1i.*ModParms.ppNK{4}(Lh);
    Noffset = ModParms.SiO2Dat{9,3};
    NK_sio2_xy = ModParms.ppNK{5}(Lh)+Noffset-1i.*ModParms.ppNK{6}(Lh);
    NK_sio2_z = ModParms.ppNK{7}(Lh)+Noffset-1i.*ModParms.ppNK{8}(Lh);
    NK = [NK_Si,NK_Si,NK_intr,NK_sio2_xy,NK_sio2_z].';
    NK = AdjustNK(NK,ALPHAp.s,BETAp.s);
elseif ModParms.NKselect == 2
    [NK_Si, NK_intr, NK_sio2_xy] = Si_SiO2_Aspnes_v2(Lh,ModParms.SiDat,ModParms.SiO2Dat);
    Noffset = ModParms.SiO2Dat{9,3};
    NK_sio2_xy = NK_sio2_xy + Noffset;
    NK_sio2_z = NK_sio2_xy;
    NK = [NK_Si,NK_Si,NK_intr,NK_sio2_xy,NK_sio2_z].';
    NK = AdjustNK(NK,ALPHAp.s,BETAp.s);
end
AirNK = ModParms.SiO2Dat{6,3} + ModParms.SiO2Dat{7,3}./(Lh./1000).^2 + ModParms.SiO2Dat{8,3}./(Lh./1000).^4;
NK = [NK; AirNK.'];
ModParms.NK = NK;

%% Integration Weights
% Atlas 3 shaper
if FlgUseShaper && max(NAp.val)>0 && NumNASlices>1
    G = ApodizerAtlasIII(NumNASlices)';    
else
    G = 1;
end

% Weight for NA integration 
dx = 2/NumNASlices;
x = (1:NumNASlices)*dx;
x = x-mean(x);
switch handles.CtrlData.BmProf
    case 1
        W = sqrt(1-x.^2);
        W = repmat(W',[1 NumL]);
    case 2
        W = ones(NumNASlices,NumL);
    case 3
        x0 = [-1; -0.5; 0; 0.5; 1];
        x0 = repmat(x0,[1 NumL]);
        x = repmat(x.',[1 NumL]);
        y = repmat(Lh.',[NumNASlices 1]);
        y0 = repmat(Lh.',[5 1]);
        W = interp2(y0,x0,BMp.s,y,x,'spline');
    case 4
        W = repmat(sqrt(1-x'.^2),[1 NumL]).*(x'*BMp.s(1,:)+1);
    case 5
        W = 2*sqrt(repmat(BMp.s(2,:),[NumNASlices 1])-repmat(x'.^2,[1 NumL]));
    case 6
        W = repmat(sqrt(1-x'.^2),[1 NumL]).*(x'*BMp.s(1,:)+1);
    case 7
        W = asind(sqrt(NAp.s.^2 - cosd(asind(NAp.s)).^2.*tand(dAOIs).^2));
        W = acosd(sind(90-AOIp.s+dAOIs).^2+cosd(90-AOIp.s+dAOIs).^2.*cosd(2.*W));
end
if FlgUseShaper
    W = W.*G;
end
% normalize weights
W = W./sum(W,1);
ToolParms.W = W;

%% create ToolParms structure
ToolParms.AOIp = AOIp;
ToolParms.dAOIs = dAOIs;
ToolParms.NAp = NAp;
ToolParms.BW_VIS = BW_VIS;     % Nonzero BW in vis?(1Sigma)
ToolParms.BW_IR = BW_IR;     % Nonzero BW in ir?(1Sigma)
ToolParms.BWp = BWp;
ToolParms.BW_nslc = BW_nslc;
ToolParms.iMv = iMv;
ToolParms.iMr = iMr;
ToolParms.DO_CE = handles.CEcal.DO;
if handles.CtrlData.NewLens ~= 1
    ToolParms.DO = [LNSp.val(1,1:3) LNSp.val(2,3)];
end
if (handles.CtrlData.mode ~= 1) && (handles.CtrlData.NewLens ~= 1)
    ToolParms.WD = [LNSp.val(3,1:3); LNSp.val(4,1:3)];
end
if handles.CtrlData.centeredWcal == 1
    W_vis_o = handles.CEcal.Wcal.nVis;
    W_ir_o = handles.CEcal.Wcal.nIR;
else
    W_vis_o = handles.CEcal.Wcal.C_VIS+handles.CEcal.Wcal.Cc_VIS;
    W_ir_o = handles.CEcal.Wcal.C_IR+handles.CEcal.Wcal.Cc_IR;
end
ToolParms.Wcal_o = [W_vis_o , W_ir_o];
ToolParms.Lc = [handles.CEcal.Wcal.Lc_Vis handles.CEcal.Wcal.Lc_IR];
ToolParms.Wcal_n = cell2mat([tabdata(6,6:10) tabdata(7,6:9)]);
ToolParms.NumNASlices = NumNASlices;
ToolParms.FlgFitThk = FlgFitThk;
ToolParms.FlgUseShaper = FlgUseShaper;
ToolParms.W = W;                   % Weight for NA integration + shaper    
ToolParms.G = G;                   % shaper weights    
ToolParms.Lo = Lo;                 % final wvl vector with normal wvl spacing
ToolParms.L = L;                   % wvl vector unwindowed for wave cal
ToolParms.Lh = Lh;                 % wvl vector with small wvl spacing to be used for BW convolution
% ToolParms.iLh_VIS = (Lh <= 1000);  % use in BW convolution to avoid spikes at the edges
% ToolParms.iLh_IR = (Lh > 1000);
ToolParms.iLh_VIS = iLh_VIS;
ToolParms.iLh_IR  = iLh_IR;
ToolParms.Ldiv = Ldiv;
ToolParms.iW = iW;
ToolParms.ThkEMA = ThkEMA;
ToolParms.ALPHAp = ALPHAp;
ToolParms.BETAp = BETAp;
ToolParms.BMp = BMp;
ToolParms.BPtype = handles.CtrlData.BmProf;
ToolParms.Depol = handles.CtrlData.Depol;
ToolParms.mode = handles.CtrlData.mode;
ToolParms.m11s = handles.CtrlData.m11s;
ToolParms.uT = handles.CtrlData.uT;
ToolParms.nlens = handles.CtrlData.NewLens;
ToolParms.Comp.enable = handles.CtrlData.CompCal;
if (handles.CtrlData.CompCal == 1)
    ToolParms.Comp.CCp = CCp;           % Compensator parameters!
    ToolParms.Comp.A = handles.CEcal.A;
    ToolParms.Comp.P = handles.CEcal.P;
    ToolParms.Comp.GamR = handles.CEcal.GamR;
    ToolParms.Comp.GamS = handles.CEcal.GamS;
end
if (handles.CtrlData.mode ~= 1) && (handles.CtrlData.NewLens == 1)
    ToolParms.LNSp = LNSp;
end
ToolParms.LimitedUpdate = []; % so it is always defined as a field

%% Setup JAW mse calculation
% v4.3a edit to include all wvls - NOPE
JAWmse = handles.CtrlData.JAWmse;
if JAWmse==2
    if handles.CtrlData.mode == 3
        SigY = handles.SigY(iW,1:16,iSelect);
    elseif handles.CtrlData.mode == 2
        SigN = sqrt(handles.SigY(iW,2,iSelect).^2 + handles.SigY(iW,5,iSelect).^2)./2;
        SigC = sqrt(handles.SigY(iW,11,iSelect).^2 + handles.SigY(iW,16,iSelect).^2)./2;
        SigS = sqrt(handles.SigY(iW,15,iSelect).^2 + handles.SigY(iW,12,iSelect).^2)./2;
        SigY = [SigN SigC SigS];
    else
        SigY = handles.SigY(iW,17:19,iSelect);
    end
else
    SigY = Ye-Ye+0.001; % JAW CE mode, comment out for homoscedasticity test
    
    % New code to support homoscedasticity - need to be in JAW CE RMSE mode
    % in Controls menu!
%     [s1,s2,~] = size(Ye);
%     hdelta = permute(Thk.*3e-7,[3 2 1]);
%     hdelta = repmat(hdelta,[s1,s2,1]);
%     SigY = Ye - Ye + 0.0001 + hdelta;
end