function varargout = GUI_WedgeDataInspector(varargin)
% GUI_WEDGEDATAINSPECTOR MATLAB code for GUI_WedgeDataInspector.fig
%      GUI_WEDGEDATAINSPECTOR, by itself, creates a new GUI_WEDGEDATAINSPECTOR or raises the existing
%      singleton*.
%
%      H = GUI_WEDGEDATAINSPECTOR returns the handle to a new GUI_WEDGEDATAINSPECTOR or the handle to
%      the existing singleton*.ll
%
%      GUI_WEDGEDATAINSPECTOR('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in GUI_WEDGEDATAINSPECTOR.M with the given input arguments.
%
%      GUI_WEDGEDATAINSPECTOR('Property','Value',...) creates a new GUI_WEDGEDATAINSPECTOR or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before GUI_WedgeDataInspector_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to GUI_WedgeDataInspector_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help GUI_WedgeDataInspector

% Last Modified by GUIDE v2.5 20-Apr-2021 19:48:08

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @GUI_WedgeDataInspector_OpeningFcn, ...
                   'gui_OutputFcn',  @GUI_WedgeDataInspector_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT

% --- Executes just before GUI_WedgeDataInspector is made visible.
function GUI_WedgeDataInspector_OpeningFcn(hObject, ~, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to GUI_WedgeDataInspector (see VARARGIN)

% Choose default command line output for GUI_WedgeDataInspector
handles.output = hObject;

%Set the version number
handles.text2.String = 'Wedge Data Inspector v4.3d'

% History
% 4.3c
% 4.3d
%    Wavelength dependence for BW
%    Added 8 compensator parameters for optical activity
%    Changed the Lev-Marq step size to a constant 1e-6 for jacobians
%    Normalize by M22 instead of M11
%

% Reset All values
ResetAll;

% Declare Global stop
global ABORT
ABORT = 0;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes GUI_WedgeDataInspector wait for user response (see UIRESUME)
% uiwait(handles.figure1);

% --- Outputs from this function are returned to the command line.
function varargout = GUI_WedgeDataInspector_OutputFcn(~, ~, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;

% --- Load Data
function loadData_Callback(hObject, ~, handles)
% read data file names and path
if ~exist('DefaultPathDataFiles.mat','file')
    PathF= [getenv('USERPROFILE') '\Desktop\'];
else
    load DefaultPathDataFiles PathF
    if ~exist('PathF','var')
        PathF = [getenv('USERPROFILE') '\Desktop\'];
    end
    if PathF == 0
        PathF = [getenv('USERPROFILE') '\Desktop\'];
    end
end
[Files, PathF] = uigetfile({'*.mat';'*.dat'},'Select Data File',PathF,'MultiSelect','on');
if PathF == 0 ; return; end
if isempty(PathF)
    PathF = [getenv('USERPROFILE') '\Desktop\'];
end
save DefaultPathDataFiles PathF
if ~iscell(Files)
    Files = {Files};
end

% open data files
if isequal(Files{1}(end-2:end),'mat')
    % this is a matlab file
    w = warning('error', 'MATLAB:load:variableNotFound');
    try
        load([PathF Files{1}],'Ver');
    catch
        msgbox('This saved file is not compatible with WI v4.0+.','ERROR','error');
        warning(w);
        return;
    end
    warning(w);
    
    if (~strcmp(Ver,'4.3a'))
        msgbox('This saved file is not compatible with WI v4.3a.','ERROR','error');
        return;
    end
    
    load([PathF Files{1}],'L','Y','SigY','Files','CEcal','M11');
    Wcal = CEcal.Wcal;
    Wcal.nVis = Wcal.C_VIS + Wcal.Cc_VIS;
    Wcal.nIR = Wcal.C_IR + Wcal.Cc_IR;
    if Wcal.nVis(end) == 0; Wcal.nVis(end) = 60; end % correct broken files???
    DO = CEcal.DO;
    WD = CEcal.WD;
    AOIcal = CEcal.AOIcal;
    handles.CEcal = CEcal;
else
    LoadData_DAT_v2; % breakout into individual file
end
upwait_v2('c',handles.axes1,' ','c',handles.axes2,' ');

ArrangeData; % breakout into individual file

guidata(hObject, handles);

% --- Save Bin
function saveBinary_Callback(~, ~, handles)
if ~exist('DefaultPathDataFiles.mat','file')
    PathF= [getenv('USERPROFILE') '\Desktop\'];
else
    load DefaultPathDataFiles PathF
    if ~exist('PathF','var')
        PathF = [getenv('USERPROFILE') '\Desktop\'];
    end
    if PathF == 0
        PathF = [getenv('USERPROFILE') '\Desktop\'];
    end
end
[FileName,PathF,~] = uiputfile('*.mat','Save Spectra',PathF);
if PathF == 0 ; return; end

% if isempty(PathF)
%     PathF = [getenv('USERPROFILE') '\Desktop\'];
% end

save DefaultPathDataFiles PathF

L = handles.L;
Y = handles.Y;
SigY = handles.SigY;
CEcal = handles.CEcal;
Files = handles.Files;
Ver = '4.3a';
M11 = handles.M11;
save([PathF FileName],'L','CEcal','Y','SigY','Files','Ver','M11');

% --- Executes on button press in checkbox1.
function checkbox1_Callback(~, ~, handles)
MyVal = get(handles.checkbox1,'val');
Data = get(handles.uitable1,'data');
Data(:,1) = {logical(MyVal)};
set(handles.uitable1,'data',Data);

% --- Serial and Parallel fit
function fitdata_Callback(hObject, eventdata, handles)
FitType = get(handles.fitdata,'String');
if strcmp(FitType,'Serial Fit') == 1
%     profile('on');
    SerialFit;
%     profile('off');
%     profile('viewer');
else
    % Parallel Spectra Fit
%     profile('on');
    ParallelFit;
%     profile('off');
%     profile('viewer');
end
guidata(hObject, handles);

% --- Plot Everything
function plot_all_Callback(hObject, ~, handles)
PlotAll;
guidata(hObject, handles);


% --- Plot Resid
function plotResidual_Callback(hObject, ~, handles)
if ~isfield(handles,'Ls')
    msgbox('Need to fit first!');
    return
end
Ys = handles.Ys;
Ye = handles.Ye;
Ls = handles.Ls;
RMSE = ((sum((Ye-Ys).^2,3))./size(Ye,3)).^0.5;
[figh,axh]=WIgrapher(handles.figh,handles.axh,Ls,RMSE,1,[0 0.8 0]);
handles.figh = figh;
handles.axh = axh;
guidata(hObject, handles);


% --- Executes when entered data in editable cell(s) in uitable1.
function uitable1_CellEditCallback(hObject, eventdata, handles)
% hObject    handle to uitable1 (see GCBO)
% eventdata  structure with the following fields (see MATLAB.UI.CONTROL.TABLE)
%	Indices: row and column indices of the cell(s) edited
%	PreviousData: previous data for the cell(s) edited
%	EditData: string(s) entered by the user
%	NewData: EditData or its converted form set on the Data property. Empty if Data was not changed
%	Error: error string when failed to convert EditData to appropriate value for Data
% handles    structure with handles and user data (see GUIDATA)


% --- Plot Menu
function plot_menu_Callback(hObject, eventdata, handles)

handles.plots = PlotMenu(handles.CtrlData,handles.plots);
guidata(hObject, handles);

% --- Sim Spect
function simulate_Callback(hObject, ~, handles)
% profile('on');
SimSpectra;
% profile('off');
% profile('viewer');
guidata(hObject, handles);

% --- Executes on button press in pushbutton8.
function pushbutton8_Callback(hObject, eventdata, handles)
!notepad.exe C:\Program Files\Nanometrics\Wedge_Inspector_v405\application\ReadMe_WI.txt

% ---Load Settings.
function loadSettings_Callback(hObject, ~, handles)
LoadSettings;

% --- Table 2 excel.
function dattable2excel_Callback(~, ~, handles)
if ~exist('DefaultPathDataFiles.mat','file')
    PathF = [getenv('USERPROFILE') '\Desktop\'];
else
    load DefaultPathDataFiles PathF
    if ~exist('PathF','var')
        PathF = [getenv('USERPROFILE') '\Desktop\'];
    end
    if PathF == 0
        PathF = [getenv('USERPROFILE') '\Desktop\'];
    end
end
[FileName,PathF] = uiputfile('*.xlsx','Export to Excel',PathF);
if PathF == 0 ; return; end
if isempty(PathF)
    PathF = [getenv('USERPROFILE') '\Desktop\'];
end
save DefaultPathDataFiles PathF

MyFile = [PathF FileName];
Data = get(handles.uitable1,'data');
ColNames = get(handles.uitable1,'ColumnName');
Data = [ColNames'; Data];
Data = Data(:, 2:end);
xlswrite(MyFile,Data);

choice = questdlg('Saving is complete, would you like to open the file?','Save PCs','Yes','No','No') ;
% Handle response
switch choice
    case 'Yes'
        system(MyFile);
    case 'No'
end

% --- Save Settings
function saveSettings_Callback(hObject, ~, handles)
SaveSettings(handles);
guidata(hObject,handles);

% --- Subselect
function subselect_Callback(hObject, eventdata, handles)
SubSelect(handles.uitable1);
guidata(hObject,handles);

% --- Copy best match data to simulation params.
function CopySim_Callback(hObject, eventdata, handles)
% hObject    handle to CopySim (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% get best match model parameters
Data = get(handles.uitable1,'data');
ColNames = get(handles.uitable1,'ColumnName');
iSelect = cell2mat(Data(:,1));
if sum(iSelect)==0
    msgbox('Error: no spectra selected.');
    return
end
Data = Data(iSelect,:);
NumF = sum(iSelect);
bestparms = sum(cell2mat(Data(:,4:end-1)),1);
bestparms = bestparms./NumF;
% bestparms = prod(cell2mat(Data(:,4:end-1)),1); % geo mean?
% bestparms = sign(bestparms).*(abs(bestparms).^(1/NumF));
bestparms = num2cell(bestparms);
iMP = cell2mat([handles.ModParms.SiDat(:,1); handles.ModParms.SiO2Dat(:,1)]);
nbp2 = sum(iMP);
% bestparms1 = bestparms(1:(end-1-sum(iMP))); %cutout select,file,thk,mse & ModParms
[~,s2] = size(bestparms);
nbp1 = s2-nbp2;
ColNames = ColNames(4:end);

% update sim table
Data = get(handles.uitable2,'data');
[s1,~]=size(Data);
for i=1:nbp1
    nstr = split(ColNames{i},'_');
    val = str2num(nstr{2});
    for j=1:s1
        if strcmp(nstr{1},Data{j,2})
            Data(j,val+5) = bestparms(i);
            break
        end
    end
end
set(handles.uitable2,'data',Data);

% update NK
bestparms = bestparms(nbp1+1:end);
iMPsi = cell2mat([handles.ModParms.SiDat(:,1)]);
nMPsi = sum(iMPsi);
if nMPsi > 0
    handles.ModParms.SiDat(iMPsi,3) = bestparms(1:sum(iMPsi));
end

bestparms = bestparms(nMPsi+1:end);
iMPsio2 = cell2mat([handles.ModParms.SiO2Dat(:,1)]);
nMPsio2 = sum(iMPsio2);
if nMPsio2 > 0
    handles.ModParms.SiO2Dat(iMPsio2,3) = bestparms(1:nMPsio2);
end
guidata(hObject,handles);

% --- Executes on button press in populateThks.
function populateThks_Callback(hObject, eventdata, handles)
% hObject    handle to populateThks (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% PopulateThks_exp1;
% PopulateThks_exp2;
% PopulateThks_exp3;
PopulateThks;
guidata(hObject, handles);

% --- Executes on button press in abortbutton.
function abortbutton_Callback(hObject, eventdata, handles)
% hObject    handle to abortbutton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global ABORT
ABORT = 1;

% --- Executes on button press in nkEdit.
function nkEdit_Callback(hObject, eventdata, handles)
% hObject    handle to nkEdit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Setup L values from data
L = handles.L;
% Type = handles.Type;
Data = get(handles.uitable2,'data');
Lmin = Data{1,6};
Lmax = Data{2,6};
alpha = Data{10,6};
iWo = (L>Lmin)&(L<=Lmax);
Lo = L(iWo);
handles.ModParms.Lo = Lo;
handles.ModParms.alpha = alpha;

% Call external NK edit
[NKargout] = NKedit(handles.ModParms);
handles.ModParms = NKargout;

guidata(hObject,handles);

% --- If Enable == 'on', executes on mouse press in 5 pixel border.
% --- Otherwise, executes on mouse press in 5 pixel border or over fitdata.
function fitdata_ButtonDownFcn(hObject, eventdata, handles)
% hObject    handle to fitdata (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
click = get(handles.figure1,'SelectionType');
if strcmp(click,'alt') == 1
    FitType = get(handles.fitdata,'String');
    if strcmp(FitType,'Serial Fit') == 1
        FitType = 'Parallel Fit';
        set(handles.fitdata,'String',FitType);
    else
        FitType = 'Serial Fit';
        set(handles.fitdata,'String',FitType);
    end
    guidata(hObject,handles); 
end

% --- Executes on button press in gendata.
function gendata_Callback(hObject, eventdata, handles)
% hObject    handle to gendata (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% get data to fit
if handles.CtrlData.mode ~=3
    msgbox('You must generate data in MM mode. NCS data will also be generated.','Error','error');
    return;
end
GenData;
guidata(hObject,handles); 

% --- Executes on button press in controls.
function controls_Callback(hObject, eventdata, handles)
% hObject    handle to controls (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% Call external Controls figure
if length(handles.M11)>1
    M11 = true;
else
    M11 = false;
end
[CTRLargout] = Controls(handles.CtrlData,M11);
tabdata = get(handles.uitable2,'data');
% [s1,~] = size(tabdata);
% disp(CTRLargout);

% handle mode and lens model changes
if CTRLargout.mode ~= handles.CtrlData.mode
    if CTRLargout.mode == 1
        tabdata = tabdata(1:18,:);
%         tabdata(17,6:8) = num2cell(handles.CEcal.DO(1:3));
        tabdata(17,:) = [{false,'DO13', 'Cauchy', false, 3},...
                num2cell(handles.CEcal.DO(1:3)), repmat({[]},[1 7]),{0.01,0.01}];
%         tabdata(18,6:8) = num2cell([0,0,handles.CEcal.DO(4)]);
        tabdata(18,:) = [{false,'DO4', 'Polynom.', false, 3, 0, 0},...
                num2cell(handles.CEcal.DO(4)), repmat({[]},[1 7]),{0.01,0.01}];
    elseif (CTRLargout.mode ~= 1) && (CTRLargout.NewLens == 0)
        adds = [{false,'WD1', 'Cauchy', false, 3},...
                num2cell(handles.CEcal.WD(1,:)), repmat({[]},[1 7]),{0.01,0.01};...
                {false,'WD2', 'Cauchy', false, 3},...
                num2cell(handles.CEcal.WD(2,:)), repmat({[]},[1 7]),{0.01,0.01}];
        tabdata = [tabdata(1:18,:); adds];
    end
end
if ((CTRLargout.NewLens ~= handles.CtrlData.NewLens) || (CTRLargout.CompCal ~= handles.CtrlData.CompCal)) && (CTRLargout.mode ~= 1)
    tabend = 0;
    if CTRLargout.NewLens == 0
        adds = [{false,'DO13', 'Cauchy', false, 3},...
                num2cell(handles.CEcal.DO(1:3)), repmat({[]},[1 7]),{0.01,0.01};...
                {false,'DO4', 'Polynom.', false, 3, 0, 0},...
                num2cell(handles.CEcal.DO(4)), repmat({[]},[1 7]),{0.01,0.01};...
                {false,'WD1', 'Cauchy', false, 3},...
                num2cell(handles.CEcal.WD(1,:)), repmat({[]},[1 7]),{0.01,0.01};...
                {false,'WD2', 'Cauchy', false, 3},...
                num2cell(handles.CEcal.WD(2,:)), repmat({[]},[1 7]),{0.01,0.01}];
        tabend = 16;
    elseif (CTRLargout.NewLens ~= handles.CtrlData.NewLens)
        adds = [{false,'NCS Ret.', 'Spline', false, 1, 0},...
                repmat({[]},[1 9]),{0.001,0.001};...
                {false,'NCS Att.', 'Spline', false, 1, 0},...
                repmat({[]},[1 9]),{0.001,0.001};...
                {false,'T1 Ret.', 'Spline', false, 1, 0},...
                repmat({[]},[1 9]),{0.001,0.001};...
                {false,'T1 Att.', 'Spline', false, 1, 0},...
                repmat({[]},[1 9]),{0.001,0.001};...
                {false,'T1 Real Act.', 'Spline', false, 1, 0},...
                repmat({[]},[1 9]),{0.001,0.001};...
                {false,'T1 Imag Act.', 'Spline', false, 1, 0},...
                repmat({[]},[1 9]),{0.001,0.001};...
                {false,'T2 Ret.', 'Spline', false, 1, 0},...
                repmat({[]},[1 9]),{0.001,0.001};...
                {false,'T2 Att.', 'Spline', false, 1, 0},...
                repmat({[]},[1 9]),{0.001,0.001};...
                {false,'T2 Real Act.', 'Spline', false, 1, 0},...
                repmat({[]},[1 9]),{0.001,0.001};...
                {false,'T2 Imag Act.', 'Spline', false, 1, 0},...
                repmat({[]},[1 9]),{0.001,0.001}];
        tabend = 16;
    end
    if CTRLargout.CompCal == 1
        addCC = [{false,'dGam1', 'Spline', false, 1, 0},...
                repmat({[]},[1 9]),{0.001,0.001};...
                {false,'iGam1', 'Spline', false, 1, 0},...
                repmat({[]},[1 9]),{0.001,0.001};...
                {false,'dPsi1', 'Spline', false, 1, 0},...
                repmat({[]},[1 9]),{0.001,0.001};...
                {false,'dPhi1', 'Spline', false, 1, 0},...
                repmat({[]},[1 9]),{0.001,0.001};...
                {false,'dGam2', 'Spline', false, 1, 0},...
                repmat({[]},[1 9]),{0.001,0.001};...
                {false,'iGam2', 'Spline', false, 1, 0},...
                repmat({[]},[1 9]),{0.001,0.001};...
                {false,'dPsi2', 'Spline', false, 1, 0},...
                repmat({[]},[1 9]),{0.001,0.001};...
                {false,'dPhi2', 'Spline', false, 1, 0},...
                repmat({[]},[1 9]),{0.001,0.001};...            
                {false,'rp11', 'Spline', false, 1, 0},...
                repmat({[]},[1 9]),{0.001,0.001};...
                {false,'ip11', 'Spline', false, 1, 0},...
                repmat({[]},[1 9]),{0.001,0.001};...
                {false,'rp12', 'Spline', false, 1, 0},...
                repmat({[]},[1 9]),{0.001,0.001};...
                {false,'ip12', 'Spline', false, 1, 0},...
                repmat({[]},[1 9]),{0.001,0.001};...
                {false,'rp21', 'Spline', false, 1, 0},...
                repmat({[]},[1 9]),{0.001,0.001};...
                {false,'ip21', 'Spline', false, 1, 0},...
                repmat({[]},[1 9]),{0.001,0.001};...
                {false,'rp22', 'Spline', false, 1, 0},...
                repmat({[]},[1 9]),{0.001,0.001};...
                {false,'ip22', 'Spline', false, 1, 0},...
                repmat({[]},[1 9]),{0.001,0.001}];
        if exist('adds','var')
            adds = [adds;addCC];
        else
            adds = addCC;
        end
        if tabend==0
            if CTRLargout.NewLens==1
                tabend = 26;
            else
                tabend = 20;
            end
        end
                
    end
    tabdata = [tabdata(1:tabend,:); adds];
end

% handle wavecal centering change
if CTRLargout.centeredWcal ~= handles.CtrlData.centeredWcal
    if CTRLargout.centeredWcal == 0
        tabdata(6,6:10) = num2cell(handles.CEcal.Wcal.C_VIS + handles.CEcal.Wcal.Cc_VIS);
        tabdata(7,6:9) = num2cell(handles.CEcal.Wcal.C_IR + handles.CEcal.Wcal.Cc_IR);
        handles.CEcal.Wcal.Lc_Vis = 0;
        handles.CEcal.Wcal.Lc_IR = 0;
    else
        tabdata(6,6:10) = num2cell(handles.CEcal.Wcal.nVis);
        tabdata(7,6:9) = num2cell(handles.CEcal.Wcal.nIR);
        handles.CEcal.Wcal.Lc_Vis = 600;
        handles.CEcal.Wcal.Lc_IR = 1350;
    end
end

set(handles.uitable2,'data',tabdata);
handles.CtrlData = CTRLargout;
guidata(hObject,handles);

% --- Executes on button press in saveallfigs.
function saveallfigs_Callback(hObject, eventdata, handles)
% hObject    handle to saveallfigs (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% SaveAllFigs();
SaveAllFigs();

% --- Executes on button press in plot_2_excel.
function plot_2_excel_Callback(hObject, eventdata, handles)
% hObject    handle to plot_2_excel (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
tabdata = get(handles.uitable2,'data');
[s1,~] = size(tabdata);
L = linspace(tabdata{1,6},tabdata{2,6},300)';
for i=1:s1
    if ((i==3) || (i==4) || (i>9) && (i<17)) && (~strcmp(tabdata{i,3},'None'))
        figure('Name',['Wvl. Dep. ' tabdata{i,2}],'windowstyle','docked');
        P.typ = tabdata{i,3};
        P.spt = tabdata{i,4};
        P.val = cell2mat(tabdata(i,6:(5+tabdata{i,5})));
        V = CalcWvlDep(L,P,1,0);
        plot(L,V);
        grid on
        xlabel('Wavelength [nm]');
    elseif (i>16) && (~strcmp(tabdata{i,3},'None'))
        if handles.CtrlData.NewLens == 1
            figure('Name',['Wvl. Dep. ' tabdata{i,2}],'windowstyle','docked');
            P.typ = tabdata{i,3};
            P.spt = tabdata{i,4};
            P.val = cell2mat(tabdata(i,6:(5+tabdata{i,5})));
            V = CalcWvlDep(L,P,1,0);
            plot(L,V);
            grid on
            xlabel('Wavelength [nm]');
        end
    end
end
if handles.CtrlData.NewLens == 0
    figure('Name','Wvl. Dep. of DOs','windowstyle','docked');
    V = 500./L.*(tabdata{17,6}+tabdata{17,7}.*(500./L).^2+tabdata{17,8}.*(500./L).^4)+tabdata{18,8}.*(L./500);
    plot(L,V);
    grid on
    xlabel('Wavelength [nm]');

    if handles.CtrlData.mode ~= 1
        for j=1:2
            figure('Name',['Wvl. Dep. ' tabdata{18+j,2}],'windowstyle','docked');
            V = 500./L.*(tabdata{18+j,6}+tabdata{18+j,7}.*(500./L).^2+tabdata{18+j,8}.*(500./L).^4);
            plot(L,V);
            grid on
            xlabel('Wavelength [nm]');
        end
    end
end

% --- Executes when entered data in editable cell(s) in uitable2.
function uitable2_CellEditCallback(hObject, eventdata, handles)
% hObject    handle to uitable2 (see GCBO)
% eventdata  structure with the following fields (see MATLAB.UI.CONTROL.TABLE)
%	Indices: row and column indices of the cell(s) edited
%	PreviousData: previous data for the cell(s) edited
%	EditData: string(s) entered by the user
%	NewData: EditData or its converted form set on the Data property. Empty if Data was not changed
%	Error: error string when failed to convert EditData to appropriate value for Data
% handles    structure with handles and user data (see GUIDATA)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% I hate this hack, but it works. Paired with the calls at the end of this function,
% this will keep the position of the screen fixed instead of scrolling back
% to the top after every edit.
% 
jscrollpane = javaObjectEDT(findjobj(hObject));
viewport    = javaObjectEDT(jscrollpane.getViewport);
P           = viewport.getViewPosition();
jtable      = javaObjectEDT( viewport.getView );
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

newlens = handles.CtrlData.NewLens;
idx = eventdata.Indices;
tabdata = get(handles.uitable2,'Data');
[s1,~] = size(tabdata);

% Lmin and Lmax handling
if ((idx(1) == 1) || (idx(1) == 2)) && (idx(2) ~= 6)
    tabdata{idx(1),idx(2)} = eventdata.PreviousData;
    set(handles.uitable2,'Data',tabdata);
    guidata(hObject, handles);
    msgbox('Lmin and Lmax are only editable at Value 1. Edit Failed.','ERROR','error');
    return;
end

% BW handling   --- only activate this code if you don't want wavelength
%                   dependence. Splitting is allowed either way.
% if (idx(1) == 5) && ((idx(2) ~= 6) && (idx(2) ~= 7) && (idx(2) ~= 1) && (idx(2) ~= 16) && (idx(2) ~= 17))
%     tabdata{idx(1),idx(2)} = eventdata.PreviousData;
%     set(handles.uitable2,'Data',tabdata);
%     guidata(hObject, handles);
%     msgbox('Bandwidth is fixed to 2 values at this time. Edit Failed.','ERROR','error');
%     return;
% end

% W_vis handling
if (idx(1) == 6) && ((idx(2) < 6) || ((idx(2) > 10) && (idx(2) < 16))) && (idx(2) ~= 1)
    tabdata{idx(1),idx(2)} = eventdata.PreviousData;
    set(handles.uitable2,'Data',tabdata);
    guidata(hObject, handles);
    msgbox('Wavelength calibration is tightly controlled to JAW standard. Edit Failed.','ERROR','error');
    return;
end

% W-IR handling
if (idx(1) == 7) && ((idx(2) < 6) || ((idx(2) > 9) && (idx(2) < 16))) && (idx(2) ~= 1)
    tabdata{idx(1),idx(2)} = eventdata.PreviousData;
    set(handles.uitable2,'Data',tabdata);
    guidata(hObject, handles);
    msgbox('Wavelength calibration is tightly controlled to JAW standard. Edit Failed.','ERROR','error');
    return;
end

% Fit thk handling
if (idx(1) == 8) && (idx(2) ~= 1)
    tabdata{idx(1),idx(2)} = eventdata.PreviousData;
    set(handles.uitable2,'Data',tabdata);
    guidata(hObject, handles);
    msgbox('Fit checkbox is the only editable value for thickness in this table. Edit Failed.','ERROR','error');
    return;
end

% Thk EMA handling
if (idx(1) == 9) && ((idx(2) ~= 1) && (idx(2) ~= 6))
    tabdata{idx(1),idx(2)} = eventdata.PreviousData;
    set(handles.uitable2,'Data',tabdata);
    guidata(hObject, handles);
    msgbox('Interface thickness can only have 1 value. Edit Failed.','ERROR','error');
    return;
end

% DO & WD handling
if newlens == 0
%     if ((16 < idx(1)) && (idx(1) < 21)) && ((5 < idx(2)) && (idx(2) < 9))
    if ((16 < idx(1)) && (idx(1) < 21)) && ~((idx(2) == 1) || ((5 < idx(2)) && (idx(2) < 9)) || (idx(2) > 15))
        tabdata{idx(1),idx(2)} = eventdata.PreviousData;
        set(handles.uitable2,'Data',tabdata);
        guidata(hObject, handles);
        msgbox('Not editable when lens model is set to JAW. See controls menu. Edit Failed.','ERROR','error');
        return;
    end
end

% Replace empty values with []
if (idx(2) >= 6) && (idx(2) <= 15)
    if isnan(eventdata.NewData)
        tabdata{idx(1),idx(2)} = [];
    end
end

% Calculate Num column
for i=1:s1
    count = 0;
    if (newlens == 0) && (i > 16) && (i < 21)
        continue;
    end
    for j=6:15
        if ~isempty(tabdata{i,j})
            count = count + 1;
        end
    end
    tabdata{i,5} = count;
end

% Split handling
if (idx(2) == 4) && (((idx(1) >= 3) && (idx(1) <=5)) || (idx(1) >= 10))
    if (eventdata.NewData == true) && (mod(tabdata{idx(1),5},2) ~= 0)
        tabdata{idx(1),idx(2)} = eventdata.PreviousData;
        set(handles.uitable2,'Data',tabdata);
        guidata(hObject, handles);
        msgbox('Can only split detectors if # of Values is even. Edit Failed.','ERROR','error');
        return;
    end
elseif ((idx(2) <= 10) && (idx(2) >= 6)) && (((idx(1) >= 3) && (idx(1) <=5)) || (idx(1) >= 10))
    if (mod(tabdata{idx(1),5},2) ~= 0) && (tabdata{idx(1),4} == true)
        tabdata{idx(1),idx(2)} = eventdata.PreviousData;
        set(handles.uitable2,'Data',tabdata);
        guidata(hObject, handles);
        msgbox('Can only split detectors if # of Values is even. Edit Failed.','ERROR','error');
        return;
    end
end

% Birds of a feather (Bm)
if ((idx(1)>11) && (idx(1)<17)) && (idx(2)==3)
    tabdata(12:16,3) = repmat({eventdata.NewData},[5 1]);
elseif ((idx(1)>11) && (idx(1)<17)) && (idx(2)==4)
    tabdata(12:16,4) = repmat({eventdata.NewData},[5 1]);  
elseif ((idx(1)>11) && (idx(1)<17)) && ((idx(2)>5) && (idx(2)<11))
    if ~isnan(eventdata.NewData)
        numvs = max([tabdata{12:16,5}]);
        for i=12:16
            adds = numvs - tabdata{i,5};
            tabdata(i,(tabdata{i,5}+6):(tabdata{i,5}+5+adds)) = repmat({0},[1 adds]);
            tabdata{i,5} = numvs;
        end
    else
        tabdata(12:16,idx(2)) = repmat({[]},[5 1]);
        tabdata(12:16,5) = repmat({idx(2)-6},[5 1]);
    end
end

% Birds of a feather (LNS???)
% if ((idx(1)>16) && (newlens)) && (idx(2)==3)
%     tabdata(17:end,3) = repmat({eventdata.NewData},[8 1]);
% if ((idx(1)>16) && (newlens)) && (idx(2)==4)
%     tabdata(17:end,4) = repmat({eventdata.NewData},[8 1]);  
if ((idx(1)>16) && (newlens) && (idx(1)<27)) && ((idx(2)>5) && (idx(2)<16))
    if ~isnan(eventdata.NewData)
        numvs = max([tabdata{17:end,5}]);
        for i=17:26
            adds = numvs - tabdata{i,5};
            tabdata(i,(tabdata{i,5}+6):(tabdata{i,5}+5+adds)) = repmat({0},[1 adds]);
            tabdata{i,5} = numvs;
        end
    else
        tabdata(17:end,idx(2)) = repmat({[]},[8 1]);
        tabdata(17:end,5) = repmat({idx(2)-6},[8 1]);
    end
end

% Birds of a feather (COMP)
if ((idx(1)>26) && (newlens)) && ((idx(2)>5) && (idx(2)<16))
    if ~isnan(eventdata.NewData)
        numvs = max([tabdata{27:end,5}]);
        for i=27:42
            adds = numvs - tabdata{i,5};
            tabdata(i,(tabdata{i,5}+6):(tabdata{i,5}+5+adds)) = repmat({0},[1 adds]);
            tabdata{i,5} = numvs;
        end
    else
        tabdata(27:end,idx(2)) = repmat({[]},[8 1]);
        tabdata(27:end,5) = repmat({idx(2)-6},[8 1]);
    end
elseif ((idx(1)>20) && (~newlens)) && ((idx(2)>5) && (idx(2)<16))
    %disp(eventdata.NewData);
    if ~isnan(eventdata.NewData)
        numvs = max([tabdata{21:end,5}]);
        for i=21:36
            adds = numvs - tabdata{i,5};
            tabdata(i,(tabdata{i,5}+6):(tabdata{i,5}+5+adds)) = repmat({0},[1 adds]);
            tabdata{i,5} = numvs;
        end
    else
        tabdata(21:end,idx(2)) = repmat({[]},[8 1]);
        tabdata(21:end,5) = repmat({idx(2)-6},[8 1]);
    end
end

% If passed all checks, upload table
set(handles.uitable2,'Data',tabdata);
guidata(hObject, handles);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Here is the code that restores the screen position that was moved during
% set(handles.uitable2,'Data',tabdata))
drawnow(); %This is necessary to ensure the view position is set after matlab hijacks it
viewport.setViewPosition(P);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% --- Executes on button press in analysis.
function analysis_Callback(hObject, eventdata, handles)
% hObject    handle to analysis (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
tab1 = get(handles.uitable1,'data');
if isempty(tab1)
    PresetAnalysis_Multiple;
%     PresetAnalysis_Multiple_J;
else
%   PresetAnalysis;
     PresetAnalysis_Jon;
end
guidata(hObject, handles);

% --- Executes on button press in resetall.
function resetall_Callback(hObject, eventdata, handles)
% hObject    handle to resetall (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
ResetAll;
guidata(hObject, handles);

% --- Executes on button press in plotI0fromuT.
function plotI0fromuT_Callback(hObject, eventdata, handles)
% hObject    handle to plotI0fromuT (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
Y = handles.Y;
L = handles.L;
if handles.M11
    uT = squeeze(Y(:,end,:));
    maxdata = importdata('M11_max.xlsx');
    m11max = spline(maxdata(:,1),maxdata(:,2),L);
    Io = max(uT,[],2)./m11max;
    figure('Name','Io from MAX');
    plot(L,Io);
    grid on
    box on
    xlabel('Wavelength [nm]');
    ylabel('I_0: System');
else
    disp('Data does not contain uT.');
end

% --- Executes on button press in export_spectra.
function export_spectra_Callback(hObject, eventdata, handles)
% hObject    handle to export_spectra (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Run GUI to get user settings
ex_settings = DataExport();

% export data based on user settings
if ex_settings.exportgo
    if ex_settings.datatype == 1
        export_simulated_data_v2;
    else
        export_corrected_spectra_v5;
    end
end
guidata(hObject, handles);


% --- Executes on button press in outlierCheck.
function outlierCheck_Callback(hObject, eventdata, handles)
% hObject    handle to outlierCheck (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% get data from table 1
Data = get(handles.uitable1,'data');
iSelect = cell2mat(Data(:,1));
if sum(iSelect)==0
    msgbox('Select all relevant files.')
    return
end
thks = Data(iSelect,3);
rmses = Data(iSelect,end);
NumF = sum(iSelect);

% check if some Thk are missing
for nF = 1:NumF
    if isempty(thks{nF})
        msgbox('One or more thicknesses are missing.')
        return
    end
end
thks = cell2mat(thks);

% check if some rmses are missing
for nF = 1:NumF
    if isempty(rmses{nF})
        msgbox('One or more RMSEs are missing.')
        return
    end
end
rmses = cell2mat(rmses);

% check for outliers
[ansr] = OutlierCheck(thks,rmses);

% give affirmative if no outliers
if ansr.isgood
    msgbox('No outliers found.');
end

% --- Executes on button press in FFTanalysis.
function FFTanalysis_Callback(hObject, eventdata, handles)
% hObject    handle to FFTanalysis (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Error checks
if handles.CtrlData.mode ~= 3
    msgbox('FFT analysis currently only works with MM data.','Error','error');
    return;
end
if size(handles.uitable1.Data,2) ~= 4
    msgbox('Last fit must be a serial fit with SiO2 thickness the only fit parameter!','Error','error');
    return;
end
iSelect = handles.uitable1.Data(:,1);
iSelect = cell2mat(iSelect);
if sum(iSelect) == 0
    msgbox('You must select some files first!','Error','error');
    return;
end
disp('FFT Analysis GO!');

% wavelength match for sigy & uT
maxl = max(handles.results.Ls);
minl = min(handles.results.Ls);
iL = (handles.L >= minl) & (handles.L <= maxl);
SigY = handles.SigY(iL,:,:);
uT = permute(handles.Y(iL,end,:),[3 1 2]);

% call GUI
FFTanalysis(handles.uitable1.Data,...
            handles.results.Ye,...
            handles.results.Ys,...
            uT,...
            SigY,...
            handles.results.Ls,...
            handles.PathMat);
