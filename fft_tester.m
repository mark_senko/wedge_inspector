% FFT tester

% Setup
sf = 1000;                  % sample frequency
nums = 2000;                % number of samples
t = (0:1:nums-1)./sf;       % time
f = ((0:(nums/2))/nums)*sf; % frequency

% Signal
a = sin(2*pi*50*t.^0.9+pi/4);
a = a + 0.2*randn(1,nums);
figure('windowstyle','docked');
plot(t,a);
ylabel('Signal (a)');
xlabel('Time [s]');
grid on

% FFT
A = fft(a)./nums;           % fft
As = A(1:nums/2+1);         % single sided spectrum

% plot amplitude
figure('windowstyle','docked');
plot(f,abs(As));
ylabel('|A|');
xlabel('Frequency [Hz]');
grid on

% plot phase
figure('windowstyle','docked');
iA = abs(As)<0.1;           % threshold for phase graphing
As2 = As;                   % copy fft result
As2(iA) = 0;                % nullify points below threshold
plot(f,angle(As2)./pi);
ylabel('\angleA');
xlabel('Frequency [Hz]');
grid on

