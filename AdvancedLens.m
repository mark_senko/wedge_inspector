    % Do Advanced Lens Integration
    if ToolParms.NumNASlices > 1
        Mlk_NA = 0;
        ons = ones(1,1,NumWvls);
        zos = zeros(1,1,NumWvls);
        Rot = [ons,zos,zos,zos;zos,ons,ons,zos;zos,ons,ons,zos;zos,zos,zos,ons];
%         psi_test = zeros(ToolParms.NumNASlices,ToolParms.NumNASlices);
        MMt = zeros(4,4,NumWvls,ToolParms.NumNASlices);
        for nS = 1:ToolParms.NumNASlices
            Mlk_NA2 = 0;
            MyAOI = AOI + ToolParms.dAOI(:,nS);
            theta = 90 - MyAOI;
            theta0 = 90 - AOI;
            if (ToolParms.alpha==1) && (ToolParms.beta==1)
                [rp,rs] = Fresnel3(MyAOI, ToolParms.Lh, NK, To, ToolParms.r01a, ToolParms.nstop);
                rs = -rs; % debug
                Mlk = JonesTF2Mueller(rp, rs);
                Mlk = permute(Mlk,[3 2 1]);
                Mlk = reshape(Mlk,[4 4 NumWvls]);
            else
                Mlk = UniMM_v5(NK, To, MyAOI, ToolParms.Lh, ToolParms.alpha, ToolParms.beta);
            end

            Mlk = complex(Mlk);
            count = 0;
            for nP = 1:ToolParms.NumNASlices
                phi = ToolParms.dAOI(:,nP);
                lim = acosd(sind(theta).*sind(theta0) + cosd(theta).*cosd(theta0).*cosd(phi));
                if lim <= ToolParms.dAOImax
                    psi = atand(cosd(90-MyAOI).*tand(phi));
%                     psi = psi./20;
%                     psi_test(nS,nP) = psi(1);
                    psi = permute(psi,[3 2 1]);
                    Rot(2,2,:) = cosd(2*psi);
                    Rot(3,3,:) = Rot(2,2,:);
                    Rot(2,3,:) = sind(2*psi);
                    Rot(3,2,:) = -Rot(2,3,:);
                    Rot = complex(Rot);
                    Mlk = MM3D_v2(permute(Rot,[2 1 3]),Mlk);
                    Mlk = MM3D_v2(Mlk,Rot);
%                     Mlk = MM3D_v2(Mlk,Rot);
                    Mlk_NA2 = Mlk_NA2 + Mlk;
                    count = count + 1;
                    if nS == 3
                        MMt(:,:,:,nP) = Mlk;
                    end
                end
            end
            Mlk_NA = Mlk_NA + Mlk_NA2/count.*permute(ToolParms.W(:,nS),[3 2 1]);
        end
        figure % plot different rotational MMs
        colormap Parula
        for nM = 1:16
            subplot(4,4,nM);
            [rw,cl]=ModB(nM,4);
            tmp = MMt(rw,cl,:,:);
            tmp = permute(tmp,[3 4 1 2]);
            plot(ToolParms.Lh,tmp);
        end
        
        figure % plot M22 average vs M11
        tmp = MMt(2,2,:,:);
        tmp = permute(tmp,[3 4 1 2]);
        tmp1 = mean(tmp,2);
        plot(ToolParms.Lh,tmp1);
        hold on
        tmp = MMt(1,1,:,:);
        tmp = permute(tmp,[3 4 1 2]);
        tmp2 = mean(tmp,2);
        plot(ToolParms.Lh,tmp2);
        hold off
        
        figure
        plot(ToolParms.Lh,tmp1./tmp2);
%         figure
%         contourf(psi_test);
        Mlk_Out_h = Mlk_NA;
        
    else  % Skip NA integration if NA = 0
        MyAOI = AOI;
        if (ToolParms.alpha==1) && (ToolParms.beta==1)
            [rp,rs] = Fresnel3(MyAOI, ToolParms.Lh, NK, To, ToolParms.r01a, ToolParms.nstop);
            rs = -rs; % debug
            Mlk = JonesTF2Mueller(rp, rs); 
        else
            Mlk = UniMM_v4(NK, To, MyAOI, ToolParms.Lh, ToolParms.alpha, ToolParms.beta);
        end
        Mlk_Out_h = Mlk;
    end