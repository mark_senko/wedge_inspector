function [data]=ChromaticSource2slit02(plotflag)
mfile=mfilename;
if nargin<1, plotflag=0; end  
%  Index          X            Y 
%           focus shift um   wavelength nm
data=[
    1         82.018449      182.642250
    2         101.135364      196.749413
    4         97.828049      214.785519
    5         80.671841      232.771131
    6         74.069384      246.784519
    7         84.603215      278.908552
    8         122.169829      321.157908
    9         195.985220      403.646463
    10         262.544216      490.119263
    11         312.614221      580.542646
    12         346.195236      674.916612
    13         371.189847      793.323491
    14         391.555988      933.772362
    15         433.637090      1160.530527
    16         493.562643      1317.166449
    17         583.175462      1453.857096
    18         670.835320      1544.417535
    19         760.484657      1614.931712
    20         888.375572      1699.622783
     ];
if plotflag
  if 0
    im=load('ChromaticSource2slit01.png');
  end
  figure;
  if 0
    imagesc(im); axis equal;
    hold on;
  end
  plot(data(:,2),data(:,3),'o-')
  xlabel('Focal Shift (\mum)');
  ylabel('\lambda (nm)');
  title('from ChromaticSource2slit.m T2LensTranslationEffects(003).ppt');
  righttext(pwdshort(3,which(mfile)));
  keyboard
end