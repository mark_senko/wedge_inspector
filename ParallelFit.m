% Parallel fit script
tic;
% Setup simulation
tabdata={};
SimSetup;
FitSetup;
 
%% Parallel Fit Routine

PP = zeros(NumF,sum(iX));
MSE = zeros(NumF,1);
NumIter = zeros(NumF,1);

[s1,s2,s3]=size(Ys);
LsSelect = handles.CtrlData.MatLS;
set(handles.abortbutton,'visible','on');
axhs = {handles.axes1,handles.axes2};
figh = handles.figh;
axh = handles.axh;
RMSEthres = handles.CtrlData.RMSEthres;

iX(1) = false;
P = X(iX);
LL = LL(iX);
UL = UL(iX);
Po = X(iX);
iX2 = (iX & ~iX);
iX2(1) = true;
kend = handles.CtrlData.ParFitIter;
dYsk = zeros(s1,s2,kend+1);
Nparms = sum(iX);

for nF = 1:NumF
    X(1) = Thk(nF);
    Yexp = Y(:,:,nF);    
    [Ysim,Yexp] = UpdateSpectra(X, iX, tX, ToolParms, ModParms, Yexp);
    
    % Save off best values
    Ys(:,:,nF) = Ysim;
    Ye(:,:,nF) = Yexp;          

    if sum(iW) > 50
        % Graph via WIgrapher
        [figh,axh] = WIgrapher(figh,axh,Ls,Yexp,1,[1 0 0]);
        WIgrapher(figh,axh,Ls,Ysim,0,[0 0 1]);  
        % Update wait bar
        upwait_v2(1,handles.axes1,['Initial Simulation of file: ' num2str(nF)],...
                  nF/NumF,handles.axes2,[num2str(nF) ' of ' num2str(NumF) ' files completed']);
    end

    % Stop if necessary
    if ABORT == 1
        ABORT = 0;
        op.iterations = -1;
        set(handles.abortbutton,'visible','off');
        upwait_v2('c',handles.axes1,'Idle','c',handles.axes2,'Idle');
        guidata(hObject, handles);
        return;
    end
end

if JAWmse~=1
    dYsk(:,:,1) = (sum(((Ye-Ys)./SigY).^2,3)./(NumF-Nparms)).^0.5;
else
    dYsk(:,:,1) = (sum((Ye-Ys).^2,3)./NumF).^0.5; %compute dYs for no iterations
end

% Update wait bar
upwait_v2(0,handles.axes1,'Initiating Parallel Fit',...
          0,handles.axes2,['0 of ' num2str(kend) ' iterations completed']);

lambda = 0.1;
% Ps_WI = zeros(length(Po),kend+1);
% Ps_WI(:,1) = Po;
for k = 1:kend
%     Ye = Y(iW,:,:); % reset Ye for parallel fit
%     Obj3H = @(Po)Obj2(Po, X, iX, tX, ToolParms, ModParms, Ye, Thk);
    Obj3H = @(Del)Obj3(Po, Del, X, iX, tX, ToolParms, ModParms, Y, Thk, axhs);
    if JAWmse ~= 1
%         [P,op]=LbMqBASICv7(Obj3H,Po,LL,UL,1e-4,RMSEthres,lambda,3,axhs,SigY);
        [P,op]=LbMqBASICv8(Obj3H,Po,LL,UL,1e-6,RMSEthres,lambda,3,SigY);
    else
%         [P,op]=LbMqBASICv7(Obj3H,Po,LL,UL,1e-4,RMSEthres,lambda,3,axhs);
        [P,op]=LbMqBASICv8(Obj3H,Po,LL,UL,1e-6,RMSEthres,lambda,3);
    end
%     [P,op]=LbMqBASICv5(Obj2H,Po,LL,UL,1e-4,RMSEthres,0.2,2,axhs);
    lambda = op.lambda;

    % Update params - ToolParms
    X(iX) = P;
%     Ps_WI(:,k+1) = P;
    ToolParms.pThk = Thk; % for Fourier model dz!
    [ToolParms,ModParms] = UpdateParms(X,iX,tX,ToolParms,ModParms);
    
    % Fit individual
%     profile on
    for nF = 1:NumF
%         Yexp = Y(iW,:,nF); %original line
        Yexp = Y(:,:,nF); % Adjusted for passing full wvl vector
        X(1) = Thk(nF);
        if FlgFitThk
            P2 = X(iX2);
            Obj1H = @(P)Obj1(P,X,iX2,tX,ToolParms,ModParms,Yexp);
            if JAWmse ~= 1
                [P2,op]= LbMqBASICv7(Obj1H,P2,0.1,0.1,1e-6,RMSEthres,0.01,3,[],SigY(:,:,nF));
            else
                [P2,op]= LbMqBASICv7(Obj1H,P2,0.1,0.1,1e-6,RMSEthres,0.01,3,[]);
            end
            Thk(nF) = P2(1);    
        end

        [Ysim,Yexp] = UpdateSpectra(X, iX, tX, ToolParms, ModParms, Yexp);

        if sum(iW) > 50
            % Print to Axes Array
            [figh,axh] = WIgrapher(figh,axh,Ls,Yexp,1,[1 0 0]);
            WIgrapher(figh,axh,Ls,Ysim,0,[0 0 1]);          
            % Update wait bar
            upwait_v2(nF/NumF,handles.axes1,['Updating Simulation: ' num2str(nF)],...
                    1,[],' ');
        end
            

        % Stop if necessary
        if ABORT == 1
            ABORT = 0;
            op.iterations = -1;
            set(handles.abortbutton,'visible','off');
            upwait_v2('c',handles.axes1,'Idle','c',handles.axes2,'Idle');
            guidata(hObject, handles);
            return;
        end

        % If last iteration, save off all variables
        if k == kend
            if FlgFitThk      
                PP(nF,:) = [Thk(nF) P'];
            else
                PP(nF,:) = P';
            end
            X(iX) = P;
%             MSE(nF) = sqrt(sum(sum((Yexp-Ysim).^2))./(s1*s2));
            MSE(nF) = MSEcalc(JAWmse,(Yexp-Ysim),SigY(:,:,nF),Nparms);
        end
        Po = P;
        Ys(:,:,nF) = Ysim;
        Ye(:,:,nF) = Yexp;
    end
%     profile off
%     profile viewer
    
    % Plot RMSE values
    if JAWmse~=1
        dYsk(:,:,k+1) = (sum(((Ye-Ys)./SigY).^2,3)./(NumF-Nparms)).^0.5;
    else
        dYsk(:,:,k+1) = (sum((Ye-Ys).^2,3)./NumF).^0.5; %compute dYs for no iterations
    end %compute dYs
    for j = 1:k+1
        % Print to Axes Array
%         disp(k);
        if j==1
            [figh,axh] = WIgrapher(figh,axh,Ls,dYsk(:,:,j),1,[0 (1-j/12) 0]);
        else
            [figh,axh] = WIgrapher(figh,axh,Ls,dYsk(:,:,j),0,[0 (1-j/12) 0]);
        end
    end
    
    % Update wait bar
    upwait_v2(1,[],' ',...
              k/kend,handles.axes2,[num2str(k) ' of ' num2str(kend) ' iterations completed']);
end

%% Wrap up - display results
% Compute total RMSE
% tRMSE = sqrt(sum(sum(sum((Ye-Ys).^2)))./(s1*s2*s3));
tRMSE = MSEcalc(JAWmse,(Ye-Ys),SigY,Nparms);
upwait_v2(1,handles.axes1,'Parallel Fit Finished',...
          1,handles.axes2,['Total RMSE: ' num2str(tRMSE)]);
set(handles.abortbutton,'visible','off');
% save('P_iter.mat','Ps_WI');

% update handles
handles.results.Ls = Ls;
handles.results.Ye = Ye;
handles.results.Ys = Ys;
handles.results.MSE = MSE;
handles.results.Nparms = Nparms;
handles.axh = axh;
handles.figh = figh;

% Find parameter names
iX(1) = false;
FloatedParmNames = pNames(iX);
FloatedParmNames = [{'Thk'}; FloatedParmNames; {'RMSE'}];
if FlgFitThk
    FloatedParmVal = [PP MSE];
else
    FloatedParmVal = [Thk PP MSE]; 
end

% Correct BW output
%iF = strcmp(FloatedParmNames,'BW_1') | strcmp(FloatedParmNames,'BW_2');
%FloatedParmVal(:,iF) = FloatedParmVal(:,iF).*2.355;

handles.FloatedParmNames = FloatedParmNames;
handles.FloatedParmVal = FloatedParmVal;

% update results table
Data = get(handles.uitable1,'data');
NewData = cell(size(Data,1),size(FloatedParmVal,2)+2);
NewData(:,1:3) = Data(:,1:3);
NewData(iSelect,3:end) = num2cell(FloatedParmVal);
ColNames = [{' ','File'} , FloatedParmNames'];
set(handles.uitable1,'ColumnName',ColNames);
set(handles.uitable1,'data',NewData);

toc
