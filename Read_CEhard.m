function [AOI,DO,WnDsp] = Read_CEhard(File_CEhard)
% reads D.O. from 'CompleteEASEhard.cnf'

% % debug
% CCC;
% File_CEhard = 'C:\Users\pvagos\Documents\Temp\CompleteEASEhard.cnf';

fid  =  fopen(File_CEhard);
Data  =  textscan(fid, '%s', 'delimiter', '\n', 'whitespace', '');
fclose(fid);
tmp = Data{1};

% Read Delta Offsets
Offset1 = str2double(tmp{76}(15:end));
Offset2 = str2double(tmp{77}(15:end));
Offset3 = str2double(tmp{78}(15:end));
Offset4 = str2double(tmp{79}(15:end));
DO = [Offset1 Offset2 Offset3 Offset4];
% disp(DO)

% Read AOI
tmp2 = tmp{10};
AOI = str2double(tmp2(15:end));

% Read WinRet & Win Disp
WnDsp=zeros(2,3);
WnDsp(1,1) = str2double(tmp{73}(9:end));
WnDsp(2,1) = str2double(tmp{74}(9:end));
WnDsp(1,2) = str2double(tmp{83}(10:end));
WnDsp(1,3) = str2double(tmp{84}(10:end));
WnDsp(2,2) = str2double(tmp{85}(12:end));
WnDsp(2,3) = str2double(tmp{86}(12:end));
