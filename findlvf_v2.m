function [lvf_c,lvf_a] = findlvf_v2(Ls,rmse_l)
% find lvf center frequency
% use new dual erf method & start at 300 nm. 3/19/2019 AGB

iL = (Ls > 300) & (Ls < 510);
L = Ls(iL);
rmse_l = rmse_l(iL);

%% Round 1, ask user for peak guess

hf = figure('Name','Please Estimate the LVF Peak Location:');
plot(rmse_l);
set(gca,'xtick',[]);
set(gca,'xticklabel',[]);
[lvf_g,~]=ginput(1);
g_x = round(lvf_g);
lvf_g = L(g_x)/g_x*lvf_g;
delete(hf);

%% Round 2, Initial fit without peak center

P0 = [0.002, 16, 8, 8, 0.8, 0.05, 0.0007].';
dfnc2 = @(P) sum((P(7)+P(6).*exp(-L/(P(5)*100)) + 0.5/erf(P(2)/2)*P(1).*(erf((L-lvf_g+P(2))./P(3)) - erf((L-lvf_g-P(2))./P(4))) -rmse_l).^2);
P1 = fminsearch(dfnc2,P0);
% LL = [1e-6,1  ,0.001,0.001,1e-6,1e-6,1e-6].';
% UL = [0.1 ,100,10   ,10   ,100 ,0.1 ,0.1 ].';
% opts.Algorithm = 'levenberg-marquardt';
% opts.StepTolerance = 1e-12;
% P1 = lsqnonlin(dfnc2,P0);
% [P1,~] = LbMqBASICv5_new(dfnc2,P0,LL,UL,1e-8,1e-8,0.2,100,[]);
if rmse_l(g_x)/P1(7) < 1.3 % check to see if peak is 30% greater than rmse floor
    lvf_a = false;
else
    lvf_a = true;
end

%% Round 3, fit for peak

% dfnc = @(P) (P(6)+P(5).*exp(-L/(P(4)*100)) + P(1).*exp(-(L-P(2)*1000).^2./(P(3)*1000))-rmse_l);
% dfnc = @(P) (P(6)+P(5).*exp(-L/(P(4)*100)) + (P(1)*P(3)^2)./((L-P(2)*1000).^2+P(3)^2)-rmse_l);
dfnc = @(P) P(7)+P(6).*exp(-L/(P(5)*100)) + 0.5/erf(P(2)/2)*P(1).*(erf((L-P(8)+P(2))./P(3)) - erf((L-P(8)-P(2))./P(4)));
dfnc2 = @(P) sum((P(7)+P(6).*exp(-L/(P(5)*100)) + 0.5/erf(P(2)/2)*P(1).*(erf((L-P(8)+P(2))./P(3)) - erf((L-P(8)-P(2))./P(4))) -rmse_l).^2);

P2 = [P1; lvf_g];
% LL =    [1e-5, 1  ,2,2,0,1e-4,1e-8,5]';
% UL =    [1    ,1  ,2,2,0,1   ,1   ,5]';
rmse_o = dfnc([P0; lvf_g]);
rmse_g = dfnc(P2);

% [P,~] = LbMqBASICv5_new(dfnc2,P2,LL,UL,1e-8,1e-8,0.2,100,[]);
opts.MaxFunEvals = 300;
P = fminsearch(dfnc2,P2,opts);
% P = lsqnonlin(dfnc2,P2);
lvf_c = P(8);

%% display results
figure('windowstyle','docked','Name','LVF Position Fit');
% ft = P(6)+P(5).*exp(-L/(P(4)*100)) + P(1).*exp(-(L-P(2)*1000).^2./(P(3)*1000));
% ft = P(6)+P(5).*exp(-L/(P(4)*100)) + (P(1)*P(3)^2)./((L-P(2)*1000).^2+P(3)^2);
ft = dfnc(P);
plot(L,rmse_l,'b');
hold on
plot(L,rmse_o,'--g');
plot(L,rmse_g,'--k');
plot(L,ft,'r');
hold off
legend('Wedge RMSE','Inital Parameters','User Guess Fit','Final Fit');
xlabel('Wavelength [nm]');
ylabel('RMSE');
title(['LVF Center Wavelength: ' num2str(lvf_c)]);
grid on
% disp(P);

end

