function [ToolParms,ModParms] = UpdateParms(X,iX,tX,ToolParms,ModParms)

% Figure out which parameters are being fit
tX2 = tX(iX);

% Readjust Lh for BW calc
BWflg = false;
sX2 = (tX2 == 4);
if sum(sX2)>0
    BWflg = true;
    sX = (tX == 4);
    ToolParms.BWp.val = X(sX)';
    ToolParms.BWp.s = CalcWvlDep(ToolParms.Lo,ToolParms.BWp,1,0)/2.355;% Convert from FWHM to sigma

    spr = linspace(-3,3,ToolParms.BW_nslc+2)'; % create vector with correct BW spacing
    spr = spr(2:end-1);
    iWv = (ToolParms.Lo<=1000);
    iWr = (ToolParms.Lo>1000);
    %ToolParms.Lh = Lh+Lspr;
    if ToolParms.BW_VIS
        Lh_v = [repmat(ToolParms.Lo(iWv),ToolParms.BW_nslc,1)];
        Lh_v = sort(Lh_v);
        Lspr_v = [];
        for ii = 1:sum(iWv)
            Lspr_v = [Lspr_v;repmat(ToolParms.BWp.s(ii),ToolParms.BW_nslc,1).*spr];
        end
    else
        Lh_v = ToolParms.Lo(iWv);
        Ldiv = length(Lh_v);
        Lspr_v = zeros(Ldiv,1);
    end
        
    if ToolParms.BW_IR
        Lh_r = [repmat(ToolParms.Lo(iWr),ToolParms.BW_nslc,1)];
        Lh_r = sort(Lh_r);
        Lspr_r=[];
        for ii = 1:sum(iWr)
            Lspr_r = [Lspr_r;repmat(ToolParms.BWp.s(ii+sum(iWv)),ToolParms.BW_nslc,1).*spr];
        end
    else
        Lh_r = ToolParms.Lo(iWr);
        Ldiv2 = length(Lh_r);
        Lspr_r = zeros(Ldiv2,1);
    end       
    Lh = [Lh_v;Lh_r];
    Lspr = [Lspr_v;Lspr_r];
    ToolParms.Lh = Lh+Lspr;
end
Lh = ToolParms.Lh;
NumL = length(Lh);
iLh = [ToolParms.iLh_VIS,ToolParms.iLh_IR];
L = ToolParms.L;

% Adjust AOIs and dAOIs
sX2 = (tX2 == 2);
% disp(CalcWvlDep(Lh,ToolParms.AOIp,1,iLh));
if (sum(sX2)>0) || BWflg
%     disp('AOI update');
    sX = (tX == 2);
    ToolParms.AOIp.val = X(sX)';
    ToolParms.AOIp.s = CalcWvlDep(Lh,ToolParms.AOIp,1,iLh);
%     disp(CalcWvlDep(Lh,ToolParms.AOIp,1,iLh));
end
sX2 = (tX2 == 3);
if (sum(sX2)>0) || BWflg
%     disp('NA update');
    sX = (tX == 3);
    ToolParms.NAp.val = X(sX)';
    NAs = CalcWvlDep(Lh,ToolParms.NAp,1,iLh);
    span = linspace(-1,1,ToolParms.NumNASlices)'; %setup NA slices
    ToolParms.dAOIs = span*asind(NAs);
    ToolParms.NAp.s = NAs;
end

% Change NKs if required
if (sum((tX2 == 8) | (tX2 == 9) | (tX2 == 16) | (tX2 == 17))>0) || BWflg
%     disp('NK update');
    sX = (tX == 8);
    ToolParms.ALPHAp.val = X(sX)';
    alphas = CalcWvlDep(Lh,ToolParms.ALPHAp,1,iLh);
    ToolParms.ALPHAp.s = alphas;
    sX = (tX == 9);
    ToolParms.BETAp.val = X(sX)';
    betas = CalcWvlDep(Lh,ToolParms.BETAp,1,iLh);
    ToolParms.BETAp.s = betas;
    if ModParms.NKselect == 2
        sX = (tX==16);
        ModParms.SiDat(:,3) = num2cell(X(sX));
        sX = (tX==17);
        ModParms.SiO2Dat(:,3) = num2cell(X(sX));
        Noffset = ModParms.SiO2Dat{9,3};
        [NK_Si, NK_intr, NK_sio2_xy] = Si_SiO2_Aspnes_v2(Lh,ModParms.SiDat,ModParms.SiO2Dat);
        NK_sio2_xy = NK_sio2_xy + Noffset;
        NK_sio2_z = NK_sio2_xy;
        NK = [NK_Si,NK_Si,NK_intr,NK_sio2_xy,NK_sio2_z].';
        NK = AdjustNK(NK,alphas,betas);
        AirNK = X(sX); AirNK = AirNK(6:8);
        ModParms.SiO2Dat(6:8,3) = num2cell(AirNK);
        AirNK = ModParms.SiO2Dat{6,3} + ModParms.SiO2Dat{7,3}./(Lh./1000).^2 + ModParms.SiO2Dat{8,3}./(Lh./1000).^4;
        NK = [NK; AirNK.'];
    else
        NK_Si = ModParms.ppNK{1}(Lh)-1i.*ModParms.ppNK{2}(Lh);
        sX = (tX==16);
        ModParms.SiDat(27:28,3) = num2cell(X(sX));
        Noffset = ModParms.SiDat{27,3};
        Koffset = ModParms.SiDat{28,3};
        NK_Si = NK_Si + Noffset - 1i*Koffset;
        NK_Si = real(NK_Si) + 1i*min(imag(NK_Si),0);
        sX = (tX == 17);
        ModParms.SiO2Dat(6:9,3) = num2cell(X(sX));
        Noffset = ModParms.SiO2Dat{9,3};
        NK_intr = ModParms.ppNK{3}(Lh)-1i.*ModParms.ppNK{4}(Lh);
        NK_sio2_xy = ModParms.ppNK{5}(Lh)-1i.*ModParms.ppNK{6}(Lh);
        NK_sio2_xy = NK_sio2_xy + Noffset;
        NK_sio2_z = ModParms.ppNK{7}(Lh)-1i.*ModParms.ppNK{8}(Lh);
        NK_sio2_z = NK_sio2_z + Noffset;
        NK = [NK_Si,NK_Si,NK_intr,NK_sio2_xy,NK_sio2_z].';
        NK = AdjustNK(NK,alphas,betas);
        AirNK = ModParms.SiO2Dat{6,3} + ModParms.SiO2Dat{7,3}./(Lh./1000).^2 + ModParms.SiO2Dat{8,3}./(Lh./1000).^4;
        NK = [NK; AirNK.'];        
    end
    ModParms.NK = NK;
end

% Weight for NA integration 
Wflg = (BWflg && (ToolParms.BPtype > 2));
if (sum(tX2 == 10)>0) || Wflg
    sX = (tX == 10);
    BMp = ToolParms.BMp;
    BMp.val = permute(reshape(X(sX),[],5),[2 1]);
    BMp.s(1,:) = CalcWvlDep(Lh,BMp,1,iLh);
    BMp.s(2,:) = CalcWvlDep(Lh,BMp,2,iLh);
    BMp.s(3,:) = CalcWvlDep(Lh,BMp,3,iLh);
    BMp.s(4,:) = CalcWvlDep(Lh,BMp,4,iLh);
    BMp.s(5,:) = CalcWvlDep(Lh,BMp,5,iLh);
    ToolParms.BMp = BMp;
    NumNASlices = ToolParms.NumNASlices;
    dx = 2/NumNASlices;
    x = (1:NumNASlices)*dx;
    x = x-mean(x);
    switch ToolParms.BPtype
        case 1
            W = 2*sqrt(1-x.^2);
            W = repmat(W',[1 NumL]);
        case 2
            W = ones(NumNASlices,NumL);
        case 3
            x0 = [-1; -0.5; 0; 0.5; 1];
            x0 = repmat(x0,[1 NumL]);
            x = repmat(x.',[1 NumL]);
            y = repmat(Lh.',[NumNASlices 1]);
            y0 = repmat(Lh.',[5 1]);
            W = interp2(y0,x0,BMp.s,y,x,'spline');
        case 4
            W = repmat(sqrt(1-x'.^2),[1 NumL]).*(x'*BMp.s(1,:)+1);
        case 5
            W = 2*sqrt(repmat(BMp.s(2,:),[NumNASlices 1])-repmat(x'.^2,[1 NumL]));
        case 6
            W = repmat(sqrt(1-x'.^2),[1 NumL]).*(x'*BMp.s(1,:)+1);
        case 7
            NAp.s = ToolParms.NAp.s;
            AOIp.s = ToolParms.AOIp.s;
            dAOIs = ToolParms.dAOIs;
            W = asind(sqrt(NAp.s.^2 - cosd(asind(NAp.s)).^2.*tand(dAOIs).^2));
            W = acosd(sind(90-AOIp.s+dAOIs).^2+cosd(90-AOIp.s+dAOIs).^2.*cosd(2.*W));
    end
    if ToolParms.FlgUseShaper
        W = W.*ToolParms.G;
    end
    % normalize weights
    W = W./sum(W,1);
    ToolParms.W = W;
end

% Update DO/WD/Lens Parameters
if (sum(tX2==11)>0)
%     disp('DO update');
    sX = (tX == 11);
    ToolParms.DO = [X(sX)' ToolParms.DO(4)];
end
if (sum(tX2==12)>0)
    sX = (tX == 12);
    DO4 = X(sX);
    ToolParms.DO = [ToolParms.DO(1:3) DO4(3)];
end
sX = (tX2 == 13);
if sum(sX)>0
    sX = (tX == 13);
    ToolParms.WD = permute(reshape(X(sX),3,2),[2 1]);
end
sX = (tX2 == 14); 
if sum(sX)>0 || (BWflg && (ToolParms.nlens==1))
    sX = (tX == 14); 
%     disp('Update made it to LNSp!');
    ToolParms.LNSp.val = permute(reshape(X(sX),[],10),[2 1]);
    ToolParms.LNSp.s = zeros(10,NumL);
    for i=1:10
        ToolParms.LNSp.s(i,:) = CalcWvlDep(Lh,ToolParms.LNSp,i,iLh);
    end
end
sX = (tX2 == 15); 
if sum(sX)>0 || (BWflg && (ToolParms.Comp.enable==1))
    sX = (tX == 15); 
    ToolParms.Comp.CCp.val = permute(reshape(X(sX),[],16),[2 1]);
    ToolParms.Comp.CCp.s = zeros(16,length(L));
    for i=1:16
        ToolParms.Comp.CCp.s(i,:) = CalcWvlDep(L,ToolParms.Comp.CCp,i,iLh);
    end
end

% Change WaveCal numbers
if (sum(tX2==5)>0)
%     disp('Wcal_VIS update');
    sX = (tX == 5);
    ToolParms.Wcal_n = [X(sX)' ToolParms.Wcal_n(6:9)];
end
if (sum(tX2==6)>0)
%     disp('Wcal_IR update');
    sX = (tX == 6);
    ToolParms.Wcal_n = [ToolParms.Wcal_n(1:5) X(sX)'];
end

sX = (tX2 == 7);
if sum(sX)>0
    sX = (tX == 7);
%     disp('ThkEMA update');
    ToolParms.ThkEMA = X(sX);
end


if ~isreal(ToolParms.AOIp.s)
    keyboard;
end

end

