function [figh,axh] = WIgrapher(figh,axh,L,Y,wipe,clr)
% Graphs spectra from WI.
% Automatically switches formats depending on data in Y.
% Expects columns of data, with either MM 11 12 13... or N C S types.
% CLR is a color variable and must be of the RGB type.

[~,s2] = size(Y);
if ~isvalid(figh)
    axh = gobjects(0);
% else
%     figure(figh);
end

if (isempty(axh) || (s2 ~= length(axh))) % Must create new axes
    if ~isempty(axh)
        close(figh);
    end
    figh = figure('Name','W.I. Grapher');
    
    if s2 == 16
        axh = gobjects(16,1);
        for i = 1:16
            axh(i) = subplot(4,8,[i*2-1 i*2]);
            n = floor(i/4.1)+1;
            m = i-floor(i/4.1)*4;
            plot(L,Y(:,i),'Color',clr);
            xlim([L(1) L(end)]);
            title(['MM' num2str(n) num2str(m)]);
            grid on
            box on
            if i < 13
                xticklabels({});
            else
                xticklabels('auto');
            end
        end
    else
        axh = gobjects(3,1);
        ylb = {'\bf{N}','\bf{C}','\bf{S}'};
        for i = 1:3
            axh(i) = subplot(3,3,[((i-1)*3+1) 3*i]);
            plot(L,Y(:,i),'Color',clr);
            xlim([L(1) L(end)]);
            ylabel(ylb{i},'Rotation',0);
            grid on
            box on
        end
    end
else
    for i = 1:length(axh)
        if wipe == 1
            cla(axh(i));
        else
            hold(axh(i),'on');
        end
        
        plot(axh(i),L,Y(:,i),'Color',clr);
        xlim(axh(i),[L(1) L(end)]);
        grid on
        box on
    end
end

end

