function [] = SaveSettings(varargin)
% save settings for WI v4.1a

if length(varargin)==1
    if ~exist('DefaultPathDataFiles.mat','file')
        PathF= [cd '\'];
    else
        load DefaultPathDataFiles PathF
        if ~exist('PathF','var')
            PathF = [cd '\'];
        end
        if PathF == 0
            PathF = [cd '\'];
        end
    end
    [FileName,PathF] = uiputfile('*.xlsx','Export to Excel',PathF);
    if PathF == 0 ; return; end
    if isempty(PathF)
        PathF = [cd '\'];
    end
    save DefaultPathDataFiles PathF
    MyFile = [PathF FileName];
else
    MyFile = varargin{2};
end

handles = varargin{1};
Data = get(handles.uitable2,'data');
ColNames = get(handles.uitable2,'ColumnName');
settings = {};
settings(1,:) = {'SiF',handles.ModParms.SiF};
settings(2,:) = {'EMAF',handles.ModParms.EMAF};
settings(3,:) = {'SiO2xyF',handles.ModParms.SiO2xyF};
settings(4,:) = {'SiO2zF',handles.ModParms.SiO2zF};
settings(5,:) = {'NKselect',handles.ModParms.NKselect};
settings(6,:) = {'mthd',handles.ModParms.mthd};
settings(7,:) = {'mode',handles.CtrlData.mode};
settings(8,:) = {'centeredWcal',handles.CtrlData.centeredWcal};
settings(9,:) = {'NewLens',handles.CtrlData.NewLens};
settings(10,:) = {'BmProf',handles.CtrlData.BmProf};
settings(11,:) = {'BwSlcs',handles.CtrlData.BwSlcs};
settings(12,:) = {'NaSlcs',handles.CtrlData.NaSlcs};
settings(13,:) = {'A3Apod',handles.CtrlData.A3Apod};
settings(14,:) = {'m11s',handles.CtrlData.m11s};
settings(15,:) = {'MatLS',handles.CtrlData.MatLS};
settings(16,:) = {'RMSEthres',handles.CtrlData.RMSEthres};
settings(17,:) = {'SerFitIter',handles.CtrlData.SerFitIter};
settings(18,:) = {'ParFitIter',handles.CtrlData.ParFitIter};
settings(19,:) = {'Depol',handles.CtrlData.Depol};
settings(20,:) = {'CompCal',handles.CtrlData.CompCal};
settings(21,:) = {'no_w4',handles.CtrlData.no_w4};
settings(22,:) = {'only_w0',handles.CtrlData.only_w0};
dumy = cell(length(handles.ModParms.SiDat)+length(handles.ModParms.SiO2Dat),1);
nkData = [handles.ModParms.SiDat; handles.ModParms.SiO2Dat];
nkData = [nkData, repmat(dumy,[1 12])];
dumy = cell(22,1);
settings = [dumy, settings, repmat(dumy,[1 14])];
Data = [ColNames'; Data; nkData; settings];
Data = Data(:, 2:end);
xlswrite(MyFile,Data);

if length(varargin)==1
    choice = questdlg('Saving is complete, would you like to open the file?','Save PCs','Yes','No','No') ;
    % Handle response
    switch choice
        case 'Yes'
            system(MyFile);
        case 'No'
    end
end

end

