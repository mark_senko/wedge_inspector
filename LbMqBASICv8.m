function [gbest,output] = LbMqBASICv8(varargin)
% [guess,output] = LbMqBASIC(func,guess,stol,ftol,lambda,maxiter)
% Basic non-linear least squares solver using Levenburg-Marquardt
% algorithm, to replace lsqnonlin in Matlab optimization package.

% func = handle of the function to be fit
% guess = vector of input parameters for func
% stol = step tolerance for stopping the fit
% ftol = function tolerance for stopping the fit
% maxiter = maximum number of iterations
% axhs = axes handles for updating display with progress

% Version 8 changes
% > Now uses Obj3 to compute entire Jacobian
% > Only for use with Parallel fit

% Version 7 changes
% > Added support for aborting during the Jacobian calculation

% Version 6 changes
% > Changed to 'varargin' cell structure
% > Added support for JAW style MSE calculation

% Version 5 changes
% > Changed limit behavior to adjust del instead of guess
% > Added support for upwait_v2
% > For use with WI v4.0+

% Version 4 changes
% > Added limits

% Version 3 changes
% > Display waitbar when maxiter = 2 (for Parallel fit)
% > Reduced MSE is always captured, regardless of rho
% > Trust region (rho) has been updated, no longer a guess
% > Uses RMSE throughout

% Turn on/off debug output
DEBUG_LM8 = false;

% Gather variables
nvarargs = length(varargin); 
if nvarargs == 8
    func = varargin{1};
    guess = varargin{2};
    LL = varargin{3};
    UL = varargin{4};
    stol = varargin{5};
    ftol = varargin{6};
    lambda = varargin{7};
    maxiter = varargin{8};
    SigY = false;
    JAWmse = false;
elseif nvarargs > 8
    func = varargin{1};
    guess = varargin{2};
    LL = varargin{3};
    UL = varargin{4};
    stol = varargin{5};
    ftol = varargin{6};
    lambda = varargin{7};
    maxiter = varargin{8};
    SigY = reshape(varargin{9},[numel(varargin{9}) 1]);
    JAWmse = true;
else
    msgbox('NOT ENOUGH INPUTS TO NON-LINEAR SOLVER: LbMqBASICv8.m');
    return
end


% Initialize
global ABORT
n = 1;
iLim = (LL ~= UL);
del = LL - LL;
del(iLim) = (UL(iLim) - LL(iLim))*0.01;
del(~iLim)= LL(~iLim);
iLim = (LL == 0) & (UL == 0);
del(iLim) = guess(iLim)*0.01;
olddel = del-del;
% disp([LL, iLim, del]);
gbest = guess;
nparms = length(guess);
dynew = func([]);
datanum = length(dynew);
% J = zeros(datanum,nparms);
Lup = 3; Ldn = 0.1; % Used for scaling lambda
chi2new = FindChi(dynew,JAWmse,datanum,nparms,SigY);
chi2best = chi2new;
if JAWmse %check lengths for parallel fit
    if length(SigY)~=datanum
        fact = datanum/length(SigY);
        if mod(fact,1)~=0
            msgbox('ERROR: For JAW MSE calc, sigma and data lengths must match!');
            return;
        end
        SigY = repmat(SigY,[fact 1]);
    end
end

% Main loop
diffstep = repmat(stol,length(del),1);
while n <= maxiter
%     J = FinDiff(J,func,del,nparms,guess,axhs); % Always compute new Jacobian
    if (DEBUG_LM8); disp (['Iteration: ' num2str(n)]);end
    J = func([olddel,diffstep]);
    chi2 = chi2new;
    dy = dynew;
    
    if ABORT == 1 % if abort break loop and return  
        output.iterations = -1;
        output.lambda = lambda;
        return;
    end
    
    % Determine new del offset
    H = J.'*J;
    top = H+lambda.*diag(diag(H));
        itop = isnan(top);
        if (DEBUG_LM8 && sum(sum(itop)))>0; disp('top has NaN.'); end
        top(itop) = 1e-6;
        itop = isinf(top);
        if (DEBUG_LM8 && sum(sum(itop))>0); disp('top has inf.'); end
        top(itop) = sign(top(itop))*1e6;
    bot = J.'*dy;
    [U,D,V] = svd(top); % SVD method to handle singular matrices
    D = diag(D);
    iD = (abs(D) < 1E-5);
    if (DEBUG_LM8 && sum(iD)>0); disp('D has a zero.'); disp(D); end
    D(iD) = 1e4;
    D(~iD) = 1./D(~iD);
    D = diag(D);
    del = V*D*U'*bot;
    
    % Fold del back within limits
    iP = (guess+del+olddel > UL) & (UL ~= LL);
    if (DEBUG_LM8 && sum(iP)>0); disp('Upper bound reached.'); end
    del(iP) = LL(iP)+0.9.*(UL(iP)-LL(iP))+0.1.*(UL(iP)-LL(iP)).*exp((UL(iP)-(guess(iP)+del(iP)+olddel(iP)))./(UL(iP)-LL(iP)))-guess(iP)-olddel(iP);
    iP = (guess+del+olddel < LL) & (UL ~= LL);
    if (DEBUG_LM8 && sum(iP)>0); disp('Lower bound reached.'); end
    del(iP) = UL(iP)-0.9.*(UL(iP)-LL(iP))-0.1.*(UL(iP)-LL(iP)).*exp(((guess(iP)+del(iP)+olddel(iP))-LL(iP))./(UL(iP)-LL(iP)))-guess(iP)-olddel(iP);
    
    % Condition del to avoid NaN and Inf
    idel = (abs(del) < 1E-6);
    if (DEBUG_LM8 && sum(idel)>0); disp('del has a zero.'); disp(del); end
    del(idel) = sign(del(idel))*1E-6;
    idel = (abs(del) < 1E-6);
    del(idel) = 1E-6;

    if ~isreal(del); keyboard; end
   
    % Recompute function value
    dynew = func(del+olddel);
    
    % Compute new chi & rho
    chi2new = FindChi(dynew,JAWmse,datanum,nparms,SigY);
    rho = (chi2^2*datanum) - (chi2new^2*datanum);
    rho = rho/(del.'*(lambda.*diag(diag(H))*del+J.'*dy));
    
    % save off best results
    if (DEBUG_LM8); disp(['chi2new = ' num2str(chi2new) ' chi2best = ' num2str(chi2best)]);end
    if (chi2new < chi2best)
        if (DEBUG_LM8); disp('new best!');end
        chi2best = chi2new;
        gbest = gbest + del;
    end
    
    % continue if mse is reduced
    if chi2new < chi2
        olddel = olddel + del;
    end
    
    % resize lambda for next iteration
    if rho > 0.9
        if (DEBUG_LM8); disp('Lambda DOWN!');end
        lambda = max(lambda*Ldn,10e-7);
%         lambda = lambda - Ldn;
    elseif rho < 0.2
        if (DEBUG_LM8); disp('Lambda UP!');end
        lambda = min(lambda*Lup,10e7);
%         lambda = lambda + Lup;
    end
    
    % Break if no change in chi2 or if less than tolerance
    if (((abs(chi2new-chi2)) < stol) && (n ~= 1)) || (abs(chi2new) < ftol)
         if (DEBUG_LM8)
             disp(['stop iteration at ' num2str(n) ...
             ' chi2new: ' num2str(abs(chi2new)) ' chidiff: ' num2str(abs(chi2new-chi2))]);
         end
        break;
    end
    
    n = n + 1;
end
output.iterations = n-1;
output.lambda = lambda;
end

% Updates Jacobi with central finite differences
% function [J] = FinDiff(J,func,del,nparms,guess,axhs)
%     global ABORT
%     for i = 1:nparms
%         gn = guess;
%         gn(i) = guess(i) + del(i);
%         dy1 = func(gn);
%         gn(i) = guess(i) - del(i);
%         dy2 = func(gn);
%         J(:,i) = (dy2-dy1)/(2*del(i));
%         if ~isempty(axhs)
%             upwait_v2(i/nparms,axhs{1},'Computing Jacobian',[],[],[]);
%             if ABORT == 1
%                 return
%             end
%         end
%     end
% end

function chi = FindChi(dynew,JAWmse,datanum,nparms,SigY)
    if JAWmse
        chi = sqrt(sum((dynew./SigY).^2)/(datanum-nparms));
    else
        chi = sqrt((dynew'*dynew)/(datanum));
    end
end
