function F = Obj2(P, X, iX, tX, ToolParms, ModParms, Y, Thk)
[~,s2,NumF] = size(Y);
s1 = length(ToolParms.Lo);
X(iX) = P;
Ys = zeros(s1,s2,NumF);
Ye = Ys;
for i = 1:NumF
    X(1) = Thk(i);
    ToolParms.LimitedUpdate = [];
    [Ysim,Yexp] = UpdateSpectra(X, iX, tX, ToolParms, ModParms, Y(:,:,i));
    Ys(:,:,i)=Ysim;
    Ye(:,:,i)=Yexp;
end
F = Ye-Ys;
F = reshape(F,[s1*s2*NumF 1]);