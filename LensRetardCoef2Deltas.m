function [do1, di1, do2, di2] = LensRetardCoef2Deltas(Lo, A1, B1, A2, B2)

l2 = ((Lo./500).^2);
q = 500./Lo;
q2 = q.*q;
q4 = q2.*q2;


% new formulas
do1 = -(q.*(A1(1) + A1(2)*q2 + A1(3)*q4));
di1 = q.*(B1(1) + B1(2)*q2   + B1(3)*q4 + B1(4)*l2);

do2 = q.*(A2(1) + A2(2)*q2 + A2(3)*q4);
di2 = q.*(B2(1) + B2(2)*q2 + B2(3)*q4 + B2(4)*l2);