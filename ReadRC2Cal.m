function [ CalPm ] = ReadRC2Cal( CalFile )
% read calibration file of RC2
% 7/25/2019 Modified for speed and added wavelength cal, AGBd

% [fnameCal, pnameCal] = uigetfile('RC2-Cal*.*', 'Get a Woollam calibration file:');
% CalFile = [pnameCal fnameCal];
% CalFile = 'C:\Users\pvagos\Desktop\Mfg QC AtlasII+ Data\#127\rc2-cal.cnf';

fid = fopen(CalFile);

%%%% Wavelength Calibration Parameters
text = fgetl(fid);
while ~contains(text, 'start_WvlRange') && ~feof(fid) 
    text = fgetl(fid);
end
text = fgetl(fid);
CalPm.WvlRange.VIS = str2num(text);
text = fgetl(fid);
CalPm.WvlRange.NIR = str2num(text);

while ~contains(text, 'start_ChanCal') && ~feof(fid) 
    text = fgetl(fid);
end
text = fgetl(fid);
CalPm.WvlCal.VIS = str2num(text);
text = fgetl(fid);
CalPm.WvlCal.cVIS = str2num(text);

while ~contains(text, 'start_ChanCal2') && ~feof(fid) 
    text = fgetl(fid);
end
text = fgetl(fid);
CalPm.WvlCal.NIR = str2num(text);

while ~contains(text, 'end_BadChannels') && ~feof(fid) 
    text = fgetl(fid);
end
while isempty(str2num(text)) && ~feof(fid) 
    text = fgetl(fid);
end
CalPm.WvlCal.cNIR = str2num(text);
if length(CalPm.WvlCal.NIR) > 4; CalPm.WvlCal.NIR = CalPm.WvlCal.NIR(1:4); CalPm.WvlCal.cNIR = CalPm.WvlCal.cNIR(1:4); end

% find next section
while ~contains(text, 'start_spCalParms') && ~feof(fid) 
    text = fgetl(fid);
end

%%%% Source compensator parameters

while ~contains(text, 'start_Sret') && ~feof(fid) 
    text = fgetl(fid);
end

text = fgetl(fid);
ind = strfind(text, '	');
CalPm.Sret.wv = str2num(text(ind(5):ind(end-1)));

text = fgetl(fid);
ind = strfind(text, '	');
CalPm.Sret.Pm = str2num(text(ind(4):ind(end)));

while ~contains(text, 'start_Sretn') && ~feof(fid) 
    text = fgetl(fid);
end

text = fgetl(fid);
ind = strfind(text, '	');
CalPm.Sretn.wv = str2num(text(ind(5):ind(end-1)));

text = fgetl(fid);
ind = strfind(text, '	');
CalPm.Sretn.Pm = str2num(text(ind(4):ind(end)));

while ~contains(text, 'start_Spsi') && ~feof(fid) 
    text = fgetl(fid);
end

text = fgetl(fid);
ind = strfind(text, '	');
CalPm.Spsi.wv = str2num(text(ind(5):ind(end-1)));

text = fgetl(fid);
ind = strfind(text, '	');
CalPm.Spsi.Pm = str2num(text(ind(4):ind(end)));

while ~contains(text, 'start_SCs') && ~feof(fid) 
    text = fgetl(fid);
end

text = fgetl(fid);
ind = strfind(text, '	');
CalPm.SCs.wv = str2num(text(ind(5):ind(end-1)));

text = fgetl(fid);
ind = strfind(text, '	');
CalPm.SCs.Pm = str2num(text(ind(4):ind(end)));

while ~contains(text, 'start_SCsn') && ~feof(fid) 
    text = fgetl(fid);
end

text = fgetl(fid);
ind = strfind(text, '	');
CalPm.SCsn.wv = str2num(text(ind(5):ind(end-1)));

text = fgetl(fid);
ind = strfind(text, '	');
CalPm.SCsn.Pm = str2num(text(ind(4):ind(end)));

while ~contains(text, 'start_SPs') && ~feof(fid) 
    text = fgetl(fid);
end

text = fgetl(fid);
ind = strfind(text, '	');
CalPm.SPs.wv = str2num(text(ind(5):ind(end-1)));

text = fgetl(fid);
ind = strfind(text, '	');
CalPm.SPs.Pm = str2num(text(ind(4):ind(end)));
 
%%%% Receive compensator parameters

while ~contains(text, 'start_Rret') && ~feof(fid) 
    text = fgetl(fid);
end

text = fgetl(fid);
ind = strfind(text, '	');
CalPm.Rret.wv = str2num(text(ind(5):ind(end-1)));

text = fgetl(fid);
ind = strfind(text, '	');
CalPm.Rret.Pm = str2num(text(ind(4):ind(end)));

while ~contains(text, 'start_Rretn') && ~feof(fid) 
    text = fgetl(fid);
end

text = fgetl(fid);
ind = strfind(text, '	');
CalPm.Rretn.wv = str2num(text(ind(5):ind(end-1)));

text = fgetl(fid);
ind = strfind(text, '	');
CalPm.Rretn.Pm = str2num(text(ind(4):ind(end)));

while ~contains(text, 'start_Rpsi') && ~feof(fid) 
    text = fgetl(fid);
end

text = fgetl(fid);
ind = strfind(text, '	');
CalPm.Rpsi.wv = str2num(text(ind(5):ind(end-1)));

text = fgetl(fid);
ind = strfind(text, '	');
CalPm.Rpsi.Pm = str2num(text(ind(4):ind(end)));

while ~contains(text, 'start_RCs') && ~feof(fid) 
    text = fgetl(fid);
end

text = fgetl(fid);
ind = strfind(text, '	');
CalPm.RCs.wv = str2num(text(ind(5):ind(end-1)));

text = fgetl(fid);
ind = strfind(text, '	');
CalPm.RCs.Pm = str2num(text(ind(4):ind(end)));

while ~contains(text, 'start_RCsn') && ~feof(fid) 
    text = fgetl(fid);
end

text = fgetl(fid);
ind = strfind(text, '	');
CalPm.RCsn.wv = str2num(text(ind(5):ind(end-1)));

text = fgetl(fid);
ind = strfind(text, '	');
CalPm.RCsn.Pm = str2num(text(ind(4):ind(end)));

while ~contains(text, 'start_RPs') && ~feof(fid) 
    text = fgetl(fid);
end

text = fgetl(fid);
ind = strfind(text, '	');
CalPm.RPs.wv = str2num(text(ind(5):ind(end-1)));

text = fgetl(fid);
ind = strfind(text, '	');
CalPm.RPs.Pm = str2num(text(ind(4):ind(end)));
 
%%%% Eph parameters

while ~contains(text, 'start_Eph') && ~feof(fid) 
    text = fgetl(fid);
end

text = fgetl(fid);
ind = strfind(text, '	');
CalPm.Eph.wv = str2num(text(ind(4):ind(end-1)));

text = fgetl(fid);
ind = strfind(text, '	');
CalPm.Eph.Pm = str2num(text(ind(3):ind(end)));
 
while ~contains(text, 'start_Ephn') && ~feof(fid) 
    text = fgetl(fid);
end

text = fgetl(fid);
ind = strfind(text, '	');
CalPm.Ephn.wv = str2num(text(ind(4):ind(end-1)));

text = fgetl(fid);
ind = strfind(text, '	');
CalPm.Ephn.Pm = str2num(text(ind(3):ind(end)));

fclose(fid);

end
