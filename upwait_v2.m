function []=upwait_v2(r1,axh1,txt1,r2,axh2,txt2)
% Update 
if ~isempty(axh1)
    axes(axh1)
    cla
    if r1 == 'c'
        rectangle('Position',[0,0,1000,20],'FaceColor',[1 1 1],'LineWidth',0.1);
        text(500,10,'Idle','HorizontalAlignment','center');
    else
        rectangle('Position',[0,0,round(1000*r1),20],'FaceColor',[0.8 0.92 0.85],'LineWidth',0.1);
        text(500,10,[txt1 ': ' num2str(round(100*r1)),'%'],'HorizontalAlignment','center');
    %     drawnow;
    end
end

if ~isempty(axh2)
    axes(axh2)
    cla
    if r2 == 'c'
        rectangle('Position',[0,0,1000,20],'FaceColor',[1 1 1],'LineWidth',0.1);
        text(500,10,'Idle','HorizontalAlignment','center');
    else
        rectangle('Position',[0,0,round(1000*r2),20],'FaceColor',[0.8 0.92 0.85],'LineWidth',0.1);
        text(500,10,[txt2 ': ' num2str(round(100*r2)),'%'],'HorizontalAlignment','center');
    end
end

drawnow;
end