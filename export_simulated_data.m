% Export Simulated Data to .dat files
% AGB 2021

% get list of files to save
tabdata = get(handles.uitable1,'data');
iSelect = cell2mat(tabdata(:,1));
if sum(iSelect)==0
    msgbox('Select some files first.')
    return
end
Thks = cell2mat(tabdata(iSelect,3));
NumF = sum(iSelect);

% grab data from most recent simulation
L = handles.Ls;
Ys = handles.Ys;
[NumL,s2,~] = size(Ys);
if s2 == 3
    mode = 'NCS';
elseif s2 == 16
    mode = 'MM';
else
    msgbox('Unknown data size, exiting...');
    return
end
Ys = Ys(:,:,iSelect);

% grab simulation values from table 2
tabdata = get(handles.uitable2,'data');
AOIp.val = cell2mat(tabdata(3,6:(5+tabdata{3,5})));
AOIp.typ = tabdata{3,3};
AOIp.spt = tabdata{3,4};
AOIp.LL = tabdata{3,16};
AOIp.UL = tabdata{3,17};
AOIp.s = CalcWvlDep(L,AOIp,1,0); % save the actual simulated AOIs, even if CE/ND/WVASE32 can't interpret them

% Ask for location to dump new files
save_dir = uigetdir([getenv('USERPROFILE') '\Desktop\'],'Select a folder for simulated .dat files:');

% setup adap saving
MMforms = {'M12','M13','M14','M21','M22','M23','M24','M31','M32','M33','M34','M41','M42','M43','M44'};
d = adapdata;

% save files
upwait_v2(0.1,handles.axes1,'Saving Simulated Spectra ',0,handles.axes2,['Saving file 1 of ' num2str(NumF) '.']);
for iF = 1:NumF
    if strcmpi(mode,'MM')
        for m = 1:15
            data = [L AOIp.s.' Ys(:,m+1,iF) Ys(:,m+1,iF) zeros(NumL,1) zeros(NumL,1) ones(NumL,1) ones(NumL,1)];
            d = set(d,MMforms{m},data);
        end
    else
        [Psi, Delta] = NCS2PsiDelta(Ys(:,1,iF),Ys(:,2,iF),Ys(:,3,iF));
        data = [L AOIp.s.' Psi Delta zeros(NumL,1) zeros(NumL,1) ones(NumL,1) ones(NumL,1)];
        d = set(d,'SE',data);
    end
    save(d,[save_dir '\WI_sim_data_' num2str(iF) '_' num2str(Thks(iF),'%.1f') 'nm.dat']);
    upwait_v2(iF/NumF,handles.axes1,'Saving Simulated Spectra ',0,handles.axes2,['Saving file ' num2str(iF) ' of ' num2str(NumF) '.']);
end

% update GUI
upwait_v2('c',handles.axes1,' ','c',handles.axes2,' ');
guidata(hObject, handles);