function [ Fa, Fp, A, P, Da, Dp, PSIa, PSIp, Eph ] = ExtractCal2( CalPm,  L)

    nm2eV = 1240./L;
    
    x = (CalPm.RCs.wv(2):CalPm.RCs.wv(4):CalPm.RCs.wv(3))/10; 
    y = CalPm.RCs.Pm;
    wv = L(L>= x(1)-1E-14 & L<=x(end)+1E-14);   % the 1E-14 is added/suvtracted to avoid numerical precison issue
    x1 = spline(x,y,wv);
    x = (CalPm.RCsn.wv(2):CalPm.RCsn.wv(4):CalPm.RCsn.wv(3))/10; 
    y = CalPm.RCsn.Pm;
    wv = L(L>= x(1)-1E-14 & L<=x(end)+1E-14);
    x2 = spline(x,y,wv);
    Fa = [x1; x2];    
    
    x = (CalPm.SCs.wv(2):CalPm.SCs.wv(4):CalPm.SCs.wv(3))/10; 
    y = CalPm.SCs.Pm;
    wv = L(L>= x(1)-1E-14 & L<=x(end)+1E-14);
    x1 = spline(x,y,wv);
    x = (CalPm.SCsn.wv(2):CalPm.SCsn.wv(4):CalPm.SCsn.wv(3))/10; 
    y = CalPm.SCsn.Pm;
    wv = L(L>= x(1)-1E-14 & L<=x(end)+1E-14);
    x2 = spline(x,y,wv);
    Fp = [x1; x2];        

    x = CalPm.RPs.wv(2):CalPm.RPs.wv(4):CalPm.RPs.wv(3); y = CalPm.RPs.Pm;
    A = spline(x,y,1240./L); 
    
    x = CalPm.SPs.wv(2):CalPm.SPs.wv(4):CalPm.SPs.wv(3); y = CalPm.SPs.Pm;
    P = spline(x,y,nm2eV); 
    
    x = (CalPm.Eph.wv(2):CalPm.Eph.wv(4):CalPm.Eph.wv(3))/10; 
    y = CalPm.Eph.Pm;
    wv = L(L>= x(1)-1E-14 & L<=x(end)+1E-14);
    x1 = spline(x,y,wv);
    x = (CalPm.Ephn.wv(2):CalPm.Ephn.wv(4):CalPm.Ephn.wv(3))/10; 
    y = CalPm.Ephn.Pm;
    wv = L(L>= x(1)-1E-14 & L<=x(end)+1E-14);
    x2 = spline(x,y,wv);
    Eph = [x1; x2];
    
    x = (CalPm.Rret.wv(2):CalPm.Rret.wv(4):CalPm.Rret.wv(3)); 
    y = CalPm.Rret.Pm;
    wv = nm2eV(nm2eV>= (x(1)-1E-14) & nm2eV<=(x(end)+1E-14));
    x1 = spline(x,y,wv);
    x = (CalPm.Rretn.wv(2):CalPm.Rretn.wv(4):CalPm.Rretn.wv(3)); 
    y = CalPm.Rretn.Pm;
    wv = nm2eV(nm2eV>= (x(1)-1E-14) & nm2eV<=(x(end)+1E-14));
    x2 = spline(x,y,wv);
    Da = [x1; x2];
    
    x = (CalPm.Sret.wv(2):CalPm.Sret.wv(4):CalPm.Sret.wv(3)); 
    y = CalPm.Sret.Pm;
    wv = nm2eV(nm2eV>= (x(1)-1E-14) & nm2eV<=(x(end)+1E-14));
    x1 = spline(x,y,wv);
    x = (CalPm.Sretn.wv(2):CalPm.Sretn.wv(4):CalPm.Sretn.wv(3)); 
    y = CalPm.Sretn.Pm;
    wv = nm2eV(nm2eV>= (x(1)-1E-14) & nm2eV<=(x(end)+1E-14));
    x2 = spline(x,y,wv);
    Dp = [x1; x2];
    
    x = CalPm.Rpsi.wv(2):CalPm.Rpsi.wv(4):CalPm.Rpsi.wv(3); y = CalPm.Rpsi.Pm;
    PSIa = spline(x,y,nm2eV); 
    
    x = CalPm.Spsi.wv(2):CalPm.Spsi.wv(4):CalPm.Spsi.wv(3); y = CalPm.Spsi.Pm;
    PSIp = spline(x,y,nm2eV); 
    
    % catch error when L contains pts between VIS and NIR default range
    if length(Fa) ~=length(L)
        xl = (CalPm.RCs.wv(2):CalPm.RCs.wv(4):CalPm.RCs.wv(3))/10; 
        yl = CalPm.RCs.Pm;
        xu = (CalPm.RCsn.wv(2):CalPm.RCsn.wv(4):CalPm.RCsn.wv(3))/10; 
        yu = CalPm.RCsn.Pm;        
        wv = L(L>= xl(1)-1E-14 & L< xu(1)+1E-14);   % the 1E-14 is added/suvtracted to avoid numerical precison issue
        x1 = spline(xl,yl,wv);        
        wv = L(L>= xu(1)-1E-14 & L<=xu(end)+1E-14);
        x2 = spline(xu,yu,wv);
        Fa = [x1; x2];
        
        xl = (CalPm.SCs.wv(2):CalPm.SCs.wv(4):CalPm.SCs.wv(3))/10; 
        yl = CalPm.SCs.Pm;
        xu = (CalPm.SCsn.wv(2):CalPm.SCsn.wv(4):CalPm.SCsn.wv(3))/10; 
        yu = CalPm.SCsn.Pm;
        wv = L(L>= xl(1)-1E-14 & L< xu(1)+1E-14);   % the 1E-14 is added/suvtracted to avoid numerical precison issue
        x1 = spline(xl,yl,wv);        
        wv = L(L>= xu(1)-1E-14 & L<=xu(end)+1E-14);
        x2 = spline(xu,yu,wv);
        Fp = [x1; x2];     

        xl = (CalPm.Eph.wv(2):CalPm.Eph.wv(4):CalPm.Eph.wv(3))/10; 
        yl = CalPm.Eph.Pm;
        xu = (CalPm.Ephn.wv(2):CalPm.Ephn.wv(4):CalPm.Ephn.wv(3))/10; 
        yu = CalPm.Ephn.Pm;
        wv = L(L>= xl(1)-1E-14 & L< xu(1)+1E-14);   % the 1E-14 is added/suvtracted to avoid numerical precison issue
        x1 = spline(xl,yl,wv);        
        wv = L(L>= xu(1)-1E-14 & L<=xu(end)+1E-14);
        x2 = spline(xu,yu,wv);
        Eph = [x1; x2];      
        
        xl = (CalPm.Rret.wv(2):CalPm.Rret.wv(4):CalPm.Rret.wv(3)); 
        yl = CalPm.Rret.Pm;
        xu = (CalPm.Rretn.wv(2):CalPm.Rretn.wv(4):CalPm.Rretn.wv(3)); 
        yu = CalPm.Rretn.Pm;
        wv = nm2eV(nm2eV> (xu(end)+1E-14) & nm2eV<=(xl(end)+1E-14));   % the 1E-14 is added/suvtracted to avoid numerical precison issue
        x1 = spline(xl,yl,wv);        
        wv = nm2eV(nm2eV>= (xu(1)-1E-14) & nm2eV<=(xu(end)+1E-14));
        x2 = spline(xu,yu,wv);        
        Da = [x1; x2];
        
        xl = (CalPm.Sret.wv(2):CalPm.Sret.wv(4):CalPm.Sret.wv(3)); 
        yl = CalPm.Sret.Pm;
        xu = (CalPm.Sretn.wv(2):CalPm.Sretn.wv(4):CalPm.Sretn.wv(3)); 
        yu = CalPm.Sretn.Pm;
        wv = nm2eV(nm2eV> (xu(end)+1E-14) & nm2eV<=(xl(end)+1E-14));   % the 1E-14 is added/suvtracted to avoid numerical precison issue
        x1 = spline(xl,yl,wv);        
        wv = nm2eV(nm2eV>= (xu(1)-1E-14) & nm2eV<=(xu(end)+1E-14));
        x2 = spline(xu,yu,wv);        
        Dp = [x1; x2];
    end    

end

