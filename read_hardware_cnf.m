function [detSN,recSN] = read_hardware_cnf(f)
% Get Settings from hardware.cnf

fid = fopen(f);
detSN = 'Detector_SerialNum=';
ldSN = length(detSN);
recSN = 'Source_SerialNum=';
lrSN = length(recSN);
fline = 'a';
while ischar(fline)
    fline = fgetl(fid);
    if length(fline) >= ldSN
        if strcmpi(detSN,fline(1:ldSN))
            detSN = fline(ldSN+1:end);
            continue
        end
    end
    if length(fline) >= lrSN
        if strcmpi(recSN,fline(1:lrSN))
            recSN = fline(lrSN+1:end);
            break
        end
    end
end
fclose(fid);

end

