function varargout = PlotMenu(varargin)
% PLOTMENU MATLAB code for PlotMenu.fig
%      PLOTMENU, by itself, creates a new PLOTMENU or raises the existing
%      singleton*.
%
%      H = PLOTMENU returns the handle to a new PLOTMENU or the handle to
%      the existing singleton*.
%
%      PLOTMENU('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in PLOTMENU.M with the given input arguments.
%
%      PLOTMENU('Property','Value',...) creates a new PLOTMENU or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before PlotMenu_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to PlotMenu_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help PlotMenu

% Last Modified by GUIDE v2.5 22-Jun-2022 11:53:21

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @PlotMenu_OpeningFcn, ...
                   'gui_OutputFcn',  @PlotMenu_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before PlotMenu is made visible.
function PlotMenu_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to PlotMenu (see VARARGIN)

% Choose default command line output for PlotMenu
handles.output = hObject;

% Get CtrlData and determine unvisible fields
handles.CtrlData = varargin{1};
handles.plots = varargin{2};

% set all checkboxes from memory
fields = fieldnames(handles.plots);
numF = size(fields,1);
for i=1:numF
    if islogical(handles.plots.(fields{i}))
        if handles.plots.(fields{i})
            handles.(fields{i}).Value = 1;
        else
            handles.(fields{i}).Value = 0;
        end
    else
        subfields = fieldnames(handles.plots.(fields{i}));
        numSF = size(subfields,1);
        for j=1:numSF
            if handles.plots.(fields{i}).(subfields{j})
                handles.([fields{i} '_' subfields{j}]).Value = 1;
            else
                handles.([fields{i} '_' subfields{j}]).Value = 0;
            end
        end
    end
end

% Lens parameter plotting
if handles.CtrlData.NewLens == 0
    handles.NCSret.Enable = 'off';
    handles.NCSatt.Enable = 'off';
    handles.T1ret.Enable = 'off';
    handles.T1att.Enable = 'off';
    handles.T2ret.Enable = 'off';
    handles.T2att.Enable = 'off';
    handles.T1reOA.Enable = 'off';
    handles.T1imOA.Enable = 'off';
    handles.T2reOA.Enable = 'off';
    handles.T2imOA.Enable = 'off';
    handles.NCSret.Value = 0;
    handles.NCSatt.Value = 0;
    handles.T1ret.Value = 0;
    handles.T1att.Value = 0;
    handles.T2ret.Value = 0;
    handles.T2att.Value = 0;
    handles.T1reOA.Value = 0;
    handles.T1imOA.Value = 0;
    handles.T2reOA.Value = 0;
    handles.T2imOA.Value = 0;
    if handles.CtrlData.mode == 1
        handles.winDisp1.Enable = 'off';
        handles.winDisp2.Enable = 'off';
        handles.winDisp1.Value = 0;
        handles.winDisp2.Value = 0;
    end
else
    handles.DOs.Enable = 'off';
    handles.winDisp1.Enable = 'off';
    handles.winDisp2.Enable = 'off';
    handles.DOs.Value = 0;
    handles.winDisp1.Value = 0;
    handles.winDisp2.Value = 0;
end
% Mueller RMSE plotting
if handles.CtrlData.mode ~= 3
    handles.allMM.Enable = 'off';
    handles.offDiags.Enable = 'off';
    handles.offDiagsMax.Enable = 'off';
    handles.onDiags.Enable = 'off';
    handles.MMthks.Enable = 'off';
    handles.m12.Enable = 'off';
    handles.m13.Enable = 'off';
    handles.m14.Enable = 'off';
    handles.m21.Enable = 'off';
    handles.m22.Enable = 'off';
    handles.m23.Enable = 'off';
    handles.m24.Enable = 'off';
    handles.m31.Enable = 'off';
    handles.m32.Enable = 'off';
    handles.m33.Enable = 'off';
    handles.m34.Enable = 'off';
    handles.m41.Enable = 'off';
    handles.m42.Enable = 'off';
    handles.m43.Enable = 'off';
    handles.m44.Enable = 'off';
    handles.m14m41.Enable = 'off';
    handles.m13m31.Enable = 'off';
    handles.m23m32.Enable = 'off';
    handles.m24m42.Enable = 'off';
    handles.cont_mmall.Enable = 'off';
    handles.cont_offdiags.Enable = 'off';
    handles.cont_offdiagcombos.Enable = 'off';
    handles.cont_ondiags.Enable = 'off';
    handles.cont_depol.Enable = 'off';
    handles.cont_mmsig.Enable = 'off';
    handles.depolind.Enable = 'off';
    handles.allMM.Value = 0;
    handles.offDiags.Value = 0;
    handles.offDiagsMax.Value = 0;
    handles.onDiags.Value = 0;
    handles.MMthks.Value = 0;
    handles.m12.Value = 0;
    handles.m13.Value = 0;
    handles.m14.Value = 0;
    handles.m21.Value = 0;
    handles.m22.Value = 0;
    handles.m23.Value = 0;
    handles.m24.Value = 0;
    handles.m31.Value = 0;
    handles.m32.Value = 0;
    handles.m33.Value = 0;
    handles.m34.Value = 0;
    handles.m41.Value = 0;
    handles.m42.Value = 0;
    handles.m43.Value = 0;
    handles.m44.Value = 0;
    handles.m14m41.Value = 0;
    handles.m13m31.Value = 0;
    handles.m23m32.Value = 0;
    handles.m24m42.Value = 0;
    handles.cont_mmall.Value = 0;
    handles.cont_offdiags.Value = 0;
    handles.cont_offdiagcombos.Value = 0;
    handles.cont_ondiags = 0;
    handles.cont_depol.Value = 0;
    handles.cont_mmsig.Value = 0;
    handles.depolind.Value = 0;
end

if ~handles.CtrlData.CompCal
    handles.dGam1.Enable = 'off';
    handles.iGam1.Enable = 'off';
    handles.dGam2.Enable = 'off';
    handles.iGam2.Enable = 'off';
    handles.dPsi1.Enable = 'off';
    handles.dPsi2.Enable = 'off';
    handles.dPhi1.Enable = 'off';
    handles.dPhi2.Enable = 'off';
    handles.rp11.Enable = 'off';
    handles.ip11.Enable = 'off';
    handles.rp12.Enable = 'off';
    handles.ip12.Enable = 'off';
    handles.rp21.Enable = 'off';
    handles.ip21.Enable = 'off';
    handles.rp22.Enable = 'off';
    handles.ip22.Enable = 'off';
    handles.dGam1.Value = 0;
    handles.iGam1.Value = 0;
    handles.dGam2.Value = 0;
    handles.iGam2.Value = 0;
    handles.dPsi1.Value = 0;
    handles.dPsi2.Value = 0;
    handles.dPhi1.Value = 0;
    handles.dPhi2.Value = 0;
    handles.rp11.Value = 0;
    handles.ip11.Value = 0;
    handles.rp12.Value = 0;
    handles.ip12.Value = 0;
    handles.rp21.Value = 0;
    handles.ip21.Value = 0;
    handles.rp22.Value = 0;
    handles.ip22.Value = 0;
end
% 


% empty structure
% handles.plots = struct;

% Update handles structure
% datamode_Callback(hObject, eventdata, handles);
guidata(hObject, handles);

% UIWAIT makes Controls wait for user response (see UIRESUME)
uiwait(handles.figure1);
% UIWAIT makes PlotMenu wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = PlotMenu_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
% varargout{1} = handles.output;
varargout{1} = handles.plots;
guidata(hObject,handles);
figure1_CloseRequestFcn(hObject, eventdata, handles);


% --- Executes on button press in accept.
function accept_Callback(hObject, eventdata, handles)
% hObject    handle to accept (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

guidata(hObject, handles);
close(handles.figure1);


% --- Executes on button press in allMM.
function allMM_Callback(hObject, eventdata, handles)
% hObject    handle to allMM (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
handles.plots.allMM = (get(hObject,'Value')==1);
guidata(hObject, handles);
% Hint: get(hObject,'Value') returns toggle state of allMM


% --- Executes on button press in offDiags.
function offDiags_Callback(hObject, eventdata, handles)
% hObject    handle to offDiags (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of offDiags
handles.plots.offDiags = (get(hObject,'Value')==1);
guidata(hObject, handles);

% --- Executes on button press in onDiags.
function onDiags_Callback(hObject, eventdata, handles)
% hObject    handle to onDiags (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of onDiags
handles.plots.onDiags = (get(hObject,'Value')==1);
guidata(hObject, handles);

% --- Executes on button press in offDiagsMax.
function offDiagsMax_Callback(hObject, eventdata, handles)
% hObject    handle to offDiagsMax (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of offDiagsMax
handles.plots.offDiagsMax = (get(hObject,'Value')==1);
guidata(hObject, handles);

% --- Executes on button press in m21.
function m21_Callback(hObject, eventdata, handles)
% hObject    handle to m21 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of m21
handles.plots.m21 = (get(hObject,'Value')==1);
guidata(hObject, handles);

% --- Executes on button press in m31.
function m31_Callback(hObject, eventdata, handles)
% hObject    handle to m31 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of m31
handles.plots.m31 = (get(hObject,'Value')==1);
guidata(hObject, handles);

% --- Executes on button press in m41.
function m41_Callback(hObject, eventdata, handles)
% hObject    handle to m41 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of m41
handles.plots.m41 = (get(hObject,'Value')==1);
guidata(hObject, handles);

% --- Executes on button press in m12.
function m12_Callback(hObject, eventdata, handles)
% hObject    handle to m12 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of m12
handles.plots.m12 = (get(hObject,'Value')==1);
guidata(hObject, handles);

% --- Executes on button press in m22.
function m22_Callback(hObject, eventdata, handles)
% hObject    handle to m22 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of m22
handles.plots.m22 = (get(hObject,'Value')==1);
guidata(hObject, handles);


% --- Executes on button press in m32.
function m32_Callback(hObject, eventdata, handles)
% hObject    handle to m32 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of m32
handles.plots.m32 = (get(hObject,'Value')==1);
guidata(hObject, handles);


% --- Executes on button press in m42.
function m42_Callback(hObject, eventdata, handles)
% hObject    handle to m42 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of m42
handles.plots.m42 = (get(hObject,'Value')==1);
guidata(hObject, handles);


% --- Executes on button press in m13.
function m13_Callback(hObject, eventdata, handles)
% hObject    handle to m13 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of m13
handles.plots.m13 = (get(hObject,'Value')==1);
guidata(hObject, handles);


% --- Executes on button press in m23.
function m23_Callback(hObject, eventdata, handles)
% hObject    handle to m23 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of m23
handles.plots.m23 = (get(hObject,'Value')==1);
guidata(hObject, handles);


% --- Executes on button press in m33.
function m33_Callback(hObject, eventdata, handles)
% hObject    handle to m33 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of m33
handles.plots.m33 = (get(hObject,'Value')==1);
guidata(hObject, handles);


% --- Executes on button press in m43.
function m43_Callback(hObject, eventdata, handles)
% hObject    handle to m43 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of m43
handles.plots.m43 = (get(hObject,'Value')==1);
guidata(hObject, handles);


% --- Executes on button press in m14.
function m14_Callback(hObject, eventdata, handles)
% hObject    handle to m14 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of m14
handles.plots.m14 = (get(hObject,'Value')==1);
guidata(hObject, handles);


% --- Executes on button press in m24.
function m24_Callback(hObject, eventdata, handles)
% hObject    handle to m24 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of m24
handles.plots.m24 = (get(hObject,'Value')==1);
guidata(hObject, handles);


% --- Executes on button press in m34.
function m34_Callback(hObject, eventdata, handles)
% hObject    handle to m34 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of m34
handles.plots.m34 = (get(hObject,'Value')==1);
guidata(hObject, handles);


% --- Executes on button press in m44.
function m44_Callback(hObject, eventdata, handles)
% hObject    handle to m44 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of m44
handles.plots.m44 = (get(hObject,'Value')==1);
guidata(hObject, handles);

% --- Executes on button press in m14m41.
function m14m41_Callback(hObject, eventdata, handles)
% hObject    handle to m14m41 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of m14m41
handles.plots.m14m41 = (get(hObject,'Value')==1);
guidata(hObject, handles);


% --- Executes on button press in m13m31.
function m13m31_Callback(hObject, eventdata, handles)
% hObject    handle to m13m31 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of m13m31
handles.plots.m13m31 = (get(hObject,'Value')==1);
guidata(hObject, handles);

% --- Executes on button press in m23m32.
function m23m32_Callback(hObject, eventdata, handles)
% hObject    handle to m23m32 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of m23m32
handles.plots.m23m32 = (get(hObject,'Value')==1);
guidata(hObject, handles);


% --- Executes on button press in m24m42.
function m24m42_Callback(hObject, eventdata, handles)
% hObject    handle to m24m42 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of m24m42
handles.plots.m24m42 = (get(hObject,'Value')==1);
guidata(hObject, handles);


% --- Executes on button press in AOI.
function AOI_Callback(hObject, eventdata, handles)
% hObject    handle to AOI (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of AOI
handles.plots.AOI = (get(hObject,'Value')==1);
guidata(hObject, handles);


% --- Executes on button press in NA.
function NA_Callback(hObject, eventdata, handles)
% hObject    handle to NA (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of NA
handles.plots.NA = (get(hObject,'Value')==1);
guidata(hObject, handles);


% --- Executes on button press in visWvls.
function visWvls_Callback(hObject, eventdata, handles)
% hObject    handle to visWvls (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of visWvls
handles.plots.visWvls = (get(hObject,'Value')==1);
guidata(hObject, handles);


% --- Executes on button press in IRWvls.
function IRWvls_Callback(hObject, eventdata, handles)
% hObject    handle to IRWvls (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of IRWvls
handles.plots.IRWvls = (get(hObject,'Value')==1);
guidata(hObject, handles);


% --- Executes on button press in allNCS.
function allNCS_Callback(hObject, eventdata, handles)
% hObject    handle to allNCS (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of allNCS
handles.plots.allNCS = (get(hObject,'Value')==1);
guidata(hObject, handles);


% --- Executes on button press in Nonly.
function Nonly_Callback(hObject, eventdata, handles)
% hObject    handle to Nonly (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of Nonly
handles.plots.Nonly = (get(hObject,'Value')==1);
guidata(hObject, handles);


% --- Executes on button press in Sonly.
function Sonly_Callback(hObject, eventdata, handles)
% hObject    handle to Sonly (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of Sonly
handles.plots.Sonly = (get(hObject,'Value')==1);
guidata(hObject, handles);


% --- Executes on button press in Conly.
function Conly_Callback(hObject, eventdata, handles)
% hObject    handle to Conly (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of Conly
handles.plots.Conly = (get(hObject,'Value')==1);
guidata(hObject, handles);


% --- Executes on button press in NCSthks.
function NCSthks_Callback(hObject, eventdata, handles)
% hObject    handle to NCSthks (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of NCSthks
handles.plots.NCSthks = (get(hObject,'Value')==1);
guidata(hObject, handles);


% --- Executes on button press in MMthks.
function MMthks_Callback(hObject, eventdata, handles)
% hObject    handle to MMthks (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of MMthks
handles.plots.MMthks = (get(hObject,'Value')==1);
guidata(hObject, handles);


% --- Executes on button press in DOs.
function DOs_Callback(hObject, eventdata, handles)
% hObject    handle to DOs (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of DOs
handles.plots.DOs = (get(hObject,'Value')==1);
guidata(hObject, handles);


% --- Executes on button press in winDisp1.
function winDisp1_Callback(hObject, eventdata, handles)
% hObject    handle to winDisp1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of winDisp1
handles.plots.WD1 = (get(hObject,'Value')==1);
guidata(hObject, handles);

% --- Executes on button press in winDisp2.
function winDisp2_Callback(hObject, eventdata, handles)
% hObject    handle to winDisp2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of winDisp2
handles.plots.WD2 = (get(hObject,'Value')==1);
guidata(hObject, handles);


% --- Executes on button press in NCSret.
function NCSret_Callback(hObject, eventdata, handles)
% hObject    handle to NCSret (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of NCSret
handles.plots.NCSret = (get(hObject,'Value')==1);
guidata(hObject, handles);


% --- Executes on button press in NCSatt.
function NCSatt_Callback(hObject, eventdata, handles)
% hObject    handle to NCSatt (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of NCSatt
handles.plots.NCSatt = (get(hObject,'Value')==1);
guidata(hObject, handles);


% --- Executes on button press in T1ret.
function T1ret_Callback(hObject, eventdata, handles)
% hObject    handle to T1ret (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of T1ret
handles.plots.T1ret = (get(hObject,'Value')==1);
guidata(hObject, handles);


% --- Executes on button press in T1att.
function T1att_Callback(hObject, eventdata, handles)
% hObject    handle to T1att (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of T1att
handles.plots.T1att = (get(hObject,'Value')==1);
guidata(hObject, handles);


% --- Executes on button press in T1reOA.
function T1reOA_Callback(hObject, eventdata, handles)
% hObject    handle to T1reOA (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of T1reOA
handles.plots.T1reOA = (get(hObject,'Value')==1);
guidata(hObject, handles);


% --- Executes on button press in T1imOA.
function T1imOA_Callback(hObject, eventdata, handles)
% hObject    handle to T1imOA (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of T1imOA
handles.plots.T1imOA = (get(hObject,'Value')==1);
guidata(hObject, handles);


% --- Executes on button press in T2ret.
function T2ret_Callback(hObject, eventdata, handles)
% hObject    handle to T2ret (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of T2ret
handles.plots.T2ret = (get(hObject,'Value')==1);
guidata(hObject, handles);

% --- Executes on button press in T2att.
function T2att_Callback(hObject, eventdata, handles)
% hObject    handle to T2att (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of T2att
handles.plots.T2att = (get(hObject,'Value')==1);
guidata(hObject, handles);

% --- Executes on button press in T2reOA.
function T2reOA_Callback(hObject, eventdata, handles)
% hObject    handle to T2reOA (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of T2reOA
handles.plots.T2reOA = (get(hObject,'Value')==1);
guidata(hObject, handles);


% --- Executes on button press in T2imOA.
function T2imOA_Callback(hObject, eventdata, handles)
% hObject    handle to T2imOA (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of T2imOA
handles.plots.T2imOA = (get(hObject,'Value')==1);
guidata(hObject, handles);

% --- Executes on button press in cont_N.
function cont_N_Callback(hObject, eventdata, handles)
% hObject    handle to cont_N (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of cont_N
handles.plots.cont.N = (get(hObject,'Value')==1);
guidata(hObject, handles);


% --- Executes on button press in cont_S.
function cont_S_Callback(hObject, eventdata, handles)
% hObject    handle to cont_S (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of cont_S
handles.plots.cont.S = (get(hObject,'Value')==1);
guidata(hObject, handles);

% --- Executes on button press in cont_C.
function cont_C_Callback(hObject, eventdata, handles)
% hObject    handle to cont_C (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of cont_C
handles.plots.cont.C = (get(hObject,'Value')==1);
guidata(hObject, handles);

% --- Executes on button press in cont_NCS.
function cont_NCS_Callback(hObject, eventdata, handles)
% hObject    handle to cont_NCS (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of cont_NCS
handles.plots.cont.NCS = (get(hObject,'Value')==1);
guidata(hObject, handles);

% --- Executes on button press in cont_offdiagcombos.
function cont_offdiagcombos_Callback(hObject, eventdata, handles)
% hObject    handle to cont_offdiagcombos (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of cont_offdiagcombos
handles.plots.cont.offdiagcombos = (get(hObject,'Value')==1);
guidata(hObject, handles);

% --- Executes on button press in cont_depol.
function cont_depol_Callback(hObject, eventdata, handles)
% hObject    handle to cont_depol (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of cont_depol
handles.plots.cont.depol = (get(hObject,'Value')==1);
guidata(hObject, handles);

% --- Executes on button press in cont_mmall.
function cont_mmall_Callback(hObject, eventdata, handles)
% hObject    handle to cont_mmall (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of cont_mmall
handles.plots.cont.mmall = (get(hObject,'Value')==1);
guidata(hObject, handles);

% --- Executes on button press in cont_offdiags.
function cont_offdiags_Callback(hObject, eventdata, handles)
% hObject    handle to cont_offdiags (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of cont_offdiags
handles.plots.cont.offdiags = (get(hObject,'Value')==1);
guidata(hObject, handles);

% --- Executes when user attempts to close figure1.
function figure1_CloseRequestFcn(hObject, eventdata, handles)
% hObject    handle to figure1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: delete(hObject) closes the figure
% delete(hObject);
if isequal(get(hObject, 'waitstatus'), 'waiting')
    % The GUI is still in UIWAIT, call UIRESUME
    uiresume(hObject);
else
    % The GUI is no longer waiting, just close it
    delete(hObject);
end


% --- Executes on button press in int.
function int_Callback(hObject, eventdata, handles)
% hObject    handle to int (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of int
handles.plots.int = (get(hObject,'Value')==1);
guidata(hObject, handles);


% --- Executes on button press in fitparms.
function fitparms_Callback(hObject, eventdata, handles)
% hObject    handle to fitparms (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of fitparms
handles.plots.fitparms = (get(hObject,'Value')==1);
guidata(hObject, handles);


% --- Executes on button press in cont_NCSsig.
function cont_NCSsig_Callback(hObject, eventdata, handles)
% hObject    handle to cont_NCSsig (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of cont_NCSsig
handles.plots.cont.NCSsig = (get(hObject,'Value')==1);
guidata(hObject, handles);


% --- Executes on button press in dGam1.
function dGam1_Callback(hObject, eventdata, handles)
% hObject    handle to dGam1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of dGam1
handles.plots.dGam1 = (get(hObject,'Value')==1);
guidata(hObject, handles);


% --- Executes on button press in iGam1.
function iGam1_Callback(hObject, eventdata, handles)
% hObject    handle to iGam1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of iGam1
handles.plots.iGam1 = (get(hObject,'Value')==1);
guidata(hObject, handles);

% --- Executes on button press in dPsi1.
function dPsi1_Callback(hObject, eventdata, handles)
% hObject    handle to dPsi1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of dPsi1
handles.plots.dPsi1 = (get(hObject,'Value')==1);
guidata(hObject, handles);

% --- Executes on button press in dPhi1.
function dPhi1_Callback(hObject, eventdata, handles)
% hObject    handle to dPhi1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of dPhi1
handles.plots.dPhi1 = (get(hObject,'Value')==1);
guidata(hObject, handles);

% --- Executes on button press in dGam2.
function dGam2_Callback(hObject, eventdata, handles)
% hObject    handle to dGam2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of dGam2
handles.plots.dGam2 = (get(hObject,'Value')==1);
guidata(hObject, handles);

% --- Executes on button press in iGam2.
function iGam2_Callback(hObject, eventdata, handles)
% hObject    handle to iGam2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of iGam2
handles.plots.iGam2 = (get(hObject,'Value')==1);
guidata(hObject, handles);

% --- Executes on button press in dPsi2.
function dPsi2_Callback(hObject, eventdata, handles)
% hObject    handle to dPsi2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of dPsi2
handles.plots.dPsi2 = (get(hObject,'Value')==1);
guidata(hObject, handles);

% --- Executes on button press in dPhi2.
function dPhi2_Callback(hObject, eventdata, handles)
% hObject    handle to dPhi2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of dPhi2
handles.plots.dPhi2 = (get(hObject,'Value')==1);
guidata(hObject, handles);

% --- Executes on button press in rp11.
function rp11_Callback(hObject, eventdata, handles)
% hObject    handle to rp11 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of rp11
handles.plots.rp11 = (get(hObject,'Value')==1);
guidata(hObject, handles);

% --- Executes on button press in ip11.
function ip11_Callback(hObject, eventdata, handles)
% hObject    handle to ip11 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of ip11
handles.plots.ip11 = (get(hObject,'Value')==1);
guidata(hObject, handles);

% --- Executes on button press in rp12.
function rp12_Callback(hObject, eventdata, handles)
% hObject    handle to rp12 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of rp12
handles.plots.rp12 = (get(hObject,'Value')==1);
guidata(hObject, handles);

% --- Executes on button press in ip12.
function ip12_Callback(hObject, eventdata, handles)
% hObject    handle to ip12 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of ip12
handles.plots.ip12 = (get(hObject,'Value')==1);
guidata(hObject, handles);

% --- Executes on button press in rp21.
function rp21_Callback(hObject, eventdata, handles)
% hObject    handle to rp21 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of rp21
handles.plots.rp21 = (get(hObject,'Value')==1);
guidata(hObject, handles);

% --- Executes on button press in ip21.
function ip21_Callback(hObject, eventdata, handles)
% hObject    handle to ip21 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of ip21
handles.plots.ip21 = (get(hObject,'Value')==1);
guidata(hObject, handles);

% --- Executes on button press in rp22.
function rp22_Callback(hObject, eventdata, handles)
% hObject    handle to rp22 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of rp22
handles.plots.rp22 = (get(hObject,'Value')==1);
guidata(hObject, handles);

% --- Executes on button press in ip22.
function ip22_Callback(hObject, eventdata, handles)
% hObject    handle to ip22 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of ip22
handles.plots.ip22 = (get(hObject,'Value')==1);
guidata(hObject, handles);

% --- Executes on button press in depolind.
function depolind_Callback(hObject, eventdata, handles)
% hObject    handle to depolind (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of depolind
handles.plots.depolind = (get(hObject,'Value')==1);
guidata(hObject, handles);


% --- Executes on button press in cont_uT.
function cont_uT_Callback(hObject, eventdata, handles)
% hObject    handle to cont_uT (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of cont_uT
handles.plots.cont.uT = (get(hObject,'Value')==1);
guidata(hObject, handles);


% --- Executes on button press in cont_mmsig.
function cont_mmsig_Callback(hObject, eventdata, handles)
% hObject    handle to cont_mmsig (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of cont_mmsig
handles.plots.cont.mmsig = (get(hObject,'Value')==1);
guidata(hObject, handles);


% --- Executes on button press in cont_ondiags.
function cont_ondiags_Callback(hObject, eventdata, handles)
% hObject    handle to cont_ondiags (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of cont_ondiags
handles.plots.cont.ondiags = (get(hObject,'Value')==1);
guidata(hObject, handles);
