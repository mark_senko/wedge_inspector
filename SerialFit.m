% Setup simulation
tabdata={};
SimSetup;
FitSetup;

%% Serial Fit Routine

PP = zeros(NumF,sum(iX));
MSE = zeros(NumF,1);
NumIter = zeros(NumF,1);

[s1,s2,s3]=size(Ys);
P = X(iX);
LL = LL(iX);
UL = UL(iX);
LsSelect = handles.CtrlData.MatLS;
set(handles.abortbutton,'visible','on');
axhs = {handles.axes1,handles.axes2};
figh = handles.figh;
axh = handles.axh;
RMSEthres = handles.CtrlData.RMSEthres;
SerFitIter = handles.CtrlData.SerFitIter;
Nparms = sum(iX);

for nF = 1:NumF
    X(1) = Thk(nF);
    Po = X(iX);
    X2 = X;
    Yexp = Y(:,:,nF);

    % Optimizer
    ObjHandle = @(P)Obj1(P,X,iX,tX,ToolParms,ModParms,Yexp);
    if LsSelect
        Options = optimoptions(@lsqnonlin,'Display','iter','StepTolerance',1e-4,'FunctionTolerance',1e-4);
        [P, ~,~,~,op] = lsqnonlin(ObjHandle,Po,[],[],Options);
    else
        if JAWmse~=1
            [P,op]=LbMqBASICv7(ObjHandle,Po,LL,UL,1e-6,RMSEthres,0.2,SerFitIter,[],SigY(:,:,nF));
        else
            [P,op]=LbMqBASICv7(ObjHandle,Po,LL,UL,1e-6,RMSEthres,0.2,SerFitIter,[]);
        end
    end
    PP(nF,:) = P;
%     X(iX) = P;
    X2(iX) = P;
    [Ysim,Yexp] = UpdateSpectra(X2, iX, tX, ToolParms, ModParms, Yexp);
    dY = (Yexp-Ysim);
    [rws,cls]=size(dY);
    dY = reshape(dY,[rws*cls 1]);
    MSE(nF) = MSEcalc(JAWmse,dY,SigY(:,:,nF),Nparms);
    
    % Save off best values
    Ys(:,:,nF) = Ysim;
    Ye(:,:,nF) = Yexp;
    NumIter(nF) = op.iterations;          

    % Graph via WIgrapher
    [figh,axh] = WIgrapher(figh,axh,Ls,Yexp,1,[1 0 0]);
    WIgrapher(figh,axh,Ls,Ysim,0,[0 0 1]);  
    
    % Update wait bar
    upwait_v2(1,handles.axes1,['Fit experiment ' num2str(nF) ' in ' num2str(NumIter(nF)) ' iterations'],...
              nF/NumF,handles.axes2,[num2str(nF) ' of ' num2str(NumF) ' files completed']);

    % Stop if necessary
    if ABORT == 1
        ABORT = 0;
        op.iterations = -1;
        set(handles.abortbutton,'visible','off');
        upwait_v2('c',handles.axes1,'Idle','c',handles.axes2,'Idle');
        guidata(hObject, handles);
        return;
    end
end

%% Wrap up - display results
% Compute total RMSE
dY = reshape((Ye-Ys),[numel(Ys) 1]);
% tRMSE = sqrt(sum(sum(sum((Ye-Ys).^2)))./(s1*s2*s3));
tRMSE = MSEcalc(JAWmse,dY,SigY,Nparms);
upwait_v2(1,handles.axes1,'Serial Fit Finished',...
          1,handles.axes2,['Total RMSE: ' num2str(tRMSE)]);
set(handles.abortbutton,'visible','off');

% update handles
handles.results.Ls = Ls;
handles.results.Ye = Ye;
handles.results.Ys = Ys;
handles.results.MSE = MSE;
handles.results.Nparms = Nparms;
handles.axh = axh;
handles.figh = figh;

% Find parameter names
iX(1) = false;
FloatedParmNames = pNames(iX);
FloatedParmNames = [{'Thk'}; FloatedParmNames; {'RMSE'}];
if FlgFitThk
    FloatedParmVal = [PP MSE];
else
    FloatedParmVal = [Thk PP MSE]; 
end

% Correct BW output
%iF = strcmp(FloatedParmNames,'BW_1') | strcmp(FloatedParmNames,'BW_2');
%FloatedParmVal(:,iF) = FloatedParmVal(:,iF).*2.355;

handles.FloatedParmNames = FloatedParmNames;
handles.FloatedParmVal = FloatedParmVal;

% update results table
Data = get(handles.uitable1,'data');
NewData = cell(size(Data,1),size(FloatedParmVal,2)+2);
NewData(:,1:3) = Data(:,1:3);
NewData(iSelect,3:end) = num2cell(FloatedParmVal);
ColNames = [{' ','File'} , FloatedParmNames'];
set(handles.uitable1,'ColumnName',ColNames);
set(handles.uitable1,'data',NewData);