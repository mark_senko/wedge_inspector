% Spectrum Residual Analysis for MFG

% Get data and compute RMSEs
Ye = handles.Ye;
Ys = handles.Ys;
Ls = handles.Ls;
rmse_l = sqrt(sum(sum((Ye-Ys).^2,3),2)./(size(Ye,3)*size(Ye,2)));
rmse_t = sqrt(sum(sum(sum((Ye-Ys).^2,3),2),1)./numel(Ye));

% Get RMSE spec from file
rmse_s = xlsread('MCRA.xlsx');
rmse_s = spline(rmse_s(:,1),rmse_s(:,2),Ls);

failFLG = false; % set fail flag
failTXT = {'TOOL FAILED!'};

% check total rmse
if rmse_t > 0.001874
    failFLG = true;
    failTXT = [failTXT, {' '}, {'ERROR TOO LARGE:'},...
              {['RMSE = ' num2str(rmse_t) ' > 0.001874 (spec.), Attempt to realign or replace SE.']}];
end

% find lvf center freq
[lvf_c,lvf_a] = findlvf_v2(Ls,rmse_l);
if (abs(lvf_c-381)>10) && lvf_a
    failFLG = true;
    shift = (381-lvf_c)*0.032;
    if shift > 0
        shift_dir = 'IR';
    else
        shift_dir = 'UV';
    end
    shift = abs(shift);
    failTXT = [failTXT, {' '}, {'LVF ERROR:'},{['Linear Variable Filter misaligned, move '...
               num2str(shift) ' mm towards the ' shift_dir '.']}];
end

% check deep uv for lens artifacts
% iL = (Ls < 320);
% if sum(rmse_l(iL)>rmse_s(iL))>0
%     failFLG = true;
%     failTXT = [failTXT, {' '}, {'DEEP UV ERROR TOO LARGE:'},...
%                {'Re-align or replace lenses and recalibrate SE.'}];
% end

% check for VIS - IR AOI offset
tabdata = get(handles.uitable2,'data');
AOIp.val = cell2mat(tabdata(3,6:9));
AOIp.typ = tabdata{3,3};
AOIp.spt = tabdata{3,4};
AOIs = CalcWvlDep(Ls,AOIp,1,0);
[~,iL] = min(abs(Ls-600));
AOI_vis = AOIs(iL);
[~,iL] = min(abs(Ls-1350));
AOI_ir = AOIs(iL);
AOI_ir_slope = tabdata{3,9}/1000;
AOI_off = AOI_ir - AOI_vis;
if abs(AOI_off) > 0.06
    failFLG = true;
    failTXT = [failTXT, {' '}, {'AOI OFFSET ERROR:'},...
              {['AOI offset = |' num2str(AOI_off) '| > 0.06 (spec.). Difference between IR and VIS AOI too large.']},...
              {'Re-align SE receiver internal IR mirror and re-calibrate SE.'}];
end

% grab angular spread - use tabdata from above
AS = asind(tabdata{4,6})*2;

% plot results
figure('windowstyle','docked','Name','Tool RMSE & A3 Spec.');
plot(Ls,rmse_s,'k');
hold on
plot(Ls,rmse_l,'b');
iL = (rmse_l > rmse_s);
plot(Ls(iL),rmse_l(iL),'or');
hold off
num_failed_wvls = sum(iL);
xlabel('Wavelength [nm]');
ylabel('RMSE vs A3 Spec');
xlim([Ls(1) Ls(end)]);
box on
grid on
legend('A3 Spec from 25 Tools','This Tool','Erroneous Wavelengths');

% fail on # failed wvls
if num_failed_wvls > 10
    failFLG = true;
    failTXT = [failTXT, {' '}, {'TOO MANY FAILED WVLS:'},...
              {['Number of failed wavelengths = ' num2str(num_failed_wvls) '. Limit is 10.']},...
              {'No specific advice to resolve error. Solutions depend on affected wavelengths.'}];
end


% Save off excel file with interesting numbers
dt_tm = strsplit(datestr(clock));
if failFLG == false
    ps_fl = 'TOOL PASSED';
else
    ps_fl = transpose(failTXT);
end
tool = strsplit(handles.PathF,'\');
results1 = {'W.I. Preset Analysis Results',' ',' ',' ';...
    ' ',' ',' ',' ';...
    'Tool (folder):',tool{end-2},' ',' ';...
    'Date:',dt_tm{1},' ',' ';...
    'Time:',dt_tm{2},' ',' ';...
    ' ',' ',' ',' '};

results2 = {' ',' ',' ',' ';...
    'Parameter','Value','LCL','UCL';...
    'RMSE',num2str(rmse_t),' ','0.001874';...
    'LVF center freq. [nm]',num2str(lvf_c),'371','391';...
    'AOI offset [deg]',num2str(AOI_off),' ','0.06';...
    'AOI IR slope [deg/nm]',num2str(AOI_ir_slope),' ',' ';...
    'Angular Spread [deg]',num2str(AS),' ',' ';...
    'Number Failed Wvls',num2str(num_failed_wvls),' ','10'};

results = [results1;ps_fl,repmat({' '},[size(ps_fl,1) 3]);results2];

Foldr = [handles.PathF 'WI_PA_Results'];
if 7==(exist(Foldr))
    delete([Foldr '\*.*']);
else
    mkdir(handles.PathF,'WI_PA_Results');
end
xlswrite([Foldr '\Results.xlsx'],results);

% save figures
SaveAllFigs(Foldr);

% save calibration
SaveSettings(handles,[Foldr '\calibration.xlsx']);

% display msgbox with results
if failFLG == false
%     [img.data,img.map] = imread('checkmark.jpg');
%     msgbox('TOOL PASSED!','Analysis Finished','custom',img.data,img.map);
    AnalysisFinished(true,results2,'Tool Passed!');
else
%     msgbox(failTXT,'Analysis Finished','error');
    AnalysisFinished(false,results2,failTXT);
end