function [LMM1,LMM2] = LensMM_D(Lh,LNSp)
% Generate the 2 matrices LMM1 and LMM2, which are effectively rotated retarders 
% with complex optical activity and absorption.

% Setup input parameters
NumL = length(Lh);
% Lh = permute(Lh,[3 2 1]);
Rot  = permute(LNSp(1,:),[1 3 2]);
%   = permute(LNSp(2,:),[1 3 2])./2;
Di1 = permute(LNSp(3,:),[1 3 2]);
Do1 = permute(LNSp(4,:),[1 3 2]);
Pi1 = permute(LNSp(5,:),[1 3 2]);
Po1 = permute(LNSp(6,:),[1 3 2]);
Di2 = permute(LNSp(7,:),[1 3 2]);
Do2 = permute(LNSp(8,:),[1 3 2]);
Pi2 = permute(LNSp(9,:),[1 3 2]);
Po2 = permute(LNSp(10,:),[1 3 2]);

% Setup rotation matrix
RotM = zeros(4,4,NumL);
ons = ones(1,1,NumL);
RotM(1,1,:) = ons;
RotM(4,4,:) = ons;
RotM(2,2,:) = (1-Rot.^2./2);
RotM(3,3,:) = RotM(2,2,:);
RotM(2,3,:) = Rot;
RotM(3,2,:) = Rot;
RotM = complex(RotM);

% Setup T1/T2 in plane retardance
Di1M = zeros(4,4,NumL);
Di1M(1,1,:) = ons;
Di1M(2,2,:) = ons;
Di2M = Di1M;
Pi1M = Di1M;
Pi2M = Di1M;
Di1M(3,3,:) = cosd(Di1);
Di1M(4,4,:) = Di1M(3,3,:);
Di1M(3,4,:) = sind(Di1);
Di1M(4,3,:) = -sind(Di1);
Di2M(3,3,:) = cosd(Di2);
Di2M(4,4,:) = Di2M(3,3,:);
Di2M(3,4,:) = sind(Di1);
Di2M(4,3,:) = -sind(Di1);
Di1M = complex(Di1M);
Di2M = complex(Di2M);
% disp(Di1M(:,:,1));

% Setup T1/T2 out of plane retardance
Do1M = zeros(4,4,NumL);
Do1M(1,1,:) = ons;
Do1M(3,3,:) = ons;
Do2M = Do1M;
Po1M = Do1M;
Po2M = Do1M;
Do1M(2,2,:) = cosd(Do1);
Do1M(4,4,:) = cosd(Do1);
Do1M(2,4,:) = sind(Do1);
Do1M(4,2,:) = -sind(Do1);
Do2M(2,2,:) = cosd(Do2);
Do2M(4,4,:) = cosd(Do2);
Do2M(2,4,:) = sind(Do2);
Do2M(4,2,:) = -sind(Do2);
Do1M = complex(Do1M);
Do2M = complex(Do2M);
% disp(Do1M(:,:,1));

% Setup T1/T2 in plane polarizer
Pi1M(1,2,:) = sind(2*Pi1);
Pi1M(2,1,:) = sind(2*Pi1);
Pi1M(3,3,:) = cosd(2*Pi1);
Pi1M(4,4,:) = cosd(2*Pi1);
Pi2M(1,2,:) = sind(2*Pi2);
Pi2M(2,1,:) = sind(2*Pi2);
Pi2M(3,3,:) = cosd(2*Pi2);
Pi2M(4,4,:) = cosd(2*Pi2);
Pi1M = complex(Pi1M);
Pi2M = complex(Pi2M);
% disp(Pi1M(:,:,1));

% Setup T1/T2 out of plane polarizer
Po1M(1,3,:) = sind(2*Po1);
Po1M(3,1,:) = sind(2*Po1);
Po1M(2,2,:) = cosd(2*Po1);
Po1M(4,4,:) = cosd(2*Po1);
Po2M(1,3,:) = sind(2*Po2);
Po2M(3,1,:) = sind(2*Po2);
Po2M(2,2,:) = cosd(2*Po2);
Po2M(4,4,:) = cosd(2*Po2);
Po1M = complex(Po1M);
Po2M = complex(Po2M);
% disp(Po1M(:,:,1));

% Mutiply T1 together
LMM1 = MM3D_v2(Di1M,Do1M);
LMM1 = MM3D_v2(Po1M,LMM1);
LMM1 = MM3D_v2(Pi1M,LMM1);
LMM1 = MM3D_v2(RotM,LMM1);

% Mutiply T2 together
LMM2 = MM3D_v2(Do2M,Di2M);
LMM2 = MM3D_v2(LMM2,Po2M);
LMM2 = MM3D_v2(LMM2,Pi2M);
RotM = permute(RotM,[2 1 3]);
LMM2 = MM3D_v2(LMM2,RotM);

end

