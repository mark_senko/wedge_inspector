function WcalSE = Read_WcalSE(File_wavecal)
% read rc2-cal Wcal settings

WcalSE = struct;

WcalSE.Rc2CalFile  =  File_wavecal;
fid  =  fopen(File_wavecal);
Data  =  textscan(fid, '%s', 'delimiter', '\n', 'whitespace', '');
fclose(fid);

Data  =  Data{1};
iC = find(strncmpi(Data,'start_ChanCal',13));
iC = iC(1)+1;
tmp = Data(iC:iC+1);
UVVis = [str2num(tmp{1}); str2num(tmp{2})]; %#ok<*ST2NM>
iC = find(strncmpi(Data,'start_ChanCal2',14));
iC = iC(1)+1;
tmp = [Data(iC); Data(iC+6)];
IR = [str2num(tmp{1}); str2num(tmp{2})];

WcalSE.C_VIS  =  UVVis(1,:);
WcalSE.Cc_VIS  =  UVVis(2,:);
WcalSE.C_IR  =  IR(1,1:4);
% WcalSE.Cc_IR  =  IR(2,:);
WcalSE.Cc_IR  =  IR(2,1:4);


