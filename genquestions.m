function varargout = genquestions(varargin)
% GENQUESTIONS MATLAB code for genquestions.fig
%      GENQUESTIONS, by itself, creates a new GENQUESTIONS or raises the existing
%      singleton*.
%
%      H = GENQUESTIONS returns the handle to a new GENQUESTIONS or the handle to
%      the existing singleton*.
%
%      GENQUESTIONS('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in GENQUESTIONS.M with the given input arguments.
%
%      GENQUESTIONS('Property','Value',...) creates a new GENQUESTIONS or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before genquestions_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to genquestions_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help genquestions

% Last Modified by GUIDE v2.5 20-Apr-2021 13:10:59

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @genquestions_OpeningFcn, ...
                   'gui_OutputFcn',  @genquestions_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before genquestions is made visible.
function genquestions_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to genquestions (see VARARGIN)

% Choose default command line output for genquestions
% handles.output = hObject;
handles.output.overthk = false;
handles.output.overwave = false;
handles.output.noise = false;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes genquestions wait for user response (see UIRESUME)
uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = genquestions_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;
guidata(hObject,handles);
figure1_CloseRequestFcn(hObject, eventdata, handles);


% --- Executes on button press in overthk.
function overthk_Callback(hObject, eventdata, handles)
% hObject    handle to overthk (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of overthk
if get(hObject,'Value')
    handles.overthkmin.Enable = 'on';
    handles.overthkmax.Enable = 'on';
    handles.overthknum.Enable = 'on';
    handles.output.overthk = true;
else
    handles.overthkmin.Enable = 'off';
    handles.overthkmax.Enable = 'off';
    handles.overthknum.Enable = 'off';
    handles.output.overthk = false;  
end
guidata(hObject, handles);


function overthkmin_Callback(hObject, eventdata, handles)
% hObject    handle to overthkmin (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of overthkmin as text
%        str2double(get(hObject,'String')) returns contents of overthkmin as a double


% --- Executes during object creation, after setting all properties.
function overthkmin_CreateFcn(hObject, eventdata, handles)
% hObject    handle to overthkmin (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function overthkmax_Callback(hObject, eventdata, handles)
% hObject    handle to overthkmax (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of overthkmax as text
%        str2double(get(hObject,'String')) returns contents of overthkmax as a double


% --- Executes during object creation, after setting all properties.
function overthkmax_CreateFcn(hObject, eventdata, handles)
% hObject    handle to overthkmax (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in enablenoise.
function enablenoise_Callback(hObject, eventdata, handles)
% hObject    handle to enablenoise (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of enablenoise
if get(hObject,'Value')
    handles.whitenoise.Enable = 'on';
    handles.output.noise = true;
else
    handles.whitenoise.Enable = 'off';
    handles.output.noise = false;  
end
guidata(hObject, handles);


function overthknum_Callback(hObject, eventdata, handles)
% hObject    handle to overthknum (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of overthknum as text
%        str2double(get(hObject,'String')) returns contents of overthknum as a double


% --- Executes during object creation, after setting all properties.
function overthknum_CreateFcn(hObject, eventdata, handles)
% hObject    handle to overthknum (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function whitenoise_Callback(hObject, eventdata, handles)
% hObject    handle to whitenoise (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of whitenoise as text
%        str2double(get(hObject,'String')) returns contents of whitenoise as a double


% --- Executes during object creation, after setting all properties.
function whitenoise_CreateFcn(hObject, eventdata, handles)
% hObject    handle to whitenoise (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in overwave.
function overwave_Callback(hObject, eventdata, handles)
% hObject    handle to overwave (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of overwave
if get(hObject,'Value')
    handles.overwavemin.Enable = 'on';
    handles.overwavemax.Enable = 'on';
    handles.overwavenum.Enable = 'on';
    handles.output.overwave = true;
else
    handles.overwavemin.Enable = 'off';
    handles.overwavemax.Enable = 'off';
    handles.overwavenum.Enable = 'off';
    handles.output.overwave = false;  
end
guidata(hObject, handles);



function overwavemin_Callback(hObject, eventdata, handles)
% hObject    handle to overwavemin (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of overwavemin as text
%        str2double(get(hObject,'String')) returns contents of overwavemin as a double


% --- Executes during object creation, after setting all properties.
function overwavemin_CreateFcn(hObject, eventdata, handles)
% hObject    handle to overwavemin (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function overwavemax_Callback(hObject, eventdata, handles)
% hObject    handle to overwavemax (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of overwavemax as text
%        str2double(get(hObject,'String')) returns contents of overwavemax as a double


% --- Executes during object creation, after setting all properties.
function overwavemax_CreateFcn(hObject, eventdata, handles)
% hObject    handle to overwavemax (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function overwavenum_Callback(hObject, eventdata, handles)
% hObject    handle to overwavenum (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of overwavenum as text
%        str2double(get(hObject,'String')) returns contents of overwavenum as a double


% --- Executes during object creation, after setting all properties.
function overwavenum_CreateFcn(hObject, eventdata, handles)
% hObject    handle to overwavenum (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in accept.
function accept_Callback(hObject, eventdata, handles)
% hObject    handle to accept (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if handles.output.overthk
    handles.output.overthkmin = str2double(handles.overthkmin.String);
    handles.output.overthkmax = str2double(handles.overthkmax.String);
    handles.output.overthknum = str2double(handles.overthknum.String);
end
if handles.output.overwave
    handles.output.overwavemin = str2double(handles.overwavemin.String);
    handles.output.overwavemax = str2double(handles.overwavemax.String);
    handles.output.overwavenum = str2double(handles.overwavenum.String);
end
if handles.output.noise
    handles.output.whitenoise = str2double(handles.whitenoise.String);
end
    
guidata(hObject, handles);
close(handles.figure1);


% --- Executes when user attempts to close figure1.
function figure1_CloseRequestFcn(hObject, eventdata, handles)
% hObject    handle to figure1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: delete(hObject) closes the figure
if isequal(get(hObject, 'waitstatus'), 'waiting')
    % The GUI is still in UIWAIT, call UIRESUME
    uiresume(hObject);
else
    % The GUI is no longer waiting, just close it
    delete(hObject);
end