function Yout = RC2WaveCalCorrection_v2(Lo,Yin,Cin,Cout,Lc)
% applies or remove the Woollam wave cal correction for RC2 data
% Inputs:
%   Lo wavelength vector (non concatenated)
%   Y, Type = multi-spectra (PsiDelta or NCS or MM)
%   Cin = wavecal already applyed to Y.
%   Cout = wavecal to be applyed to Y (Cin will be repkaced by Cout).
%
% Yin must contain only SE spectra (PsiDelta XOR NCS xor MM)
%
% Wvacal correction is based on the formulas:
% UV/VIS detector:
% Channel(w) = (c0+c0c) + w(c1+c1c) + w2(c2+c2c) + w3(c3+c3c) + w4(c4+c4c)
% IR detector:
% Channel(w) = (c0_2+c0c_2) + w(c1_2+c1c_2) + w2(c2_2+c2c_2) + w3(c3_2+c3c_2)
% Cin and Cout are the sums (Cn+Cnc)
% Cin (or Cout) elements 1:5 -> UV/VIs , 6:9 -> IR
%
% This code is designed for speed efficiency
% Pedro Vagos 7/16/16

% Edited by AGB to add wave-cal centering and allow multiple data types
% without use of the Type input.

% UV/VIS
iUV = Lo <= 1000;
L_UV = (Lo(iUV)-Lc(1))/1000;
Px0 = (Cin(1) + L_UV.*(Cin(2) + L_UV.*(Cin(3) + L_UV.*(Cin(4) + L_UV.*Cin(5))))).';
Px1 = (Cout(1) + L_UV.*(Cout(2) + L_UV.*(Cout(3) + L_UV.*(Cout(4) + L_UV.*Cout(5))))).';

% IR
iIR = Lo > 1000;
L_IR = (Lo(iIR)-Lc(2))/1000;
Px2 = (Cin(6) + L_IR.*(Cin(7) + L_IR.*(Cin(8) + L_IR.*(Cin(9))))).';
Px3 = (Cout(6) + L_IR.*(Cout(7) + L_IR.*(Cout(8) + L_IR.*(Cout(9))))).';

Yout = Yin;
[~,s2] = size(Yin);
for nY = 1:s2 
    y = Yin(:,nY);

    % UV/VIS
    if ~isempty(Px0)                    
        y1 = y(iUV,:);
        y1c = spline(Px0,y1',Px1)'; 
        % cut spikes due to edge extrapolation  
%         ix = Px1 < Px0(1);
%         sm = sum(ix);
%         if sm >0            
%             y1c(ix,:) = repmat(y1(1,:),sm,1);
%         end
%         ix = Px1 > Px0(end);
%         sm = sum(ix);
%         if sm >0
%             y1c(ix,:) = repmat(y1(end,:),sm,1);
%         end
    else
        y1c = [];
    end

    % IR   
    if ~isempty(Px2)
        y2 = y(iIR,:);
        y2c = spline(Px2,y2',Px3)'; 
        % cut spikes due to edge extrapolation  
%         ix = Px3 < Px2(end);
%         sm = sum(ix);
%         if sm > 0
%             y2c(ix,:) = repmat(y2(end),sm,1);
%         end
%         ix = Px3 > Px2(1);
%         sm = sum(ix);
%         if sm >0
%             y2c(ix,:) = repmat(y2(1),sm,1);
%         end
    else
        y2c = [];
    end

    Yout(:,nY) = [y1c; y2c];        
end
