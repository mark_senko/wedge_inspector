function y=sinc(x)
  ix=(x==0);
  x(ix)=eps;
  pix=x.*pi;
  y=sin(pix)./pix;