function [] = SaveAllFigsJon(varargin)
% Saves all the numbered figures that are currently open

% read path
if ~exist('DefaultPathDataFiles.mat','file')
    PathF= [cd '\'];
else
    load DefaultPathDataFiles PathF
    if ~exist('PathF','var')
        PathF = [cd '\'];
    end
    if PathF == 0
        PathF = [cd '\'];
    end
end

% Test to see if called internally
if isempty(varargin)
    Foldr = uigetdir(PathF);
else
    Foldr = varargin{1};
end
    

% Get list of figures, save all that are not named
FigList=findobj(allchild(0),'flat','Type','figure');
lst = get(FigList,'Name');
wig = strcmp(lst,'W.I. Grapher');
close(FigList(wig));
FigList=findobj(allchild(0),'flat','Type','figure');
for i=length(FigList):-1:1
    C1 = ~strcmp(get(FigList(i),'Name'),'GUI_WedgeDataInspector');
    if C1
        name=['f' num2str(i)];
        savefig(FigList(i),[Foldr '\' name '.fig'],'compact');
%         saveas(FigList(i),[Foldr '\' name '.png']);
    end
end

end

