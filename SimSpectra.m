% Container function for simulating spectra, previously part of the main
% GUI and removed for scripting and editing purposes.

% Setup Simulation
SimSetup;

%% Perform the simulation for each exp file loaded
MSE = zeros(NumF,1);
set(handles.abortbutton,'visible','on');
figh = handles.figh;
axh = handles.axh;

% gif filename
fname = 'WI_GIF_CREATOR.gif';
writegif = true;

for nF = 1:NumF
    Thk(nF);
    if ToolParms.uT && ~isempty(handles.M11)
        if ToolParms.mode == 3
            MyM11 = Y(:,1,nF);
            Yexp = [ones(size(Y,1),1) Y(:,2:end,nF)];
        else
            MyM11 = imag(Y(:,1,nF));
            Yexp = real(Y(:,:,nF));
        end
        [Ysim,~] = IntegrateSpectrum([ThkEMA Thk(nF)], NK, ToolParms);
    else
        [Ysim,MyM11] = IntegrateSpectrum([ThkEMA Thk(nF)], NK, ToolParms);
        Yexp = Y(:,:,nF);
    end
    Yexp = ModifySpectrum(ToolParms,Yexp,MyM11);
    dY = (Yexp-Ysim);
    [rws,cls]=size(dY);
    dY = reshape(dY,[rws*cls 1]);
    MSE(nF) = sqrt(dY'*dY/length(dY));
    
    Ys(:,:,nF) = Ysim;
    Ye(:,:,nF) = Yexp;
    
    % Graph via WIgrapher
    [figh,axh] = WIgrapher(figh,axh,Ls,Yexp,1,[1 0 0]);
    WIgrapher(figh,axh,Ls,Ysim,0,[0 0 1]);
    
    % Write GIF if desired
    if writegif
        frm = getframe(figh);
        img = frame2im(frm);
        [img2,cm] = rgb2ind(img,256);
        if nF==1
            imwrite(img2,cm,fname,'gif','DelayTime',0.05,'Loopcount',inf);
        else
            imwrite(img2,cm,fname,'gif','DelayTime',0.05,'WriteMode','append');
        end
    end
    
    % Update wait bar
    upwait_v2(1,handles.axes1,['Simulating File ' num2str(nF)],...
              nF/NumF,handles.axes2,[num2str(nF-1) ' of ' num2str(NumF) ' files completed']);
    
    % Stop if necessary
    handles = guidata(hObject);
    if handles.STOP == 1
        handles.STOP = 0;
        set(handles.abortbutton,'visible','off');
        guidata(hObject, handles);
        return;
    end
end

%% Wrap up
% Compute total RMSE
[s1,s2,s3]=size(Ys);
% tRMSE = MSEcalc(JAWmse,(Ye-Ys),SigY,1);
tRMSE = MSEcalc_v2(JAWmse,0,Ye,Ys,SigY,1);
upwait_v2(1,handles.axes1,'Simulation Finished',...
          1,handles.axes2,['Total RMSE: ' num2str(tRMSE)]);
set(handles.abortbutton,'visible','off');

% update handles
handles.results.Ls = Ls;
handles.results.Ye = Ye;
handles.results.Ys = Ys;
handles.results.MSE = MSE;
handles.results.Nparms = 1;
handles.axh = axh;
handles.figh = figh;
% handles.iSelect = iSelect;

% update uitable1
Data = get(handles.uitable1,'data');
NumP = size(Data,2);
if NumP == 3
    NewData = cell(size(Data,1),4);
    NewData(:,1:3) = Data(:,1:3);
    ColNames = {' ','File','Thk','RMSE'};
    set(handles.uitable1,'ColumnName',ColNames);
    NewData(iSelect,4) = num2cell(MSE);
else
    NewData = Data;
    NewData(iSelect,end) = num2cell(MSE);
end
set(handles.uitable1,'data',NewData);