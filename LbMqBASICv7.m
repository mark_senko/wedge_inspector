function [gbest,output] = LbMqBASICv7(varargin)
% [guess,output] = LbMqBASIC(func,guess,stol,ftol,lambda,maxiter)
% Basic non-linear least squares solver using Levenburg-Marquardt
% algorithm, to replace lsqnonlin in Matlab optimization package.

% func = handle of the function to be fit
% guess = vector of input parameters for func
% stol = step tolerance for stopping the fit
% ftol = function tolerance for stopping the fit
% maxiter = maximum number of iterations
% axhs = axes handles for updating display with progress

% Version 7 changes
% > Added support for aborting during the Jacobian calculation

% Version 6 changes
% > Changed to 'varargin' cell structure
% > Added support for JAW style MSE calculation

% Version 5 changes
% > Changed limit behavior to adjust del instead of guess
% > Added support for upwait_v2
% > For use with WI v4.0+

% Version 4 changes
% > Added limits

% Version 3 changes
% > Display waitbar when maxiter = 2 (for Parallel fit)
% > Reduced MSE is always captured, regardless of rho
% > Trust region (rho) has been updated, no longer a guess
% > Uses RMSE throughout

% Gather variables
nvarargs = length(varargin); 
if nvarargs == 9
    func = varargin{1};
    guess = varargin{2};
    LL = varargin{3};
    UL = varargin{4};
    stol = varargin{5};
    ftol = varargin{6};
    lambda = varargin{7};
    maxiter = varargin{8};
    axhs = varargin{9};
    SigY = false;
    JAWmse = false;
elseif nvarargs > 9
    func = varargin{1};
    guess = varargin{2};
    LL = varargin{3};
    UL = varargin{4};
    stol = varargin{5};
    ftol = varargin{6};
    lambda = varargin{7};
    maxiter = varargin{8};
    axhs = varargin{9};
    SigY = reshape(varargin{10},[numel(varargin{10}) 1]);
    JAWmse = true;
else
    msgbox('NOT ENOUGH INPUTS TO NON-LINEAR SOLVER: LbMqBASICv7.m');
    return
end


% Initialize
global ABORT
n = 1;
iLim = (LL ~= UL);
del = LL - LL;
del(iLim) = (UL(iLim) - LL(iLim))*0.01;
del(~iLim)= LL(~iLim);
iLim = (LL == 0) & (UL == 0);
del(iLim) = guess(iLim)*0.01;
% disp([LL, iLim, del]);
gbest = guess;
nparms = length(guess);
dynew = func(guess);
datanum = length(dynew);
J = zeros(datanum,nparms);
Lup = 3; Ldn = 0.1; % Used for scaling lambda
chi2new = FindChi(dynew,JAWmse,datanum,nparms,SigY);
chi2best = chi2new;
if JAWmse %check lengths for parallel fit
    if length(SigY)~=datanum
        fact = datanum/length(SigY);
        if mod(fact,1)~=0
            msgbox('ERROR: For JAW MSE calc, sigma and data lengths must match!');
            return;
        end
        SigY = repmat(SigY,[fact 1]);
    end
end

% Main loop
while n <= maxiter
    J = FinDiff(J,func,stol,nparms,guess,axhs); % Always compute new Jacobian
    chi2 = chi2new;
    dy = dynew;
    
    if ABORT == 1 % if abort break loop and return  
        output.iterations = -1;
        output.lambda = lambda;
        return;
    end
    
    % Determine new del offset
    H = J.'*J;
    top = H+lambda.*diag(diag(H));
        itop = isnan(top);
        top(itop) = 1e-6;
        itop = isinf(top);
        top(itop) = sign(top(itop))*1e6;
    bot = J.'*dy;
    [U,D,V] = svd(top); % SVD method to handle singular matrices
    D = diag(D);
    iD = (abs(D) < 1E-5);
    D(iD) = 1e4;
    D(~iD) = 1./D(~iD);
    D = diag(D);
%     disp('Del @ SVD');
    del = V*D*U'*bot;
    
    % Fold del back within limits
    iP = (guess+del > UL) & (UL ~= LL);
    del(iP) = LL(iP)+0.9.*(UL(iP)-LL(iP))+0.1.*(UL(iP)-LL(iP)).*exp((UL(iP)-(guess(iP)+del(iP)))./(UL(iP)-LL(iP)))-guess(iP);
    iP = (guess+del < LL) & (UL ~= LL);
    del(iP) = UL(iP)-0.9.*(UL(iP)-LL(iP))-0.1.*(UL(iP)-LL(iP)).*exp(((guess(iP)+del(iP))-LL(iP))./(UL(iP)-LL(iP)))-guess(iP);
    
    % Condition del to avoid NaN and Inf
    idel = (abs(del) < 1E-6);
    del(idel) = sign(del(idel))*1E-6;
    
%     disp('Del @ Limits');
%     disp(del);
    
    % Recompute function value
    dynew = func(guess + del);
    
    % Compute new chi & rho
    chi2new = FindChi(dynew,JAWmse,datanum,nparms,SigY);
    rho = (chi2^2*datanum) - (chi2new^2*datanum);
    rho = rho/(del.'*(lambda.*diag(diag(H))*del+J.'*dy));
    
    % save off best results
    if (chi2new < chi2best)
%         disp('new best!');
        chi2best = chi2new;
        gbest = guess + del;
    end
    
    % continue if mse is reduced
    if chi2new < chi2
        guess = guess + del;
    end
    
    % resize lambda for next iteration
    if rho > 0.9
%         disp('Lambda DOWN!');
        lambda = max(lambda*Ldn,10e-7);
%         lambda = lambda - Ldn;
    elseif rho < 0.2
%         disp('Lambda UP!');
        lambda = min(lambda*Lup,10e7);
%         lambda = lambda + Lup;
    end
    
    % Break if no change in chi2 or if less than tolerance
    if (((abs(chi2new-chi2)) < stol) && (n ~= 1)) || (abs(chi2new) < ftol)
%         disp(abs(chi2new));
%         disp(abs(chi2new-chi2));
%         disp(n);
        break;
    end
    
    n = n + 1;
end
output.iterations = n-1;
output.lambda = lambda;
end

% Updates Jacobi with central finite differences
function [J] = FinDiff(J,func,stol,nparms,guess,axhs)
    global ABORT
    for i = 1:nparms
        gn = guess;
        gn(i) = guess(i) + stol; %del(i);
        dy1 = func(gn);
        gn(i) = guess(i) - stol; %del(i);
        dy2 = func(gn);
        J(:,i) = (dy2-dy1)/(2*stol); %(2*del(i));
        if ~isempty(axhs)
            upwait_v2(i/nparms,axhs{1},'Computing Jacobian',[],[],[]);
            if ABORT == 1
                return
            end
        end
    end
end

function chi = FindChi(dynew,JAWmse,datanum,nparms,SigY)
    if JAWmse
        chi = sqrt(sum((dynew./SigY).^2)/(datanum-nparms));
    else
        chi = sqrt((dynew'*dynew)/(datanum));
    end
end
