function [] = SaveAllFigs(varargin)
% Saves all the numbered figures that are currently open

% read path
if ~exist('DefaultPathDataFiles.mat','file')
    PathF= [cd '\'];
else
    load DefaultPathDataFiles PathF
    if ~exist('PathF','var')
        PathF = [cd '\'];
    end
    if PathF == 0
        PathF = [cd '\'];
    end
end

% Test to see if called internally
if isempty(varargin)
    Foldr = uigetdir(PathF);
else
    Foldr = varargin{1};
end
    

% Get list of figures, save all that are not named
FigList=findobj(allchild(0),'flat','Type','figure');
for i=length(FigList):-1:1
    if ~strcmp(get(FigList(i),'Name'),'GUI_WedgeDataInspector')
        name=get(FigList(i),'Name');
        savefig(FigList(i),[Foldr '\' name '.fig'],'compact');
        saveas(FigList(i),[Foldr '\' name '.png']);
    end
end

end

