% Populate Thicknesses

% Experimental revision based on diff

% Plot estimates?
plt = [0 0 1];

% Grab data
Data = get(handles.uitable1,'data');
NumF = size(Data,1);
L = handles.L;
iL = (L <= 1600 & L >= 200);
L = L(iL);
S = handles.Y(iL,19,:);
S = permute(S,[1 3 2]);
S = S(:,:,1);
AOI = handles.CEcal.AOIcal;

% Get SiO2 N&K
if isempty(handles.PathMat)
    PathMat = [pwd '\Mats'];
    if ~exist(PathMat,'dir')
        uiwait(msgbox({'No MATERIALS folder found.' 'Please select MATERIALS folder.'},'Folder Not Found','error'));
        PathMat = uigetdir(pwd,'Please Select the Materials Folder');
        handles.PathMat = PathMat;
    else
        handles.PathMat = PathMat;
    end
end
NK_sio2 = GetNKs([handles.PathMat '\SiO2_jaw.txt'],L);
% keyboard;

% For each file, estimate thickness
thks = zeros(NumF,1);
for i = 1:1:NumF
    dS = diff(S(:,i));
%     dS = smoothdata(dS,'gaussian',20);
    dS = smoothdata(dS,'sgolay');
%     dS = smoothdata(dS);
    if i==1; L = L(1:end-1); end
    if plt(1)
        figure('windowstyle','docked');
        plot(handles.L(iL),S(:,i)); grid on; hold on;
        plot(L,diff(S(:,i)));
        plot(L,dS);
    end
    iZ = (dS < 0);
    iZ = xor(iZ,circshift(iZ,1));
    iZ(1) = false;
    iZ(end) = false; 
    iZ = find(iZ);
    iZ1 = linspace(1,length(iZ),length(iZ)).';
    iZ1 = logical(mod(iZ1,2));
    iZ2 = iZ(~iZ1);
    iZ1 = iZ(iZ1);
    esthk = 0;
    NumT = min([length(iZ1) length(iZ2)]);
    esthk1 = zeros(NumT-2,1);
    esthk2 = esthk1;
    
    if NumT == 0
        thks(i) = 10;
    elseif length(iZ1)<2 && length(iZ2)<2
%         if plt; keyboard; end
        tS = S(:,i);
        iZ = (tS < 0);
        iZ = xor(iZ,circshift(iZ,1));
        iZ(1) = false;
        iZ(end) = false; 
        if sum(iZ)==0
            thks(i) = 10;
        else
            mxs = max(handles.L(iZ));
            thks(i) = interp1([200 590],[32 150],mxs);
        end
    elseif length(iZ1)<2
        if min(L(iZ2))> 590
            thks(i) = 10;
        else
            zL2 = L(iZ2);
            zNK2 = NK_sio2(iZ2);
            zCT2  = cosd(asind(sind(AOI)./zNK2));
            thks(i) = 1/2*(zL2(2)*zL2(1))/(zNK2(1)*zCT2(1)*zL2(2)-zNK2(2)*zCT2(2)*zL2(1));
        end
%         if plt; keyboard; end
    elseif length(iZ2)<2
        if min(L(iZ1))> 590
            thks(i) = 10;
        else
            zL1 = L(iZ1);
            zNK1 = NK_sio2(iZ1);
            zCT1  = cosd(asind(sind(AOI)./zNK1));
            thks(i) = 1/2*(zL1(2)*zL1(1))/(zNK1(1)*zCT1(1)*zL1(2)-zNK1(2)*zCT1(2)*zL1(1));
        end
%         if plt; keyboard; end
    else
        zL1 = L(iZ1);
        zL2 = L(iZ2);
        if min(zL1)>590 || min(zL2)>590
            thks(i) = 10;
        else
            zL1 = L(iZ1);
            zL2 = L(iZ2);
            zNK1 = NK_sio2(iZ1);
            zNK2 = NK_sio2(iZ2);
            zCT1  = cosd(asind(sind(AOI)./zNK1));
            zCT2  = cosd(asind(sind(AOI)./zNK2));
            for j = 1:1:NumT-1
                esthk1(j) = 1/2*(zL1(end+1-j)*zL1(end-j))/(zNK1(end-j)*zCT1(end-j)*zL1(end+1-j)-zNK1(end+1-j)*zCT1(end+1-j)*zL1(end-j));
                esthk2(j) = 1/2*(zL2(end+1-j)*zL2(end-j))/(zNK2(end-j)*zCT2(end-j)*zL2(end+1-j)-zNK2(end+1-j)*zCT2(end+1-j)*zL2(end-j));
            end

    %         thks(i) = 0.5*(sum(esthk1) + sum(esthk2))/(NumT-1);
            thks(i) = 1000*(prod(esthk1./1000)*prod(esthk2./1000))^(1/(2*NumT-2));
            if plt(2)
                figure('windowstyle','docked');
                plot(esthk1); hold on; grid on;
                plot(esthk2);
                title(['Average Thickness = ' num2str(thks(i)) ' nm']);
                keyboard;
            end
    %         disp(thks(i));
        end
    end
end
Data(:,3) = num2cell(thks);
set(handles.uitable1,'data',Data);

if plt(3)
    figure('windowstyle','docked');
    plot(thks);
    xlabel('Point No.');ylabel('Thickness Estimate SiO2 [nm]');
    grid on;
end