function [WLMM1,WLMM2] = JAWlensMM(DO,WD,L)
% Generates JAW lens Mueller matrices based on DO and WD values.

Lz(1,1,:) = L;
NumL = length(L);
% disp(WD(1,3));
% disp(WD(2,3));
% Deli(1,1,:) = 250./Lz.*(DO(1)+DO(2).*(500/Lz).^2+DO(3).*(500/Lz).^4+DO(4).*(Lz./500).^2);
Deli(1,1,:) = 250./Lz.*(DO(1)+DO(2).*(500./Lz).^2+DO(3).*(500./Lz).^4+DO(4).*(Lz./500).^2);
Delo1(1,1,:) = (-500)./Lz.*(WD(1,1)+WD(1,2).*(500./Lz).^2+WD(1,3).*(500./Lz).^4);
% Delo1(1,1,:) = WD(1,1).*(500./Lz+WD(1,2).*(500./Lz).^3+WD(1,3).*(500./Lz).^5);
Delo2(1,1,:) = (500)./Lz.*(WD(2,1)+WD(2,2).*(500./Lz).^2+WD(2,3).*(500./Lz).^4);
% Delo2(1,1,:) = WD(2,1).*(500./Lz+WD(2,2).*(500./Lz).^3+WD(2,3).*(500./Lz).^5);
ons = ones(1,1,length(L));
Mo1t = zeros(4,4,NumL);
Mo1t(1,1,:) = ons;
Mo1t(3,3,:) = ons;
Mo2t = Mo1t;
Mo1t(2,2,:) = cosd(Delo1);
Mo1t(2,4,:) = sind(Delo1);
Mo1t(4,2,:) = -sind(Delo1);
Mo1t(4,4,:) = cosd(Delo1);
Mo2t(2,2,:) = cosd(Delo2);
Mo2t(2,4,:) = sind(Delo2);
Mo2t(4,2,:) = -sind(Delo2);
Mo2t(4,4,:) = cosd(Delo2);

Mi1t = zeros(4,4,NumL);
Mi1t(1,1,:) = ons;
Mi1t(2,2,:) = ons;
Mi1t(3,3,:) = cosd(Deli);
Mi1t(3,4,:) = sind(Deli);
Mi1t(4,3,:) = -sind(Deli);
Mi1t(4,4,:) = cosd(Deli);

% WLMM1 = zeros(4,4,NumL);
% WLMM2 = WLMM1;
% for i=1:NumL
%     WLMM1(:,:,i) = Mo1t(:,:,i)\Mi1t(:,:,i)\eye(4);
%     WLMM2(:,:,i) = Mi1t(:,:,i)\Mo2t(:,:,i)\eye(4);
% end

Mi1t = complex(Mi1t);
Mo1t = complex(Mo1t);
Mo2t = complex(Mo2t);
WLMM1 = MM3D_v2(Mo1t,Mi1t);
WLMM2 = MM3D_v2(Mi1t,Mo2t);
% WLMM1 = MM3D_v2(Mi1t,Mo1t);
% WLMM2 = MM3D_v2(Mo2t,Mi1t);
