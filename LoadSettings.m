% Load Settings

% Breaking this out into a separate script file.
% AGB 2021

if ~exist('DefaultPathDataFiles.mat','file')
    PathF= [getenv('USERPROFILE') '\Desktop\'];
else
    load DefaultPathDataFiles PathF
    if ~exist('PathF','var')
        PathF = [getenv('USERPROFILE') '\Desktop\'];
    end
    if PathF == 0
        PathF = [getenv('USERPROFILE') '\Desktop\'];
    end
end
[FileName,PathF,FI] = uigetfile('*.xlsx','Read from Excel',PathF);
if FI == 0 ; return; end
if isempty(PathF)
    PathF = [getenv('USERPROFILE') '\Desktop\'];
end
save DefaultPathDataFiles PathF
MyFile = [PathF FileName];

% Pull out Table 2
% We don't need the original values
%Data = get(handles.uitable2,'data'); 
[~,~,newData] = xlsread(MyFile);
[s1,~]=size(newData);
fin = 0;
for i=1:s1 % find end of table 2
    if strcmp(newData{i,1},'Eps Inf.')
        break;
    else
        fin = fin +1;
    end
end
Data(1:(fin-1),2:17) = newData(2:fin,1:16);
for i=1:(fin-1)
    for j=4:17
        if isnan(Data{i,j})
            Data{i,j} = [];
        end
    end
end
Data(3:(fin-1),1) = {false};
% For compatibility, if the last param is dPhi2 of the comp params,
% Add the additional 8 new params
if strcmp(Data(fin-1,2),'dPhi2')
    names={'rp11';'ip11';'rp12';'ip12';'rp21';'ip21';'rp22';'ip22'};
    defvals={'Spline',false,1,0,[],[],[],[],[],[],[],[],[],0.001,0.001};
    Data= [Data;[repmat({false},8,1),names,repmat(defvals,8,1)]];
end
if size(Data) ~= 0
    set(handles.uitable2,'data',Data);
end

% Pull out SiDat from NK edit
SiDat = handles.ModParms.SiDat;
nSi = 28;
SiDat(:,3) = newData(fin+1:fin+nSi,2);
handles.ModParms.SiDat = SiDat;
fin = fin + nSi;

% Pull out SiO2Dat from NK edit
SiO2Dat = handles.ModParms.SiO2Dat;
nSiO2 = 9;
if ~strcmp(newData{fin+nSiO2,1},'Nfit_SiO2')
    SiO2Dat(9,1) = {[false]};  %don't fit   
    SiO2Dat(9,3) = {[0.0]};  %zero value
    nSiO2 = 8;                   %Old file before Nfit_SiO2 was added
end
SiO2Dat(1:nSiO2,3) = newData(fin+1:fin+nSiO2,2);
handles.ModParms.SiO2Dat = SiO2Dat;
fin = fin + nSiO2; % reset for next section!

% Pull out 
for i=(fin+1):(fin+6)
    handles.ModParms.(cell2mat(newData(i,1))) = cell2mat(newData(i,2));
end
fin = fin + 6; % reset for next section!

% Pull out CtrlData
for i=(fin+1):(fin+16) 
    handles.CtrlData.(cell2mat(newData(i,1))) = cell2mat(newData(i,2));
end
guidata(hObject, handles);