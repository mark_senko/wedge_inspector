Wedge Inspector v4.0e
5/28/2019 AGB
[Fork of Wedge Inspector v1.1 by Pedro Vagos]

Differences from v4.0d to v4.0e

1) Changed MM custom lens model to Jon's proposed model:
Uses almost a complete Jones matrix description for each lens. 10 total parameters that can each vary with wavelength.

2) Added "Export Corrected Spectra" button:
Uses the custom lens model to correct the MM data of .dat files loaded into WI.

3) Changed the Preset Analysis function:
Now has new limits and requires the user to select a peak for the LVF as a fitting seed.

4) Changed the Preset Analysis multi-folder type:
Now if you load .dats it and hit run preset analysis it will run as before. If you don't have any data loaded and hit the "run preset analysis" button it will ask for a folder. If you give it a folder with an SE directory in it, it will run just that wedge. If you give it a folder of folders that each have an SE directory it will run all of them in series.

5) Attempted to fix the SVD errors:
Code actively monitors the Jacobian matrix to avoid Inf and NaN. 

6) Upgraded the ABORT button:
It now responds immediately no matter what the user is doing, and exits cleanly.

Differences from v4.0c to v4.0d

1) Altered preset analysis upper and lower control limits:
Now reports angular spread, but no limit associated with it. LVF center wavelength changed from +/-5 nm to +/-10 nm. If LVF peak is in fit noise no limit is enforced. Number of erroneous wavelengths now capped at 10.

2) Altered preset analysis functionality:
Can now do batch processing. If data is loaded into WI, will behave the same as previous. If there is no data loaded, it will ask for a folder. If this folder contains an "SE" folder, it will crunch a single wedge. If this folder contains tool names with SE subfolders, it will crunch all wedges it can find.

3) Reduced number of parallel fit iterations:
Jon noticed no difference between 3 and 5 iterations, reduced to 3. 

4) Changed LVF peak finder:
Now uses dual error function peak finder such that "plateau" style peaks can be fit easily, and find the correct central wavelength.

5) Fixed Si anisotropy bug.

6) Added intensity data loading and normalization:
Now loads uT data from file if available. Uses the maximum uT for each wavelength across the wedge to establish a pseudo lamp intensity. This can then be used to back out experimental M11. Denormalization can use this experimental M11 instead of a calculation, preserving the experimental integrity.

7) Repaired the populate thicknesses button:
Now handles thicknesses up to 10 um, with better accuracy for thick films (> 1 um). Uses geometric mean instead of arithmetic mean.

Differences from v4.0b to v4.0c

1) Fixed critical point model simulation. Data points can now be simulated and fit with the critical point model.

2) Fixed error that caused data and model to be corrupted when switching between 

Differences from v4.0a to v4.0b

1) Added "Preset Analysis" button in top left corner. This button allows manufacturing to perform a basic wedge analysis automatically. Output results are saved in the original folder, in a new folder named "WI_PA_Results". This folder is overwritten each time a new preset analysis is run. All figures are saved. Two excel files are created, one with a loadable calibration (calibration.xlsx) and one with highlighted results (Results.xlsx).

2) Fixed bugs in v4.0a. (There were a lot)

Differences from v3.0x to v4.0a

1) Version 4 is a complete redesign of wedge inspector. It uses a seperate figure window to graph either NCS or MM data, and can fit either. The user can also seamlessly move between MM and NCS modes. The control window still uses the dual table layout with files and fit results in the left table and the model parameters in the right table. A new "controls" window contains options for data processing and fitting.

2) The maximum number of iterations can now be controled in the "controls" window.

3) The threshold to halt Levenberg Marquardt execution can be controlled in the "controls" window.

4) As of right now the generate data button and critical point model are both inoperative.

5) All parameters can now be made wavelength dependent, with 10 potential coefficients. Change the wavelength behavior by using the dropdown box.

6) All parameters can now be split by detector. Use the split checkbox. The split occurs at 1000 nm.

Differences from v2.2 to v3.0

1) Added Mueller matrix fitting and advanced lens parameters in calibration. All version 3s stayed in development and were not released.

Differences from v2.1 to v2.2

1) Adjusted beam fitting parameters.

Differences from v2.0:

1) Bandwidth calculation has been completely overhauled
BW is now calculated for each measured point by a Gaussian with a specific number of sampled points (adjustable with BW #Slices, parameter 24) and centered at the original measured data point. We have given up a lot of speed moving to a proper numerical integration. For that reason, the program now loads with 0 bandwidth when started. A value of 0 for BW will skip the bandwidth calculation and improve the speed. The VIS and IR calculations are separated, so you can have 0 for the IR to improve speed while fitting on the VIS bandwidth. That being said, the BW algorithm is as fast as possible in Matlab without rewritting in C/C++.

2) Total RMSE calculation
The overall RMSE for the wedge is now calculated and displayed in the bottom left corner (after Idle...). It is calculated the same way as other RMSEs but uses the entire wedge, for the wavelengths determined by Lmin and Lmax. The data must be simulated or fit to display this value.

3) Beta parameter added (Si anisotropy)
You can now fit the beta parameter (#40) which adds anisotropy to the Si substrate. Si is modeled as a uniaxial material with beta as the ratio between the vertical and horizontal dielectric function:
beta = [eps(horizontal)-1] / [eps(vertical)-1]

4) Separate angle parameters for IR (AOI_ir, AOI_ir_l, NA_ir)
The infrared now has offset parameters for AOI, NA, and an optional linear AOI term for misalignment diagnosis. These are model parameters #41-43. They are offsets of parameters 3 and 4, and so are ideally 0. The linear term uses 1000 nm as its starting location (zero crossing). 

5) Function tolerance (ftol) for Levenburg-Marquardt algorithm
The ftol has been dropped from 0.003 to 0.0001 at Jon's request, in order to keep the fitting going for excellent fits. Might look into creating a parameter for this in the future...

6) Wedge Data Generation
You can now generate data with the "Gen Data" button. It will replace any selected files in the measurement table with generated data based on the parameters in the model table. It will use the thicknesses from the measurement table. This means you need to load data into the program to generate data, even if you're going to replace it all. This establishes the rc2-cal.cnf and CompleteEASEhard.cnf files from the measured data as the starting point for generating data, as if you were generating data from that specific system.

7) Fixed file loading issues
Files were loading inconsistently, sometimes slow and sometimes fast. It had to do with multiple layers of file opening code from Pedro, operating with "adap" file opening programs built to load Nanometrics data. One of these files (loaddata.m) had a bug which would cause Pedro's software to use a backup loading method which was extremely slow. I fixed that bug, and would encourage you to overwrite the loaddata.m file in Pedro's toolbox. I also wrote a new program specific to WI (openSEspectra.m) that will handle the file loading through loaddata.m.

8) Loading alternative data from files
In the upper left corner of the GUI you can now select where WI pulls NCS data from. Pedro's original program imported Psi and Delta and converted the values to NCS in order to preserve the error information (the adap loading program adds ncs automatically from the data, but does not convert the error information). You may now select Psi & Delta (original loading method), MM elements (these are averaged MM elements, ie. M12 & M21 for N), or ADAP NCS (converted via Pedro's ADAP toolbox algorithm). If you load from MM elements, the data will be normalized by M11 but not normalized by (N^2+C^2+S^2)^(1/2). In order to ensure the correct calcuation, the drop down menu must be set to "Mueller Matrix" to avoid normalizing to the NCS distance.

9) Slice Fit
The slice fit mode is currently broken, and will be fixed in v2.2 hopefully.

Welcome to v2.0!
Differences from v1.1e:

1) Now uses C code to speed up MM generation
C code implemented as mex file to speed up MM generation. The code in question is MM3D_v2.c and MM3D_v2.mexw64. They are written to perform matrix multiplication along dimensions 1 and 2 of a 3 dimensional complex matrix (matrix must be complex and square). This is much much faster than performing the same calculation in Matlab.

2) Anisotropic code is now standard
Because of speed increases in #1, we can now regulary compute the MM with uniaxial SiO2. The parameter "alpha" controls this behavior. For alpha=1 the SiO2 is isotropic, and the code uses the (still faster) Fresnel calculation routine. For alpha<1 the z axis has a higher index than the x-y axes. For alpha>1 the z axis has a lower index than the x-y axes. For both cases the code uses the (slightly slower) 4x4 matrix method for calculating the MM. The model has been changed to better reflect the strain hypothesis, in which the z axis is neutral and the x-y axes are squeezed by alpha.

3) A warning about the Beam Spreading model
The beam spreading model (a.k.a. lateral spread, clipping model, etc.) is still under development and should not inherently be trusted. If you're using this feature please talk to Alex about what its capabilties really are. It does NOT work if alpha!=1. In other words you cannot model beam spreading and anisotropy at the same time (currently).

4) Parameter Limits
This is a big change, and a long time coming. You can now limit fit parameters in columns 4 and 5 of the model table. If both the upper and lower limit are the same value the parameter will have an unlimited range. If the parameter exceeds its limits the value will be reflected back somewhere within 10% of the limit based on how far it exceeded said limit. This style of limiting behaves nicely with the current least squares fitting routine.

5) Radius Adjusted Circular Beam Profile
As requested by Jon Opsal, this is a circular beam profile with an adjustable radius so you can effectively get a rounded box shape. You MUST have a radius>=1, so always check your parameter limits. The radius is controlled by the Bm1 parameter. If the radius dips below 1 imaginary numbers will invade non-imaginary spaces and eventually the program will error with NaN.

Differences from v1.1d:

1) Mathematical index of refraction for SiO2
Now uses a Epsilon infinity + UV pole + IR pole approach. This change was made because the pole model can accurately duplicate the SiO2_jaw.txt index of refraction. So it is now easier to switch between the material file based index and mathematical index without destroying your fit. There is also a new parameter entitled "EMA Void". This adds air pockets to the SiO2 layer, and was implemented to test the equivalent parameter inside the CompleteEASE calibration.

2) Load Settings button
In the top right corner the reset settings button has been replaced with a load settings button. This reads in a saved .xlsx file from a previous fit.

3) Denormalize NCS
In the lower right corner the "Enable Plots" checkbox was replaced with a "Denormalize NCS" checkbox. Denormalize NCS multiplies both the calculated and measurement values of N, C, and S by the calculated M11. This is to avoid improper weighting of of specific wavelength values.

4) Beam Spreading Parameters (aka Lateral Spreading)
Parameters 37 through 39, "r01a", "nstopa", and "nstopd" model the limited collection of light from a sample. "r01a" is the percentage of light collected from the first reflection (top of the SiO2), with 1 being 100%. "nstopa" is the number of internal bounces collected (between the top and bottom of the SiO2). "nstopd" is the difference in the number of bounces collected between 's' and 'p' polarized light. This model is a work in progress, see Alex for further details.

5) Arc Factor
The JAW Arc Factor has been added as an option under the Beam Profile selection box in the bottom right. It uses the Bm1 parameter as the arc factor. Make sure to set this to 0 (its ideal value) before starting a fitting procedure. The arc factor combines a line (slope given by Bm1) with a semicircle to create an asymmetric beam profile.

6) IR Wavelength Calibration Parameters
These parameters are now correctly pulled in from the calibration files, so the IR spectrum can be used. Fitting on the wave-cal parameters was buggy and has been fixed.

7) Copy 2 Simulation button
This button now takes the average values over all selected files in the results table (left) for each parameter and places the averages into the simulation table (right). Previously it found and copied the lowest MSE values.

8) Levenburg Marquardt updated
New Levenburg Marquardt routine has an adjusted trust region model more in line with our fitting expectations (it's more forgiving). The whole algorithm now uses RMSE as a cost function, and will always accept a new parameter set with lower RMSE (previously it would reject paramemter sets which fell outside of the trust region). 

9) Serial Fit and Parallel Fit
Serial fit is equivalent to the previous "single model fit" in which all the selected files in the results table (left) will be fit one at a time in series. Right clicking on the Serial Fit button will change the fit mode to "Parallel Fit". This is now equivalent to global optimization. In this mode all the selected files will be fit at once, with the thicknesses as local variables and all other selected parameters used as global variables. The global variables will be adjusted first, and then the thicknesses will be fit for each file individually. This process is repeated 10 times and can be halted with the abort button. No results will be saved upon abortion.

Differences from v1.1c:

1) NK Edit button
"NK Edit" brings up a separate menu where the materials files can be changed, the materials path can be reset, or the dispersion can be calculated from a critical point model (for Si), a Maxwell effective medium approximation (for the interlayer), and a Cauchy function (for SiO2). Each of the parameters can be selected for fitting from within this menu.

2) Populate Thickness button behavior
The "Pop. Thk" button now responds to the model selected in NK edit. If the material files are used, it will use the .txt file corresponding to SiO2 for the thickness estimate. If the mathematical model is selected, it will use a Cauchy for the calculation.

3) Copy to Simulation button behavior
The "Cpy 2 Sim" button will now also copy the best match model from the loaded files on the left into the NK edit window.

Differences from v1.1b:

1) Speed
For loops have been vectorized for speed, regaining Pedro's original speed without sacrificing functionality.

2) NA handling
NA now has 4 wavelength parameters named NA1.. NA4, replacing the CaucA, CaucB, and CaucC parameters. The behavior of these parameters is determined by the "NA Wvl Dep." selection box. If "Polynomial" is selected, the equation is:
	NA(wvl) = NA*(1 + NA1*wvl + NA2*wvl^2 ... ).
If "Thin Lens Approximation" is selected, the equation is:
	NA(wvl) = atan([n(wvl) - 1]*D2R), where...
	n(wvl) = NA1 + NA2/wvl^2 + NA3/wvl^4 (Cauchy) and...
	D2R = tan[asin(NA)]/[n(NA4)-1].
This approximates the axial color induced by a thin lens with index of refraction n(wvl), with a NA equal to NA at wvl = NA4.

3) Beam Profile handling
The beam profile now has a selection box such that Circular, Square, and Bm parameter profiles can be selected. When "Use Bm Parameters" is selected the beam profile is a linear interpolation of the Bm1 .. Bm5 parameters in the model window.

4) Bandwidth modeling
Bandwidth modeling is now a hybrid between a "true" bandwidth calculation and Pedro's ingenius circshift technique. The "BW #Slices" value determines the number of calculated points within a distance of BW_VIS*6 (read: 6 sigma). Thus even if BW_VIS is changing the number of resampled points within the Gaussian will remain equal to BW #Slices. 

5) Additional Lens birefringence
Lens birefringence is added via the equation:
	Rp = Rp*exp(i*1e4*PthF/wvl*[1-cos(dAOI{slice})]) ...
or in other words it simply adjusts the phase of Rp in relation to Rs, doing so differently for each change in dAOI per slice.

6) Slice Fit number of slices
The number of slices in a slice fit is now controlable with the "SF #Slices" parameter in the model table.

7) Abort Button
During the fitting routine an abort button will appear in the bottom right of the screen, if you press it, it will abort the current procedure AFTER the most current fit has finished. No results will be saved.

8) Populate Thickness button
This is our version of "Cycle Select", it populates the thickness column by estimating the thickness from the fringe spacing in S. See Alex if you're curious about why S is used instead of N.

9) Docked plot windows
Either when the "Enable Plots" checkbox is on or when the "Plot Fit Parms" button is pushed, the resulting figures will be displayed in a docked window. No more clicking "X" 10,000 times!

10) Adjusted Levenburg Marquardt algorithm
New version of Levenburg Marquardt, called "LbMqBASICv2". Has been vectorized to speed up calculation and new measures are in place to prevent "infinity" and "not-a-number" errors. Thanks Jon! Also there's a new halt criteria that stops the fit if Chi^2 has changed by less than 10^-4 between iterations.


Differences from v1.1a:

1) Corrected Slice Fit bug
Slice fit now responds to "Use Matlab LS" checkbox, will use homebrew least squares algorithm for fitting if unchecked.

2) Changed fitting behavior for Thk for multiple files
If there is a value for the row 1 thickness but not for the remainder of thicknesses selected, it will use the row 1 thickness as a seed to begin the fitting process (used 1 nm before). Subsequent thicknesses will use the previous best fit thickness.

3) Changed MSE behavior
"Chi-squared" checkbox now changed to "JAW MSE" checkbox. Will use Woollam calculation for MSE if checked. Default behavior is to calculate RMSE as: [1/N*Sum{(Model_i-Exp_i)^2,i=1..N}]^0.5

4) Changed Material file location behavior
Path for materials is now saved, if it doesn't exist you will be prompted! No path changes required.


Differences from v1.1:

1) Model change - EMA
This version uses an effective medium approximation in place of the INTR_jaw.txt layer. This file is included in the folder but must be placed in your specific "Materials" folder. The thickness of this layer can be fit by checking the "ThkEMA" box in the right hand table.

2) "Cpy 2 Sim" button
This button searches the spectra in the left hand table for the lowest MSE and copies those best-match parameters to the simulation table (right hand side). The correct parameters must be selected in the right hand table or the copy will fail.

3) "Enable Plots" checkbox
This checkbox enables the automatic plotting of fit parameters after a fit. Useful for wedges, annoying for single spectra.

4) "Use MATLAB LS" checkbox
This checkbox tells the program to use Matlab's implementation for the least-squares fitting. If unchecked Alex's version of the Levenburg Marquardt algorithm will be used. KNOWN ISSUE: Alex's code not setup for "slice fit".

5) "Slice Fit" button
Replaces the global fit in v1.1 with a fit that slices the experimental spectra in energy space and fits these slices individually. The N,C, and S graphs will be updated at the end of the fit, and the MSE will be reported in the Matlab command window.

6) "Cauc" parameters
CaucA, CaucB, and CaucC are Cauchy parameters that describe the index of the T1 lens approximated as a thin lens. This changes the focus length and thus the NA with wavelength. Default values are 2,0,0, which keeps NA the same over all wavelengths. LamF is the center frequency used to normalize the thin lens approximation. LamF should not affect the spectrum, but might affect the fitting process. Leave unselected in normal use.

7) "PthF" parameter
Adds lens birefringence to T1, implemented as a phase shift in the Jones matrix.

8) "Bm" parameters
Controls the beam profile used for NA integration. All ones indicates a uniform beam. The total width of the beam is controlled by NA, with these 5 point evenly distributed between. Values of the beam intensity between the points are interpolated. Values are automatically normalized.
Suggested values:	[1, 1, 1, 1, 1] = uniform
			[0, 86, 100, 86, 0] = "circular-ish"
			[1, 32, 100, 32, 1] = "Gaussian-ish"