function [rss,rpp] = UniJM_v5(NK,thk,AOI,Lo,alpha,beta)
% This function calculates the Mueller Matrix of SiO2 given the input
% angle of incidence, and the uniaxial anisotropy factor alpha. A
% vector of wavelengths Lo is also required.

% Schubert's matrix method is used for the calculation.

% MM elements are arranged in column format, with each row as a new Wvl,
% and the rows organized as:
% 11 12 13 14 21 22 23 24 31 32 33 34 41 42 43 44
% tic; disp('Start');

% Dummy values for Run & Time
% NK,thk,AOI,Lo,alpha
% Lo = linspace(200,1700,1500).';
% PathMat = 'C:\Mats\';
% NK_Si_jaw = GetNKs([PathMat '\Si_JAW.txt'],Lo);
% NK_int_jaw = GetNKs([PathMat '\INTR_JAW.txt'],Lo);
% NK_sio2_jaw = GetNKs([PathMat '\SiO2_JAW.txt'],Lo);
% NK = [NK_Si_jaw, NK_int_jaw, NK_sio2_jaw, Lo-Lo+1];
% thk = [0, 3, 1000, 0];
% AOI = 65.*ones(1500,1);
% alpha = 1.0031; % SiO2 Anisotropy
% beta = 1; % Si Anisotropy
% disp('Start'); tic;

% Thk correction for WI v4.2x+
thk = [0 thk 0];

% Rotate to z direction and calculated uniaxial properties
SiO2_ext(1,1,:) = NK(:,3);
intr_ord(1,1,:) = NK(:,2);
alpha = permute(alpha,[3 2 1]);
SiO2_ord = sqrt(alpha.*(SiO2_ext.^2-1)+1);
Si_ext(1,1,:) = NK(:,1);
% Si_ord = sqrt((abs(Si_ext.^2)-beta.*(real(Si_ext.^2)-1)+1)./2)+imag(Si_ext);
beta = permute(beta,[3 2 1]);
Si_ord = sqrt(beta.*(Si_ext.^2-1)+1);

% Convert Angles to Radians
AOI = AOI.*pi/180;

% Rotate Eps & Build Delta & Calc Mueller Elements
kx(1,1,:) = sin(AOI);
% cosf(1,1,:) = sqrt(1-(kx./SiNK).^2);
q = sqrt(intr_ord.^2-kx.^2);
zos = zeros(1,1,length(Lo));
ons = ones(1,1,length(Lo));
k0(1,1,:) = 2*pi./Lo;

% toc;

T1 = [cos(k0*thk(2).*q), zos, zos, 1i.*(q./intr_ord).*sin(k0*thk(2).*q);...
      zos, cos(k0*thk(2).*q), -1i./q.*sin(k0.*thk(2).*q), zos;...
      zos, -1i.*q.*sin(k0.*thk(2).*q), cos(k0*thk(2).*q), zos;...
      1i.*(intr_ord./q).*sin(k0*thk(2).*q), zos, zos, cos(k0*thk(2).*q)];
T1 = complex(T1);
  
Nxz = SiO2_ord.*sqrt(1-(kx./SiO2_ord).^2);
Nyy = SiO2_ext.*sqrt(1-(kx./SiO2_ext).^2);
Kp = k0.*thk(3).*Nxz;
Ks = k0.*thk(3).*Nyy;

T2 = [cos(Kp), zos, zos, 1i.*(Nxz./SiO2_ord.^2).*sin(Kp);...
      zos, cos(Ks), -1i./Nyy.*sin(Ks), zos;...
      zos, -1i.*Nyy.*sin(Ks), cos(Ks), zos;...
      1i.*(SiO2_ord.^2./Nxz).*sin(Kp), zos, zos, cos(Kp)];
T2 = complex(T2);

cosy(1,1,:) = sqrt(1-(kx./Si_ord).^2);
cosz(1,1,:) = sqrt(1-(kx./Si_ext).^2);
Lx = [zos,         zos, cosz, zos;...
      ons,         zos,  zos, zos;...
      -Si_ord.*cosy, zos,  zos, zos;...
      zos,         zos, Si_ord, zos];
Lx = complex(Lx);

cosf(1,1,:) = 1./cos(AOI);
La = [zos, ons, -cosf, zos;...
      zos, ons,  cosf, zos;...
      cosf, zos,  zos, ons;...
     -cosf, zos,  zos, ons].*(1/2);
La = complex(La);

% toc;

% Calculate total transfer matrix
T = MM3D_v2(T1,T2);
T = MM3D_v2(T,Lx);
T = MM3D_v2(La,T);

% toc;

denom1=(-T(1,3,:).*T(3,1,:)+T(1,1,:).*T(3,3,:));
denom2=(-T(3,3,:).*T(1,1,:)+T(1,3,:).*T(3,1,:));
rss = (-T(2,3,:).*T(3,1,:)+T(2,1,:).*T(3,3,:))./denom1;
rpp = (-T(4,3,:).*T(1,1,:)+T(4,1,:).*T(1,3,:))./denom2;
% rps = (-T(4,3,:).*T(3,1,:)+T(4,1,:).*T(3,3,:))./denom1;
% rsp = (-T(2,3,:).*T(1,1,:)+T(2,1,:).*T(1,3,:))./denom2;

% toc;
rss = permute(rss,[3 2 1]);
rpp = permute(rpp,[3 2 1]);
% rsp = permute(rsp,[3 2 1]);
% rps = permute(rps,[3 2 1]);
% JM = [rpp, rsp, rps, rss];
% JM = [rss,rpp];
% J1 = [rpp, rsp; rps, rss];
% J2 = conj(J1);
% MM = zeros(4,4,length(Lo));
% MM(1:2,1:2,:) = J1(1,1,:).*J2; % Kronecker product
% MM(1:2,3:4,:) = J1(1,2,:).*J2;
% MM(3:4,1:2,:) = J1(2,1,:).*J2;
% MM(3:4,3:4,:) = J1(2,2,:).*J2;
% QQ = [1 0 0 1;1 0 0 -1; 0 1 1 0; 0 -1i 1i 0];
% QQinv = [0.5 0.5 0 0 ; 0 0 0.5 0.5i; 0 0 0.5 -0.5i; 0.5 -0.5 0 0];
% MM = MM3D_v2(MM,QQinv);
% MM = MM3D_v2(QQ,MM);
% MM = permute(MM, [3 2 1]);
% MM = reshape(MM, [], 16);

% toc;

%% Optional, Plot Jones Matrix Values
% figure('windowstyle','docked');
% plot(Lo,real(rpp),Lo,real(rsp),Lo,real(rps),Lo,real(rss));
% xlabel('Wavelength [nm]');
% legend('rpp','rsp','rps','rss');
% title('Real Jones');
% figure('windowstyle','docked');
% plot(Lo,imag(rpp),Lo,imag(rsp),Lo,imag(rps),Lo,imag(rss));
% xlabel('Wavelength [nm]');
% legend('rpp','rsp','rps','rss');
% title('Imaginary Jones');

end

