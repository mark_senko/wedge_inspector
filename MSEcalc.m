function [MSE] = MSEcalc(JAWmse,dY,SigY,Nparms)
% Calculate MSE (JAW) or RMSE

if size(SigY,2) > 0
    SigY = reshape(SigY,[numel(SigY) 1]);
end

if size(dY,2) > 0
    dY = reshape(dY,[numel(dY) 1]);
end

if JAWmse~=1
    MSE = sqrt(sum((dY./SigY).^2)/(numel(dY)-Nparms));
%     MSE = sqrt(sum((dY./0.001).^2)/(numel(dY)-Nparms));
else
    MSE = sqrt((dY'*dY)/numel(dY));
end

end

