function [PD,SigPD] = NCStoPD(NCS,SigNCS)
% Simple function to create Psi and Delta from NCS.
% Based on Pedro Vagos' NCS2Deg3() without types, weights, wvls, AOIs, etc.
% Still converts sigmas.
% AGB 2021

% Define parms
deg = 180/pi;
N = NCS(:,1);
C = NCS(:,2);
S = NCS(:,3);
SigN = SigNCS(:,1);
SigC = SigNCS(:,2);
SigS = SigNCS(:,3);

% Psi and Delta calculation
Delta = atan2(S,C);
Delta = unwrap(Delta);
Delta = mod(Delta+pi/2,2*pi)-pi/2;
Psi = 0.5.*acos(N);
Psi = deg.*Psi;
Delta = -360+deg.*Delta;

% Psi sigma calculation
SigmaPsi = SigN./(2*sqrt(1-N.*N));
SigmaPsi = SigmaPsi*deg;

% Delta sigma calculation
C2 = C.*C;
S2 = S.*S;
SigmaDelta = sqrt(S2.*SigC.*SigC + C2.*SigS.*SigS)./(C2+S2);
SigmaDelta = SigmaDelta*deg;

% collect for output
PD = [Psi,Delta];
SigPD = [SigmaPsi,SigmaDelta];

end

