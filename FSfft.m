function [U,f]=FSfft(u,dt)
% use FSfft(...)/sqrt(n) for energy conservation
  n=size(u);
  U=fftshift(fft(ifftshift(u,1),n(1),1),1);
  if nargout>1,
    if nargin<2, dt=1; end
    [m,n]=size(u); %ASSUME m is the length
    T=m*dt;
    df=1/T;
    f=[-ceil((m-1)/2):floor((m-1)/2)]'*df;
  end