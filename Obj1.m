function F = Obj1(P, X, iX, tX, ToolParms, ModParms, Ye)

X(iX) = P;
ToolParms.LimitedUpdate = [];
[Ysim,Yexp] = UpdateSpectra(X, iX, tX, ToolParms, ModParms, Ye);

F = Yexp-Ysim;
[s1,s2]=size(F);
F = reshape(F,[s1*s2 1]);