% Spectrum Residual Analysis for Jon's lens model

% Get data and compute RMSEs
Ye = handles.Ye;
Ys = handles.Ys;
Ls = handles.Ls;
rmse_l = sqrt(sum(sum((Ye-Ys).^2,3),2)./(size(Ye,3)*size(Ye,2)));
rmse_t = sqrt(sum(sum(sum((Ye-Ys).^2,3),2),1)./numel(Ye));

% Get RMSE spec from file
rmse_s = xlsread('MCRA.xlsx');
rmse_s = spline(rmse_s(:,1),rmse_s(:,2),Ls);

% find lvf center freq
[lvf_c,lvf_a] = findlvf_v2(Ls,rmse_l);

% plot results
figure('windowstyle','docked','Name','Tool RMSE & A3 Spec.');
plot(Ls,rmse_s,'k');
hold on
plot(Ls,rmse_l,'b');
iL = (rmse_l > rmse_s);
plot(Ls(iL),rmse_l(iL),'or');
hold off
num_failed_wvls = sum(iL);
xlabel('Wavelength [nm]');
ylabel('RMSE vs A3 Spec');
xlim([Ls(1) Ls(end)]);
box on
grid on
legend('A3 Spec from 25 Tools','This Tool','Erroneous Wavelengths');

% Find folder
Foldr = [handles.PathF 'WI_JLC_Results'];
if 7==(exist(Foldr))
    delete([Foldr '\*.*']);
else
    mkdir(handles.PathF,'WI_JLC_Results');
end

% save figures
SaveAllFigs(Foldr);

% save calibration
SaveSettings(handles,[Foldr '\calibration.xlsx']);