% Preset Analysis for MFG
tic;

% Estimate thicknesses and fit for them
PopulateThks;
SerialFit;
if op.iterations == -1; return; end

% Change up parameters
handles.CtrlData.m11s = true;
tabdata = get(handles.uitable2,'data');
F = {' ',' ',true,true,true,true,false,true,false,false,false,false,false,...
     false,false,false,true,true}';
tabdata(:,1) = F; 
tabdata(3,3:9) = {'None',true,4,tabdata{3,6},0,tabdata{3,6},0};
tabdata(3,16:17) = {0.01,0.01};
tabdata(4,3:7) = {'None',false,1,0.065,[]};
tabdata(4,16:17) = {0.001,0.001};
set(handles.uitable2,'data',tabdata);
guidata(hObject, handles);

% Subselect data
SubSelect(handles.uitable1,'2');
hGuiFig = findobj('Tag','subselectfig','Type','figure');
if ~isempty(hGuiFig)
    % get the handles
    subhands = guidata(hGuiFig);
    % call the pushbutton1 callback using the name of the GUI
    SubSelect('pushbutton1_Callback',subhands.pushbutton1,[],subhands);
end

% Parallel Fit Vis
ParallelFit;
if op.iterations == -1; return; end

% Copy results over
GUI_WedgeDataInspector('CopySim_Callback',handles.CopySim,[],handles);

% Change up parameters
tabdata = get(handles.uitable2,'data');
tabdata(1:2,6) = {1010;1650};
tabdata(3,3) = {'Polynom.'};
tabdata(3,7) = {0};
F = {' ',' ',true,false,true,false,true,false,false,false,false,false,false,...
     false,false,false,false,false}';
tabdata(:,1) = F;
set(handles.uitable2,'data',tabdata);
guidata(hObject, handles);

% Parallel Fit IR
ParallelFit;
if op.iterations == -1; return; end

% Copy results over
GUI_WedgeDataInspector('CopySim_Callback',handles.CopySim,[],handles);

% Reselect all files
data = get(handles.uitable1,'data');
data(:,1) = repmat({true},[size(data,1) 1]);
set(handles.uitable1,'data',data);
guidata(hObject, handles);

% Change up parameters
tabdata = get(handles.uitable2,'data');
tabdata(1:2,6) = {210;1650};
F = {' ',' ',false,false,false,false,false,true,false,false,false,false,false,...
     false,false,false,false,false}';
tabdata(:,1) = F;
set(handles.uitable2,'data',tabdata);
guidata(hObject, handles);

% serial fit thickness for all experiments
SerialFit;
if op.iterations == -1; return; end

% create figures
close(handles.figh);
GUI_WedgeDataInspector('plotFitParams_Callback',handles.plotFitParams,[],handles);
GUI_WedgeDataInspector('plotwvldeps_Callback',handles.plotwvldeps,[],handles);
pause(2);

% spectrum analysis
ResidualAnalysis;

toc