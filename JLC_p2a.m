% Jon's Lens calibration phase 2a

% get table 2 and store in tabdata
tabdata = get(handles.uitable2,'data');
% set new wavelengths 420 to 990
tabdata(1:2,6) = {210;990};
% set new AOI and NA parameters
tabdata(3,3:17) = [{'Spline', true, 10},...
                   repmat(tabdata(3,6),[1 10]),{0.001,0.001}];
tabdata(4,3:17) = [{'Spline', true, 10},...
                   repmat(tabdata(4,6),[1 10]),{0.001,0.001}];
% set fitting parameters (Thk, AOI, NA, W-vis)
F = {' ',' ',... % Lmin & Lmax
    true,...     % AOI
    true,...     % NA
    true,...     % BW
    true,...     % W-vis
    false,...    % W-ir
    false,...    % Thickness
    false,...    % EMA
    false,...    % alpha (SiO2 aniso)
    false,...    % beta (Si aniso)
    false,...    % Bm1
    false,...    % Bm2
    false,...    % Bm3
    false,...    % Bm4
    false,...    % Bm5
    true,...     % NCS Ret.
    true,...     % NCS Att.
    false,...    % T1 Ret.
    false,...    % T1 Att.
    false,...    % T1 Real O.A.
    false,...    % T1 Imag O.A.
    false,...    % T2 Ret.
    false,...    % T2 Att.
    false,...    % T2 Real O.A.
    false};      % T2 Imag O.A.
tabdata(:,1) = F; 

% update tabdata
set(handles.uitable2,'data',tabdata);
guidata(hObject, handles);

% Parallel Fit
ParallelFit;
if op.iterations == -1; return; end

% Copy results over
GUI_WedgeDataInspector('CopySim_Callback',handles.CopySim,[],handles);