function [gbest,output] = LbMqBASICv5(func,guess,LL,UL,stol,ftol,lambda,maxiter,axhs)
% [guess,output] = LbMqBASIC(func,guess,stol,ftol,lambda,maxiter)
% Basic non-linear least squares solver using Levenburg-Marquardt
% algorithm, to replace lsqnonlin in Matlab optimization package.

% func = handle of the function to be fit
% guess = vector of input parameters for func
% stol = step tolerance for stopping the fit
% ftol = function tolerance for stopping the fit
% maxiter = maximum number of iterations
% txthand = text handle for updating UI

% Version 5 changes
% > Changed limit behavior to adjust del instead of guess
% > Added support for upwait_v2
% > For use with WI v4.0+

% Version 4 changes
% > Added limits

% Version 3 changes
% > Display waitbar when maxiter = 2 (for Parallel fit)
% > Reduced MSE is always captured, regardless of rho
% > Trust region (rho) has been updated, no longer a guess
% > Uses RMSE throughout

% Initialize
n = 1;
del = 0.001*guess;
idel = (abs(del) < 1E-5);
if sum(sign(del(idel))) == 0
    del(idel) = 1E-5;
else
    del(idel) = sign(del(idel))*1E-5;
end

% Limits
iP = (guess+del > UL) & (UL ~= LL);
del(iP) = LL(iP)+0.9.*(UL(iP)-LL(iP))+0.1.*(UL(iP)-LL(iP)).*exp((UL(iP)-(guess(iP)+del(iP)))./(UL(iP)-LL(iP)))-guess(iP);
iP = (guess+del < LL) & (UL ~= LL);
del(iP) = UL(iP)-0.9.*(UL(iP)-LL(iP))-0.1.*(UL(iP)-LL(iP)).*exp(((guess(iP)+del(iP))-LL(iP))./(UL(iP)-LL(iP)))-guess(iP);

% Initialize again
gbest=guess;
nparms = length(guess);
dynew = func(guess);
datanum = length(dynew);
J = zeros(datanum,nparms);
Lup = 9; Ldn = 1/7; % Used for scaling lambda
chi2new = sqrt((dynew'*dynew)/(datanum));
chi2best = chi2new;

% Main loop
while n <= maxiter
    J = FinDiff(J,func,del,nparms,guess,axhs); % Always compute new Jacobi
    chi2 = chi2new;
    dy = dynew;
    
    % Determine new del offset
    H = J.'*J;
    top = H+lambda.*diag(diag(H));
    itop = isnan(top) | ~isfinite(top); % Condition top to avoid NaN
    top(itop) = 1E-5;
    bot = J.'*dy;
    [U,D,V] = svd(top); % SVD method to handle singular matrices
    D = diag(D);
    iD = (abs(D) < 1E-5);
    D(iD) = 1E-5;
    D(~iD) = 1./D(~iD);
    D = diag(D);
    del = V*D*U'*bot;
    
    % Condition del to avoid NaN and Inf
    idel = (abs(del) < 1E-5);
    del(idel) = sign(del(idel))*1E-5;
    idel = isnan(del);
    del(idel) = 1E-5;
    
    % Fold del back within limits
    iP = (guess+del > UL) & (UL ~= LL);
    del(iP) = LL(iP)+0.9.*(UL(iP)-LL(iP))+0.1.*(UL(iP)-LL(iP)).*exp((UL(iP)-(guess(iP)+del(iP)))./(UL(iP)-LL(iP)))-guess(iP);
    iP = (guess+del < LL) & (UL ~= LL);
    del(iP) = UL(iP)-0.9.*(UL(iP)-LL(iP))-0.1.*(UL(iP)-LL(iP)).*exp(((guess(iP)+del(iP))-LL(iP))./(UL(iP)-LL(iP)))-guess(iP);
    
    % Recompute function value
    dynew = func(guess + del);
    
    % Compute new chi & rho
    chi2new = sqrt((dynew'*dynew)/(datanum));
    rho = (chi2^2*datanum) - (chi2new^2*datanum);
    rho = rho/(del.'*(lambda.*diag(diag(H))*del+J.'*dy));
    
    % save off best results
%     if (chi2new < chi2best) && all(guess+del < UL) && all(guess+del > LL)
    if (chi2new < chi2best)
%         disp('new best!');
        chi2best = chi2new;
        gbest = guess + del;
    end
    
    % continue if mse is reduced
    if chi2new < chi2
        guess = guess + del;
    end
    
    % resize lambda for next iteration
    if rho > 0.9
        disp('Lambda DOWN!');
        lambda = max(lambda*Ldn,10e-7);
    elseif rho < 0.2
        disp('Lambda UP!');
        lambda = min(lambda*Lup,10e7);
    end
    
    % Break if no change in chi2 or if less than tolerance
    if (((abs(chi2new-chi2)) < stol) && (n ~= 1)) || (abs(chi2new) < ftol)
        disp(abs(chi2new));
        disp(abs(chi2new-chi2));
        disp(n);
        break;
    end
    
    n = n + 1;
end
output.iterations = n-1;
end

% Updates Jacobi with central finite differences
function [J] = FinDiff(J,func,del,nparms,guess,axhs)
    for i = 1:nparms
        gn = guess;
        gn(i) = guess(i) + del(i);
        dy1 = func(gn);
        gn(i) = guess(i) - del(i);
        dy2 = func(gn);
        J(:,i) = (dy2-dy1)/(2*del(i));
        if ~isempty(axhs)
            upwait_v2(i/nparms,axhs{1},'Computing Jacobian',[],[],[]);
        end
    end
end

% Updates Jacobi with central finite differences
function [J] = FinDiff_C2(J,func,del,nparms,guess,axhs)
    for i = 1:nparms
        gn = guess;
        gn(i) = guess(i) + del(i);
        dy1 = func(gn);
        gn(i) = guess(i) - guess(i)*0.05;
        dy2 = func(gn);
        J(:,i) = (dy2-dy1)/(2*del(i));
        if ~isempty(axhs)
            upwait_v2(i/nparms,axhs{1},'Computing Jacobian',[],[],[]);
        end
    end
end

% Updates Jacobi with forward differences
function [J] = FinDiff_F(J,func,del,nparms,guess,dy1,axhs)
    for i = 1:nparms
        gn = guess;
        gn(i) = guess(i) + del(i);
        dy2 = func(gn);
        J(:,i) = (dy2-dy1)/(del(i));
        if ~isempty(axhs)
            upwait_v2(i/nparms,axhs{1},'Computing Jacobi',[],[],[]);
        end
    end
end

% Updates Jacobi with Broyden method
function [J] = Broyden(J,dy,dynew,del)
    J = J + ((dynew - dy - J*del)*del.')/(del.'*del);
end

