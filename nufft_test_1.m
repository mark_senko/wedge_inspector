% FFT vs NUFFT test

% define x vectors for test
x0 = linspace(0,1000,2048);
% x1 = [linspace(1,100,120), linspace(150,800,1301),linspace(800,1000,381)];
x1 = [linspace(1,100,140),linspace(150,600,1000),linspace(600,790,500),linspace(800,1000,181)];
x0 = x0./1000;
x1 = x1./1000;
NumX0 = length(x0);
NumX1 = length(x1);

% create signals
y0 = sin(50.*2.*pi.*x0) + 1.5.*cos(150.*2.*pi.*x0) + 2.3*sin(333.*2.*pi.*x0) + 2.*randn(size(x0)) + 4;
y1 = sin(50.*2.*pi.*x1) + 1.5.*cos(150.*2.*pi.*x1) + 2.3*sin(333.*2.*pi.*x1) + 2.*randn(size(x1)) + 4;
figure('windowstyle','docked');
plot(x0.*1000,y0);
hold on;
plot(x1.*1000,y1);
title('Signals');
xlabel('t [ms]');
grid on;
legend('normal signal','non-uniform signal');

% FFT
Y0 = abs(fft(y0)./NumX0);
Y0 = Y0(1:NumX0/2+1);
Y0(2:end-1) = 2.*Y0(2:end-1);
fx0 = (2048/NumX0).*(0:(NumX0/2));
figure('windowstyle','docked');
plot(fx0,Y0);
xlabel('freq. [Hz]');
ylabel('|fft(y0)|');
grid on;
xlim([0 max(fx0)]);

% NUFFT
Y1 = abs(nufft(y1,x1.*NumX1)./NumX1);
Y1 = Y1(1:floor(NumX1/2)+1);
Y1(2:end-1) = 2.*Y1(2:end-1);
fx1 = (0:(NumX1/2));
figure('windowstyle','docked');
plot(fx1,Y1);
xlabel('freq. [Hz]');
ylabel('|fft(y1)|');
grid on;
xlim([0 max(fx1)]);

