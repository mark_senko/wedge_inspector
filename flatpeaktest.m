% gaussian test
x = -2*pi:0.01:2*pi;
% g1 = exp(-x.^2);
% g2 = (-1/1.1).*exp(-x.^2.*(1.1));
% g3 = (-1/5).*exp(-x.^2.*(5));
% figure
% plot(x,g1,x,g2,x,g1+g2,x,g1+g3);
% figure
% plot(x,5/4.*(g1+g3),x,11.*(g1+g2));

f = 3;
s = (1:0.1:5);
x0 = 0;
A = 1;

figure
plot(x,flat_gauss(A,x,x0,1,f));
hold on
for i=1:length(s)
    G = flat_gauss(A,x,x0,s(i),f);
    mG(i) = max(G);
    plot(x,G);
end
hold off
grid on; box on;