% Populate Thicknesses

mthd = 2;

plt = [0 0 0];

Data = get(handles.uitable1,'data');
NumF = size(Data,1);
L = handles.L;
iL = (L <= 1400 & L >= 210);
L = L(iL);
S = handles.Y(iL,19,:);
S = permute(S,[1 3 2]);
S = S(:,:,1);
% figure
% plot(L,S(:,1));

ModParms = handles.ModParms;
if ModParms.NKselect == 1
    if isempty(handles.PathMat)
        PathMat = [pwd '\Mats'];
        if ~exist(PathMat,'dir')
            uiwait(msgbox({'No MATERIALS folder found.' 'Please select MATERIALS folder.'},'Folder Not Found','error'));
            PathMat = uigetdir(pwd,'Please Select the Materials Folder');
            handles.PathMat = PathMat;
        else
            handles.PathMat = PathMat;
        end
    end
    NK_sio2 = GetNKs([handles.PathMat '\SiO2_jaw.txt'],L);
elseif ModParms.NKselect == 2
    [~, ~, NK_sio2] = Si_SiO2_Aspnes(L,ModParms.SiDat,ModParms.SiO2Dat);
end
NK_sio2 = NK_sio2 + ModParms.SiO2Dat{9,3};
% add thicker film limits
iL = (L >= 600);
L_2 = L(iL);
NK_sio2_2 = NK_sio2(iL);

% plot(L,S);
thks = zeros(NumF,1);
for i = NumF:-1:1
    MyY = S(:,i);
    iMyY = (MyY < 0);
    iMyY = xor(iMyY,circshift(iMyY,1));
    iMyY(1) = false;
    iMyY(end) = false;    
    esthk1 = 1;
    
    % plot if requested
    if plt(1)
        figure('windowstyle','docked');
        plot(L,MyY);
        grid on; hold on;
        ylabel('S');
        xlabel('Wavelength [nm]');
        plot(L(iMyY),MyY(iMyY),'ok');
        keyboard;
    end
    
%     disp(sum(iMyY));
    if sum(iMyY) == 0
        thks(i) = 10;
    elseif sum(iMyY) <= 2
        mxs = max(L(iMyY));
        thks(i) = interp1([200 590],[32 150],mxs);
    elseif (sum(iMyY) > 2) && (sum(iMyY) <= 20)
        MyY = MyY-(NK_sio2-NK_sio2(end)).*5;
        iMyY = (MyY < 0);
        iMyY = xor(iMyY,circshift(iMyY,1));
        iMyY(1) = false;
        iMyY(end) = false;
        MyL = L(iMyY);
        MyNK = NK_sio2(iMyY);
        CT2  = cosd(asind(sind(65)./MyNK));
        NumT = sum(iMyY)-2;
        for j = 1:1:NumT
            if mthd == 1
                esthk = 0.95/pi*(MyL(end+1-j)*MyL(end-j))/(MyNK(end-j)*MyL(end+1-j)-MyNK(end+1-j)*MyL(end-j));
            else
                esthk = 1/4*(MyL(end+1-j)*MyL(end-j))/(MyNK(end-j)*CT2(end-j)*MyL(end+1-j)-MyNK(end+1-j)*CT2(end+1-j)*MyL(end-j));
            end
%             disp(esthk);
            esthk1 = esthk/1000 * esthk1;
        end
        thks(i) = 1000*(esthk1)^(1/NumT);
    elseif sum(iMyY) > 20    
        % get new Ys
        MyY = MyY(iL);
        
        % find zero crossings
        iMyY = (MyY < 0);
        iMyY = xor(iMyY,circshift(iMyY,1));
        iMyY(1) = false;
        iMyY(end) = false;
        MyL = L_2(iMyY);
        MyNK = NK_sio2_2(iMyY);
        CT2  = cosd(asind(sind(65)./MyNK));
        NumT = sum(iMyY)-2;
        esthk1 = zeros(NumT,1);
        for j = 1:1:NumT
            if mthd == 1
                esthk = 1/pi*(MyL(end+1-j)*MyL(end-j))/(MyNK(end-j)*MyL(end+1-j)-MyNK(end+1-j)*MyL(end-j));
            else
                esthk = 1/4*(MyL(end+1-j)*MyL(end-j))/(MyNK(end-j)*CT2(end-j)*MyL(end+1-j)-MyNK(end+1-j)*CT2(end+1-j)*MyL(end-j));
            end
%             disp(esthk);
            if esthk > 12000
                NumT = NumT - 1;
            else
                esthk1(j) = esthk/1000;
            end
        end
        thks(i) = 1000*(prod(esthk1))^(1/NumT);
        
        % plot if requested
        if plt(2)
            figure('windowstyle','docked');
            plot(esthk1.*1000);
            title(['Estimated thickness = ' num2str(thks(i))]);
            ylabel('Estimated thickness [nm]');
            xlabel('Zero Xing Pair No.');
            grid on
            keyboard;
        end
    end
end
Data(:,3) = num2cell(thks);
set(handles.uitable1,'data',Data);

disp(thks);
if plt(3)
    figure('windowstyle','docked');
    plot(thks);
    xlabel('Point No.');ylabel('Thickness Estimate SiO2 [nm]');
    grid on;
end