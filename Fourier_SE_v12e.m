function [MM]=Fourier_SE_v12e(NK,To,AOI,Lh,alpha,beta,NA,AF,dz,Nx)
% Uses Fred's Fourier optics model of the SE to compute MM data with
% respect to angular spread, defocus, and beam-walk.
% Fred Stanke & Alex Boosalis, 2019

%%% THE OPERATOR CAN MANUALLY EDIT THIS FILE TO TURN ON/OFF "diag" PLOTS &
%%%  CONTROL THE NUMBER OF PSF'S IN THE SOURCE

% NK: [nlayer,nlambda] complex optical constants of media
% To: [1,nlayer] thicknesses in nm
% AOI: [1,nlambda] angle of incidence of optic axis degrees
% Lh: [1,nlambda] lambda nm at pixels +/-3BW for smoothing
% dz: focus, um, 0=optic axis through center of detector slit, dz>0 moves
%   wafer up, towards optics and reduces optical path Bm3
% AF: highjacked parameter from WI now q3Dpow. 1->circular weighting of
%     components in the plane of incidence to simulate 3D with circular
%     aperture in 2D model

% Outpus MM, a 3D matrix of Mueller data vs wavelength

% Version 2 
% > Attempted to optimize speed. Version 1 takes ~6 seconds per calculation
% > Killed outer for loop (lambda), introduced logical indexing
% Version 3
% > Put fftshifts in most convenient places, down to ~2.5 s
% Version 4
% > Attempted to remmove references to speed up calculation, failed
% Version 5
% > Rotate all rectangular matrices so wavelength is by columns and x by rows
% > FFT and IFFT are faster in dominant row direction
% Version 6
% > Reorganize fft locations to operate on all psf at once, failed
% > Finding correct indexing slower than for loop over psf
% Version 7
% > Start with version 5
% > remove extraneous calculations
% Version 8
% > Use custom TIER_1 mex engine for Fresnel calculation, down to ~1.5 s
% Version 8a
% > successfully regression tested against  
% > \Atlas\Models\FourGauss01\testFourier24Cregress on 12 Sep 2019
% Version 8b
% > re-enable ShapeAdj and q3Dpow, with iffed out attempt at aberration
% Version 8c, try to regularize Alex' aberration to faciliate verification
%  and bring it up to Fred's understanding: made progress but got lost
% Version 8d re-start back from 8b and try to add the functionality of 8c
% message confirming entry
% Version 8e: attempt to implement Chromatic and defocus aberrations.  It
%   has one remaining error, fixed in Version 9, that was identified and
%   fixed in
% Version 8eGoodman (whose abandoned drafts are 8eGoodman00 & 01) uses 1
%  point beam and no shaper to model the Fourier method'a aberrated OTF 
%  for comparison to Goodman's analytic expressions.  This version found an
%  error in 8e: d7 used in the place of d5 with frequencies at the wafer
% Version 8f was an abortive attempt to speed things up more
% Version 9 fixes the error found in 8eGoodman
% w00 removes logical indexing from sinth space, which is truly rectangular
%  as determined by the hard pupil aperture, but leaves it in space space
%  it treats a "strip" in sinth space that is 2*NA high with width NL.  It
%  copies the narrow strip into the full ifft domain in a manner that 
%  implements "manual" fftshifts based on timings of 100 ffts without and
%  with fftshifts:
% t1 =  3.2496, t2 = 3.5512, t2-t1 =  0.0928, (t2-t1)/t1*100=9%
% u=[-4:3];
% [u;fftshift(u,2);ifftshift(u,2);fftshift(ifftshift(u,2),2);ifftshift(fftshift(u,2),2)]
%     -4    -3    -2    -1     0     1     2     3
%      0     1     2     3    -4    -3    -2    -1
%      0     1     2     3    -4    -3    -2    -1
%     -4    -3    -2    -1     0     1     2     3
%     -4    -3    -2    -1     0     1     2     3
% w01 attempts to fftshift x0 so all of the variables are in the correct
%  place from the beginning, but doesn't work
% w02 has been debugged and gives identical results to Version 9 in half
%   the time.  I suspect there is some speedup to be had...
% w03 is a minor speedup of w02, and both are a minor speedup of v9, but
%  they essentially got rid of logical indices in Fourier space, so you 
%  can see what is going on
% Ver 10a 4 Nov 2019 restores diagnostic plots at wavelengths that were lost 
%   in the speedup modifications
% Ver 10e 4 Dec 2019, reviewed effect of defocus with Krishna's help and
%   found an error in the longitudinal displacement that goes into Goodman's
%   expression for aberration
% Ver10e00 has an sign error in dR_slit due to dz
% Fourier_SE_v10g.m abortive attempt to remove glitches from sincs
% Fourier_SE_v10gPOR.m works well except for glitch due to cutoff of sincs
% Fourier_SE_v10i00.m attempt to taper sincs at edges, I assume, to get rid
%  of the jags solved 2020 10 24 at Det Slit rather than PH
% Fourier_SE_v10j.m presumes the glitch above was *just* due to Gibbs
%  Phenomenon, and so elimintates this by assuming the FFT of the beam is 
%  unity in x_ph except at the edges where it is 1/2, modulated by correct 
%  phase for placements of the PSFs in the pinhole.  Now, visualizing the
%  field in the ph is optional, but requires an ifft, just to verify it is
%  what we are assuming.
% Fourier_SE_v11a.m 2020 06 04 FES
%   Compare Jd=TIER_1 with Jd=TierND showing a 0.38% discrepancy in
%    wavelengths.  TIER_1 agrees with ND, so presume Tier_1 is wrong!
% Fourier_SE_v11b.m 2020 06 30 FES
%   Go ahead and replace TIER_1 with TierND
% Fourier_SE_v12a.m 2020 07 14 revert back to Alex' Tier_2 and use a
%   function to setup Tier inversion of many stack parameters
% Fourier_SE_v12b.m 2020 10 19, 
% - put weight on slit edge optionally @50% (little effect)
% - plot(x_ph,abs(e_ph).^2) as it has defocus aberration and to put figure
%   all in intensity
%  2020 10 24 InterpolationCuresSteppedNCSvsWvl01.pptx
%  interpolation of physical det. slit apertures onto Wvl scaled detector
%    Rx(indTop)=(Rtop>0.5).*Rx(indTop).*(Rtop-0.5); etc.
% 2020 11 18 Fourier_SE_v12bY.m branched off while Fourier_SE_v12b.m is
%    used to investigate validity of not using illum. ph to gate PSFs
% 2020 12 29 Fourier_SE_v12c.m: images of field variables to aid in
%   understanding what the intensity scaling could be
% 2020 12 29 Fourier_SE_v12d.m modification to intensity scaling to be
%   logical and consistent with WI so LM criteria can be the same
% 2021 01 08 Previous versions computed MM as Stokes vector.
%   Fourier_SE_v12e.m computes Stokes vectors from perfect reflector to
%   normalize Stokes vectors into MM

TierStart=tic;
%Setup=tic;
diag(1)=0; %1-> diagnostic plots of a few wavelengths
diag(2)=0; %1-> Fields in Fourier Spaces
%% Define System Parameters
%disp(['NA=' num2str(NA,3)]);
mfile=mfilename;
shapeAdj=1.0; %This is sort of an alternative to q3Dpow, which "made the cut"
q3Dpow=AF(1); 
HNps=1.0*2; %number of source points across half of the pinhole

NL=length(Lh); % Replaces Nlam0
iL=1:NL;
% d0=23.25; %mm, arc to relay lens
d1=69.75; %mm, relay lens to pinhole
d2=314.3; %mm, pinhole to shaper
d3=44; %mm, shaper to t1
d4=24.6; %Dan email 22 Jul 25; %mm, t1 to wafer
d5=22.7624; %mm, wafer to t2
% d6=280; %mm, t2 to t3 mirror
d7=75; %mm t3 mirror to slit
% old f1=1/(1/d4+1/(d2+d3)); % 23; %Dan email 22 Jul  %=23.37mm, focal length of T1, 
f2=d5; %mm, Krishna: 22.7 for T2!
F2=f2*1000; %um
F3=d7*1000; %um
M_illum=d4/(d2+d3); %~1/15 Krishna, actually negative, ~1/14.57
M_det=d7/d5; % ???1/3; %Dan, ~3.295
R_ph=1.0*98/2; % um, pinhole semi minor axis of 358 um x 98 um elliptical hole
if 0 %enlarged ill pinhole
  R_ph=R_ph*4;
  sound(sin(2*pi*250*[1:.3e4]/1e4),1e4);
end
if 1, %enlarged det pinhole
  R_slit=1.0*75/2; %um radius of round detector slit
%  sound(sin(2*pi*250*[1:.3e4]/1e4),1e4);
else
  R_slit=75/2; %um radius of round detector slit 
end
R_relay=0.3*25.4/2; %Dia. of clear aperture=0.3"
NA_relay=sind(atand(R_relay/d1));
% R_shaper=NA_ph*d2; %outer radius of the area used on shaper, 1.726mm
dxpsf=2*R_ph/(1+2*HNps); %estimate distance between PSFs
xpsf=dxpsf*(-HNps:HNps); %locations of PSFs
Npsf=length(xpsf); %number of "physical" source points

if Nx==0; Nx=2^11; end % If no valid number given, use 2048 points
% Nx=2^12; %test with expanded fourier domain
% old sqrtNx=sqrt(Nx);
x0=ifftshift(-Nx/2:Nx/2-1).'; %[0:N/2-1 -N/2:-1]'

% message confirming setup finished
% disp('Fourier setup finished, calculating...');

%% Calculation Setup
% Define lambda based parameters
lam=Lh./1000; % um from nm - all computations in um
NA_ph =(NA(1).*M_illum); %.0055
R_shaper=NA_ph(1).*d2; %outer radius of the area used on shaper, 1.726mm

% set up Fourier spaces
x_ph=x0.*lam./(4*NA_relay);
sinth_ph=x0./Nx*4*NA_relay;

% setup slit geometry
x_slit=x_ph.*M_illum*M_det; % now a rectangular matrix!!
dR_slit=-2.*dz.*sind(AOI(1))*M_det; %lateral disp. of slit due to dz
dx_slit=lam./(4*NA_relay)*M_illum*M_det;

% Define the beam
iBeam=find(-NA_ph(1)<=sinth_ph & sinth_ph<NA_ph(1));
Npw=length(iBeam);
th_ph=asind(sinth_ph); % to transform to shaper coordinates
top=(R_slit+dR_slit)./dx_slit+1; %+1 because 1st pixel represents origin
bot=(-R_slit+dR_slit)./dx_slit+1+Nx; %+Nx for fftshifted images
itop=ceil(top); %"prewrapped" y-ind of top of slit
ibot=floor(bot); %"prewrapped" y-ind of bot of slit
Rtop=top-itop+1; %interpolation distance is computed prewrapped
Rbot=ibot-bot+1;
Nslit=[1-Nx:2*Nx]'; %prewrapped domain is padded for sections that will wrap
iSlit=((Nslit<=itop) | (Nslit>=ibot)); %prewrapped slit domain
iSlit=iSlit(1:Nx,:)&iSlit(Nx+1:2*Nx,:)&iSlit(2*Nx+1:3*Nx,:); %wrapped slit
% iSlit=ones(size(iSlit)); trial that DID NOT WORK
% at focus, the top half of the slit is low in the FFT domain and
% vice-versa.
hitop=find(mod(itop-1,2048)+1>2048);
hibot=find(mod(ibot-1,2048)+1>2048);
lotop=find(mod(itop-1,2048)+1<1);
lobot=find(mod(ibot-1,2048)+1<1);
if any(hitop)
  bopper err;
  disp('Top too high');
  keyboard;
end
if any(hibot)
  bopper err;
  disp('Bot too high');
  keyboard;
end
if any(lotop)
  bopper err;
  disp('Top too low');
  keyboard;
end
if any(lobot)
  bopper err;
  disp('Bot to low');
  keyboard;
end
indTop=sub2ind([Nx,NL],mod(itop-1,Nx)+1,iL);
indTopM1=sub2ind([Nx,NL],mod(itop-2,Nx)+1,iL);
indBot=sub2ind([Nx,NL],mod(ibot-1,Nx)+1,iL);
indBotP1=sub2ind([Nx,NL],mod(ibot,Nx)+1,iL);
% Define the shaper
x_shaper=d2.*tand(th_ph(iBeam)); %independent variable for shaper
Shaper0=exp(-2.*abs(x_shaper./2.7).^3); %POR, /2 for E beam not intensity.0*
W_shaper=real(sqrt(R_shaper^2-x_shaper.^2)./...
  R_shaper).^q3Dpow; %rigorous cord lengths to a power that should be 1/2
Shaper=real(Shaper0.^shapeAdj.*W_shaper);
%time=toc(Setup);  disp(['Setup: ' num2str(time)]);
% Fresnel Computation, new TIER_1 mex code calculates rp and rs at
% all angles and wvls in < 0.2 s
%angles for Fresnel computation:
% FresnelSetup=tic;
th_focus=repmat(asind(sinth_ph(iBeam)/M_illum)+AOI(1),NL,1)';
NK_si_o=repmat(NK(1,:),Npw,1); % rearrange all the NKs to us TIER_1 engine
NK_ema=repmat(NK(2,:),Npw,1);
NK_sio2_o=repmat(NK(3,:),Npw,1);
NK_si_e=(NK(1,:).^2-1).*beta + 1;
NK_si_e=sqrt((abs(NK_si_e)+real(NK_si_e))./2) - 1i.*sqrt((abs(NK_si_e)-real(NK_si_e))./2);
NK_si_e=repmat(NK_si_e,Npw,1);
NK_sio2_e=(NK(3,:).^2-1).*alpha + 1;
NK_sio2_e=sqrt((abs(NK_sio2_e)+real(NK_sio2_e))./2) -...
  1i.*sqrt((abs(NK_sio2_e)-real(NK_sio2_e))./2);
NK_sio2_e=repmat(NK_sio2_e,Npw,1);
rNK=[NK_si_o(:).'; NK_si_e(:).'; NK_ema(:).'; NK_sio2_o(:).';...
  NK_sio2_e(:).'];
rLam=repmat(Lh,Npw,1); %repeated Lh for, e.g., zeniths
%time=toc(FresnelSetup); disp(['FresnelSetup: ' num2str(time)]);
if 0, Fresnel=tic; end
Jd=TIER_2(th_focus,real(rNK),imag(rNK),rLam(:)',To);
%     Jd            93375x2                2988000  double    complex
%     To                1x2                     16  double
%     rLam             83x1125              747000  double
%     rNK               5x93375            7470000  double    complex
%     th_focus          1x93375             747000  double
%     Lh        1x1125             9000  double
% Npw=83, length(Lh)=99375
% num2str(To,4) =    '2       1050'
if 0
  [JM] = Tmatrix00(NK,[0 To 0],th_focus,rLam(:)',...
    reshape(repmat(alpha,Npw,1),1,Npw*NL),...
    reshape(repmat(beta,Npw,1),1,Npw*NL));
end
if 0, time=toc(Fresnel); disp(['Fresnel: ' num2str(time)]); end
% LoopSetup=tic;
%"w" version, this is part of Fred's fftshift for iBeam
%old rs=zeros(Nx,NL);
%old rp=rs;
if 0, % sanity check for perfect reflector
  Jd=repmat([1 -1],Npw*NL,1);
end
rs=reshape(Jd(:,2),Npw,NL); %ok
rp=reshape(Jd(:,1),Npw,NL); %ok
if 0, %sanity check for perfect reflector -> [N C S]=[0 1 0]
  rp=ones(size(rp));
  rs=-rp;
end
if diag(2),
  
  [Lsort,isort]=sort(Lh);
  figure;
  pagefig;
  subplot(2,2,1);
  imagesc(Lsort,fftshift(sinth_ph(iBeam)/M_illum),fftshift(abs(rp(:,isort)),1));
  hc=colorbar; set(get(hc,'Title'),'String','|r_p|');
  ylabel('sin(Zenith-AOI)'); xlabel('\lambda (nm)');
  title('Reflection Coefficients for illuminating beam');
  
  subplot(2,2,2);
  imagesc(Lsort,fftshift(sinth_ph(iBeam)/M_illum),...
    fftshift(unwrap(unwrap(unwrap(angle(rp(:,isort)),[],2)),[],2),1));
  hc=colorbar; set(get(hc,'Title'),'String','\angler_p rad');
  ylabel('sin(Zenith-AOI)'); xlabel('\lambda (nm)');
  
  subplot(2,2,3);
  imagesc(Lsort,fftshift(sinth_ph(iBeam)/M_illum),fftshift(abs(rs(:,isort)),1));
  hc=colorbar; set(get(hc,'Title'),'String','|r_s|');
  ylabel('sin(Zenith-AOI)'); xlabel('\lambda (nm)');
  
  subplot(2,2,4); cla;
  imagesc(Lsort,fftshift(sinth_ph(iBeam)/M_illum),...
    fftshift(unwrap(unwrap(unwrap(angle(rs(:,isort)),[],2)),[],2),1));
  hc=colorbar; set(get(hc,'Title'),'String','\angler_s rad');
  ylabel('sin(Zenith-AOI)'); xlabel('\lambda (nm)');
  righttext(pwdshort(3,which(mfile)));
  
end
%   tanPsi=abs(rp./rs);
%   cosDelta=cos(angle(rp./rs));
  % field initialization for memory efficiency
  Ep=zeros(Nx,NL); %E field for p polarized PWs
  Es=Ep; %angular spectrum
  E0=Ep;
  if diag(1)
    jplot=[1 round(NL/2) NL]; %indices of wavelengths to plot
    nplot=length(jplot);
    Rpd=zeros(Nx,nplot); %diag. array for whole of Rp (not just the Beam)
    Rsd=zeros(Nx,nplot); %diag. array for whole of Rs (not just the Beam)
%    Rxd=zeros(Nx,nplot); %diag. array for whole of Rx (not just the Beam)
  end
%   E_beam=zeros(size(rp));
  Rp=zeros(size(iSlit));
  Rs=Rp;
  Rx=Rp;
  R0=Rp;
  Rpt=Ep;
  Rst=Ep;
  Rxt=Ep;
  
  % Adjust sizes for larger calculation
  %lam=repmat(lam,Nx,1);
  %cosAOI=repmat(cosAOI,Nx,1);
  
  ChromData=ChromaticSource2slit02; %mm mfile of "Raw" Zemax data digitized from figure
  lamChrom=ChromData(:,3)'/1000; %um from nm
  Chrom0=ChromData(:,2)'; %um (as acquired)
  Chrom1=spline(lamChrom,Chrom0,lam(1,:));
  izero=near(lam(1,:),.950); %Dan recommends using 950nm as on focus
  if 1 %switch for Chrom aberration
  Chrom=Chrom1-Chrom1(izero); %perturbation due to axial chromatic aber. OKw'
  else
    Chrom=0;
  end
  if 0
    figure
    plot(ChromData(:,3)/1000,ChromData(:,2)-Chrom1(izero),'o');
    hold on;
    plot(lam,Chrom);
    ylabel('Focus Shift (\mum)');
    xlabel('\lambda (\mum)');
    legend('Data','Spline','Location','northwest');
    title(pwdshort(3,which('ChromaticSource2slit02')),'interp','n');
    grid on;
  end
  
  % dz aberration calculation

%exponent of Goodman eqs. 6-33 & 6-35 /wo eps
% so the following in terms of d5 at objective is OK
W0=pi/(M_illum)^2*sinth_ph(iBeam).^2./lam*(d5*1000)^2; %2pi/2=pi, aber coef
%Fred's 1st-order perturbation, notebook: 3 Sep 2019, p 13 Sep 2019
% is accurate but exact expression could be used.
cosAOI=cosd(AOI(1));
d6=1/(1/F2-1/(F2-2*dz*cosAOI)); %focal distance of T2
d63=F2+F3-d6; %"d6" for T3
eps=1/d63+1/F3-1./(F3+Chrom); %+1/F3
if 1 %aberration switch
  W=exp(1i*W0.*eps); % OK
else
  W=ones(Npw,NL); %Kill aberrations
end

clear ChromData Chrom0 Chrom1

%% Main Loop, point source functions only!
RLam=repmat(Lh/1000,Npw,1); %
% old arg=2*NA_relay./RLam(i_ph);
if diag(1) %diagnostic plot
  figure(100); clf;
  halffig;
  h10=subplot(4,1,1); 
  h20=subplot(4,1,2); 
  h30=subplot(4,1,3); 
  h40=subplot(4,1,4); 
  righttext(pwdshort(2,which(mfile)));
end
arg=1i*2*pi*sinth_ph(iBeam)./RLam;
if 0
scale=Shaper.*W/sqrt(sum(Shaper.^2)*Npsf/Nx); %PSF/FFT inten. normalization
else
scale=Shaper;
end

% time=toc(LoopSetup); disp(['LoopSetup: ' num2str(time)]); %timing option
%Loop=tic; %timing option
%Illum=0; %timing option
%Detect=0; %timing option
for ipsf=1:Npsf %loop to incoherently average coherent point spread functions (psf)
  % IllumLoop=tic; %timing option
  %  tic; %timing option
  % old, pre TierBeam e_ph(i_ph)=sinc((x_ph(i_ph)-xpsf(ipsf)).*arg); %arg.*; *sqrtNx;
  %figure; plot(x_ph,e_ph)  %  toc
  %  E_ph=fft(e_ph,Nx,1);
  E_beam=exp(arg*xpsf(ipsf)).*scale;
  if diag(1)
    E_ph=zeros(Nx,NL);
    E_ph(iBeam,:)=E_beam;
    e_ph=ifft(E_ph,Nx,1);
  end
  Ep(iBeam,:)=E_beam.*rp; % "reflected" p beam
  %toc, tic; %timing option
  Es(iBeam,:)=E_beam.*rs; % "reflected" s beam
  %toc, tic; %timing option
  E0(iBeam,:)=E_beam;
%   FFT=tic; %timing option
%   ep=ifft(Ep,Nx,1); % ep es are the spatial fields for
%   es=ifft(Es,Nx,1); % sets of plane waves Ep & Es
%   e0=ifft(E0,Nx,1); % sets of plane perfect reflection
  E = cat(3,Ep,Es,E0);
  e = ifft(E,Nx,1);
  ep = e(:,:,1);
  es = e(:,:,2);
  e0 = e(:,:,3);
  % ep es are the spatial fields for
%   es=ifft(Es,Nx,1); % sets of plane waves Ep & Es
%   e0=ifft(E0,Nx,1); % sets of plane perfect reflection
  %toc, tic; %timing option
%   time=toc(FFT); disp(['FFT: ' num2str(time)])
  %     ep(iSlit)=0;
  %     es(iSlit)=0;
  %     I0(:,ipsf)=e0(iSlit,ipsf)'*e0(iSlit,ipsf); %Incident on slit
  Rp(iSlit)=conj(ep(iSlit)).*ep(iSlit) + Rp(iSlit); %incoherent sum
  %toc, tic; %timing option
  Rs(iSlit)=conj(es(iSlit)).*es(iSlit) + Rs(iSlit); 
  %toc, tic; %timing option
  Rx(iSlit)=conj(es(iSlit)).*ep(iSlit) + Rx(iSlit);
  R0(iSlit)=conj(e0(iSlit)).*e0(iSlit) + R0(iSlit);
  %toc, tic; %timing option
  if diag(1) %diag plot,
    Rpd(:,:,ipsf)=conj(ep(:,jplot)).*ep(:,jplot); % + Rpd; %incoherent sum
    Rsd(:,:,ipsf)=conj(es(:,jplot)).*es(:,jplot); % + Rsd; 
%    Rxd=conj(es(:,jplot)).*ep(:,jplot) + Rxd;
    axes(h10); 
    for ic=1:length(jplot) 
      h11(ic,ipsf)=plot(fftshift(x_ph(:,jplot(ic))),...
        abs(fftshift(e_ph(:,jplot(ic)))).^2,'.-','color',plotcolors(ic));
            hold on; 
    end
    set(h10,'xlim',R_ph*4*[-1 1]);
  end
  %Detect=toc(DetectLoop)+Detect;
end %for ipsf=1:Npsf, %loop for point spread functions (psf)
%time=toc(Loop); disp(['Loop: ' num2str(time)]);
%disp(['IllumLoop: ' num2str(Illum)]);
%disp(['DetectLoop: ' num2str(Detect)]);
% Finish=tic;
if diag(1)
  axes(h10); 
  hl=legend(h11(:,1),num2str(1000*lam(jplot)',4),'AutoUpdate','off');
  vline(R_ph*[-1 1],'k--'); grid on;
  text(-R_ph*1.05,0,' Pinhole','Rotation',90','VerticalAlignment','bottom')
  ylabel('I_{PSF} (au, apod., aberr.)'); xlabel('X_{pinhole} (\mum)');
  set(get(hl,'Title'),'String','\lambda (nm)')
  title({[strrep(mfile,'_','-') ': <AOI>=' num2str(mean(AOI),3) ...
    ', <NA>=' num2str(mean(NA),3) ', Thk= ' num2str(To(2))],...
    ['<\alpha>=' num2str(mean(alpha)) ', <\beta>=' num2str(mean(beta)),...
    ', q3D^{' num2str(q3Dpow,3) '}, dZ=' num2str(dz,3)]});
  axes(h20);
  h21=plot(fftshift(sinth_ph(iBeam)/M_illum),fftshift(abs(E_beam(:,jplot))...
    .^2,1)./repmat(max(abs(E_beam(:,jplot))),length(iBeam),1),'-');
  %for ic=1:length(jplot), set(h21(ic),'color',plotcolors(ic)); end
  hold on;
  plot(fftshift(sinth_ph(iBeam)/M_illum),fftshift(Shaper0,1),'k-');
  plot(fftshift(sinth_ph(iBeam)/M_illum),fftshift(W_shaper,1),'k--');
  grid on;
  %was strvcat...
  legend(char(num2str(1000*lam(jplot)',4),'Shaper','Wt_{3D}'),'Location','best');
  set(gca,'ylim',[0 1]);
  ylabel('$\mathcal{F}\{I\}$','interp','latex'); 
  xlabel('sin(\theta_{Wafer})');
  axis tight;
end
%% MM Computation

% re-assign to rectangular matrix so we can add in x domain
if diag(1) %diag
  axes(h30); cla; hold on;
  for ipsf=1:Npsf
    h31(:,ipsf)=plot(fftshift(-x_slit(:,jplot),1)+dR_slit,...
      fftshift(squeeze(Rpd(:,:,ipsf)),1),plotlines(ipsf));
    for jj=1:nplot 
      set(h31(jj,ipsf),'color',plotcolors(jj)); 
    end
  end
  %set(gca,'yscale','log');
  ylim=get(gca,'ylim');
  %set(gca,'ylim',[1e-5 ylim(2)])
  vline(R_slit*[-1 1],'k--');
  grid on; ylabel('I_p (au)'); 
  set(gca,'xlim',R_slit*[-2 3]);
  
  axes(h40); cla; hold on;
  for ipsf=1:Npsf
    h41(:,ipsf)=plot(fftshift(-x_slit(:,jplot),1)+dR_slit,...
     fftshift(squeeze(Rsd(:,:,ipsf)),1),plotlines(ipsf));
%   fftshift(squeeze(sqrt(Rsd(:,:,ipsf))),1),plotlines(ipsf));
%      fftshift(squeeze(Rsd(:,:,ipsf)-Rpd(:,:,ipsf)),1),plotlines(ipsf));
     for jj=1:nplot, 
      set(h41(jj,ipsf),'color',plotcolors(jj)); 
    end
  end
  %set(gca,'yscale','log');
  ylim=get(gca,'ylim');
  %set(gca,'ylim',[1e-5 ylim(2)])
  vline(R_slit*[-1 1],'k--');
  grid on; 
  xlabel('x_{slit} (\mum)'); grid on; 
  ylabel('I_s (au)'); 
%  ylabel('I_s-I_p (au)');
  set(gca,'xlim',R_slit*[-2 3]);
  linkaxes([h30 h40],'x');
  if 0, %more diag's
    fileout='guessMinus24';
    today=now;
    save(fileout,'lam','jplot','x_slit','dR_slit','Rsd','Npsf','R_slit','mfile','To','today');
  end
  W=2*To(end)*tan(asin(sind(mean(AOI))/mean(mean(real(NK_sio2_o)))))*...
    M_det*cosd(mean(AOI))/1000;
  h1=vline(dR_slit,'rd:'); h2=vline(dR_slit+W,'mv:');
  legend([h1 h2],'Top','1^{st}Bot');
end
% interpolation of physical det. slit apertures onto Wvl scaled detector
Rx(indTop)=(Rtop>0.5).*Rx(indTop).*(Rtop-0.5); %logical arrays to enforce
Rx(indTopM1)=Rx(indTopM1).*min(ones(1,NL),Rtop+0.5); %... conditions
Rp(indTop)=(Rtop>0.5).*Rp(indTop).*(Rtop-0.5); %logical arrays to enforce
Rp(indTopM1)=Rp(indTopM1).*min(ones(1,NL),Rtop+0.5); %... conditions
Rs(indTop)=(Rtop>0.5).*Rs(indTop).*(Rtop-0.5); %logical arrays to enforce
Rs(indTopM1)=Rs(indTopM1).*min(ones(1,NL),Rtop+0.5); %... conditions
R0(indTop)=(Rtop>0.5).*R0(indTop).*(Rtop-0.5); %logical arrays to enforce
R0(indTopM1)=R0(indTopM1).*min(ones(1,NL),Rtop+0.5); %... conditions

Rx(indBotP1)=Rx(indBotP1).*min(ones(1,NL),Rbot+0.5); %... conditions
Rx(indBot)=(Rbot>0.5).*Rx(indBot).*(Rbot-0.5); %logical arrays to enforce
Rp(indBotP1)=Rp(indBotP1).*min(ones(1,NL),Rbot+0.5); %... conditions
Rp(indBot)=(Rbot>0.5).*Rp(indBot).*(Rbot-0.5); %logical arrays to enforce
Rs(indBotP1)=Rs(indBotP1).*min(ones(1,NL),Rbot+0.5); %... conditions
Rs(indBot)=(Rbot>0.5).*Rs(indBot).*(Rbot-0.5); %logical arrays to enforce
R0(indBotP1)=R0(indBotP1).*min(ones(1,NL),Rbot+0.5); %... conditions
R0(indBot)=(Rbot>0.5).*R0(indBot).*(Rbot-0.5); %logical arrays to enforce

%figure; imagesc(Rs); ylabel('Y'); xlabel('X'); hold on; hl1=colorbar; grid on; axis xy; title(['Rs, dz=' num2str(dz)]); righttext(pwdshort(1)); plot(mod(top+1,2048)-1,'r.'); plot(mod(bot+1,2048)-1,'r.')
if 1
  R0t=sum(R0,1);
  Rxt=sum(Rx,1)./R0t;
  Rst=sum(Rs,1)./R0t;
  Rpt=sum(Rp,1)./R0t;
else
  Rxt=mean(Rx,1);
  Rst=mean(Rs,1);
  Rpt=mean(Rp,1);
end

if diag(2)
  [Lsort,isort]=sort(Lh); clear ylim;
  figure;
  pagefig;
  
  subplot(2,2,1); cla;
  imagesc(Lsort,fftshift(x_slit(:,1),1),fftshift(Rp(:,isort),1));
  hc=colorbar; set(get(hc,'Title'),'String','R_p'); axis tight;
  axis xy, hold on; set(gca,'ylim',1.2*R_slit*[-1 1]+dR_slit); grid on;
  hs=plot(Lsort,(top(isort)-1)*dx_slit(1),'w--');
  plot(Lsort,(bot(isort)-1-Nx)*dx_slit(1),'w--');
  ylabel('x_{slit} (\mum)'); xlabel('\lambda (nm)');
  legend(hs,'Slit Edges','Location','best','TextColor',[1 1 1],'Color',...
    'none','Box','off');
  title('Intensities Illuminating Detection Slit')
  
  subplot(2,2,2); cla;
  imagesc(Lsort,fftshift(x_slit(:,1),1),fftshift(Rs(:,isort),1));
  hc=colorbar; set(get(hc,'Title'),'String','R_s'); axis tight;
  axis xy, hold on; set(gca,'ylim',1.2*R_slit*[-1 1]+dR_slit); grid on;
  hs=plot(Lsort,(top(isort)-1)*dx_slit(1),'w--');
  plot(Lsort,(bot(isort)-1-Nx)*dx_slit(1),'w--');
  ylabel('x_{slit} (\mum)'); xlabel('\lambda (nm)');
  legend(hs,'Slit Edges','Location','best','TextColor',[1 1 1],'Color',...
    'none','Box','off');
  
  subplot(2,2,3); cla;
  imagesc(Lsort,fftshift(x_slit(:,1),1),fftshift(real(Rx(:,isort)),1));
  hc=colorbar; set(get(hc,'Title'),'String','Re{R_x}'); axis tight;
  axis xy, hold on; set(gca,'ylim',1.2*R_slit*[-1 1]+dR_slit); grid on;
  hs=plot(Lsort,(top(isort)-1)*dx_slit(1),'w--');
  plot(Lsort,(bot(isort)-1-Nx)*dx_slit(1),'w--');
  ylabel('x_{slit} (\mum)'); xlabel('\lambda (nm)');
  legend(hs,'Slit Edges','Location','best','TextColor',[1 1 1],'Color',...
    'none','Box','off');
  
  subplot(2,2,4); cla;
  imagesc(Lsort,fftshift(x_slit(:,1),1),fftshift(imag(Rx(:,isort)),1));
  hc=colorbar; set(get(hc,'Title'),'String','Im{R_x}'); axis tight;
  axis xy, hold on; set(gca,'ylim',1.2*R_slit*[-1 1]+dR_slit); grid on;
  hs=plot(Lsort,(top(isort)-1)*dx_slit(1),'w--');
  plot(Lsort,(bot(isort)-1-Nx)*dx_slit(1),'w--');
  ylabel('x_{slit} (\mum)'); xlabel('\lambda (nm)');
  legend(hs,'Slit Edges','Location','best','TextColor',[1 1 1],'Color',...
    'none','Box','off');
  righttext(pwdshort(3,which(mfile)));
end  
% Assign to MM
MM=zeros(4,4,NL);
MM(1,1,:)=0.5.*permute(Rpt+Rst,[1 3 2]);
MM(2,2,:)=MM(1,1,:);
MM(1,2,:)=0.5.*permute(Rpt-Rst,[1 3 2]); %N
MM(2,1,:)=MM(1,2,:);
MM(3,3,:)=permute(real(Rxt),[1 3 2]); %C
MM(4,4,:)=MM(3,3,:);
MM(3,4,:)=permute(imag(Rxt),[1 3 2]); %S
MM(4,3,:)=-MM(3,4,:);
%save MM1x MM Rxt Rpt Rst

% FES 25 May 2020, I do not understand why I put in the following lines and
% am taking them out!
%%% disp(MM(4,4,10));
%%% if abs(MM(4,4,10))<1e-10; bopper error; keyboard; end
%   MM=MM./repmat(MM(1,1,:),4,4); % Normalize MM if desired...
% time=toc(Finish); disp(['Finish: ' num2str(time)]);
disp([num2str(toc(TierStart)) '  ' datestr(now)])
%disp('.');