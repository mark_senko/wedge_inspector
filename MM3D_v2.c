/* Lets try a matlab .c file */
#include "mex.h"

/* The computational routine */
void MM3D_v2(double *Ar, double *Ai, double *Br, double *Bi, double *Cr,
          double *Ci, int flgA, int flgB, const mwSize *dimA, const mwSize *dimB)
{
    mwIndex i;
    mwIndex k;
    mwIndex j;
    mwIndex h;
    int m;
    int n;
    int dex;
    int pge;
    int status = 0;
    m=dimA[0];
    if (flgA == 0) {
        n=dimA[2];
    } else {
        n=dimB[2];
    }
    
    /* If both 3D matrices, do multiplication */
    if ((flgA == 0) && (flgB == 0)) {
        status = 1;
        for (k=0; k<n; k++) {
            for (j=0; j<m; j++) {
                for (i=0; i<m; i++) {
                    dex = i+m*j+m*m*k;
                    pge = m*m*k;
                    Cr[dex] = 0;
                    Ci[dex] = 0;
                    for (h=0; h<m; h++) {
                        Cr[dex] = Ar[i+m*h+pge]*Br[h+m*j+pge]-Ai[i+m*h+pge]*Bi[h+m*j+pge]+Cr[dex];
                        Ci[dex] = Ar[i+m*h+pge]*Bi[h+m*j+pge]+Ai[i+m*h+pge]*Br[h+m*j+pge]+Ci[dex];
                    }
                }
            }
        }
    }
    /* If A is single matrix, just keep rotating through */
    if ((flgA == 1) && (flgB == 0)) {
        status = 1;
        for (k=0; k<n; k++) {
            for (j=0; j<m; j++) {
                for (i=0; i<m; i++) {
                    dex = i+m*j+m*m*k;
                    pge = m*m*k;
                    Cr[dex] = 0;
                    Ci[dex] = 0;
                    for (h=0; h<m; h++) {
                        Cr[dex] = Ar[i+m*h]*Br[h+m*j+pge]-Ai[i+m*h]*Bi[h+m*j+pge]+Cr[dex];
                        Ci[dex] = Ar[i+m*h]*Bi[h+m*j+pge]+Ai[i+m*h]*Br[h+m*j+pge]+Ci[dex];
                    }
                }
            }
        }
    }
    /* Same if B is single matrix, not 3D */
    if ((flgA == 0) && (flgB == 1)) {
        status = 1;
        for (k=0; k<n; k++) {
            for (j=0; j<m; j++) {
                for (i=0; i<m; i++) {
                    dex = i+m*j+m*m*k;
                    pge = m*m*k;
                    Cr[dex] = 0;
                    Ci[dex] = 0;
                    for (h=0; h<m; h++) {
                        Cr[dex] = Ar[i+m*h+pge]*Br[h+m*j]-Ai[i+m*h+pge]*Bi[h+m*j]+Cr[dex];
                        Ci[dex] = Ar[i+m*h+pge]*Bi[h+m*j]+Ai[i+m*h+pge]*Br[h+m*j]+Ci[dex];
                    }
                }
            }
        }
    }
    /* otherwise throw an error */
    if (status == 0) {
        mexErrMsgIdAndTxt("MM3D:multiply","Flag status error: incorrect matrix sizes.");
    }
}

/* The gateway function */
void mexFunction( int nlhs, mxArray *plhs[],
                  int nrhs, const mxArray *prhs[])
{
    double *Amatxr;              /* input scalar */
    double *Amatxi;
    double *Bmatxr;               /* 1xN input matrix */
    double *Bmatxi; 
    double *Cmatxr;              /* output matrix */
    double *Cmatxi;
    const mwSize *dimA;
    const mwSize *dimB;
    int flgA = 0;
    int flgB = 0;
    mwSize n;
    
    /* check for proper number of arguments */
    if(nrhs!=2) {
        mexErrMsgIdAndTxt("MM3D:nrhs","Two inputs required.");
    }
    if(nlhs!=1) {
        mexErrMsgIdAndTxt("MM3D:nlhs","One output required.");
    }
    
//     /* make sure the first input argument is type double */
//     if( !mxIsDouble(prhs[0]) || 
//          mxIsComplex(prhs[0]) ||
//          mxGetNumberOfElements(prhs[0])!=1 ) {
//         mexErrMsgIdAndTxt("MyToolbox:arrayProduct:notScalar","Input A matrix must be type double.");
//     }
//     
//     /* make sure the second input argument is type double */
//     if( !mxIsDouble(prhs[1]) || 
//          mxIsComplex(prhs[1])) {
//         mexErrMsgIdAndTxt("MyToolbox:arrayProduct:notDouble","Input B matrix must be type double.");
//     }
    
    /* create pointer to real and imag data of A matrix  */
    Amatxr = mxGetPr(prhs[0]);
    Amatxi = mxGetPi(prhs[0]);

    /* create pointer to real and imag data of B matrix  */
    Bmatxr = mxGetPr(prhs[1]);
    Bmatxi = mxGetPi(prhs[1]);

    /* find dimensions and set flags */
    dimA = mxGetDimensions(prhs[0]);
    dimB = mxGetDimensions(prhs[1]);
//     mexPrintf("The size of A is: %d, %d, %d.\n",dimA[0],dimA[1],dimA[2]);
//     mexPrintf("The size of B is: %d, %d, %d.\n",dimB[0],dimB[1],dimB[2]);
//     mexEvalString("drawnow;");
    
    if (mxGetNumberOfDimensions(prhs[0]) < 3) {
        flgA = 1;
//         mexPrintf("A has this many dimensions: %d.\n",mxGetNumberOfDimensions(prhs[0]));
//         mexEvalString("drawnow;");
    }
    if (mxGetNumberOfDimensions(prhs[1]) < 3) {
        flgB = 1;
    }
    if (dimA[0] != dimB[0]) {
        mexErrMsgIdAndTxt("MM3D:dimBad1","A and B matrices must have matching dimensions.");
    }
    if (dimA[1] != dimB[1]) {
        mexErrMsgIdAndTxt("MM3D:dimBad2","A and B matrices must have matching dimensions.");
    }     

    /* create the output matrix */
    if (flgA == 0) {
        plhs[0] = mxCreateNumericArray(mxGetNumberOfDimensions(prhs[0]),
                                       dimA, mxGetClassID(prhs[0]), mxCOMPLEX);
    } 
    else {
        plhs[0] = mxCreateNumericArray(mxGetNumberOfDimensions(prhs[1]),
                                       dimB, mxGetClassID(prhs[1]), mxCOMPLEX);
    }

    /* get a pointer to the real data in the output matrix */
    Cmatxr = mxGetPr(plhs[0]);
    Cmatxi = mxGetPi(plhs[0]);
    
    /* call the computational routine */
    MM3D_v2(Amatxr,Amatxi,Bmatxr,Bmatxi,Cmatxr,Cmatxi,flgA,flgB,dimA,dimB);
}